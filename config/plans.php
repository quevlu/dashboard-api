<?php

use Binaccle\Enums\Plans\PlanEnum;

$plans = [
    PlanEnum::FREE,
    PlanEnum::BASIC,
    PlanEnum::STANDARD,
    PlanEnum::PREMIUM,
    PlanEnum::ENTERPRISE,
];

$characteristicsPlans = [];

foreach ($plans as $plan) {
    $planUpper = strtoupper($plan);

    $attributes = [
        'base_price',
        'base_limit',
        'pack_price',
        'discount_pack',
        'annual_discount',
        'step_pack',
        'trial_days',
    ];

    $characteristicsPlans[$plan] = [];
    $characteristicsPlans[$plan]['name'] = $plan;

    foreach ($attributes as $attribute) {
        $envAttribute = strtoupper($attribute);
        $characteristicsPlans[$plan][$attribute] = env("{$planUpper}_{$envAttribute}");
    }
}

return $characteristicsPlans;
