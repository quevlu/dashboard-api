# Tests

## Execute test battery

php artisan test --parallel --testsuite=Feature

php artisan test --testsuite=Feature --filter SendVerificationTest

## Get coverage of tests
php artisan test --coverage