<?php

use App\Enums\Routes\RouteParamEnum;
use App\Enums\ThrottleEnum;
use App\Http\Controllers\Auth;
use App\Http\Middlewares\Auth\NotAuthenticatedMiddleware;
use App\Http\Middlewares\ThrottleMiddleware;
use Illuminate\Support\Facades\Route;

Route::prefix('auth')->middleware(NotAuthenticatedMiddleware::class)->group(function () {
    Route::post('', Auth\LoginController::class)
        ->name('login')
        ->middleware(
            middleware(ThrottleMiddleware::class, ThrottleEnum::LOGIN_RETRIES, ThrottleEnum::LOGIN_TIME_TO_WAIT)
        );

    Route::post('register', Auth\Register\RegisterController::class)
        ->name('register')
        ->middleware(
            middleware(ThrottleMiddleware::class, ThrottleEnum::LOGIN_RETRIES, ThrottleEnum::LOGIN_TIME_TO_WAIT)
        );

    Route::post('verifications', Auth\Verifications\SendVerificationController::class)
        ->name('verifications')
        ->middleware(
            middleware(ThrottleMiddleware::class, ThrottleEnum::NOTIFIABLE_RETRIES, ThrottleEnum::NOTIFIABLE_TIME_TO_WAIT)
        );

    Route::delete('verifications/' . param(RouteParamEnum::TOKEN), Auth\Verifications\VerifyTokenController::class)
        ->name('verifications-token')
        ->middleware(
            middleware(ThrottleMiddleware::class, ThrottleEnum::NOTIFIABLE_RETRIES, ThrottleEnum::NOTIFIABLE_TIME_TO_WAIT)
        );

    Route::post('reset-passwords', Auth\ResetPassword\SendResetPasswordController::class)
        ->name('reset-passwords')
        ->middleware(
            middleware(ThrottleMiddleware::class, ThrottleEnum::NOTIFIABLE_RETRIES, ThrottleEnum::NOTIFIABLE_TIME_TO_WAIT)
        );

    Route::put('reset-passwords/' . param(RouteParamEnum::TOKEN), Auth\ResetPassword\ResetPasswordController::class)
        ->name('reset-passwords-token')
        ->middleware(
            middleware(ThrottleMiddleware::class, ThrottleEnum::NOTIFIABLE_RETRIES, ThrottleEnum::NOTIFIABLE_TIME_TO_WAIT)
        );

    Route::put('invitations/' . param(RouteParamEnum::TOKEN), Auth\Invitations\ProcessInvitationController::class)
        ->name('invitations')
        ->middleware(
            middleware(ThrottleMiddleware::class, ThrottleEnum::NOTIFIABLE_RETRIES, ThrottleEnum::NOTIFIABLE_TIME_TO_WAIT)
        );
});
