<?php

use App\Enums\Routes\RouteNameEnum;
use App\Enums\Routes\RouteParamEnum;
use App\Http\Controllers;
use App\Http\Middlewares\Roles\HasAFreePlanMiddleware;
use App\Http\Middlewares\Roles\HasPermissionMiddleware;
use Binaccle\Enums\Permissions\PermissionEnum;
use Illuminate\Support\Facades\Route;

Route::prefix('sectors')->group(function () {
    Route::middleware(
        middleware(HasPermissionMiddleware::class, PermissionEnum::SECTORS_READ)
    )->group(function () {
        Route::get('', Controllers\Sectors\ListSectorController::class)
            ->name(RouteNameEnum::LIST_SECTORS);

        Route::get(param(RouteParamEnum::SECTOR_ID), Controllers\Sectors\ViewSectorController::class);
        Route::get(param(RouteParamEnum::SECTOR_ID) . '/users', Controllers\Sectors\ListUserSectorController::class)
            ->name(RouteNameEnum::LIST_SECTOR_USERS);
    });

    Route::middleware([
        HasAFreePlanMiddleware::class,
        middleware(HasPermissionMiddleware::class, PermissionEnum::SECTORS_CUD),
    ])->group(function () {
        Route::post('', Controllers\Sectors\CreateSectorController::class);
        Route::put(param(RouteParamEnum::SECTOR_ID), Controllers\Sectors\UpdateSectorController::class);
        Route::delete(param(RouteParamEnum::SECTOR_ID), Controllers\Sectors\DeleteSectorController::class);

        Route::delete(param(RouteParamEnum::SECTOR_ID) . '/users/{userIdOrAction}', Controllers\Sectors\RemoverOrCleanSectorController::class);
    });
});
