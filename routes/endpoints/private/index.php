<?php

use App\Http\Middlewares\Auth\AuthenticateMiddleware;
use App\Http\Middlewares\Auth\DecryptJwtMiddleware;
use Illuminate\Support\Facades\Route;

require 'auth.php';

Route::middleware(
    DecryptJwtMiddleware::class,
    AuthenticateMiddleware::class
)->group(function () {
    $files = [
        'system',
        'sector',
        'user',
        'company',
        'equipment/index',
    ];

    foreach ($files as $file) {
        require "{$file}.php";
    }
});
