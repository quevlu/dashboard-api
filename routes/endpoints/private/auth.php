<?php

use App\Http\Controllers;
use App\Http\Middlewares\Auth\AuthenticateMiddleware;
use App\Http\Middlewares\Auth\DecryptJwtMiddleware;
use Illuminate\Support\Facades\Route;

Route::prefix('auth')
    ->middleware(DecryptJwtMiddleware::class)
    ->group(function () {
        Route::put('', Controllers\Auth\RefreshController::class);

        Route::middleware(AuthenticateMiddleware::class)
        ->group(function () {
            Route::get('', Controllers\Auth\UserLoggedController::class);
            Route::delete('', Controllers\Auth\LogoutController::class);
            Route::put('change-password', Controllers\Auth\ChangePassword\ChangePasswordController::class);
        });
    });
