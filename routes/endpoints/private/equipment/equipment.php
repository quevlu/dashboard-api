<?php

use App\Enums\Routes\RouteNameEnum;
use App\Enums\Routes\RouteParamEnum;
use App\Http\Controllers\Equipments;
use App\Http\Middlewares\Equipments\MassiveUploads\ProcessingMassiveUploadMiddleware;
use App\Http\Middlewares\Roles\HasAFreePlanMiddleware;
use App\Http\Middlewares\Roles\HasPermissionMiddleware;
use Binaccle\Enums\Permissions\PermissionEnum;
use Illuminate\Support\Facades\Route;

Route::prefix('equipments')->group(function () {
    Route::middleware(
        middleware(HasPermissionMiddleware::class, PermissionEnum::EQUIPMENTS_READ)
    )->group(function () {
        Route::get('', Equipments\ListEquipmentController::class)
            ->name(RouteNameEnum::LIST_EQUIPMENTS);

        Route::prefix('massive-uploads')
            ->middleware(
                middleware(HasPermissionMiddleware::class, PermissionEnum::EQUIPMENTS_MASSIVE_UPLOADS_READ),
            )
            ->group(function () {
                Route::get('', Equipments\MassiveUploads\ListMassiveUploadController::class)
                    ->name(RouteNameEnum::LIST_MASSIVE_EQUIPMENT_UPLOADS);

                Route::get(param(RouteParamEnum::MASSIVE_UPLOAD_ID), Equipments\MassiveUploads\ViewMassiveUploadController::class);

                Route::get('templates/download', Equipments\MassiveUploads\DownloadUploadCsvTemplateController::class);
            });

        Route::get(param(RouteParamEnum::EQUIPMENT_ID), Equipments\ViewEquipmentController::class);

        Route::middleware(HasAFreePlanMiddleware::class)->group(function () {
            Route::post('print/' . param(RouteParamEnum::TYPE), Equipments\MassivePrintEquipmentController::class)
            ->middleware(
                middleware(HasPermissionMiddleware::class, PermissionEnum::EQUIPMENTS_MASSIVE_PRINT)
            );

            Route::get(param(RouteParamEnum::EQUIPMENT_ID) . '/print/' . param(RouteParamEnum::TYPE), Equipments\PrintEquipmentController::class);
        });
    });

    Route::middleware(HasAFreePlanMiddleware::class)->group(function () {
        Route::middleware(ProcessingMassiveUploadMiddleware::class)->group(function () {
            Route::post('massive-uploads', Equipments\MassiveUploads\MassiveUploadController::class)
                ->middleware(
                    middleware(HasPermissionMiddleware::class, PermissionEnum::EQUIPMENTS_MASSIVE_UPLOADS_CREATE)
                );

            Route::post('', Equipments\CreateEquipmentController::class)
                ->middleware(
                    middleware(HasPermissionMiddleware::class, PermissionEnum::EQUIPMENTS_CUD)
                );
        });

        Route::middleware(
            middleware(HasPermissionMiddleware::class, PermissionEnum::EQUIPMENTS_CUD)
        )->group(function () {
            Route::put(param(RouteParamEnum::EQUIPMENT_ID), Equipments\UpdateEquipmentController::class);
            Route::delete(param(RouteParamEnum::EQUIPMENT_ID), Equipments\DeleteEquipmentController::class);
        });
    });
});
