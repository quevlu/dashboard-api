<?php

use App\Enums\Routes\RouteNameEnum;
use App\Enums\Routes\RouteParamEnum;
use App\Http\Controllers\Equipments\Audits;
use App\Http\Middlewares\Roles\HasPermissionMiddleware;
use Binaccle\Enums\Permissions\PermissionEnum;
use Illuminate\Support\Facades\Route;

Route::prefix('equipments/audits')
    ->middleware(
        middleware(HasPermissionMiddleware::class, PermissionEnum::AUDITS_READ)
    )->group(function () {
        Route::get('', Audits\ListAuditController::class)
            ->name(RouteNameEnum::LIST_EQUIPMENT_AUDITS);

        Route::get(param(RouteParamEnum::EQUIPMENT_AUDIT_ID), Audits\ViewAuditController::class);

        Route::get(param(RouteParamEnum::EQUIPMENT_AUDIT_ID) . '/audited', Audits\Audited\ListAuditedController::class)
            ->name(RouteNameEnum::LIST_EQUIPMENT_AUDIT_AUDITED);

        Route::middleware(
            middleware(HasPermissionMiddleware::class, PermissionEnum::AUDITS_CUD),
        )->group(function () {
            Route::post('', Audits\CreateAuditController::class);
            Route::put(param(RouteParamEnum::EQUIPMENT_AUDIT_ID), Audits\UpdateAuditController::class);
            Route::delete(param(RouteParamEnum::EQUIPMENT_AUDIT_ID), Audits\DeleteAuditController::class);
            Route::put(param(RouteParamEnum::EQUIPMENT_AUDIT_ID) . '/finish', Audits\FinishAuditController::class);

            Route::post(param(RouteParamEnum::EQUIPMENT_AUDIT_ID) . '/audited', Audits\Audited\AddAuditedController::class);
            Route::delete(param(RouteParamEnum::EQUIPMENT_AUDIT_ID) . '/audited', Audits\Audited\DeleteAuditedController::class);
            Route::put(param(RouteParamEnum::EQUIPMENT_AUDIT_ID) . '/audited', Audits\Audited\UpdateAuditedController::class);
        });
    });
