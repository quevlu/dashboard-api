<?php

use App\Enums\Routes\RouteNameEnum;
use App\Enums\Routes\RouteParamEnum;
use App\Http\Controllers\Equipments\Types;
use App\Http\Middlewares\Equipments\MassiveUploads\ProcessingMassiveUploadMiddleware;
use App\Http\Middlewares\Roles\HasAFreePlanMiddleware;
use App\Http\Middlewares\Roles\HasPermissionMiddleware;
use Binaccle\Enums\Permissions\PermissionEnum;
use Illuminate\Support\Facades\Route;

Route::prefix('equipments/types')->group(function () {
    Route::middleware(
        middleware(HasPermissionMiddleware::class, PermissionEnum::EQUIPMENTS_READ)
    )->group(function () {
        Route::get('', Types\ListTypeController::class)
            ->name(RouteNameEnum::LIST_EQUIPMENT_TYPES);

        Route::prefix('massive-uploads')
            ->middleware(
                middleware(HasPermissionMiddleware::class, PermissionEnum::EQUIPMENTS_MASSIVE_UPLOADS_READ)
            )
            ->group(function () {
                Route::get('', Types\MassiveUploads\ListMassiveUploadController::class)
                    ->name(RouteNameEnum::LIST_MASSIVE_EQUIPMENT_TYPE_UPLOADS);

                Route::get(param(RouteParamEnum::MASSIVE_UPLOAD_ID), Types\MassiveUploads\ViewMassiveUploadController::class);

                Route::get('templates/download', Types\MassiveUploads\DownloadCsvTemplateController::class);
            });

        Route::get(param(RouteParamEnum::EQUIPMENT_TYPE_ID), Types\ViewTypeController::class);
    });

    Route::middleware(HasAFreePlanMiddleware::class)->group(function () {
        Route::middleware(ProcessingMassiveUploadMiddleware::class)->group(function () {
            Route::post('massive-uploads', Types\MassiveUploads\MassiveUploadController::class)
                ->middleware(
                    middleware(HasPermissionMiddleware::class, PermissionEnum::EQUIPMENTS_MASSIVE_UPLOADS_CREATE)
                );

            Route::post('', Types\CreateTypeController::class)
                ->middleware(
                    middleware(HasPermissionMiddleware::class, PermissionEnum::EQUIPMENTS_CUD)
                );
        });

        Route::middleware(
            middleware(HasPermissionMiddleware::class, PermissionEnum::EQUIPMENTS_CUD)
        )->group(function () {
            Route::put(param(RouteParamEnum::EQUIPMENT_TYPE_ID), Types\UpdateTypeController::class);
            Route::delete(param(RouteParamEnum::EQUIPMENT_TYPE_ID), Types\DeleteTypeController::class);
        });
    });
});
