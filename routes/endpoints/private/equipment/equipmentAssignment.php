<?php

use App\Enums\Routes\RouteNameEnum;
use App\Enums\Routes\RouteParamEnum;
use App\Http\Controllers\Equipments\Assignments;
use App\Http\Middlewares\Roles\HasAFreePlanMiddleware;
use App\Http\Middlewares\Roles\HasPermissionMiddleware;
use Binaccle\Enums\Permissions\PermissionEnum;
use Illuminate\Support\Facades\Route;

Route::prefix('equipments')->group(function () {
    Route::middleware(
        HasAFreePlanMiddleware::class,
        middleware(HasPermissionMiddleware::class, PermissionEnum::EQUIPMENTS_ASSIGN),
    )->group(function () {
        Route::post(param(RouteParamEnum::EQUIPMENT_ID) . '/assignments', Assignments\AssignEquipmentController::class);
        Route::delete(param(RouteParamEnum::EQUIPMENT_ID) . '/assignments', Assignments\UnassignEquipmentController::class);
    });

    Route::prefix('assignments')
        ->middleware(
            middleware(HasPermissionMiddleware::class, PermissionEnum::EQUIPMENTS_READ),
        )
        ->group(function () {
            Route::get('', Assignments\ListAssignmentController::class)
                ->name(RouteNameEnum::LIST_EQUIPMENT_ASSIGNMENTS);

            Route::get(param(RouteParamEnum::EQUIPMENT_ID), Assignments\ViewAssignmentController::class);

            Route::get(param(RouteParamEnum::EQUIPMENT_ID) . '/history', Assignments\HistoryAssignmentController::class)
                ->name(RouteNameEnum::LIST_EQUIPMENT_ASSIGNMENT_HISTORY);
        });
});
