<?php

use App\Http\Controllers\Companies;
use App\Http\Middlewares\Roles\HasAFreePlanMiddleware;
use App\Http\Middlewares\Roles\HasPermissionMiddleware;
use Binaccle\Enums\Permissions\PermissionEnum;
use Illuminate\Support\Facades\Route;

Route::prefix('companies')
    ->middleware(
        middleware(HasPermissionMiddleware::class, PermissionEnum::COMPANIES_CONFIG)
    )->group(function () {
        Route::get('current', Companies\ViewCompanyController::class);

        Route::middleware(HasAFreePlanMiddleware::class)->group(function () {
            Route::post('current', Companies\UpdateCompanyController::class);
        });

        Route::delete('current', Companies\DeleteCompanyController::class);
    });
