<?php

use App\Enums\Routes\RouteNameEnum;
use App\Http\Controllers;
use Illuminate\Support\Facades\Route;

Route::get('roles', Controllers\Roles\ListRoleController::class)
    ->name(RouteNameEnum::LIST_ROLES);
