<?php

use App\Enums\Routes\RouteNameEnum;
use App\Enums\Routes\RouteParamEnum;
use App\Http\Controllers\Users;
use App\Http\Middlewares\Roles\HasAFreePlanMiddleware;
use App\Http\Middlewares\Roles\HasPermissionMiddleware;
use App\Http\Middlewares\Roles\InteractsWithAdminMiddleware;
use App\Http\Middlewares\Users\PreventDestroyYourselfMiddleware;
use Binaccle\Enums\Permissions\PermissionEnum;
use Illuminate\Support\Facades\Route;

Route::prefix('users')->group(function () {
    Route::middleware(
        middleware(HasPermissionMiddleware::class, PermissionEnum::USERS_READ)
    )->group(function () {
        Route::get('', Users\ListUserController::class)
            ->name(RouteNameEnum::LIST_USERS);

        Route::prefix('massive-uploads')
            ->middleware(
                middleware(HasPermissionMiddleware::class, PermissionEnum::USERS_MASSIVE_UPLOADS_READ)
            )
            ->group(function () {
                Route::get('', Users\MassiveUploads\ListMassiveUploadController::class)
                ->name(RouteNameEnum::LIST_MASSIVE_USER_UPLOADS);

                Route::get(param(RouteParamEnum::MASSIVE_UPLOAD_ID), Users\MassiveUploads\ViewMassiveUploadController::class);

                Route::get('templates/download', Users\MassiveUploads\DownloadCsvTemplateController::class);
            });

        Route::get(param(RouteParamEnum::USER_ID), Users\ViewUserController::class);
    });

    Route::middleware(HasAFreePlanMiddleware::class)->group(function () {
        Route::post('massive-uploads', Users\MassiveUploads\MassiveUploadController::class)
            ->middleware(
                middleware(HasPermissionMiddleware::class, PermissionEnum::USERS_MASSIVE_UPLOADS_CREATE)
            );

        Route::middleware(
            middleware(HasPermissionMiddleware::class, PermissionEnum::USERS_CUD)
        )->group(function () {
            Route::post('', Users\CreateUserController::class);

            Route::middleware(InteractsWithAdminMiddleware::class)->group(function () {
                Route::put(param(RouteParamEnum::USER_ID), Users\UpdateUserController::class);
                Route::delete(param(RouteParamEnum::USER_ID), Users\DeleteUserController::class)
                    ->middleware(PreventDestroyYourselfMiddleware::class);
            });

            Route::post(param(RouteParamEnum::USER_ID) . '/invitations/send', Users\Invitations\SendInvitationController::class);
        });
    });
});
