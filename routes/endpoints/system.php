<?php

use App\Http\Controllers;
use Illuminate\Support\Facades\Route;

Route::get('plans', Controllers\Plans\ListPlanController::class);
