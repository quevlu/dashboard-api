<?php

$files = [
    'system',
    'auth',
    'private/index',
];

foreach ($files as $file) {
    require "endpoints/{$file}.php";
}
