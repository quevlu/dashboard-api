<?php

namespace Binaccle\Observers;

class Observer
{
    private const COMPANY_ID = 'company_id';

    public function creating(object $model)
    {
        $this->onSave($model);
    }

    public function saving(object $model)
    {
        $this->onSave($model);
    }

    protected function onSave(object &$model): void
    {
        if (auth()->hasUser() && $model->hasAttribute(self::COMPANY_ID)) {
            $model->{$model::COMPANY_ID} = auth()->user()->companyId();
        }
    }
}
