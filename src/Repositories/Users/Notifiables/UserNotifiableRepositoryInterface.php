<?php

namespace Binaccle\Repositories\Users\Notifiables;

use Binaccle\Models\Users\UserNotifiable;
use Binaccle\Repositories\AbstractRepositoryInterface;

interface UserNotifiableRepositoryInterface extends AbstractRepositoryInterface
{
    public const MODEL = UserNotifiable::class;

    public function findByTypeAndToken(string $type, string $token): ?UserNotifiable;

    public function findByTypeAndUserId(string $type, ?string $userId): ?UserNotifiable;

    public function userDoesntHaveToBeVerified(string $userId): bool;
}
