<?php

namespace Binaccle\Repositories\Users;

use Binaccle\Models\Users\User;
use Binaccle\Repositories\AbstractRepositoryInterface;
use Infrastructure\Repositories\Criterias\Sectors\ListUserSectorCriteria;
use Infrastructure\Repositories\Criterias\Users\ListUserCriteria;

interface UserRepositoryInterface extends AbstractRepositoryInterface
{
    public const MODEL = User::class;

    public function cleanOrRemoveSectors(string $sectorId, string $userIdOrAction): void;

    public function hasPassword(string $userId): bool;

    public function list(ListUserCriteria $criteria);

    public function listBySector(ListUserSectorCriteria $criteria);
}
