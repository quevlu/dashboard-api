<?php

namespace Binaccle\Repositories\Sectors;

use Binaccle\Models\Sectors\Sector;
use Binaccle\Repositories\AbstractRepositoryInterface;
use Infrastructure\Repositories\Criterias\Sectors\ListSectorCriteria;

interface SectorRepositoryInterface extends AbstractRepositoryInterface
{
    public const MODEL = Sector::class;

    public function hasUsers(string $id): bool;

    public function list(ListSectorCriteria $criteria);
}
