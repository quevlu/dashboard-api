<?php

namespace Binaccle\Repositories\Roles;

use Binaccle\Models\Roles\Role;
use Binaccle\Repositories\AbstractRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

interface RoleRepositoryInterface extends AbstractRepositoryInterface
{
    public const MODEL = Role::class;

    public function allWithoutAdmin(): Collection;

    public function whereNameIn(array $roles): Collection;
}
