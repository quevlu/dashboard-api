<?php

namespace Binaccle\Repositories\Companies;

use Binaccle\Models\Companies\Company;
use Binaccle\Repositories\AbstractRepositoryInterface;

interface CompanyRepositoryInterface extends AbstractRepositoryInterface
{
    public const MODEL = Company::class;
}
