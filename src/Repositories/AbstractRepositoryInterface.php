<?php

namespace Binaccle\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Infrastructure\Repositories\Criterias\BaseCriteria;

interface AbstractRepositoryInterface
{
    public function all(): Collection;

    public function baseWhereIn(array $values, ?string $keyToSearch = null): Builder;

    public function create(object $payload): object;

    public function exists(string $column, $value): bool;

    public function find(string $id): ?object;

    public function findLikeBy(string $column, $value): Collection;

    public function findOrFail(string $id): object;

    public function firstBy(string $column, $value): ?object;

    public function firstOrCreate(array $conditions, array $attributes = []): object;

    public function firstOrFailBy(string $column, $value): ?object;

    public function firstOrNew(array $conditions, array $attributes = []): object;

    public function handleReturn(object $eloquentObject, BaseCriteria $criteria);

    public function insert(array $records): bool;

    public function instance(...$vars): object;

    public function model(): string;

    public function random(int $limit = 1);

    public function table(): string;

    public function truncate(): void;

    public function update(object $model, array $attributes): void;

    public function whereIn(array $values, ?string $keyToSearch = null): Collection;

    public function whereInCount(array $values, ?string $keyToSearch = null): ?int;
}
