<?php

namespace Binaccle\Repositories\Files;

use Binaccle\Models\Files\File;
use Binaccle\Repositories\AbstractRepositoryInterface;

interface FileRepositoryInterface extends AbstractRepositoryInterface
{
    public const MODEL = File::class;
}
