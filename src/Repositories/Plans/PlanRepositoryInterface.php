<?php

namespace Binaccle\Repositories\Plans;

use Binaccle\Models\Plans\Plan;
use Binaccle\Repositories\AbstractRepositoryInterface;

interface PlanRepositoryInterface extends AbstractRepositoryInterface
{
    public const MODEL = Plan::class;
}
