<?php

namespace Binaccle\Repositories\Equipments\Types;

use Binaccle\Models\Equipments\EquipmentType;
use Binaccle\Repositories\AbstractRepositoryInterface;
use Infrastructure\Repositories\Criterias\Equipments\Types\ListTypeCriteria;

interface TypeRepositoryInterface extends AbstractRepositoryInterface
{
    public const MODEL = EquipmentType::class;

    public function list(ListTypeCriteria $criteria);
}
