<?php

namespace Binaccle\Repositories\Equipments\Assignments;

use Binaccle\Models\Equipments\Equipment;
use Binaccle\Models\Equipments\EquipmentAssignment;
use Binaccle\Repositories\AbstractRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Infrastructure\Repositories\Criterias\Equipments\Assignments\HistoryAssignmentCriteria;
use Infrastructure\Repositories\Criterias\Equipments\Assignments\ListAssignmentCriteria;

interface AssignmentRepositoryInterface extends AbstractRepositoryInterface
{
    public const MODEL = EquipmentAssignment::class;

    public function activeEquipmentsBy(string $actor, string $actorId): ?Builder;

    public function equipmentIsAssigned(string $equipmentId): bool;

    public function hasAnEquipmentAssigned(string $actorId): bool;

    public function history(HistoryAssignmentCriteria $criteria);

    public function isTheSameDestination(string $equipmentId, ?string $userId, ?string $sectorId): bool;

    public function list(ListAssignmentCriteria $criteria);

    public function unassign($mixedValue): void;

    public function view(string $equipmentId): ?Equipment;
}
