<?php

namespace Binaccle\Repositories\Equipments;

use Binaccle\Models\Equipments\Equipment;
use Binaccle\Repositories\AbstractRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Infrastructure\Repositories\Criterias\Equipments\ListEquipmentCriteria;

interface EquipmentRepositoryInterface extends AbstractRepositoryInterface
{
    public const MODEL = Equipment::class;

    public function list(ListEquipmentCriteria $criteria);

    public function nextIdentifier(): int;

    public function whereInWithAssignments(array $equipmentIds): Collection;

    public function whereInWithTypes(array $equipmentIds): Collection;
}
