<?php

namespace Binaccle\Repositories\Equipments\Audits;

use Binaccle\Models\Equipments\EquipmentAuditState;
use Binaccle\Repositories\AbstractRepositoryInterface;

interface AuditStateRepositoryInterface extends AbstractRepositoryInterface
{
    public const MODEL = EquipmentAuditState::class;
}
