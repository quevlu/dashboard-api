<?php

namespace Binaccle\Repositories\Equipments\Audits\Audited;

use Binaccle\Models\Equipments\EquipmentAuditAudited;
use Binaccle\Repositories\AbstractRepositoryInterface;
use Infrastructure\Repositories\Criterias\Equipments\Audits\Audited\ListAuditedCriteria;

interface AuditedRepositoryInterface extends AbstractRepositoryInterface
{
    public const MODEL = EquipmentAuditAudited::class;

    public function anyHaveBeenAudited(string $equipmentAuditId, array $equipmentIds): bool;

    public function deleteByEquipmentAuditId(string $equipmentAuditId): void;

    public function list(ListAuditedCriteria $criteria);
}
