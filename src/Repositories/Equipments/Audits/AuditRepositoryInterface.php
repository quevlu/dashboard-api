<?php

namespace Binaccle\Repositories\Equipments\Audits;

use Binaccle\Models\Equipments\EquipmentAudit;
use Binaccle\Repositories\AbstractRepositoryInterface;
use Infrastructure\Repositories\Criterias\Equipments\Audits\ListAuditCriteria;

interface AuditRepositoryInterface extends AbstractRepositoryInterface
{
    public const MODEL = EquipmentAudit::class;

    public function auditNotFailedOrFinished(string $equipmentAuditId): EquipmentAudit;

    public function list(ListAuditCriteria $criteria);

    public function transition(string $equipmentAuditId, string $state): void;

    public function view(string $equipmentAuditId): EquipmentAudit;
}
