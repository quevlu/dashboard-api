<?php

namespace Binaccle\Repositories\Permissions;

use Binaccle\Models\Permissions\Permission;
use Binaccle\Repositories\AbstractRepositoryInterface;

interface PermissionRepositoryInterface extends AbstractRepositoryInterface
{
    public const MODEL = Permission::class;
}
