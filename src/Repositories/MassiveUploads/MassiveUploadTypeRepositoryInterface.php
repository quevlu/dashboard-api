<?php

namespace Binaccle\Repositories\MassiveUploads;

use Binaccle\Models\MassiveUploads\MassiveUploadType;
use Binaccle\Repositories\AbstractRepositoryInterface;

interface MassiveUploadTypeRepositoryInterface extends AbstractRepositoryInterface
{
    public const MODEL = MassiveUploadType::class;
}
