<?php

namespace Binaccle\Repositories\MassiveUploads;

use Binaccle\Models\MassiveUploads\MassiveUpload;
use Binaccle\Repositories\AbstractRepositoryInterface;
use Infrastructure\Repositories\Criterias\MassiveUploads\Interfaces\ListMassiveUploadCriteriaInterface;

interface MassiveUploadRepositoryInterface extends AbstractRepositoryInterface
{
    public const MODEL = MassiveUpload::class;

    public function isThereAnyPendingMassiveEquipmentUpload(string $companyId): bool;

    public function list(ListMassiveUploadCriteriaInterface $criteria);

    public function view(string $massiveUploadId, string $type): MassiveUpload;
}
