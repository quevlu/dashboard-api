<?php

namespace Binaccle\Repositories\MassiveUploads;

use Binaccle\Models\MassiveUploads\MassiveUploadState;
use Binaccle\Repositories\AbstractRepositoryInterface;

interface MassiveUploadStateRepositoryInterface extends AbstractRepositoryInterface
{
    public const MODEL = MassiveUploadState::class;
}
