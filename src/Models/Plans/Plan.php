<?php

namespace Binaccle\Models\Plans;

use Binaccle\Enums\Roles\RoleEnum;
use Binaccle\Traits\Models\BaseModelTrait;
use Binaccle\Traits\Models\CacheModelTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    use BaseModelTrait;
    use CacheModelTrait;

    public const ID = 'id';

    public const NAME = 'name';

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        self::ID,
        self::NAME,
        self::CREATED_AT,
        self::UPDATED_AT,
    ];

    protected $guard_name = RoleEnum::GUARD;

    public static function instance(string $name): self
    {
        $now = now()->getTimestamp();

        return new self([
            self::ID => randomUuid(),
            self::NAME => $name,
            self::CREATED_AT => $now,
            self::UPDATED_AT => $now,
        ]);
    }

    public function name(): string
    {
        return $this->getAttribute(self::NAME);
    }

    public function permissionsRel(): Collection
    {
        return $this->permissions;
    }
}
