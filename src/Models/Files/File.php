<?php

namespace Binaccle\Models\Files;

use Binaccle\Traits\Models\BaseModelTrait;
use Binaccle\Traits\Models\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use BaseModelTrait;
    use HasFactory;

    public const ID = 'id';

    public const RELATIVE_PATH = 'relative_path';

    public const URL = 'url';

    public $incrementing = false;

    protected $fillable = [
        self::ID,
        self::URL,
        self::RELATIVE_PATH,
    ];

    public static function instance(object $payload): self
    {
        return new self([
            self::ID => randomUuid(),
            self::URL => $payload->url(),
            self::RELATIVE_PATH => $payload->relativePath(),
        ]);
    }

    public function relativePath(): string
    {
        return $this->getAttribute(self::RELATIVE_PATH);
    }

    public function url(): string
    {
        return $this->getAttribute(self::URL);
    }
}
