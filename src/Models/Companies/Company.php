<?php

namespace Binaccle\Models\Companies;

use Binaccle\Models\Equipments\EquipmentAudit;
use Binaccle\Models\Equipments\EquipmentType;
use Binaccle\Models\Files\File;
use Binaccle\Models\MassiveUploads\MassiveUpload;
use Binaccle\Models\Plans\Plan;
use Binaccle\Models\Sectors\Sector;
use Binaccle\Models\Users\User;
use Binaccle\Payloads\Companies\UpdateCompanyPayload;
use Binaccle\Traits\Models\BaseModelTrait;
use Binaccle\Traits\Models\HasFactory;
use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Cashier\Billable;

class Company extends Model
{
    use BaseModelTrait;
    use Billable;
    use CascadeSoftDeletes;
    use HasFactory;
    use SoftDeletes;

    public const EQUIPMENT_AUDITS_RELATIONSHIP = 'equipmentAudits';

    public const EQUIPMENT_TYPES_RELATIONSHIP = 'equipmentTypes';

    public const ID = 'id';

    public const LOGO_ID = 'logo_id';

    public const LOGO_RELATIONSHIP = 'logo';

    public const MASSIVE_UPLOADS_RELATIONSHIP = 'massiveUploads';

    public const NAME = 'name';

    public const PLAN_ID = 'plan_id';

    public const PLAN_RELATIONSHIP = 'plan';

    public const SECTORS_RELATIONSHIP = 'sectors';

    public const USERS_RELATIONSHIP = 'users';

    public $incrementing = false;

    protected $cascadeDeletes = [
        self::MASSIVE_UPLOADS_RELATIONSHIP,
        self::EQUIPMENT_AUDITS_RELATIONSHIP,
        self::EQUIPMENT_TYPES_RELATIONSHIP,
        self::USERS_RELATIONSHIP,
        self::SECTORS_RELATIONSHIP,
        self::LOGO_RELATIONSHIP,
    ];

    protected $casts = [
        self::ID => 'string',
    ];

    protected $fillable = [
        self::ID,
        self::NAME,
        self::LOGO_ID,
        self::PLAN_ID,
    ];

    public function equipmentAudits(): HasMany
    {
        return $this->hasMany(EquipmentAudit::class, EquipmentAudit::COMPANY_ID);
    }

    public function equipmentTypes(): HasMany
    {
        return $this->hasMany(EquipmentType::class, EquipmentType::COMPANY_ID);
    }

    public static function instance(object $payload): self
    {
        return new self([
            self::ID => randomUuid(),
            self::NAME => $payload->companyName(),
            self::PLAN_ID => $payload->planId(),
        ]);
    }

    public function logo(): BelongsTo
    {
        return $this->belongsTo(File::class, self::LOGO_ID);
    }

    public function logoId(): string
    {
        return $this->getAttribute(self::LOGO_ID);
    }

    public function logoRel(): ?File
    {
        return $this->getAttribute(self::LOGO_RELATIONSHIP);
    }

    public function massiveUploads(): HasMany
    {
        return $this->hasMany(MassiveUpload::class, MassiveUpload::COMPANY_ID);
    }

    public function name(): string
    {
        return $this->getAttribute(self::NAME);
    }

    public function plan(): BelongsTo
    {
        return $this->belongsTo(Plan::class, self::PLAN_ID);
    }

    public function planId(): string
    {
        return $this->getAttribute(self::PLAN_ID);
    }

    public function planRel(): Plan
    {
        return $this->getAttribute(self::PLAN_RELATIONSHIP);
    }

    public function sectors(): HasMany
    {
        return $this->hasMany(Sector::class, Sector::COMPANY_ID);
    }

    public function updateInfo(UpdateCompanyPayload $payload): void
    {
        $this->{self::NAME} = $payload->name();

        if ($logoCreated = $payload->logoCreated()) {
            $this->logo()->dissociate();
            $this->logo()->associate($logoCreated);
        }

        $this->save();
    }

    public function users(): HasMany
    {
        return $this->hasMany(User::class, User::COMPANY_ID);
    }
}
