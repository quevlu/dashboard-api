<?php

namespace Binaccle\Models\Sectors;

use Binaccle\Models\Users\User;
use Binaccle\Traits\Models\BaseModelTrait;
use Binaccle\Traits\Models\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sector extends Model
{
    use BaseModelTrait;
    use HasFactory;
    use SoftDeletes;

    public const COMPANY_ID = 'company_id';

    public const DELETED_AT = 'deleted_at';

    public const ID = 'id';

    public const NAME = 'name';

    public const USERS_RELATIONSHIP = 'users';

    public $incrementing = false;

    protected $casts = [
        self::ID => 'string',
    ];

    protected $fillable = [
        self::ID,
        self::NAME,
        self::COMPANY_ID,
    ];

    public function companyId(): string
    {
        return $this->getAttribute(self::COMPANY_ID);
    }

    public static function instance(object $payload): self
    {
        return new self([
            self::ID => randomUuid(),
            self::NAME => $payload->name(),
        ]);
    }

    public function name(): string
    {
        return $this->getAttribute(self::NAME);
    }

    public function updateName(string $name): void
    {
        $this->{self::NAME} = $name;
        $this->save();
    }

    public function users(): HasMany
    {
        return $this->hasMany(User::class, User::SECTOR_ID);
    }
}
