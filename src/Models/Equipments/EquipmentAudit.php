<?php

namespace Binaccle\Models\Equipments;

use Binaccle\Enums\Equipments\Audits\AuditStateEnum;
use Binaccle\Models\Sectors\Sector;
use Binaccle\Payloads\Equipments\Audits\CreateAuditPayload;
use Binaccle\Payloads\Equipments\Audits\UpdateAuditPayload;
use Binaccle\Traits\Models\BaseModelTrait;
use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class EquipmentAudit extends Model
{
    use BaseModelTrait;
    use CascadeSoftDeletes;
    use SoftDeletes;

    public const COMPANY_ID = 'company_id';

    public const EQUIPMENT_AUDIT_AUDITED = 'audited';

    public const EQUIPMENT_AUDIT_SECTOR_RELATIONSHIP = 'sector';

    public const EQUIPMENT_AUDIT_STATE_ID = 'equipment_audit_state_id';

    public const EQUIPMENT_AUDIT_STATE_RELATIONSHIP = 'state';

    public const ID = 'id';

    public const SECTOR_ID = 'sector_id';

    public const TITLE = 'title';

    public $incrementing = false;

    protected $cascadeDeletes = [
        self::EQUIPMENT_AUDIT_AUDITED,
    ];

    protected $casts = [
        self::ID => 'string',
    ];

    protected $fillable = [
        self::ID,
        self::TITLE,
        self::SECTOR_ID,
        self::COMPANY_ID,
        self::EQUIPMENT_AUDIT_STATE_ID,
    ];

    public function audited(): HasMany
    {
        return $this->hasMany(EquipmentAuditAudited::class, EquipmentAuditAudited::EQUIPMENT_AUDIT_ID);
    }

    public function companyId(): string
    {
        return $this->getAttribute(self::COMPANY_ID);
    }

    public function customUpdate(UpdateAuditPayload $payload): void
    {
        $this->{self::TITLE} = $payload->title();

        if ($this->stateRel()->name() != AuditStateEnum::CREATING) {
            $this->{self::SECTOR_ID} = $payload->sectorId();
        }

        $this->save();
    }

    public function equipmentAuditStateId(): string
    {
        return $this->getAttribute(self::EQUIPMENT_AUDIT_STATE_ID);
    }

    public static function instance(CreateAuditPayload $payload)
    {
        return new self([
            self::ID => randomUuid(),
            self::TITLE => $payload->title(),
            self::SECTOR_ID => $payload->sectorId(),
            self::EQUIPMENT_AUDIT_STATE_ID => $payload->equipmentAuditStateId(),
        ]);
    }

    public function sector(): BelongsTo
    {
        return $this->belongsTo(Sector::class, self::SECTOR_ID)->withTrashed();
    }

    public function sectorId(): ?string
    {
        return $this->getAttribute(self::SECTOR_ID);
    }

    public function sectorRel(): ?Sector
    {
        return $this->getAttribute(self::EQUIPMENT_AUDIT_SECTOR_RELATIONSHIP);
    }

    public function state(): BelongsTo
    {
        return $this->belongsTo(EquipmentAuditState::class, self::EQUIPMENT_AUDIT_STATE_ID);
    }

    public function stateRel(): EquipmentAuditState
    {
        return $this->getAttribute(self::EQUIPMENT_AUDIT_STATE_RELATIONSHIP);
    }

    public function title(): string
    {
        return $this->getAttribute(self::TITLE);
    }
}
