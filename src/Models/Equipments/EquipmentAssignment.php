<?php

namespace Binaccle\Models\Equipments;

use Binaccle\Models\Sectors\Sector;
use Binaccle\Models\Users\User;
use Binaccle\Payloads\Equipments\Assignments\AssignEquipmentPayload;
use Binaccle\Traits\Models\BaseModelTrait;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class EquipmentAssignment extends Model
{
    use BaseModelTrait;

    public const ACTIVE = 'active';

    public const ASSIGNED_TIME = 'assigned_time';

    public const EQUIPMENT_ASSIGNED_RELATIONSHIP = 'assigned';

    public const EQUIPMENT_ID = 'equipment_id';

    public const EQUIPMENT_RELATIONSHIP = 'equipment';

    public const ID = 'id';

    public const OBSERVATION = 'observation';

    public const SECTOR_ID = 'sector_id';

    public const SECTOR_RELATIONSHIP = 'sector';

    public const USER_ID = 'user_id';

    public const USER_RELATIONSHIP = 'user';

    public $incrementing = false;

    protected $casts = [
        self::ID => 'string',
    ];

    protected $fillable = [
        self::ID,
        self::EQUIPMENT_ID,
        self::SECTOR_ID,
        self::USER_ID,
        self::ACTIVE,
    ];

    public function active(): bool
    {
        return $this->getAttribute(self::ACTIVE);
    }

    public function assignedTo(): ?string
    {
        if ($this->relationLoaded(self::USER_RELATIONSHIP) && $this->relationLoaded(self::SECTOR_RELATIONSHIP)) {
            return ($this->userRel() ?? $this->sectorRel())->name();
        }

        throw new Exception('The relationships "' . self::USER_RELATIONSHIP . ',' . self::SECTOR_RELATIONSHIP . '" are not loaded');
    }

    public function equipment(): BelongsTo
    {
        return $this->belongsTo(Equipment::class, self::EQUIPMENT_ID)->withTrashed();
    }

    public function equipmentId(): string
    {
        return $this->getAttribute(self::EQUIPMENT_ID);
    }

    public function equipmentRel(): Equipment
    {
        return $this->getAttribute(self::EQUIPMENT_RELATIONSHIP);
    }

    public static function instance(AssignEquipmentPayload $payload): self
    {
        return new self([
            self::ID => randomUuid(),
            self::EQUIPMENT_ID => $payload->equipmentId(),
            self::SECTOR_ID => $payload->sectorId(),
            self::USER_ID => $payload->userId(),
            self::ACTIVE => true,
            self::OBSERVATION => $payload->observation(),
        ]);
    }

    public function observation(): ?string
    {
        return $this->getAttribute(self::OBSERVATION);
    }

    public function sector(): BelongsTo
    {
        return $this->belongsTo(Sector::class, self::SECTOR_ID)->withTrashed();
    }

    public function sectorId(): string
    {
        return $this->getAttribute(self::SECTOR_ID);
    }

    public function sectorRel(): ?Sector
    {
        return $this->getAttribute(self::SECTOR_RELATIONSHIP);
    }

    public function unassign(?string $observation = null): void
    {
        $this->{self::ACTIVE} = false;
        $this->{self::OBSERVATION} = $observation;
        $this->save();
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, self::USER_ID)->withTrashed();
    }

    public function userId(): string
    {
        return $this->getAttribute(self::USER_ID);
    }

    public function userRel(): ?User
    {
        return $this->getAttribute(self::USER_RELATIONSHIP);
    }
}
