<?php

namespace Binaccle\Models\Equipments;

use Binaccle\Traits\Models\BaseModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class EquipmentAuditAudited extends Model
{
    use BaseModelTrait;

    public const AUDITED = 'audited';

    public const EQUIPMENT_ASSIGNMENT_ID = 'equipment_assignment_id';

    public const EQUIPMENT_ASSIGNMENT_RELATIONSHIP = 'equipmentAssignment';

    public const EQUIPMENT_AUDIT_ID = 'equipment_audit_id';

    public const EQUIPMENT_AUDIT_RELATIONSHIP = 'equipmentAudit';

    public const EQUIPMENT_ID = 'equipment_id';

    public const EQUIPMENT_RELATIONSHIP = 'equipment';

    public const ID = 'id';

    public $incrementing = false;

    protected $casts = [
        self::ID => 'string',
        self::CREATED_AT => 'datetime:U',
        self::UPDATED_AT => 'datetime:U',
    ];

    protected $fillable = [
        self::ID,
        self::EQUIPMENT_ID,
        self::EQUIPMENT_ASSIGNMENT_ID,
        self::EQUIPMENT_AUDIT_ID,
        self::AUDITED,
        self::CREATED_AT,
        self::UPDATED_AT,
    ];

    public function audited(): bool
    {
        return $this->getAttribute(self::AUDITED);
    }

    public function equipment(): BelongsTo
    {
        return $this->belongsTo(Equipment::class, self::EQUIPMENT_ID)->withTrashed();
    }

    public function equipmentAssignment(): ?BelongsTo
    {
        return $this->belongsTo(EquipmentAssignment::class, self::EQUIPMENT_ASSIGNMENT_ID);
    }

    public function equipmentAssignmentRel(): ?EquipmentAssignment
    {
        return $this->getAttribute(self::EQUIPMENT_ASSIGNMENT_RELATIONSHIP);
    }

    public function equipmentAudit(): BelongsTo
    {
        return $this->belongsTo(EquipmentAudit::class, self::EQUIPMENT_AUDIT_ID);
    }

    public function equipmentAuditId(): string
    {
        return $this->getAttribute(self::EQUIPMENT_AUDIT_ID);
    }

    public function equipmentAuditRel(): EquipmentAudit
    {
        return $this->getAttribute(self::EQUIPMENT_AUDIT_RELATIONSHIP);
    }

    public function equipmentRel(): Equipment
    {
        return $this->getAttribute(self::EQUIPMENT_RELATIONSHIP);
    }

    public static function instance(object $payload)
    {
        $now = now()->getTimestamp();

        return new self([
            self::ID => randomUuid(),
            self::EQUIPMENT_ID => $payload->equipmentId(),
            self::EQUIPMENT_AUDIT_ID => $payload->equipmentAuditId(),
            self::EQUIPMENT_ASSIGNMENT_ID => $payload->equipmentAssignmentId(),
            self::AUDITED => $payload->audited(),
            self::CREATED_AT => $now,
            self::UPDATED_AT => $now,
        ]);
    }
}
