<?php

namespace Binaccle\Models\Equipments;

use Binaccle\Payloads\Equipments\DeleteEquipmentPayload;
use Binaccle\Payloads\Equipments\UpdateEquipmentPayload;
use Binaccle\Traits\Models\BaseModelTrait;
use Dyrynda\Database\Support\CascadeSoftDeletes;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Equipment extends Model
{
    use BaseModelTrait;
    use CascadeSoftDeletes;
    use SoftDeletes;

    public const ASSIGNMENTS_COUNT = 'assignments_count';

    public const COMPANY_ID = 'company_id';

    public const EQUIPMENT_ASSIGNED_RELATIONSHIP = 'assigned';

    public const EQUIPMENT_ASSIGNMENTS_RELATIONSHIP = 'assignments';

    public const EQUIPMENT_TYPE_ID = 'equipment_type_id';

    public const EQUIPMENT_TYPE_RELATIONSHIP = 'type';

    public const FULL_IDENTIFIER = 'full_identifier';

    public const ID = 'id';

    public const IDENTIFIER = 'identifier';

    public const OBSERVATION = 'observation';

    public const TRASHED = 'trashed';

    public $incrementing = false;

    public $table = 'equipments';

    protected $cascadeDeletes = [
        self::EQUIPMENT_ASSIGNMENTS_RELATIONSHIP,
    ];

    protected $casts = [
        self::ID => 'string',
        self::CREATED_AT => 'datetime:U',
        self::UPDATED_AT => 'datetime:U',
    ];

    protected $fillable = [
        self::ID,
        self::IDENTIFIER,
        self::EQUIPMENT_TYPE_ID,
        self::COMPANY_ID,
        self::CREATED_AT,
        self::UPDATED_AT,
    ];

    public function assigned(): HasOne
    {
        return $this->hasOne(EquipmentAssignment::class, EquipmentAssignment::EQUIPMENT_ID)
            ->where(EquipmentAssignment::ACTIVE, true);
    }

    public function assignedRel(): ?EquipmentAssignment
    {
        return $this->getAttribute(self::EQUIPMENT_ASSIGNED_RELATIONSHIP);
    }

    public function assignedTo(): ?string
    {
        if ($this->relationLoaded(self::EQUIPMENT_ASSIGNED_RELATIONSHIP)) {
            $assignedRelationship = $this->assignedRel();

            if ($assignedRelationship) {
                if ($assignedRelationship->relationLoaded(EquipmentAssignment::USER_RELATIONSHIP) && $assignedRelationship->relationLoaded(EquipmentAssignment::SECTOR_RELATIONSHIP)) {
                    $relationAssigned = $assignedRelationship->userRel() ?? $assignedRelationship->sectorRel();

                    return $relationAssigned->name();
                }

                throw new Exception('The relationships "' . EquipmentAssignment::USER_RELATIONSHIP . ',' . EquipmentAssignment::SECTOR_RELATIONSHIP . '" are not loaded');
            }

            return null;
        }

        throw new Exception('The relationship "' . self::EQUIPMENT_ASSIGNED_RELATIONSHIP . '" is not loaded');
    }

    public function assignments(): HasMany
    {
        return $this->hasMany(EquipmentAssignment::class, EquipmentAssignment::EQUIPMENT_ID);
    }

    public function companyId(): string
    {
        return $this->getAttribute(self::COMPANY_ID);
    }

    public function customDelete(DeleteEquipmentPayload $payload)
    {
        $this->{self::OBSERVATION} = $payload->observation();
        $this->save();

        $this->delete();
    }

    public function fullIdentifier(): string
    {
        if ($this->relationLoaded(self::EQUIPMENT_TYPE_RELATIONSHIP)) {
            return $this->typeRel()->prefix() . $this->getAttribute(self::IDENTIFIER);
        }

        throw new Exception('The relationship "' . self::EQUIPMENT_TYPE_RELATIONSHIP . '" is not loaded');
    }

    public function identifier(): string
    {
        return $this->getAttribute(self::IDENTIFIER);
    }

    public static function instance(object $payload): self
    {
        $now = now()->getTimestamp();

        return new self([
            self::ID => randomUuid(),
            self::IDENTIFIER => $payload->identifier(),
            self::EQUIPMENT_TYPE_ID => $payload->equipmentTypeId(),
            self::COMPANY_ID => $payload->companyId(),
            self::CREATED_AT => $now,
            self::UPDATED_AT => $now,
        ]);
    }

    public function observation(): ?string
    {
        return $this->getAttribute(self::OBSERVATION);
    }

    public function totalAssignments(): int
    {
        return $this->getAttribute(self::ASSIGNMENTS_COUNT);
    }

    public function type(): BelongsTo
    {
        return $this->belongsTo(EquipmentType::class, self::EQUIPMENT_TYPE_ID);
    }

    public function typeRel(): EquipmentType
    {
        return $this->getAttribute(self::EQUIPMENT_TYPE_RELATIONSHIP);
    }

    public function updateInfo(UpdateEquipmentPayload $payload): void
    {
        $this->{self::EQUIPMENT_TYPE_ID} = $payload->equipmentTypeId();
        $this->save();
    }
}
