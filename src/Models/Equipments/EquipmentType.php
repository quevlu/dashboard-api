<?php

namespace Binaccle\Models\Equipments;

use Binaccle\Payloads\Equipments\Types\CreateTypePayload;
use Binaccle\Payloads\Equipments\Types\UpdateTypePayload;
use Binaccle\Traits\Models\BaseModelTrait;
use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class EquipmentType extends Model
{
    use BaseModelTrait;
    use CascadeSoftDeletes;
    use SoftDeletes;

    public const BRAND = 'brand';

    public const CHARACTERISTICS = 'characteristics';

    public const COMPANY_ID = 'company_id';

    public const DELETED_AT = 'deleted_at';

    public const EQUIPMENTS_RELATIONSHIP = 'equipments';

    public const ID = 'id';

    public const MODEL = 'model';

    public const NAME = 'name';

    public const PREFIX = 'prefix';

    public $incrementing = false;

    protected $cascadeDeletes = [
        self::EQUIPMENTS_RELATIONSHIP,
    ];

    protected $casts = [
        self::ID => 'string',
    ];

    protected $fillable = [
        self::ID,
        self::NAME,
        self::BRAND,
        self::MODEL,
        self::PREFIX,
        self::CHARACTERISTICS,
        self::COMPANY_ID,
    ];

    public function brand(): ?string
    {
        return $this->getAttribute(self::BRAND);
    }

    public function characteristics(): ?string
    {
        return $this->getAttribute(self::CHARACTERISTICS);
    }

    public function companyId(): string
    {
        return $this->getAttribute(self::COMPANY_ID);
    }

    public function equipments(): HasMany
    {
        return $this->hasMany(Equipment::class, Equipment::EQUIPMENT_TYPE_ID);
    }

    public static function instance(CreateTypePayload $payload): self
    {
        return new self([
            self::ID => randomUuid(),
            self::NAME => $payload->name(),
            self::BRAND => $payload->brand(),
            self::MODEL => $payload->model(),
            self::PREFIX => strtoupper($payload->prefix()),
            self::CHARACTERISTICS => $payload->characteristics(),
            self::COMPANY_ID => $payload->companyId(),
        ]);
    }

    public function model(): ?string
    {
        return $this->getAttribute(self::MODEL);
    }

    public function name(): string
    {
        return $this->getAttribute(self::NAME);
    }

    public function prefix(): string
    {
        return $this->getAttribute(self::PREFIX);
    }

    public function updateInfo(UpdateTypePayload $payload): void
    {
        $this->{self::NAME} = $payload->name();
        $this->{self::BRAND} = $payload->brand();
        $this->{self::MODEL} = $payload->model();
        $this->{self::PREFIX} = strtoupper($payload->prefix());
        $this->{self::CHARACTERISTICS} = $payload->characteristics();
        $this->save();
    }
}
