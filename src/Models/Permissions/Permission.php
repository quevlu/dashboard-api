<?php

namespace Binaccle\Models\Permissions;

use Binaccle\Enums\Roles\RoleEnum;
use Binaccle\Traits\Models\BaseModelTrait;
use Spatie\Permission\Models\Permission as SpatiePermission;

class Permission extends SpatiePermission
{
    use BaseModelTrait;

    public const GUARD_NAME = 'guard_name';

    public const ID = 'id';

    public const NAME = 'name';

    public $incrementing = false;

    public $timestamps = false;

    protected $casts = [
        self::ID => 'string',
    ];

    protected $guard_name = RoleEnum::GUARD;

    public function guardName(): string
    {
        return $this->getAttribute(self::GUARD_NAME);
    }

    public static function instance(string $name, string $guardName): self
    {
        $now = now()->getTimestamp();

        return new self([
            self::ID => randomUuid(),
            self::NAME => $name,
            self::GUARD_NAME => $guardName,
            self::CREATED_AT => $now,
            self::UPDATED_AT => $now,
        ]);
    }

    public function name(): string
    {
        return $this->getAttribute(self::NAME);
    }
}
