<?php

namespace Binaccle\Models\Users;

use Binaccle\Enums\Roles\RoleEnum;
use Binaccle\Models\Companies\Company;
use Binaccle\Models\Roles\Role;
use Binaccle\Models\Sectors\Sector;
use Binaccle\Traits\Models\BaseModelTrait;
use Binaccle\Traits\Models\HasFactory;
use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use BaseModelTrait;
    use CascadeSoftDeletes;
    use HasFactory;
    use HasRoles;
    use Notifiable;
    use SoftDeletes;

    public const COMPANY_ID = 'company_id';

    public const COMPANY_RELATIONSHIP = 'company';

    public const EMAIL = 'email';

    public const ID = 'id';

    public const IDENTITY_DOCUMENT = 'identity_document';

    public const INVITATION_RELATIONSHIP = 'invitation';

    public const NAME = 'name';

    public const PASSWORD = 'password';

    public const RESET_RELATIONSHIP = 'reset';

    public const ROLES_RELATIONSHIP = 'roles';

    public const SECTOR_ID = 'sector_id';

    public const SECTOR_RELATIONSHIP = 'sector';

    public const TIMEZONE = 'timezone';

    public const VERIFICATION_RELATIONSHIP = 'verification';

    public $incrementing = false;

    protected $cascadeDeletes = [
        self::INVITATION_RELATIONSHIP,
        self::RESET_RELATIONSHIP,
        self::VERIFICATION_RELATIONSHIP,
    ];

    protected $casts = [
        self::ID => 'string',
    ];

    protected $fillable = [
        self::ID,
        self::NAME,
        self::EMAIL,
        self::PASSWORD,
        self::COMPANY_ID,
        self::SECTOR_ID,
        self::IDENTITY_DOCUMENT,
        self::TIMEZONE,
    ];

    protected $guard_name = RoleEnum::GUARD;

    protected $hidden = [
        self::PASSWORD,
    ];

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class, self::COMPANY_ID);
    }

    public function companyId(): ?string
    {
        return $this->getAttribute(self::COMPANY_ID);
    }

    public function companyRel(): Company
    {
        return $this->getAttribute(self::COMPANY_RELATIONSHIP);
    }

    public function email(): string
    {
        return $this->getAttribute(self::EMAIL);
    }

    public function getJWTCustomClaims(): array
    {
        return [];
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function identityDocument(): string
    {
        return $this->getAttribute(self::IDENTITY_DOCUMENT);
    }

    public static function instance(object $payload): self
    {
        $user = new self([
            self::ID => randomUuid(),
            self::NAME => $payload->name(),
            self::PASSWORD => $payload->password() ? password($payload->password()) : null,
            self::EMAIL => $payload->email(),
            self::COMPANY_ID => $payload->companyId(),
            self::SECTOR_ID => $payload->sectorId(),
            self::IDENTITY_DOCUMENT => $payload->identityDocument(),
        ]);

        $roleNames = $payload->roles()->pluck(Role::NAME)->toArray();
        $user->assignRole($roleNames);

        return $user;
    }

    public function invitation(): HasOne
    {
        return $this->hasOne(UserNotifiable::class, UserNotifiable::USER_ID)
            ->where(UserNotifiable::TYPE, UserNotifiable::INVITATION_TYPE);
    }

    public function invitationRel(): ?UserNotifiable
    {
        return $this->getAttribute(self::INVITATION_RELATIONSHIP);
    }

    public function isAdmin(): bool
    {
        return $this->hasRole(RoleEnum::ADMIN);
    }

    public function isUser(): bool
    {
        return $this->hasRole(RoleEnum::USER);
    }

    public function name(): string
    {
        return $this->getAttribute(self::NAME);
    }

    public function password(): ?string
    {
        return $this->getAttribute(self::PASSWORD);
    }

    public function reset(): HasOne
    {
        return $this->hasOne(UserNotifiable::class, UserNotifiable::USER_ID)
            ->where(UserNotifiable::TYPE, UserNotifiable::RESET_TYPE);
    }

    public function resetRel(): ?UserNotifiable
    {
        return $this->getAttribute(self::RESET_RELATIONSHIP);
    }

    public function rolesRel()
    {
        return $this->getAttribute(self::ROLES_RELATIONSHIP);
    }

    public function sector(): BelongsTo
    {
        return $this->belongsTo(Sector::class, self::SECTOR_ID);
    }

    public function sectorId(): ?string
    {
        return $this->getAttribute(self::SECTOR_ID);
    }

    public function sectorRel(): ?Sector
    {
        return $this->getAttribute(self::SECTOR_RELATIONSHIP);
    }

    public function timezone(): string
    {
        return $this->getAttribute(self::TIMEZONE);
    }

    public function updateFromUserModule(object $payload): self
    {
        $this->{self::NAME} = $payload->name();
        $this->{self::SECTOR_ID} = $payload->sectorId();
        $this->{self::IDENTITY_DOCUMENT} = $payload->identityDocument();

        $roles = $payload->roles();

        if (! $this->isAdmin()) {
            $roleNames = $roles->pluck(Role::NAME)->toArray();
            $this->syncRoles($roleNames);
        }

        if ($roles->where(Role::NAME, RoleEnum::USER)->count()) {
            $this->{self::PASSWORD} = null;

            $invitationNotifiable = $this->invitationRel();

            if ($invitationNotifiable) {
                $invitationNotifiable->delete();
            }
        }

        $this->save();

        return $this;
    }

    public function updatePassword(string $password): void
    {
        $this->{self::PASSWORD} = password($password);
        $this->save();
    }

    public function verification(): HasOne
    {
        return $this->hasOne(UserNotifiable::class, UserNotifiable::USER_ID)
            ->where(UserNotifiable::TYPE, UserNotifiable::VERIFICATION_TYPE);
    }

    public function verificationRel(): ?UserNotifiable
    {
        return $this->getAttribute(self::VERIFICATION_RELATIONSHIP);
    }
}
