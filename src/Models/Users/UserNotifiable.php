<?php

namespace Binaccle\Models\Users;

use Binaccle\Traits\Models\BaseModelTrait;
use Binaccle\Traits\Models\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Str;

class UserNotifiable extends Model
{
    use BaseModelTrait;
    use HasFactory;

    public const ID = 'id';

    public const INVITATION_TYPE = 'invitation';

    public const LEFT = 'left';

    public const LEFT_LIMIT = 5;

    public const RESET_TYPE = 'reset';

    public const TOKEN = 'token';

    public const TOKEN_LENGTH = 64;

    public const TYPE = 'type';

    public const USER_ID = 'user_id';

    public const USER_RELATIONSHIP = 'user';

    public const USER_RELATIONSHIP_FK = 'user_id';

    public const VERIFICATION_TYPE = 'verification';

    public const VERIFIED = 'verified';

    public $incrementing = false;

    protected $casts = [
        self::ID => 'string',
    ];

    protected $fillable = [
        self::ID,
        self::USER_ID,
        self::TOKEN,
        self::LEFT,
        self::TYPE,
    ];

    public function decrease()
    {
        $this->{self::LEFT}--;
        $this->save();
    }

    public static function instance(object $payload): self
    {
        return new self([
            self::ID => randomUuid(),
            self::USER_ID => $payload->currentUser()->id,
            self::TOKEN => Str::random(self::TOKEN_LENGTH),
            self::TYPE => $payload->typeToNotify(),
            self::LEFT => self::LEFT_LIMIT - 1,
        ]);
    }

    public function left(): int
    {
        return $this->getAttribute(self::LEFT);
    }

    public function token(): string
    {
        return $this->getAttribute(self::TOKEN);
    }

    public function type(): string
    {
        return $this->getAttribute(self::TYPE);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, self::USER_RELATIONSHIP_FK);
    }

    public function userId(): string
    {
        return $this->getAttribute(self::USER_ID);
    }

    public function userRel(): ?User
    {
        return $this->getAttribute(self::USER_RELATIONSHIP);
    }
}
