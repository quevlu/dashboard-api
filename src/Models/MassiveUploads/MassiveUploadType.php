<?php

namespace Binaccle\Models\MassiveUploads;

use Binaccle\Traits\Models\BaseModelTrait;
use Binaccle\Traits\Models\CacheModelTrait;
use Illuminate\Database\Eloquent\Model;

class MassiveUploadType extends Model
{
    use BaseModelTrait;
    use CacheModelTrait;

    public const ID = 'id';

    public const NAME = 'name';

    public $incrementing = false;

    public $timestamps = false;

    protected $casts = [
        self::ID => 'string',
    ];

    protected $fillable = [
        self::ID,
        self::NAME,
    ];

    public static function instance(string $name): self
    {
        return new self([
            self::ID => randomUuid(),
            self::NAME => $name,
        ]);
    }

    public function name(): string
    {
        return $this->getAttribute(self::NAME);
    }
}
