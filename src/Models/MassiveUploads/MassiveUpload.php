<?php

namespace Binaccle\Models\MassiveUploads;

use Binaccle\Payloads\MassiveUploads\MassiveUploadPayload;
use Binaccle\Traits\Models\BaseModelTrait;
use Binaccle\Traits\Models\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MassiveUpload extends Model
{
    use BaseModelTrait;
    use HasFactory;

    public const COMPANY_ID = 'company_id';

    public const ERROR = 'error';

    public const ID = 'id';

    public const MASSIVE_UPLOAD_STATE_ID = 'massive_upload_state_id';

    public const MASSIVE_UPLOAD_STATE_RELATIONSHIP = 'state';

    public const MASSIVE_UPLOAD_TYPE_ID = 'massive_upload_type_id';

    public const MASSIVE_UPLOAD_TYPE_RELATIONSHIP = 'type';

    public const PROCESSED = 'processed';

    public const RESULT = 'result';

    public $incrementing = false;

    protected $casts = [
        self::ID => 'string',
    ];

    protected $fillable = [
        self::ID,
        self::COMPANY_ID,
        self::PROCESSED,
        self::ERROR,
        self::RESULT,
        self::MASSIVE_UPLOAD_TYPE_ID,
        self::MASSIVE_UPLOAD_STATE_ID,
    ];

    public function companyId(): ?string
    {
        return $this->getAttribute(self::COMPANY_ID);
    }

    public function error(): ?int
    {
        return $this->getAttribute(self::ERROR);
    }

    public static function instance(MassiveUploadPayload $payload): self
    {
        return new self([
            self::ID => randomUuid(),
            self::COMPANY_ID => $payload->companyId(),
            self::MASSIVE_UPLOAD_TYPE_ID => $payload->massiveUploadTypeId(),
            self::MASSIVE_UPLOAD_STATE_ID => $payload->massiveUploadStateId(),
        ]);
    }

    public function massiveUploadStateId(): string
    {
        return $this->getAttribute(self::MASSIVE_UPLOAD_STATE_ID);
    }

    public function massiveUploadTypeId(): string
    {
        return $this->getAttribute(self::MASSIVE_UPLOAD_TYPE_ID);
    }

    public function processed(): ?int
    {
        return $this->getAttribute(self::PROCESSED);
    }

    public function register(int $processed, int $error, array $result, string $massiveUploadStateId): void
    {
        $this->{self::PROCESSED} = $processed;
        $this->{self::ERROR} = $error;
        $this->{self::RESULT} = $result ? json_encode($result) : null;
        $this->{self::MASSIVE_UPLOAD_STATE_ID} = $massiveUploadStateId;
        $this->save();
    }

    public function result(): ?string
    {
        return $this->getAttribute(self::RESULT);
    }

    public function state(): BelongsTo
    {
        return $this->belongsTo(MassiveUploadState::class, self::MASSIVE_UPLOAD_STATE_ID);
    }

    public function stateRel(): MassiveUploadState
    {
        return $this->getAttribute(self::MASSIVE_UPLOAD_STATE_RELATIONSHIP);
    }

    public function type(): BelongsTo
    {
        return $this->belongsTo(MassiveUploadType::class, self::MASSIVE_UPLOAD_TYPE_ID);
    }

    public function typeRel(): MassiveUploadType
    {
        return $this->getAttribute(self::MASSIVE_UPLOAD_TYPE_RELATIONSHIP);
    }
}
