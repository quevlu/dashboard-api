<?php

namespace Binaccle\Enums\Users;

use Binaccle\Enums\AbstractEnum;

class UserVerifiedEnum extends AbstractEnum
{
    public const DONT_APPLY = 'dont_apply';

    public const PENDING = 'pending';

    public const VERIFIED = 'verified';
}
