<?php

namespace Binaccle\Enums;

use Illuminate\Support\Str;
use InvalidArgumentException;
use ReflectionClass;

abstract class AbstractEnum
{
    public function __construct(
        protected ?string $value
    ) {
        static::assert($value);
    }

    public function __toString()
    {
        return $this->value() ?? '';
    }

    public static function allAsObjects(): array
    {
        return self::internalAsAllObjects();
    }

    public static function allValues(): array
    {
        return array_values((new ReflectionClass(get_called_class()))->getConstants());
    }

    public static function fromString(string $name = null): self
    {
        return new static ($name);
    }

    public function is(string $name = null): bool
    {
        return $this->value === $name;
    }

    public function isAny(array $names): bool
    {
        return in_array($this->value, $names, true);
    }

    public function isNot(string $name = null): bool
    {
        return $this->value !== $name;
    }

    public function isNotAny(array $names): bool
    {
        return ! $this->isAny($names);
    }

    public function jsonSerialize()
    {
        return $this->value();
    }

    public static function returnAsStringOrAsObject(array $constants, bool $itemAsObject = false): array
    {
        return $itemAsObject ? array_map(fn ($constant) => new static($constant), $constants) : $constants;
    }

    public function value(): ?string
    {
        return $this->value;
    }

    public static function whenContain($contain): ?array
    {
        return self::internalAsAllObjects(function ($value) use ($contain) {
            return Str::contains($value, $contain);
        });
    }

    public static function whenNotContain(...$notContain): ?array
    {
        $values = self::allValues();

        return array_filter($values, function ($value) use ($notContain) {
            return ! Str::contains($value, $notContain);
        });
    }

    protected static function assert(string $name = null): void
    {
        if (! in_array($name, static::allValues(), false)) {
            $oClass = new ReflectionClass(get_called_class());

            throw new InvalidArgumentException('enum.' . $oClass->getShortName() . '.notFound');
        }
    }

    private static function internalAsAllObjects(?object $function = null): ?array
    {
        return array_map(function (?string $value) {
            return new static($value);
        }, array_filter(
            static::allValues(),
            fn ($value) => ! \in_array($value, ['', null], true) && (gettype($function) == 'object' ? $function($value) : true)
        ));
    }
}
