<?php

namespace Binaccle\Enums\Equipments\Audits;

use Binaccle\Enums\AbstractEnum;

class AuditStateEnum extends AbstractEnum
{
    public const CREATED = 'created';

    public const CREATING = 'creating';

    public const FAILED = 'failed';

    public const FINISHED = 'finished';

    public static function states()
    {
        return [
            self::CREATING,
            self::CREATED,
            self::FAILED,
            self::FINISHED,
        ];
    }
}
