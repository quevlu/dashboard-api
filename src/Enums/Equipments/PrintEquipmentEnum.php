<?php

namespace Binaccle\Enums\Equipments;

use Binaccle\Enums\AbstractEnum;

class PrintEquipmentEnum extends AbstractEnum
{
    public const BARCODE = 'barcode';

    public const QR = 'qr';
}
