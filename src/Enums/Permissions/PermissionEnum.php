<?php

namespace Binaccle\Enums\Permissions;

use Binaccle\Enums\AbstractEnum;
use Binaccle\Enums\Plans\PlanEnum;
use Binaccle\Enums\Roles\RoleEnum;
use Illuminate\Support\Collection;

class PermissionEnum extends AbstractEnum
{
    public const AUDITS_CUD = 'audits.cud';

    public const AUDITS_READ = 'audits.read';

    public const COMPANIES_CONFIG = 'companies.config';

    public const COMPANIES_PLAN = 'companies.plan';

    public const EQUIPMENTS_ASSIGN = 'equipments.assign';

    public const EQUIPMENTS_CUD = 'equipments.cud';

    public const EQUIPMENTS_MASSIVE_PRINT = 'equipments.massive_print';

    public const EQUIPMENTS_MASSIVE_UPLOADS_CREATE = 'equipments.massive_uploads.create';

    public const EQUIPMENTS_MASSIVE_UPLOADS_READ = 'equipments.massive_uploads.read';

    public const EQUIPMENTS_READ = 'equipments.read';

    public const GUARD = 'api';

    public const ROLE_PERMISSIONS_BY_PLAN = [
        PlanEnum::FREE => [
            RoleEnum::ADMIN => [
                self::COMPANIES_CONFIG,
                self::COMPANIES_PLAN,

                self::EQUIPMENTS_READ,

                self::SECTORS_READ,

                self::USERS_READ,
            ],
            RoleEnum::READER => [
                self::EQUIPMENTS_READ,

                self::SECTORS_READ,

                self::USERS_READ,
            ],
            RoleEnum::RR_HH => [
                self::SECTORS_READ,

                self::USERS_READ,
            ],
            RoleEnum::UPLOADER => [
                self::EQUIPMENTS_READ,

                self::SECTORS_READ,
            ],
        ],
        PlanEnum::BASIC => [
            RoleEnum::ADMIN => [
                self::EQUIPMENTS_CUD,
                self::EQUIPMENTS_ASSIGN,

                self::SECTORS_CUD,

                self::USERS_CUD,
            ],
            RoleEnum::READER => [],
            RoleEnum::RR_HH => [
                self::SECTORS_CUD,

                self::USERS_CUD,
            ],
            RoleEnum::UPLOADER => [
                self::EQUIPMENTS_CUD,
                self::EQUIPMENTS_ASSIGN,
            ],
        ],
        PlanEnum::STANDARD => [
            RoleEnum::ADMIN => [
                self::USERS_MASSIVE_UPLOADS_CREATE,
                self::USERS_MASSIVE_UPLOADS_READ,

            ],
            RoleEnum::READER => [
                self::USERS_MASSIVE_UPLOADS_READ,
            ],
            RoleEnum::RR_HH => [
                self::USERS_MASSIVE_UPLOADS_CREATE,
                self::USERS_MASSIVE_UPLOADS_READ,
            ],
            RoleEnum::UPLOADER => [],
        ],
        PlanEnum::PREMIUM => [
            RoleEnum::ADMIN => [
                self::EQUIPMENTS_MASSIVE_UPLOADS_CREATE,
                self::EQUIPMENTS_MASSIVE_UPLOADS_READ,
                self::EQUIPMENTS_MASSIVE_PRINT,

            ],
            RoleEnum::READER => [
                self::EQUIPMENTS_MASSIVE_PRINT,
                self::EQUIPMENTS_MASSIVE_UPLOADS_READ,
            ],
            RoleEnum::RR_HH => [],
            RoleEnum::UPLOADER => [
                self::EQUIPMENTS_MASSIVE_PRINT,
                self::EQUIPMENTS_MASSIVE_UPLOADS_CREATE,
                self::EQUIPMENTS_MASSIVE_UPLOADS_READ,
            ],
        ],
        PlanEnum::ENTERPRISE => [
            RoleEnum::ADMIN => [
                self::AUDITS_CUD,
                self::AUDITS_READ,
            ],
            RoleEnum::READER => [
                self::AUDITS_READ,
            ],
            RoleEnum::RR_HH => [],
            RoleEnum::UPLOADER => [
                self::AUDITS_CUD,
                self::AUDITS_READ,
            ],
        ],
    ];

    public const SECTORS_CUD = 'sectors.cud';

    public const SECTORS_READ = 'sectors.read';

    public const USERS_CUD = 'users.cud';

    public const USERS_MASSIVE_UPLOADS_CREATE = 'users.massive_upload.create';

    public const USERS_MASSIVE_UPLOADS_READ = 'users.massive_uploads.read';

    public const USERS_READ = 'users.read';

    public static function hasPermission(string $companyPlan, Collection $roles, string $permission): bool
    {
        $permissions = self::rolePermissionsByPlan($companyPlan, $roles);

        return array_search($permission, $permissions) !== null;
    }

    public static function rolePermissionsByPlan(string $companyPlan, Collection $roles): array
    {
        $permissions = [];
        $needABreak = false;

        foreach ($roles as $role) {
            foreach (self::ROLE_PERMISSIONS_BY_PLAN as $planName => $rolePlanPermissions) {
                $needABreak = $planName == $companyPlan;

                $permissions = array_merge($permissions, $rolePlanPermissions[$role]);

                if ($needABreak) {
                    break;
                }
            }
        }

        sort($permissions);

        return $permissions;
    }
}
