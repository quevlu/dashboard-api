<?php

namespace Binaccle\Enums\MassiveUploads;

use Binaccle\Enums\AbstractEnum;

class MassiveUploadStateEnum extends AbstractEnum
{
    public const PENDING = 'pending';

    public const PROCESSED = 'processed';
}
