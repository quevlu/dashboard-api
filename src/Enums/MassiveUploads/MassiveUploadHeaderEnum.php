<?php

namespace Binaccle\Enums\MassiveUploads;

use Binaccle\Enums\AbstractEnum;

class MassiveUploadHeaderEnum extends AbstractEnum
{
    public const HEADERS = [
        MassiveUploadTypeEnum::USERS => [
            'name',
            'email',
            'sector',
            'identity_document',
            'roles',
        ],

        MassiveUploadTypeEnum::EQUIPMENTS => [
            'name',
            'quantity',
        ],

        MassiveUploadTypeEnum::EQUIPMENT_TYPES => [
            'name',
            'brand',
            'model',
            'prefix',
            'characteristics',
            'quantity',
        ],
    ];
}
