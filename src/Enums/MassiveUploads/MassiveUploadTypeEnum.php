<?php

namespace Binaccle\Enums\MassiveUploads;

use Binaccle\Enums\AbstractEnum;

class MassiveUploadTypeEnum extends AbstractEnum
{
    public const DELIMITERS = [
        self::USERS => 4,
        self::EQUIPMENTS => 5,
        self::EQUIPMENT_TYPES => 5,
    ];

    public const EQUIPMENT_TYPES = 'equipment_types';

    public const EQUIPMENTS = 'equipments';

    public const USERS = 'users';

    public static function types()
    {
        return [
            self::USERS,
            self::EQUIPMENTS,
            self::EQUIPMENT_TYPES,
        ];
    }
}
