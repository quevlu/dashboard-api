<?php

namespace Binaccle\Enums\Roles;

use Binaccle\Enums\AbstractEnum;

class RoleEnum extends AbstractEnum
{
    public const ADMIN = 'admin';

    public const GUARD = 'api';

    public const READER = 'reader';

    public const RR_HH = 'rr_hh';

    public const UPLOADER = 'uploader';

    public const USER = 'user';

    public static function allWithoutAdmin(): array
    {
        return self::whenNotContain(self::GUARD, self::ADMIN);
    }

    public static function roles(bool $itemAsObject = false): array
    {
        $roles = [
            self::ADMIN,
            self::USER,
            self::READER,
            self::UPLOADER,
            self::RR_HH,
        ];

        return self::returnAsStringOrAsObject($roles, $itemAsObject);
    }
}
