<?php

namespace Binaccle\Enums\Plans;

use Binaccle\Enums\AbstractEnum;

class PlanEnum extends AbstractEnum
{
    public const BASIC = 'basic';

    public const ENTERPRISE = 'enterprise';

    public const FREE = 'free';

    public const PREMIUM = 'premium';

    public const STANDARD = 'standard';

    public static function plans(): array
    {
        return config('plans');
    }

    public static function plansWithoutFree(): array
    {
        return array_filter(self::plans(), fn ($options, $plan) => $plan != self::FREE, ARRAY_FILTER_USE_BOTH);
    }

    // public static function plansWithoutFree
}
