<?php

namespace Binaccle\Services\Files;

use Binaccle\Models\Files\File;
use Infrastructure\Storage\S3Storage;

class DeleteFileService
{
    public function __construct(
        private S3Storage $s3Storage
    ) {
    }

    public function delete(File $file): void
    {
        $this->s3Storage->delete($file->relativePath());

        $file->delete();
    }
}
