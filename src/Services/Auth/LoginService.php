<?php

namespace Binaccle\Services\Auth;

use Binaccle\Payloads\Auth\LoginPayload;
use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Auth;
use Infrastructure\Jwt\JwtEncryption;

class LoginService
{
    public function __construct(
        private JwtEncryption $encryptionService
    ) {
    }

    public function login(LoginPayload $payload): string
    {
        $guard = auth();
        $user = $payload->currentUser();
        $token = $this->encryptionService->encryptJwt(Auth::login($user));

        event(new Login($guard, $user, true));

        return $token;
    }
}
