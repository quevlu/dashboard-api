<?php

namespace Binaccle\Services\Auth;

use Binaccle\Models\Companies\Company;
use Binaccle\Models\Users\User;

class UserLoggedService
{
    public function logged(User $user): User
    {
        $user->loadMissing(User::COMPANY_RELATIONSHIP . '.' . Company::PLAN_RELATIONSHIP);

        return $user;
    }
}
