<?php

namespace Binaccle\Services\Auth\ResetPassword;

use App\Http\Requests\Auth\ResetPassword\ResetPasswordRequest;
use Binaccle\Events\Shared\SuccessEvent;
use Illuminate\Support\Facades\DB;
use Throwable;

class ResetPasswordService
{
    public function reset(ResetPasswordRequest $request): void
    {
        $resetPasswordNotifiable = $request->userNotifiable();

        if ($resetPasswordNotifiable) {
            try {
                DB::beginTransaction();

                $resetPasswordNotifiable->delete();
                $request->currentUser()->updatePassword($request->password());

                event(new SuccessEvent());

                DB::commit();
            } catch (Throwable $exception) {
                DB::rollback();

                throw $exception;
            }
        }
    }
}
