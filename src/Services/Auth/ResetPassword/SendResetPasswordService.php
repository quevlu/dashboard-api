<?php

namespace Binaccle\Services\Auth\ResetPassword;

use Binaccle\Payloads\Users\Notifiables\UserNotifiablePayload;
use Binaccle\Services\Users\Notifiables\UserNotifiableService;

class SendResetPasswordService extends UserNotifiableService
{
    public function send(UserNotifiablePayload $payload): void
    {
        $user = $payload->currentUser();

        if ($user && $user->password() && ! $user->verification()->exists()) {
            parent::send($payload);
        }
    }
}
