<?php

namespace Binaccle\Services\Auth;

use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;
use Infrastructure\Jwt\JwtEncryption;
use Throwable;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\JWTAuth;

class RefreshService
{
    public function __construct(
        private JWTAuth $auth,
        private Request $request,
        private JwtEncryption $encryptionService
    ) {
    }

    public function refresh(): ?string
    {
        $token = null;

        try {
            if (! $this->auth->parser()->setRequest($this->request)->hasToken() || ! $this->auth->parseToken()->authenticate()) {
                throw new UnauthorizedException();
            }

            $token = $this->refreshToken();
        } catch (TokenExpiredException $th) {
            $token = $this->refreshToken();
        } catch (Throwable $th) {
            throw new UnauthorizedException();
        }

        return $token;
    }

    private function refreshToken(): string
    {
        return $this->encryptionService->encryptJwt($this->auth->parseToken()->refresh(true, true));
    }
}
