<?php

namespace Binaccle\Services\Auth\Register;

use Binaccle\Events\Shared\SuccessEvent;
use Binaccle\Repositories\Users\UserRepositoryInterface;
use Binaccle\Services\Companies\CreateCompanyService;
use Binaccle\Services\Users\Notifiables\UserNotifiableService;
use Illuminate\Support\Facades\DB;
use Throwable;

class RegisterService
{
    public function __construct(
        private CreateCompanyService $createCompanyService,
        private UserRepositoryInterface $userRepository,
        private UserNotifiableService $userNotifiableService
    ) {
    }

    public function create(object $payload): void
    {
        try {
            DB::beginTransaction();

            $company = $this->createCompanyService->create($payload);

            $payload->setCompanyId($company->id());

            $user = $this->userRepository->create($payload);

            $payload->setCurrentUser($user);

            $this->userNotifiableService->send($payload);

            event(new SuccessEvent());

            DB::commit();
        } catch (Throwable $exception) {
            DB::rollback();

            throw $exception;
        }
    }
}
