<?php

namespace Binaccle\Services\Auth\ChangePassword;

use Binaccle\Payloads\Auth\ChangePassword\ChangePasswordPayload;

class ChangePasswordService
{
    public function change(ChangePasswordPayload $payload): void
    {
        $payload->currentUser()->updatePassword($payload->newPassword());
    }
}
