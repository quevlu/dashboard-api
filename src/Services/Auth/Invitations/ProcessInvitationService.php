<?php

namespace Binaccle\Services\Auth\Invitations;

use Binaccle\Payloads\Auth\Invitations\ProcessInvitationPayload;
use Illuminate\Support\Facades\DB;
use Throwable;

class ProcessInvitationService
{
    public function process(ProcessInvitationPayload $payload): void
    {
        $invitationNotifiable = $payload->userNotifiable();

        if ($invitationNotifiable) {
            try {
                DB::beginTransaction();

                $invitationNotifiable->delete();
                $payload->currentUser()->updatePassword($payload->password());

                DB::commit();
            } catch (Throwable $th) {
                DB::rollBack();
                throw $th;
            }
        }
    }
}
