<?php

namespace Binaccle\Services\Auth;

class LogoutService
{
    public function logout(): void
    {
        auth()->logout(true);
    }
}
