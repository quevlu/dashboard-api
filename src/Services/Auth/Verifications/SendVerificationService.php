<?php

namespace Binaccle\Services\Auth\Verifications;

use Binaccle\Payloads\Users\Notifiables\UserNotifiablePayload;
use Binaccle\Services\Users\Notifiables\UserNotifiableService;

class SendVerificationService extends UserNotifiableService
{
    public function send(UserNotifiablePayload $payload): void
    {
        if ($payload->modelToNotify()) {
            parent::send($payload);
        }
    }
}
