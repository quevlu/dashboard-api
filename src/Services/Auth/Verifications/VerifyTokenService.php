<?php

namespace Binaccle\Services\Auth\Verifications;

use App\Http\Requests\Auth\Verifications\VerifyTokenRequest;
use Binaccle\Events\Shared\SuccessEvent;

class VerifyTokenService
{
    public function verify(VerifyTokenRequest $request): void
    {
        $verification = $request->userNotifiable();

        if ($verification) {
            $verification->delete();

            event(new SuccessEvent());
        }
    }
}
