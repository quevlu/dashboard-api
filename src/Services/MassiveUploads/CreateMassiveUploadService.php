<?php

namespace Binaccle\Services\MassiveUploads;

use Binaccle\Models\MassiveUploads\MassiveUpload;
use Binaccle\Payloads\MassiveUploads\MassiveUploadPayload;
use Binaccle\Repositories\MassiveUploads\MassiveUploadRepositoryInterface;

class CreateMassiveUploadService
{
    public function __construct(
        private MassiveUploadRepositoryInterface $massiveUploadRepository
    ) {
    }

    public function create(MassiveUploadPayload $payload): MassiveUpload
    {
        return $this->massiveUploadRepository->create($payload);
    }
}
