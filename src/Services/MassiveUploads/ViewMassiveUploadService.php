<?php

namespace Binaccle\Services\MassiveUploads;

use Binaccle\Models\MassiveUploads\MassiveUpload;
use Binaccle\Repositories\MassiveUploads\MassiveUploadRepositoryInterface;

class ViewMassiveUploadService
{
    public function __construct(
        private MassiveUploadRepositoryInterface $massiveUploadRepository
    ) {
    }

    public function view(string $massiveUploadId, string $type): MassiveUpload
    {
        return $this->massiveUploadRepository->view($massiveUploadId, $type);
    }
}
