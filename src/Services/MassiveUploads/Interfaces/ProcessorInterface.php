<?php

namespace Binaccle\Services\MassiveUploads\Interfaces;

interface ProcessorInterface
{
    public function process(array $recordToBeProcessed): void;
}
