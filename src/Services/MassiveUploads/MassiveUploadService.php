<?php

namespace Binaccle\Services\MassiveUploads;

use Binaccle\Events\MassiveUploads\MassiveUploadEvent;
use Binaccle\Models\Users\User;
use Binaccle\Payloads\MassiveUploads\MassiveUploadPayload;
use Binaccle\Repositories\Files\FileRepositoryInterface;
use Binaccle\Services\Utils\Storage\PathResolver;
use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Infrastructure\Storage\S3Storage;
use Throwable;

class MassiveUploadService
{
    public function __construct(
        private PathResolver $pathResolverService,
        private S3Storage $s3Storage,
        private FileRepositoryInterface $fileRepository,
        private CreateMassiveUploadService $createMassiveUploadService
    ) {
    }

    public function upload(MassiveUploadPayload $payload): void
    {
        $path = $this->pathResolverService->folder(PathResolver::COMPANIES_PATH, [
            User::COMPANY_ID => $payload->companyId(),
        ]);

        $relativePathFileUploaded = $this->s3Storage->putPrivate($path, $payload->uploadedFile());
        $fullUrl = $this->s3Storage->url($relativePathFileUploaded);

        $payload->setUrl($fullUrl);
        $payload->setRelativePath($relativePathFileUploaded);

        try {
            DB::beginTransaction();

            $fileCreated = $this->fileRepository->create($payload);
            $massiveUpload = $this->createMassiveUploadService->create($payload);

            event(new MassiveUploadEvent($fileCreated, $massiveUpload, App::getLocale()));

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();

            $this->s3Storage->delete($relativePathFileUploaded);

            throw new Exception($th->getMessage(), 0, $th);
        }
    }
}
