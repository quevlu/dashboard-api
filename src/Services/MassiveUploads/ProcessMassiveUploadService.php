<?php

namespace Binaccle\Services\MassiveUploads;

use Binaccle\Decorators\Equipments\MassiveUploadDecorator as MassiveUploadEquipmentDecorator;
use Binaccle\Decorators\Equipments\Types\MassiveUploadDecorator as MassiveUploadTypeDecorator;
use Binaccle\Decorators\Users\MassiveUploadDecorator as MassiveUploadUserDecorator;
use Binaccle\Enums\MassiveUploads\MassiveUploadStateEnum;
use Binaccle\Enums\MassiveUploads\MassiveUploadTypeEnum;
use Binaccle\Models\Files\File;
use Binaccle\Models\MassiveUploads\MassiveUpload;
use Binaccle\Models\MassiveUploads\MassiveUploadState;
use Binaccle\Repositories\MassiveUploads\MassiveUploadStateRepositoryInterface;
use Binaccle\Services\Equipments\MassiveUploads\MassiveUploadService as MassiveUploadEquipmentService;
use Binaccle\Services\Equipments\Types\MassiveUploads\MassiveUploadService as MassiveUploadTypeService;
use Binaccle\Services\Files\DeleteFileService;
use Binaccle\Services\Users\MassiveUploads\MassiveUploadService as MassiveUploadUserService;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Infrastructure\Storage\S3Storage;
use League\Csv\Reader;
use Throwable;

class ProcessMassiveUploadService
{
    private const DELIMITER_COLUMN = ',';

    private const KEY_GENERIC_ERROR = 'errors.csvs.process_error';

    private ?string $brokenLine = null;

    private File $csvFile;

    private object $decorator;

    private array $failures = [];

    private bool $isABrokenLine = false;

    private MassiveUpload $massiveUpload;

    private object $processor;

    private string $type;

    public function __construct(
        private MassiveUploadStateRepositoryInterface $massiveUploadStateRepository,
        private DeleteFileService $deleteFileService,
        private S3Storage $s3Storage,
    ) {
    }

    public function process(): void
    {
        $reader = Reader::createFromStream($this->stream());

        $position = 1;
        $headerSkipped = false;
        $bytes = 1024;
        $processed = 0;
        $errors = 0;

        foreach ($reader->chunk($bytes) as $chunk) {
            $chunk = (string) $chunk;

            $recordLines = explode(PHP_EOL, $chunk);

            $totalRecordsOfCurrentChunk = count($recordLines);

            foreach ($recordLines as $numberLine => $recordLine) {
                if (! $headerSkipped && ! $numberLine) {
                    $headerSkipped = true;
                    continue;
                }

                if ($this->brokenLine) {
                    $recordLine = $this->brokenLine . $recordLine;
                    $this->setIsABrokenLine();
                }

                if ($this->isLastRecord($totalRecordsOfCurrentChunk, $numberLine) && $this->isABrokenLine($recordLine)) {
                    $this->setIsABrokenLine();
                    $this->brokenLine = $recordLine;
                    continue;
                }

                $recordLineToArray = $this->splitInColumns($recordLine);

                $position++;

                try {
                    $recordDecorated = $this->decorator->decorate($recordLineToArray);

                    $this->processor->process($recordDecorated);

                    $processed++;
                } catch (ValidationException $validationException) {
                    $this->registerFailure($position, $validationException->validator->failed());

                    $errors++;
                } catch (Throwable $th) {
                    Log::error($th);

                    $this->registerFailure($position, self::KEY_GENERIC_ERROR);

                    $errors++;
                }
            }
        }

        $this->massiveUpload->register($processed, $errors, $this->failures, $this->processedResultStateId());

        $this->deleteFileService->delete($this->csvFile);
    }

    public function setCsvFile(File $csvFile): self
    {
        $this->csvFile = $csvFile;

        return $this;
    }

    public function setDecorator(object $decorator): void
    {
        $this->decorator = $decorator;
    }

    public function setMassiveUpload(MassiveUpload $massiveUpload): self
    {
        $this->massiveUpload = $massiveUpload;

        $massiveUploadTypeName = $massiveUpload->typeRel()->name();

        $this->config($massiveUploadTypeName);

        return $this;
    }

    public function setProcessor(object $processor): void
    {
        $this->processor = $processor;
    }

    private function config(string $type): self
    {
        $this->type = $type;

        list($decoratorClass, $processorClass) = [
            MassiveUploadTypeEnum::EQUIPMENTS => [
                MassiveUploadEquipmentDecorator::class,
                MassiveUploadEquipmentService::class,
            ],
            MassiveUploadTypeEnum::EQUIPMENT_TYPES => [
                MassiveUploadTypeDecorator::class,
                MassiveUploadTypeService::class,
            ],
            MassiveUploadTypeEnum::USERS => [
                MassiveUploadUserDecorator::class,
                MassiveUploadUserService::class,
            ],
        ][$type];

        $this->decorator = app($decoratorClass);
        $this->decorator->setAdditionalData([
            $this->massiveUpload::COMPANY_ID => $this->massiveUpload->companyId(),
        ]);

        $this->processor = app($processorClass);

        return $this;
    }

    private function isABrokenLine(string $record): bool
    {
        return substr_count($record, self::DELIMITER_COLUMN) != MassiveUploadTypeEnum::DELIMITERS[$this->type] || ! str_contains($record, "\r");
    }

    private function isLastRecord(int $totalRecords, int $numberLine): bool
    {
        return $totalRecords - 1 == $numberLine;
    }

    private function processedResultStateId(): string
    {
        return $this->massiveUploadStateRepository->firstOrFailBy(MassiveUploadState::NAME, MassiveUploadStateEnum::PROCESSED)->id();
    }

    private function registerFailure(int $numberLine, $messageOrMessages): void
    {
        $this->failures[$numberLine] = $messageOrMessages;
    }

    private function setIsABrokenLine(): void
    {
        $this->isABrokenLine = ! $this->isABrokenLine;

        if (! $this->isABrokenLine) {
            $this->brokenLine = null;
        }
    }

    private function splitInColumns(string $record): array
    {
        $record = str_replace("\r", '', $record);

        return explode(self::DELIMITER_COLUMN, $record);
    }

    private function stream()
    {
        return $this->s3Storage->stream($this->csvFile->relativePath());
    }
}
