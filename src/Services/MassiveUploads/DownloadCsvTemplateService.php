<?php

namespace Binaccle\Services\MassiveUploads;

use Binaccle\Services\Utils\Storage\PathResolver;
use Illuminate\Support\Facades\App;
use Infrastructure\Storage\S3Storage;

class DownloadCsvTemplateService
{
    public function __construct(
        private PathResolver $pathResolverService,
        private S3Storage $s3Storage
    ) {
    }

    public function download(string $type)
    {
        $fileName = trans("csvs.file_names.massive_uploads.{$type}");

        $path = $this->pathResolverService->folder($this->pathResolverService::MASSIVE_UPLOADS_PATH, [
            'lang' => App::getLocale(),
            'fileName' => $fileName,
        ]);

        return $this->s3Storage->download($path);
    }
}
