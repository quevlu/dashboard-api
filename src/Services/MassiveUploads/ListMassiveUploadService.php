<?php

namespace Binaccle\Services\MassiveUploads;

use Binaccle\Repositories\MassiveUploads\MassiveUploadRepositoryInterface;
use Infrastructure\Repositories\Criterias\MassiveUploads\Interfaces\ListMassiveUploadCriteriaInterface;

class ListMassiveUploadService
{
    public function __construct(
        private MassiveUploadRepositoryInterface $massiveUploadRepository
    ) {
    }

    public function list(ListMassiveUploadCriteriaInterface $criteria)
    {
        return $this->massiveUploadRepository->list($criteria);
    }
}
