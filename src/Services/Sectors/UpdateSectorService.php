<?php

namespace Binaccle\Services\Sectors;

use Binaccle\Models\Sectors\Sector;
use Binaccle\Payloads\Sectors\UpdateSectorPayload;

class UpdateSectorService
{
    public function update(Sector $sector, UpdateSectorPayload $payload): void
    {
        $sector->updateName($payload->name());
    }
}
