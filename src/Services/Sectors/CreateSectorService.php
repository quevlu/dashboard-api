<?php

namespace Binaccle\Services\Sectors;

use Binaccle\Payloads\Sectors\CreateSectorPayload;
use Binaccle\Repositories\Sectors\SectorRepositoryInterface;

class CreateSectorService
{
    public function __construct(
        private SectorRepositoryInterface $sectorRepository
    ) {
    }

    public function create(CreateSectorPayload $payload): void
    {
        $this->sectorRepository->create($payload);
    }
}
