<?php

namespace Binaccle\Services\Sectors;

use Binaccle\Repositories\Sectors\SectorRepositoryInterface;
use Infrastructure\Repositories\Criterias\Sectors\ListSectorCriteria;

class ListSectorService
{
    public function __construct(
        private SectorRepositoryInterface $sectorRepository
    ) {
    }

    public function list(ListSectorCriteria $criteria)
    {
        return $this->sectorRepository->list($criteria);
    }
}
