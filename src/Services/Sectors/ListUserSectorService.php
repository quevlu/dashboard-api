<?php

namespace Binaccle\Services\Sectors;

use Binaccle\Repositories\Users\UserRepositoryInterface;
use Infrastructure\Repositories\Criterias\Sectors\ListUserSectorCriteria;

class ListUserSectorService
{
    public function __construct(
        private UserRepositoryInterface $userRepository
    ) {
    }

    public function list(ListUserSectorCriteria $criteria)
    {
        return $this->userRepository->listBySector($criteria);
    }
}
