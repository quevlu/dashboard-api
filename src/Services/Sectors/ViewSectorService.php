<?php

namespace Binaccle\Services\Sectors;

use Binaccle\Models\Sectors\Sector;
use Binaccle\Repositories\Sectors\SectorRepositoryInterface;

class ViewSectorService
{
    public function __construct(
        private SectorRepositoryInterface $sectorRepository
    ) {
    }

    public function view(string $sectorId): Sector
    {
        return $this->sectorRepository->findOrFail($sectorId);
    }
}
