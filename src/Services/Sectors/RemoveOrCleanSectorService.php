<?php

namespace Binaccle\Services\Sectors;

use App\Events\Cache\InvalidateCacheEvent;
use App\Observers\Observer;
use Binaccle\Dtos\Users\UserSectorInvalidationDto;
use Binaccle\Payloads\Sectors\RemoveOrCleanSectorPayload;
use Binaccle\Repositories\Users\UserRepositoryInterface;

class RemoveOrCleanSectorService
{
    public function __construct(
        private UserRepositoryInterface $userRepository
    ) {
    }

    public function cleanOrRemove(RemoveOrCleanSectorPayload $payload): void
    {
        $this->userRepository->cleanOrRemoveSectors($payload->sectorId(), $payload->userIdOrAction());

        $fakeUser = new UserSectorInvalidationDto($payload->companyId());

        event(new InvalidateCacheEvent($fakeUser, Observer::UPDATED));
    }
}
