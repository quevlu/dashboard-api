<?php

namespace Binaccle\Services\Equipments;

use Binaccle\Payloads\Equipments\DeleteEquipmentPayload;
use Binaccle\Repositories\Equipments\Assignments\AssignmentRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Throwable;

class DeleteEquipmentService
{
    public function __construct(
        private AssignmentRepositoryInterface $equipmentAssignmentRepository
    ) {
    }

    public function delete(DeleteEquipmentPayload $payload): void
    {
        try {
            DB::beginTransaction();

            $equipment = $payload->equipment();

            $this->equipmentAssignmentRepository->unassign($equipment->id());

            $equipment->customDelete($payload);

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }
}
