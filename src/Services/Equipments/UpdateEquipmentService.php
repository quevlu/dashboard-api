<?php

namespace Binaccle\Services\Equipments;

use Binaccle\Payloads\Equipments\UpdateEquipmentPayload;

class UpdateEquipmentService
{
    public function update(UpdateEquipmentPayload $payload): void
    {
        $payload->equipment()->updateInfo($payload);
    }
}
