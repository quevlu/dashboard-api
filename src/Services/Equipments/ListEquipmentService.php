<?php

namespace Binaccle\Services\Equipments;

use Binaccle\Repositories\Equipments\EquipmentRepositoryInterface;
use Infrastructure\Repositories\Criterias\Equipments\ListEquipmentCriteria;

class ListEquipmentService
{
    public function __construct(
        private EquipmentRepositoryInterface $equipmentRepository
    ) {
    }

    public function list(ListEquipmentCriteria $criteria)
    {
        return $this->equipmentRepository->list($criteria);
    }
}
