<?php

namespace Binaccle\Services\Equipments;

use Binaccle\Enums\Equipments\PrintEquipmentEnum;
use Binaccle\Payloads\Equipments\MassivePrintEquipmentPayload;
use Binaccle\Services\Utils\Pdfs;

class MassivePrintEquipmentService
{
    public function print(MassivePrintEquipmentPayload $payload)
    {
        $equipments = $payload->equipments();

        $printerClass = [
            PrintEquipmentEnum::BARCODE => Pdfs\BarcodePdfCreatorService::class,
            PrintEquipmentEnum::QR => Pdfs\QrPdfCreatorService::class,
        ][$payload->type()];

        $printer = app($printerClass);
        $printer->setEquipments($equipments);

        if ($payload->type() == PrintEquipmentEnum::QR && $payload->logo()) {
            $printer->setLogo($payload->logo());
        }

        return $printer->create();
    }
}
