<?php

namespace Binaccle\Services\Equipments\MassiveUploads;

use App\Http\Requests\Equipments\CreateEquipmentRequest;
use Binaccle\Dtos\Equipments\EquipmentFromTypePayloadDto;
use Binaccle\Services\Equipments\CreateEquipmentService;
use Binaccle\Services\MassiveUploads\Interfaces\ProcessorInterface;
use Illuminate\Support\Facades\Validator;

class MassiveUploadService implements ProcessorInterface
{
    public function __construct(
        private CreateEquipmentRequest $request,
        private CreateEquipmentService $service
    ) {
    }

    public function process(array $recordToBeProcessed): void
    {
        $companyId = $recordToBeProcessed[CreateEquipmentRequest::COMPANY_ID];

        $this->request->setCompanyId($companyId);

        Validator::make($recordToBeProcessed, $this->request->rules())->validate();

        $payload = new EquipmentFromTypePayloadDto(
            $recordToBeProcessed[CreateEquipmentRequest::EQUIPMENT_TYPE_ID],
            $recordToBeProcessed[CreateEquipmentRequest::QUANTITY],
            $companyId,
        );

        $this->service->create($payload);
    }
}
