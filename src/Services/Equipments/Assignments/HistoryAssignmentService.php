<?php

namespace Binaccle\Services\Equipments\Assignments;

use Binaccle\Repositories\Equipments\Assignments\AssignmentRepositoryInterface;
use Infrastructure\Repositories\Criterias\Equipments\Assignments\HistoryAssignmentCriteria;

class HistoryAssignmentService
{
    public function __construct(
        private AssignmentRepositoryInterface $equipmentAssignmentRepository
    ) {
    }

    public function history(HistoryAssignmentCriteria $criteria)
    {
        return $this->equipmentAssignmentRepository->history($criteria);
    }
}
