<?php

namespace Binaccle\Services\Equipments\Assignments;

use Binaccle\Repositories\Equipments\Assignments\AssignmentRepositoryInterface;

class UnassignEquipmentService
{
    public function __construct(
        private AssignmentRepositoryInterface $equipmentAssignmentRepository
    ) {
    }

    public function unassign(object $payload): void
    {
        $this->equipmentAssignmentRepository->unassign($payload);
    }
}
