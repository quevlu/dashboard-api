<?php

namespace Binaccle\Services\Equipments\Assignments;

use Binaccle\Repositories\Equipments\Assignments\AssignmentRepositoryInterface;
use Infrastructure\Repositories\Criterias\Equipments\Assignments\ListAssignmentCriteria;

class ListAssignmentService
{
    public function __construct(
        private AssignmentRepositoryInterface $equipmentAssignmentRepository
    ) {
    }

    public function list(ListAssignmentCriteria $criteria)
    {
        return $this->equipmentAssignmentRepository->list($criteria);
    }
}
