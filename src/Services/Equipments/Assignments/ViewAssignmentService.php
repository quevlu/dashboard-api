<?php

namespace Binaccle\Services\Equipments\Assignments;

use Binaccle\Models\Equipments\Equipment;
use Binaccle\Repositories\Equipments\Assignments\AssignmentRepositoryInterface;

class ViewAssignmentService
{
    public function __construct(
        private AssignmentRepositoryInterface $equipmentAssignmentRepository
    ) {
    }

    public function view(string $equipmentId): ?Equipment
    {
        return $this->equipmentAssignmentRepository->view($equipmentId);
    }
}
