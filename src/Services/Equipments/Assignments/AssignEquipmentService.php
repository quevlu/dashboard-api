<?php

namespace Binaccle\Services\Equipments\Assignments;

use Binaccle\Payloads\Equipments\Assignments\AssignEquipmentPayload;
use Binaccle\Repositories\Equipments\Assignments\AssignmentRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Throwable;

class AssignEquipmentService
{
    public function __construct(
        private AssignmentRepositoryInterface $equipmentAssignmentRepository,
        private UnassignEquipmentService $unassignEquipmentService
    ) {
    }

    public function assign(AssignEquipmentPayload $payload): void
    {
        try {
            DB::beginTransaction();

            $this->unassignEquipmentService->unassign($payload);
            $this->equipmentAssignmentRepository->create($payload);

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();

            throw $th;
        }
    }
}
