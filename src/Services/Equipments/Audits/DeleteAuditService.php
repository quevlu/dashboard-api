<?php

namespace Binaccle\Services\Equipments\Audits;

use Binaccle\Payloads\Equipments\Audits\DeleteAuditPayload;

class DeleteAuditService
{
    public function delete(DeleteAuditPayload $payload): void
    {
        $payload->equipmentAudit()->forceDelete();
    }
}
