<?php

namespace Binaccle\Services\Equipments\Audits;

use Binaccle\Events\Equipments\Audits\Audited\CreateAuditedEvent;
use Binaccle\Payloads\Equipments\Audits\CreateAuditPayload;
use Binaccle\Repositories\Equipments\Audits\AuditRepositoryInterface;

class CreateAuditService
{
    public function __construct(
        private AuditRepositoryInterface $equipmentAuditRepository
    ) {
    }

    public function create(CreateAuditPayload $payload): void
    {
        $equipmentAudit = $this->equipmentAuditRepository->create($payload);

        if ($payload->sectorId()) {
            event(new CreateAuditedEvent($equipmentAudit));
        }
    }
}
