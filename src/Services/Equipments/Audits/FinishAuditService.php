<?php

namespace Binaccle\Services\Equipments\Audits;

use Binaccle\Enums\Equipments\Audits\AuditStateEnum;
use Binaccle\Payloads\Equipments\Audits\FinishAuditPayload;
use Binaccle\Repositories\Equipments\Audits\AuditRepositoryInterface;

class FinishAuditService
{
    public function __construct(
        private AuditRepositoryInterface $equipmentAuditRepository
    ) {
    }

    public function finish(FinishAuditPayload $payload): void
    {
        $this->equipmentAuditRepository->transition($payload->equipmentAudit()->id(), AuditStateEnum::FINISHED);
    }
}
