<?php

namespace Binaccle\Services\Equipments\Audits;

use Binaccle\Enums\Equipments\Audits\AuditStateEnum;
use Binaccle\Events\Equipments\Audits\Audited\CreateAuditedEvent;
use Binaccle\Payloads\Equipments\Audits\UpdateAuditPayload;
use Binaccle\Repositories\Equipments\Audits\Audited\AuditedRepositoryInterface;

class UpdateAuditService
{
    public function __construct(
        private AuditedRepositoryInterface $equipmentAuditAuditedRepository
    ) {
    }

    public function update(UpdateAuditPayload $payload): void
    {
        $equipmentAudit = $payload->equipmentAudit();
        $oldSectorId = $equipmentAudit->sectorId();

        $equipmentAudit->customUpdate($payload);

        if ($oldSectorId != $payload->sectorId() && $equipmentAudit->stateRel()->name() != AuditStateEnum::CREATING) {
            $this->equipmentAuditAuditedRepository->deleteByEquipmentAuditId($equipmentAudit->id());

            if ($payload->sectorId()) {
                event(new CreateAuditedEvent($equipmentAudit));
            }
        }
    }
}
