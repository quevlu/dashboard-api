<?php

namespace Binaccle\Services\Equipments\Audits\Audited;

use Binaccle\Repositories\Equipments\Audits\Audited\AuditedRepositoryInterface;
use Infrastructure\Repositories\Criterias\Equipments\Audits\Audited\ListAuditedCriteria;

class ListAuditedService
{
    public function __construct(
        private AuditedRepositoryInterface $equipmentAuditAuditedRepository
    ) {
    }

    public function list(ListAuditedCriteria $criteria)
    {
        return $this->equipmentAuditAuditedRepository->list($criteria);
    }
}
