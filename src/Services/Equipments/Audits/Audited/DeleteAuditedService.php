<?php

namespace Binaccle\Services\Equipments\Audits\Audited;

use App\Events\Cache\InvalidateCacheEvent;
use App\Observers\Observer;
use Binaccle\Dtos\Equipments\Audits\Audited\AuditedInvalidationDto;
use Binaccle\Models\Equipments\EquipmentAuditAudited;
use Binaccle\Payloads\Equipments\Audits\Audited\UpeleteAuditedPayload;
use Binaccle\Repositories\Equipments\Audits\Audited\AuditedRepositoryInterface;

class DeleteAuditedService
{
    public function __construct(
        private AuditedRepositoryInterface $equipmentAuditAuditedRepository
    ) {
    }

    public function delete(UpeleteAuditedPayload $payload): void
    {
        $equipmentIds = $payload->equipmentIds();

        $this->equipmentAuditAuditedRepository->baseWhereIn($equipmentIds, EquipmentAuditAudited::EQUIPMENT_ID)->delete();

        $equipmentAudit = $payload->equipmentAudit();

        $fakeEquipmentAuditAudited = new AuditedInvalidationDto($equipmentAudit->companyId(), $equipmentAudit->id());

        event(new InvalidateCacheEvent($fakeEquipmentAuditAudited, Observer::DELETED));
    }
}
