<?php

namespace Binaccle\Services\Equipments\Audits\Audited;

use Binaccle\Models\Equipments\EquipmentAudit;
use Binaccle\Repositories\Equipments\Audits\AuditRepositoryInterface;

class ViewAuditedService
{
    public function __construct(
        private AuditRepositoryInterface $equipmentAuditRepository
    ) {
    }

    public function view(string $equipmentAuditId): EquipmentAudit
    {
        return $this->equipmentAuditRepository->view($equipmentAuditId);
    }
}
