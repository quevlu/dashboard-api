<?php

namespace Binaccle\Services\Equipments\Audits\Audited;

use App\Events\Cache\InvalidateCacheEvent;
use App\Observers\Observer;
use Binaccle\Dtos\Equipments\Audits\Audited\AuditedInvalidationDto;
use Binaccle\Dtos\Equipments\Audits\Audited\AuditedPayloadDto;
use Binaccle\Enums\Equipments\Audits\AuditStateEnum;
use Binaccle\Models\Equipments\EquipmentAssignment;
use Binaccle\Models\Equipments\EquipmentAudit;
use Binaccle\Repositories\Equipments\Audits\Audited\AuditedRepositoryInterface;
use Binaccle\Repositories\Equipments\Audits\AuditRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class MassiveAuditedService
{
    public function __construct(
        private AuditedRepositoryInterface $equipmentAuditAuditedRepository,
        private AuditRepositoryInterface $equipmentAuditRepository
    ) {
    }

    public function add(EquipmentAudit $equipmentAudit, Collection $equipments, bool $isAudited = false, bool $withEvents = false): void
    {
        $equipmentsToInsert = [];

        if ($isAudited && $equipmentAudit->stateRel()->name() != AuditStateEnum::CREATING) {
            $this->equipmentAuditRepository->transition($equipmentAudit->id(), AuditStateEnum::CREATING);
        }

        foreach ($equipments as $equipment) {
            if ($equipment instanceof EquipmentAssignment) {
                $equipmentId = $equipment->equipmentId();
                $equipmentAssignmentId = $equipment->id();
            } else {
                $equipmentId = $equipment->id();
                $equipmentAssignmentId = callIf($equipment->assignedRel(), 'id');
            }

            $createEquipmentAuditAuditedPayloadDto = new AuditedPayloadDto(
                $equipmentAudit->id(),
                $equipmentId,
                $equipmentAssignmentId,
                $isAudited
            );

            $equipmentsToInsert[] = $this->equipmentAuditAuditedRepository->instance($createEquipmentAuditAuditedPayloadDto);
        }

        $this->equipmentAuditAuditedRepository->insert($equipmentsToInsert);

        if ($withEvents) {
            $fakeEquipmentAuditAudited = new AuditedInvalidationDto($equipmentAudit->companyId(), $equipmentAudit->id());

            event(new InvalidateCacheEvent($fakeEquipmentAuditAudited, Observer::CREATED));
        }
    }
}
