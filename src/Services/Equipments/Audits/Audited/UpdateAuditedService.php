<?php

namespace Binaccle\Services\Equipments\Audits\Audited;

use App\Events\Cache\InvalidateCacheEvent;
use App\Observers\Observer;
use Binaccle\Dtos\Equipments\Audits\Audited\AuditedInvalidationDto;
use Binaccle\Models\Equipments\EquipmentAuditAudited;
use Binaccle\Payloads\Equipments\Audits\Audited\UpeleteAuditedPayload;
use Binaccle\Repositories\Equipments\Audits\Audited\AuditedRepositoryInterface;

class UpdateAuditedService
{
    public function __construct(
        private AuditedRepositoryInterface $equipmentAuditAuditedRepository
    ) {
    }

    public function update(UpeleteAuditedPayload $payload): void
    {
        $equipmentIds = $payload->equipmentIds();

        $this->equipmentAuditAuditedRepository->baseWhereIn($equipmentIds, EquipmentAuditAudited::EQUIPMENT_ID)->update([
            EquipmentAuditAudited::AUDITED => true,
        ]);

        $equipmentAudit = $payload->equipmentAudit();

        $fakeEquipmentAuditAudited = new AuditedInvalidationDto($equipmentAudit->companyId(), $equipmentAudit->id());

        event(new InvalidateCacheEvent($fakeEquipmentAuditAudited, Observer::UPDATED));
    }
}
