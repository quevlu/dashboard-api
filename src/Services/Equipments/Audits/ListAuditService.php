<?php

namespace Binaccle\Services\Equipments\Audits;

use Binaccle\Repositories\Equipments\Audits\AuditRepositoryInterface;
use Infrastructure\Repositories\Criterias\Equipments\Audits\ListAuditCriteria;

class ListAuditService
{
    public function __construct(
        private AuditRepositoryInterface $equipmentAuditRepository
    ) {
    }

    public function list(ListAuditCriteria $criteria)
    {
        return $this->equipmentAuditRepository->list($criteria);
    }
}
