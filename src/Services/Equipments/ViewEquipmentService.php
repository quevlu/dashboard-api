<?php

namespace Binaccle\Services\Equipments;

use Binaccle\Models\Equipments\Equipment;
use Binaccle\Models\Equipments\EquipmentAssignment;
use Binaccle\Repositories\Equipments\EquipmentRepositoryInterface;

class ViewEquipmentService
{
    public function __construct(
        private EquipmentRepositoryInterface $equipmentRepository
    ) {
    }

    public function view(string $equipmentId): Equipment
    {
        $equipment = $this->equipmentRepository->findOrFail($equipmentId);
        $equipment->loadMissing(
            Equipment::EQUIPMENT_TYPE_RELATIONSHIP,
            Equipment::EQUIPMENT_ASSIGNED_RELATIONSHIP . '.' . EquipmentAssignment::USER_RELATIONSHIP,
            Equipment::EQUIPMENT_ASSIGNED_RELATIONSHIP . '.' . EquipmentAssignment::SECTOR_RELATIONSHIP
        );

        return $equipment;
    }
}
