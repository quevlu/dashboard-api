<?php

namespace Binaccle\Services\Equipments\Types;

class UpdateTypeService
{
    public function update(object $payload): void
    {
        $payload->equipmentType()->updateInfo($payload);
    }
}
