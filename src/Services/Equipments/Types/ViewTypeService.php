<?php

namespace Binaccle\Services\Equipments\Types;

use Binaccle\Models\Equipments\EquipmentType;
use Binaccle\Repositories\Equipments\Types\TypeRepositoryInterface;

class ViewTypeService
{
    public function __construct(
        private TypeRepositoryInterface $equipmentTypeRepository
    ) {
    }

    public function view(string $equipmentTypeId): EquipmentType
    {
        return $this->equipmentTypeRepository->findOrFail($equipmentTypeId);
    }
}
