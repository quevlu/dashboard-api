<?php

namespace Binaccle\Services\Equipments\Types\MassiveUploads;

use App\Http\Requests\Equipments\Types\CreateTypeRequest;
use Binaccle\Dtos\Equipments\Types\TypePayloadDto;
use Binaccle\Services\Equipments\Types\CreateTypeService;
use Binaccle\Services\MassiveUploads\Interfaces\ProcessorInterface;
use Illuminate\Support\Facades\Validator;

class MassiveUploadService implements ProcessorInterface
{
    public function __construct(
        private CreateTypeRequest $request,
        private CreateTypeService $service
    ) {
    }

    public function process(array $recordToBeProcessed): void
    {
        $companyId = $recordToBeProcessed[CreateTypeRequest::COMPANY_ID];

        $this->request->setCompanyId($companyId);

        Validator::make($recordToBeProcessed, $this->request->rules())->validate();

        $payload = new TypePayloadDto(
            $recordToBeProcessed[CreateTypeRequest::NAME],
            $recordToBeProcessed[CreateTypeRequest::BRAND],
            $recordToBeProcessed[CreateTypeRequest::MODEL],
            $recordToBeProcessed[CreateTypeRequest::PREFIX],
            $recordToBeProcessed[CreateTypeRequest::CHARACTERISTICS],
            $recordToBeProcessed[CreateTypeRequest::QUANTITY],
            $companyId
        );

        $this->service->create($payload);
    }
}
