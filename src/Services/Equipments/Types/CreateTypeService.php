<?php

namespace Binaccle\Services\Equipments\Types;

use Binaccle\Dtos\Equipments\EquipmentFromTypePayloadDto;
use Binaccle\Payloads\Equipments\Types\CreateTypePayload;
use Binaccle\Repositories\Equipments\Types\TypeRepositoryInterface;
use Binaccle\Services\Equipments\CreateEquipmentService;
use Illuminate\Support\Facades\DB;
use Throwable;

class CreateTypeService
{
    public function __construct(
        private TypeRepositoryInterface $equipmentTypeRepository,
        private CreateEquipmentService $createEquipmentService
    ) {
    }

    public function create(CreateTypePayload $payload): void
    {
        try {
            DB::beginTransaction();

            $equipmentType = $this->equipmentTypeRepository->create($payload);
            $quantity = $payload->quantity();

            if ($quantity) {
                $equipmentPayload = new EquipmentFromTypePayloadDto($equipmentType->id(), $equipmentType->companyId(), $quantity);
                $this->createEquipmentService->create($equipmentPayload);
            }

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();

            throw $th;
        }
    }
}
