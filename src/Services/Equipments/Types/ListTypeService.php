<?php

namespace Binaccle\Services\Equipments\Types;

use Binaccle\Repositories\Equipments\Types\TypeRepositoryInterface;
use Infrastructure\Repositories\Criterias\Equipments\Types\ListTypeCriteria;

class ListTypeService
{
    public function __construct(
        private TypeRepositoryInterface $equipmentTypeRepository
    ) {
    }

    public function list(ListTypeCriteria $criteria)
    {
        return $this->equipmentTypeRepository->list($criteria);
    }
}
