<?php

namespace Binaccle\Services\Equipments\Types;

use Binaccle\Payloads\Equipments\Types\DeleteTypePayload;

class DeleteTypeService
{
    public function delete(DeleteTypePayload $payload): void
    {
        $payload->equipmentType()->delete();
    }
}
