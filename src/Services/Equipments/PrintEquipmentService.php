<?php

namespace Binaccle\Services\Equipments;

use Binaccle\Enums\Equipments\PrintEquipmentEnum;
use Binaccle\Payloads\Equipments\PrintEquipmentPayload;
use Binaccle\Services\Utils\Tags;

class PrintEquipmentService
{
    public function print(PrintEquipmentPayload $payload)
    {
        $equipment = $payload->equipment();

        $printerClass = [
            PrintEquipmentEnum::BARCODE => Tags\BarcodeCreatorService::class,
            PrintEquipmentEnum::QR => Tags\QrCreatorService::class,
        ][$payload->type()];

        $printer = app($printerClass);
        $printer->setEquipment($equipment);

        if ($payload->type() == PrintEquipmentEnum::QR && $payload->logo()) {
            $printer->setLogo($payload->logo());
        }

        return $printer->print();
    }
}
