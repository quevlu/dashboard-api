<?php

namespace Binaccle\Services\Equipments;

use App\Events\Cache\InvalidateCacheEvent;
use App\Observers\Observer;
use Binaccle\Dtos\Equipments\EquipmentPayloadDto;
use Binaccle\Payloads\Equipments\CreateEquipmentPayload;
use Binaccle\Repositories\Equipments\EquipmentRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Throwable;

class CreateEquipmentService
{
    public function __construct(
        private EquipmentRepositoryInterface $equipmentRepository
    ) {
    }

    public function create(CreateEquipmentPayload $payload): void
    {
        try {
            DB::beginTransaction();

            $equipments = [];

            $nextIdentifier = $this->equipmentRepository->nextIdentifier();

            for ($i = 0; $i < $payload->quantity(); $i++) {
                $createEquipmentPayloadDto = new EquipmentPayloadDto(
                    $nextIdentifier,
                    $payload->equipmentTypeId(),
                    $payload->companyId()
                );

                $equipments[] = $this->equipmentRepository->instance($createEquipmentPayloadDto);

                $nextIdentifier++;
            }

            $this->equipmentRepository->insert($equipments);

            event(new InvalidateCacheEvent($equipments[0], Observer::CREATED));

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();

            throw $th;
        }
    }
}
