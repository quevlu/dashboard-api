<?php

namespace Binaccle\Services\Roles;

use Binaccle\Repositories\Roles\RoleRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class ListRoleService
{
    public function __construct(
        private RoleRepositoryInterface $roleRepository
    ) {
    }

    public function list(): Collection
    {
        return $this->roleRepository->allWithoutAdmin();
    }
}
