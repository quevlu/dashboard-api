<?php

namespace Binaccle\Services\Companies;

use Binaccle\Models\Companies\Company;
use Binaccle\Repositories\Companies\CompanyRepositoryInterface;

class CreateCompanyService
{
    public function __construct(
        private CompanyRepositoryInterface $companyRepository
    ) {
    }

    public function create(object $payload): Company
    {
        return $this->companyRepository->create($payload);
    }
}
