<?php

namespace Binaccle\Services\Companies;

use Binaccle\Models\Users\User;
use Binaccle\Payloads\Companies\UpdateCompanyPayload;
use Binaccle\Repositories\Files\FileRepositoryInterface;
use Binaccle\Services\Files\DeleteFileService;
use Binaccle\Services\Utils\Storage\PathResolver;
use Exception;
use Illuminate\Support\Facades\DB;
use Infrastructure\Storage\S3Storage;
use Throwable;

class UpdateCompanyService
{
    public function __construct(
        private PathResolver $pathResolverService,
        private S3Storage $s3Storage,
        private FileRepositoryInterface $fileRepository,
        private DeleteFileService $deleteFileService
    ) {
    }

    public function update(UpdateCompanyPayload $payload): void
    {
        $company = $payload->company();
        $currentLogo = $company->logoRel();
        $logoUploaded = $payload->logo();
        $logoCreated = null;

        if ($logoUploaded) {
            $path = $this->pathResolverService->folder(PathResolver::COMPANIES_PATH, [
                User::COMPANY_ID => $company->id(),
            ]);

            $relativePathFileUploaded = $this->s3Storage->put($path, $logoUploaded);
            $fullUrl = $this->s3Storage->url($relativePathFileUploaded);

            $payload->setUrl($fullUrl);
            $payload->setRelativePath($relativePathFileUploaded);
        }

        try {
            DB::beginTransaction();

            if ($logoUploaded) {
                $logoCreated = $this->fileRepository->create($payload);
                $payload->setLogoCreated($logoCreated);
            }

            $company->updateInfo($payload);

            if ($logoUploaded && $currentLogo) {
                $this->deleteFileService->delete($currentLogo);
                $currentLogo->delete();
            }

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();

            if ($logoCreated) {
                $this->deleteFileService->delete($logoCreated);
            }

            throw new Exception($th->getMessage(), 0, $th);
        }
    }
}
