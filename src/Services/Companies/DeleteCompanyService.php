<?php

namespace Binaccle\Services\Companies;

use Binaccle\Events\Companies\DeleteCompanyEvent;
use Binaccle\Payloads\Companies\DeleteCompanyPayload;
use Binaccle\Services\Auth\LogoutService;

class DeleteCompanyService
{
    public function __construct(
        private LogoutService $logoutService
    ) {
    }

    public function delete(DeleteCompanyPayload $payload): void
    {
        event(new DeleteCompanyEvent($payload->company()));

        $this->logoutService->logout();
    }
}
