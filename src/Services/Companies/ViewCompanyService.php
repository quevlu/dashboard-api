<?php

namespace Binaccle\Services\Companies;

use Binaccle\Models\Companies\Company;

class ViewCompanyService
{
    public function view(): Company
    {
        $company = auth()->user()->companyRel();
        $company->loadMissing(Company::LOGO_RELATIONSHIP);

        return $company;
    }
}
