<?php

namespace Binaccle\Services\Users;

use Binaccle\Repositories\Users\UserRepositoryInterface;
use Binaccle\Services\Users\Notifiables\UserNotifiableService;
use Illuminate\Support\Facades\DB;
use Throwable;

class CreateUserService
{
    public function __construct(
        private UserRepositoryInterface $userRepository,
        private UserNotifiableService $userNotifiableService
    ) {
    }

    public function create(object $payload): void
    {
        try {
            DB::beginTransaction();

            $user = $this->userRepository->create($payload);
            $payload->setCurrentUser($user);

            if (! $user->isUser()) {
                $this->userNotifiableService->send($payload);
            }

            DB::commit();
        } catch (Throwable $exception) {
            DB::rollback();

            throw $exception;
        }
    }
}
