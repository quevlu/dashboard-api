<?php

namespace Binaccle\Services\Users\Notifiables;

use Binaccle\Events\Users\Notifiables\UserNotifiableEvent;
use Binaccle\Payloads\Users\Notifiables\UserNotifiablePayload;
use Binaccle\Repositories\Users\Notifiables\UserNotifiableRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Throwable;

class UserNotifiableService
{
    public function __construct(
        private UserNotifiableRepositoryInterface $userNotifiableRepository
    ) {
    }

    public function send(UserNotifiablePayload $payload): void
    {
        try {
            DB::beginTransaction();

            $userNotifiable = $payload->modelToNotify();

            if ($userNotifiable && @$userNotifiable->left() === 0) {
                $userNotifiable->delete();
                $userNotifiable = null;
            }

            if (! $userNotifiable) {
                $userNotifiable = $this->userNotifiableRepository->create($payload);
            } else {
                $userNotifiable->decrease();
            }

            event(new UserNotifiableEvent(
                $payload->currentUser()->email(),
                $userNotifiable->type(),
                $userNotifiable->token()
            ));

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();

            throw $th;
        }
    }
}
