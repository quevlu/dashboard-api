<?php

namespace Binaccle\Services\Users;

use Binaccle\Repositories\Users\UserRepositoryInterface;
use Infrastructure\Repositories\Criterias\Users\ListUserCriteria;

class ListUserService
{
    public function __construct(
        private UserRepositoryInterface $userRepository
    ) {
    }

    public function list(ListUserCriteria $criteria)
    {
        return $this->userRepository->list($criteria);
    }
}
