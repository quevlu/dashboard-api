<?php

namespace Binaccle\Services\Users;

use Binaccle\Models\Users\User;

class DeleteUserService
{
    public function delete(User $user): void
    {
        $user->syncRoles([]);
        $user->delete();
    }
}
