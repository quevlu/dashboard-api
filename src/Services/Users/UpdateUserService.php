<?php

namespace Binaccle\Services\Users;

use Binaccle\Enums\Roles\RoleEnum;
use Binaccle\Models\Roles\Role;
use Binaccle\Services\Users\Notifiables\UserNotifiableService;
use Illuminate\Support\Facades\DB;
use Throwable;

class UpdateUserService
{
    public function __construct(
        private UserNotifiableService $userNotifiableService
    ) {
    }

    public function update(object $payload): void
    {
        try {
            DB::beginTransaction();

            $user = $payload->currentUser()->updateFromUserModule($payload);

            $isUser = $payload->roles()->where(Role::NAME, RoleEnum::USER)->count();

            if (! $isUser && ! $user->password() && ! $user->invitation()->exists()) {
                $this->userNotifiableService->send($payload);
            }

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();

            throw $th;
        }
    }
}
