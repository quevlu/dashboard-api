<?php

namespace Binaccle\Services\Users\Invitations;

use Binaccle\Payloads\Users\Notifiables\UserNotifiablePayload;
use Binaccle\Services\Users\Notifiables\UserNotifiableService;
use Carbon\Carbon;
use Illuminate\Http\Exceptions\ThrottleRequestsException;

class SendInvitationUserService extends UserNotifiableService
{
    public function send(UserNotifiablePayload $payload): void
    {
        $invitationNotifiable = $payload->modelToNotify();

        if ($invitationNotifiable) {
            $now = Carbon::now();
            $updatedAt = $invitationNotifiable->updatedAt()->addHour();

            if (! $invitationNotifiable->left() && ! $updatedAt->diffInHours($now)) {
                throw new ThrottleRequestsException();
            }

            parent::send($payload);
        }
    }
}
