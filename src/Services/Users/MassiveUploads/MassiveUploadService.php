<?php

namespace Binaccle\Services\Users\MassiveUploads;

use App\Http\Requests\Users\AbstractUpsertUserRequest;
use App\Http\Requests\Users\CreateUserRequest;
use Binaccle\Dtos\Users\UserDto;
use Binaccle\Services\MassiveUploads\Interfaces\ProcessorInterface;
use Binaccle\Services\Users\CreateUserService;
use Illuminate\Support\Facades\Validator;

class MassiveUploadService implements ProcessorInterface
{
    public function __construct(
        private CreateUserRequest $request,
        private CreateUserService $createUserService
    ) {
    }

    public function process(array $recordToBeProcessed): void
    {
        $companyId = $recordToBeProcessed[AbstractUpsertUserRequest::COMPANY_ID];

        $this->request->setCompanyId($companyId);

        Validator::make($recordToBeProcessed, $this->request->rules())->validate();

        $payload = new UserDto(
            $recordToBeProcessed[AbstractUpsertUserRequest::NAME],
            $recordToBeProcessed[AbstractUpsertUserRequest::EMAIL],
            $recordToBeProcessed[AbstractUpsertUserRequest::SECTOR_ID],
            $recordToBeProcessed[AbstractUpsertUserRequest::IDENTITY_DOCUMENT],
            $recordToBeProcessed[AbstractUpsertUserRequest::ROLES],
            $companyId,
        );

        $this->createUserService->create($payload);
    }
}
