<?php

namespace Binaccle\Services\Users;

use Binaccle\Models\Users\User;
use Binaccle\Repositories\Users\UserRepositoryInterface;

class ViewUserService
{
    public function __construct(
        private UserRepositoryInterface $userRepository
    ) {
    }

    public function view(string $userId): User
    {
        return $this->userRepository->findOrFail($userId)->loadMissing(User::SECTOR_RELATIONSHIP);
    }
}
