<?php

namespace Binaccle\Services\Utils\Pdfs;

use Binaccle\Enums\Equipments\PrintEquipmentEnum;

class BarcodePdfCreatorService extends AbstractPrinter
{
    protected const TYPE = PrintEquipmentEnum::BARCODE;

    public function process(): void
    {
        foreach ($this->equipments as $equipment) {
            $this->pdf->Cell(0, 0, $equipment->fullIdentifier(), 0, 1, 'C');
            $this->pdf->write1DBarcode($equipment->id(), 'C128', '', '', '', 18, 0.4, $this->styles, 'N');
            $this->pdf->Ln();
        }
    }
}
