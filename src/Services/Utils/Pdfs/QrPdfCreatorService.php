<?php

namespace Binaccle\Services\Utils\Pdfs;

use Binaccle\Enums\Equipments\PrintEquipmentEnum;
use Binaccle\Models\Files\File;
use Infrastructure\Storage\S3Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class QrPdfCreatorService extends AbstractPrinter
{
    protected const TYPE = PrintEquipmentEnum::QR;

    private const NEXT_ROW_QR_POSITION_AT_Y = 40;

    private const NEXT_ROW_TEXT_POSITION_AT_Y = 40;

    private const QR_POSITIONS = [
        1 => 'TL',
        2 => 'TC',
        3 => 'TR',
    ];

    private const START_ACCUMULATOR_VALUE = 0;

    private const START_QR_POSITION = 1;

    private const START_QR_POSITION_AT_X = 15;

    private const START_QR_POSITION_AT_Y = 35;

    private const START_TEXT_POSITION_AT_Y = 30;

    private ?File $logo = null;

    public function __construct(
        private S3Storage $s3Storage
    ) {
        parent::__construct();
    }

    public function process(): void
    {
        $contentImage = null;

        if ($this->logo) {
            $contentImage = $this->s3Storage->get($this->logo->relativePath());
        }

        $accumulatorQrAtRow = self::START_QR_POSITION;
        $qrPositionAtX = self::START_QR_POSITION_AT_X;
        $qrPositionAtY = self::START_QR_POSITION_AT_Y;
        $textPositionAtY = self::START_TEXT_POSITION_AT_Y;
        $totalPerDocument = self::START_ACCUMULATOR_VALUE;

        foreach ($this->equipments as $equipment) {
            $qr = QrCode::format('png')
                ->errorCorrection('H')
                ->size(250)
                ->style('round')
                ->eye('square');

            if ($contentImage) {
                $qr->mergeString($contentImage, .5);
            }

            $qrImage = $qr->generate($equipment->id());

            $qrPosition = self::QR_POSITIONS[$accumulatorQrAtRow];

            $this->pdf->MultiCell(30, 5, $equipment->fullIdentifier(), 0, 'C', false, 0, $qrPositionAtX, $textPositionAtY, true, 0, false, true, 40, 'T');
            $this->pdf->Image('@' . $qrImage, $qrPositionAtX, $qrPositionAtY, 30, 30, 'PNG', '', '', false, 300, '', false, false, 1, $qrPosition, false, false);

            if ($accumulatorQrAtRow == 3) {
                $this->resetRow($accumulatorQrAtRow, $qrPositionAtX, $qrPositionAtY, $textPositionAtY);
            } else {
                $accumulatorQrAtRow++;
                $this->positionateNextQr($qrPositionAtX);
            }

            if ($totalPerDocument == 17) {
                $this->resetPage($accumulatorQrAtRow, $qrPositionAtX, $qrPositionAtY, $textPositionAtY, $totalPerDocument);
            } else {
                $totalPerDocument++;
            }
        }
    }

    public function setLogo(File $logo): void
    {
        $this->logo = $logo;
    }

    private function positionateNextQr(int &$qrPositionAtX): void
    {
        $qrPositionAtX += 75;
    }

    private function resetPage(int &$accumulatorQrAtRow, int &$qrPositionAtX, int &$qrPositionAtY, int &$textPositionAtY, int &$totalPerDocument): void
    {
        $accumulatorQrAtRow = self::START_QR_POSITION;
        $qrPositionAtX = self::START_QR_POSITION_AT_X;
        $qrPositionAtY = self::START_QR_POSITION_AT_Y;
        $textPositionAtY = self::START_TEXT_POSITION_AT_Y;
        $totalPerDocument = self::START_ACCUMULATOR_VALUE;

        $this->pdf->AddPage();
    }

    private function resetRow(int &$accumulatorQrAtRow, int &$qrPositionAtX, int &$qrPositionAtY, int &$textPositionAtY): void
    {
        $accumulatorQrAtRow = self::START_QR_POSITION;
        $qrPositionAtX = self::START_QR_POSITION_AT_X;
        $qrPositionAtY += self::NEXT_ROW_QR_POSITION_AT_Y;
        $textPositionAtY += self::NEXT_ROW_TEXT_POSITION_AT_Y;
    }
}
