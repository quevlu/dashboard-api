<?php

namespace Binaccle\Services\Utils\Pdfs;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Carbon;
use TCPDF;

abstract class AbstractPrinter
{
    protected const PDF_CREATOR = 'pdf';

    protected Collection $equipments;

    protected TCPDF $pdf;

    protected array $styles = [
        'position' => 'C',
        'align' => 'C',
        'stretch' => false,
        'fitwidth' => true,
        'cellfitalign' => '',
        'border' => false,
        'hpadding' => 'auto',
        'vpadding' => 'auto',
        'fgcolor' => [0, 0, 0],
        'bgcolor' => false,
        'text' => true,
        'font' => 'helvetica',
        'fontsize' => 8,
        'stretchtext' => 1,
    ];

    public function __construct()
    {
        $this->createPdf();
    }

    public function create()
    {
        $this->process();

        return $this->pdf->Output($this->fileName(), 'I');
    }

    public function setEquipments(Collection $equipments): void
    {
        $this->equipments = $equipments;
    }

    protected function fileName(): string
    {
        $now = Carbon::now()->getTimestamp();

        return $this->type() . '_' . $now . '.pdf';
    }

    abstract protected function process(): void;

    private function author(): string
    {
        return config('app.name');
    }

    private function createPdf(): void
    {
        $this->pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $this->pdf->SetCreator($this->author());
        $this->pdf->SetAuthor($this->author());
        $this->pdf->SetTitle($this->title());
        $this->pdf->SetSubject($this->subject());

        // set default header data
        $this->pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 009', PDF_HEADER_STRING);

        // set header and footer fonts
        $this->pdf->setHeaderFont([PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN]);
        $this->pdf->setFooterFont([PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA]);

        $this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        $this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $this->pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        $this->pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
        $this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $this->pdf->SetFont('helvetica', '', 10);
        $this->pdf->AddPage();
    }

    private function subject(): string
    {
        return trans('tags.title') . $this->type();
    }

    private function title(): string
    {
        return $this->subject() . ' - ' . $this->author();
    }

    private function type(): string
    {
        return trans('tags.types.' . static::TYPE);
    }
}
