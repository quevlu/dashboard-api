<?php

namespace Binaccle\Services\Utils\Storage;

class PathResolver
{
    public const COMPANIES_PATH = 'companies';

    public const MASSIVE_UPLOADS_PATH = 'massive-uploads';

    private const PATHS = [
        self::COMPANIES_PATH => 'companies/{company_id}',
        self::MASSIVE_UPLOADS_PATH => 'massive-uploads/{lang}/{fileName}',
    ];

    public function folder(string $path, array $args = []): string
    {
        $args = $this->replaceKeys($args);

        return strtr(self::PATHS[$path], $args);
    }

    private function replaceKeys(array $args): array
    {
        foreach ($args as $key => $value) {
            $args["{{$key}}"] = $value;
        }

        return $args;
    }
}
