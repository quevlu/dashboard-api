<?php

namespace Binaccle\Services\Utils\Tags;

use Milon\Barcode\Facades\DNS1DFacade;

class BarcodeCreatorService extends AbstractPrinter
{
    public function print()
    {
        return base64_decode(DNS1DFacade::getBarcodePNG($this->equipment->id(), 'C128', 1, 40, [0, 0, 0], true));
    }
}
