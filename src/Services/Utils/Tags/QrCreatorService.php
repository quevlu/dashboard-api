<?php

namespace Binaccle\Services\Utils\Tags;

use Binaccle\Models\Files\File;
use Infrastructure\Storage\S3Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class QrCreatorService extends AbstractPrinter
{
    private ?File $logo = null;

    public function __construct(
        private S3Storage $s3Storage
    ) {
    }

    public function print()
    {
        $contentImage = null;

        if ($this->logo) {
            $contentImage = $this->s3Storage->get($this->logo->relativePath());
        }

        $qr = QrCode::format('png')
                ->errorCorrection('H')
                ->size(250)
                ->style('round')
                ->eye('square');

        if ($contentImage) {
            $qr->mergeString($contentImage, .5);
        }

        return $qr->generate($this->equipment->id());
    }

    public function setLogo(File $logo): void
    {
        $this->logo = $logo;
    }
}
