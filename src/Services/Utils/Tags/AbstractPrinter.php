<?php

namespace Binaccle\Services\Utils\Tags;

use Binaccle\Models\Equipments\Equipment;

abstract class AbstractPrinter
{
    protected Equipment $equipment;

    abstract public function print();

    public function setEquipment(Equipment $equipment): void
    {
        $this->equipment = $equipment;
    }
}
