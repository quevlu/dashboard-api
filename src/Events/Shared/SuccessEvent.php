<?php

namespace Binaccle\Events\Shared;

use Illuminate\Foundation\Events\Dispatchable;

class SuccessEvent
{
    use Dispatchable;
}
