<?php

namespace Binaccle\Events\Users\Notifiables;

use Illuminate\Foundation\Events\Dispatchable;

class UserNotifiableEvent
{
    use Dispatchable;

    public function __construct(
        private string $email,
        private string $type,
        private string $token
    ) {
    }

    public function email(): string
    {
        return $this->email;
    }

    public function token(): string
    {
        return $this->token;
    }

    public function type(): string
    {
        return $this->type;
    }
}
