<?php

namespace Binaccle\Events\MassiveUploads;

use Binaccle\Models\Files\File;
use Binaccle\Models\MassiveUploads\MassiveUpload;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class MassiveUploadEvent
{
    use Dispatchable;
    use SerializesModels;

    public function __construct(
        private File $file,
        private MassiveUpload $massiveUpload,
        private string $locale
    ) {
    }

    public function file(): File
    {
        return $this->file;
    }

    public function locale(): string
    {
        return $this->locale;
    }

    public function massiveUpload(): MassiveUpload
    {
        return $this->massiveUpload;
    }
}
