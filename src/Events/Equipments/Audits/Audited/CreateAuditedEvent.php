<?php

namespace Binaccle\Events\Equipments\Audits\Audited;

use Binaccle\Models\Equipments\EquipmentAudit;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CreateAuditedEvent
{
    use Dispatchable;
    use SerializesModels;

    public function __construct(
        private EquipmentAudit $equipmentAudit
    ) {
    }

    public function equipmentAudit(): EquipmentAudit
    {
        return $this->equipmentAudit;
    }
}
