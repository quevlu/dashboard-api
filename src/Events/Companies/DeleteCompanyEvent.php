<?php

namespace Binaccle\Events\Companies;

use Binaccle\Models\Companies\Company;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class DeleteCompanyEvent
{
    use Dispatchable;
    use SerializesModels;

    public function __construct(
        private Company $company
    ) {
    }

    public function company(): Company
    {
        return $this->company;
    }
}
