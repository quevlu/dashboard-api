<?php

namespace Binaccle\Rules\MassiveUploads;

use Illuminate\Contracts\Validation\Rule;
use League\Csv\Reader;

class CsvHeaderRule implements Rule
{
    public function __construct(
        private array $headerCsv
    ) {
    }

    public function message(): string
    {
        return trans('validation.invalid_csv_header');
    }

    public function passes($attribute, $file): bool
    {
        $csv = Reader::createFromPath($file);
        $csv->setHeaderOffset(0);

        return $csv->getHeader() == $this->headerCsv;
    }
}
