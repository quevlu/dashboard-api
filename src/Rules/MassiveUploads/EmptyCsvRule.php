<?php

namespace Binaccle\Rules\MassiveUploads;

use Illuminate\Contracts\Validation\Rule;
use League\Csv\Reader;

class EmptyCsvRule implements Rule
{
    public function message(): string
    {
        return trans('validation.empty_csv');
    }

    public function passes($attribute, $file): bool
    {
        $reader = Reader::createFromPath($file);
        $reader->setHeaderOffset(0);
        $reader->skipEmptyRecords();

        return (bool) $reader->count();
    }
}
