<?php

namespace Binaccle\Rules\Users;

use Binaccle\Enums\Roles\RoleEnum;
use Binaccle\Models\Roles\Role;
use Binaccle\Repositories\Roles\RoleRepositoryInterface;
use Illuminate\Contracts\Validation\Rule;

class OnlyUserRoleRule implements Rule
{
    private RoleRepositoryInterface $roleRepository;

    public function __construct()
    {
        $this->roleRepository = app(RoleRepositoryInterface::class);
    }

    public function message(): string
    {
        return trans('validation.user_has_user_role_and_others');
    }

    public function passes($attribute, $roles): bool
    {
        $roleObjects = $this->roleRepository->whereIn($roles);

        if ($roleObjects->count()) {
            $userRoleExists = $roleObjects->where(Role::NAME, RoleEnum::USER)->count();
            $quantity = $roleObjects->count();

            return (! $userRoleExists && $quantity) || ($userRoleExists && $quantity == 1);
        }

        return true;
    }
}
