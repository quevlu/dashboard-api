<?php

namespace Binaccle\Rules\Users;

use Binaccle\Models\Users\User;
use Binaccle\Repositories\Users\UserRepositoryInterface;
use Illuminate\Contracts\Validation\Rule;

class IsVerifiedRule implements Rule
{
    private UserRepositoryInterface $userRepository;

    public function __construct()
    {
        $this->userRepository = app(UserRepositoryInterface::class);
    }

    public function message(): string
    {
        return trans('validation.user_has_to_be_verified');
    }

    public function passes($attribute, $email): bool
    {
        return ! $this->userRepository->firstOrFailBy(User::EMAIL, $email)->verification()->exists();
    }
}
