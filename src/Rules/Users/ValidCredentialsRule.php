<?php

namespace Binaccle\Rules\Users;

use App\Http\Requests\Auth\LoginRequest;
use Binaccle\Models\Users\User;
use Binaccle\Repositories\Users\UserRepositoryInterface;
use Binaccle\Rules\AbstractRule;

class ValidCredentialsRule extends AbstractRule
{
    private UserRepositoryInterface $userRepository;

    public function __construct()
    {
        $this->userRepository = app(UserRepositoryInterface::class);
    }

    public function message(): string
    {
        return trans('validation.credentials_are_invalid');
    }

    public function passes($attribute, $email): bool
    {
        $user = $this->userRepository->firstBy(User::EMAIL, $email);
        $result = false;

        if ($user && $user->password()) {
            $result = password($this->data[LoginRequest::PASSWORD], $user->password());
        }

        return $result;
    }
}
