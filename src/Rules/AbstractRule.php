<?php

namespace Binaccle\Rules;

use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\Rule;

abstract class AbstractRule implements DataAwareRule, Rule
{
    protected array $data = [];

    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }
}
