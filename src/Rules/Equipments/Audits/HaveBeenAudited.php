<?php

namespace Binaccle\Rules\Equipments\Audits;

use Binaccle\Repositories\Equipments\Audits\Audited\AuditedRepositoryInterface;
use Illuminate\Contracts\Validation\Rule;

class HaveBeenAudited implements Rule
{
    private AuditedRepositoryInterface $equipmentAuditAuditedRepository;

    public function __construct(
        private string $equipmentAuditId
    ) {
        $this->equipmentAuditAuditedRepository = app(AuditedRepositoryInterface::class);
    }

    public function message(): string
    {
        return trans('validation.almost_one_has_been_audited');
    }

    public function passes($attribute, $equipmentIds): bool
    {
        return ! $this->equipmentAuditAuditedRepository->anyHaveBeenAudited($this->equipmentAuditId, $equipmentIds);
    }
}
