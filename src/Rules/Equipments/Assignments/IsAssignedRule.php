<?php

namespace Binaccle\Rules\Equipments\Assignments;

use Binaccle\Repositories\Equipments\Assignments\AssignmentRepositoryInterface;
use Illuminate\Contracts\Validation\Rule;

class IsAssignedRule implements Rule
{
    private AssignmentRepositoryInterface $equipmentAssignmentRepository;

    public function __construct()
    {
        $this->equipmentAssignmentRepository = app(AssignmentRepositoryInterface::class);
    }

    public function message(): string
    {
        return trans('validation.equipment_is_assigned');
    }

    public function passes($attribute, $equipmentId): bool
    {
        return ! $this->equipmentAssignmentRepository->equipmentIsAssigned($equipmentId);
    }
}
