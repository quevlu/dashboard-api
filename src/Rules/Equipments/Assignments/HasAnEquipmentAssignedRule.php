<?php

namespace Binaccle\Rules\Equipments\Assignments;

use Binaccle\Repositories\Equipments\Assignments\AssignmentRepositoryInterface;
use Illuminate\Contracts\Validation\Rule;

class HasAnEquipmentAssignedRule implements Rule
{
    private AssignmentRepositoryInterface $equipmentAssignmentRepository;

    public function __construct()
    {
        $this->equipmentAssignmentRepository = app(AssignmentRepositoryInterface::class);
    }

    public function message(): string
    {
        return trans('validation.equipment_is_assigned_to');
    }

    public function passes($attribute, $actorId): bool
    {
        return ! $this->equipmentAssignmentRepository->hasAnEquipmentAssigned($actorId);
    }
}
