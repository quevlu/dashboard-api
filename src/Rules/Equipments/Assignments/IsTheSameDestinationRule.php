<?php

namespace Binaccle\Rules\Equipments\Assignments;

use App\Http\Requests\Equipments\Assignments\AssignEquipmentRequest;
use Binaccle\Repositories\Equipments\Assignments\AssignmentRepositoryInterface;
use Binaccle\Rules\AbstractRule;
use Illuminate\Contracts\Validation\Rule;

class IsTheSameDestinationRule extends AbstractRule implements Rule
{
    private AssignmentRepositoryInterface $equipmentAssignmentRepository;

    public function __construct()
    {
        $this->equipmentAssignmentRepository = app(AssignmentRepositoryInterface::class);
    }

    public function message(): string
    {
        return trans('validation.is_the_same_assignment_destination');
    }

    public function passes($attribute, $equipmentId): bool
    {
        return ! $this->equipmentAssignmentRepository->isTheSameDestination(
            $equipmentId,
            @$this->data[AssignEquipmentRequest::USER_ID],
            @$this->data[AssignEquipmentRequest::SECTOR_ID]
        );
    }
}
