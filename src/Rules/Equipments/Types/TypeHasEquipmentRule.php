<?php

namespace Binaccle\Rules\Equipments\Types;

use Binaccle\Models\Equipments\Equipment;
use Binaccle\Repositories\Equipments\EquipmentRepositoryInterface;
use Illuminate\Contracts\Validation\Rule;

class TypeHasEquipmentRule implements Rule
{
    private EquipmentRepositoryInterface $equipmentRepository;

    public function __construct()
    {
        $this->equipmentRepository = app(EquipmentRepositoryInterface::class);
    }

    public function message(): string
    {
        return trans('validation.equipment_has_the_type_specified');
    }

    public function passes($attribute, $equipmentTypeId): bool
    {
        return ! $this->equipmentRepository->exists(Equipment::EQUIPMENT_TYPE_ID, $equipmentTypeId);
    }
}
