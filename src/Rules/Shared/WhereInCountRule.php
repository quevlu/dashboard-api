<?php

namespace Binaccle\Rules\Shared;

use Binaccle\Repositories\AbstractRepositoryInterface;
use Illuminate\Contracts\Validation\Rule;

class WhereInCountRule implements Rule
{
    public function __construct(
        private AbstractRepositoryInterface $repository,
        private ?string $column = null
    ) {
    }

    public function message(): string
    {
        return trans('validation.where_in');
    }

    public function passes($attribute, $elements): bool
    {
        return $this->repository->whereInCount($elements, $this->column) == count($elements);
    }
}
