<?php

namespace Binaccle\Rules\Sectors;

use Binaccle\Repositories\Sectors\SectorRepositoryInterface;
use Illuminate\Contracts\Validation\Rule;

class HasUsersRule implements Rule
{
    private object $sectorRepository;

    public function __construct()
    {
        $this->sectorRepository = app(SectorRepositoryInterface::class);
    }

    public function message(): string
    {
        return trans('validation.sector_has_users');
    }

    public function passes($attribute, $sectorId): bool
    {
        return ! $this->sectorRepository->hasUsers($sectorId);
    }
}
