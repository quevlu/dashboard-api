<?php

namespace Binaccle\Traits\Migrations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use ReflectionClass;

trait MagicMigrationTrait
{
    protected string $table;

    public function __construct()
    {
        $calledClass = get_called_class();

        $constant = @(new ReflectionClass($calledClass))->getConstants()['REPOSITORY'];

        if ($constant) {
            $repository = app($constant);

            $this->table = $repository->table();
        }
    }

    public function nativeDeletedAt(Blueprint &$table)
    {
        $table->integer('deleted_at')->nullable();
    }

    public function nativeTimestamps(Blueprint &$table)
    {
        $table->integer(Model::CREATED_AT);
        $table->integer(Model::UPDATED_AT);
    }
}
