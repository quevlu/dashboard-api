<?php

namespace Binaccle\Traits\Models;

use App\Enums\CacheEnum;
use Rennokki\QueryCache\Traits\QueryCacheable;

trait CacheModelTrait
{
    use QueryCacheable;

    public $cacheDriver = 'cache';

    public $cacheFor = CacheEnum::FOREVER_TIME;

    protected static $flushCacheOnUpdate = true;

    protected function cacheTagsValue()
    {
        return [get_called_class()];
    }
}
