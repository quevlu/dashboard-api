<?php

namespace Binaccle\Traits\Models;

use Binaccle\Scopes\CompanyScope;
use Illuminate\Support\Carbon;

trait BaseModelTrait
{
    public function createdAt(): Carbon
    {
        return $this->getAttribute(static::CREATED_AT);
    }

    public function getDateFormat()
    {
        return 'U';
    }

    public function hasAttribute($attr): bool
    {
        return in_array($attr, $this->fillable);
    }

    public function id(): ?string
    {
        return $this->getAttribute(static::ID);
    }

    public function propertyHasChanged(string $property): bool
    {
        return in_array($property, array_keys($this->getChanges()));
    }

    public function updatedAt(): Carbon
    {
        return $this->getAttribute(static::UPDATED_AT);
    }

    protected static function booted()
    {
        static::addGlobalScope(new CompanyScope());
    }
}
