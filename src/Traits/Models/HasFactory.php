<?php

namespace Binaccle\Traits\Models;

use Database\Factories\AbstractFactory;

trait HasFactory
{
    public static function factory(...$parameters)
    {
        $factory = static::newFactory() ?: AbstractFactory::factoryForModel(get_called_class());

        return $factory
                    ->count(is_numeric($parameters[0] ?? null) ? $parameters[0] : null)
                    ->state(is_array($parameters[0] ?? null) ? $parameters[0] : ($parameters[1] ?? []));
    }

    protected static function newFactory()
    {
    }
}
