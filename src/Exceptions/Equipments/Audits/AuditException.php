<?php

namespace Binaccle\Exceptions\Equipments\Audits;

use Binaccle\Exceptions\AbstractDomainException;
use Illuminate\Http\Response;

class AuditException extends AbstractDomainException
{
    public static function isNotInCreatedCreatingOrUpdating()
    {
        return new self(trans('errors.equipments.audits.is_not_in_created_creating_or_updating'), Response::HTTP_BAD_REQUEST);
    }
}
