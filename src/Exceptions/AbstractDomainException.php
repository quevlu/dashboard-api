<?php

namespace Binaccle\Exceptions;

use DomainException;

abstract class AbstractDomainException extends DomainException
{
}
