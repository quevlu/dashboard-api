<?php

namespace Binaccle\Exceptions\Shared;

use Binaccle\Exceptions\AbstractDomainException;

class PaymentRequiredException extends AbstractDomainException
{
}
