<?php

namespace Binaccle\Decorators\Users;

use App\Http\Requests\Users\CreateUserRequest;
use Binaccle\Decorators\MassiveUploads\AbstractMassiveUploadDecorator;
use Binaccle\Models\Roles\Role;
use Binaccle\Models\Sectors\Sector;
use Binaccle\Repositories\Roles\RoleRepositoryInterface;
use Binaccle\Repositories\Sectors\SectorRepositoryInterface;

class MassiveUploadDecorator extends AbstractMassiveUploadDecorator
{
    public function __construct(
        private SectorRepositoryInterface $sectorRepository,
        private RoleRepositoryInterface $roleRepository
    ) {
    }

    public function decorate(array $columns): array
    {
        $rolesArray = explode('|', $columns[4]);

        if ($columns[2]) {
            $sectorObject = $this->sectorRepository->firstOrNew([Sector::NAME => $columns[2]]);
            $columns[2] = $sectorObject->id() ? $sectorObject->id() : $columns[2];
        } else {
            $columns[2] = null;
        }

        $roles = $this->roleRepository->whereNameIn($rolesArray);

        $columns[4] = $roles->pluck(Role::ID)->toArray();

        return [
            CreateUserRequest::NAME => $columns[0],
            CreateUserRequest::EMAIL => $columns[1],
            CreateUserRequest::SECTOR_ID => $columns[2],
            CreateUserRequest::IDENTITY_DOCUMENT => $columns[3],
            CreateUserRequest::ROLE_IDS => $columns[4],
            CreateUserRequest::COMPANY_ID => $this->additionalData[CreateUserRequest::COMPANY_ID],
            CreateUserRequest::ROLES => $roles,
        ];
    }
}
