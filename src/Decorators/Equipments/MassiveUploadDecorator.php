<?php

namespace Binaccle\Decorators\Equipments;

use App\Http\Requests\Equipments\CreateEquipmentRequest;
use Binaccle\Decorators\MassiveUploads\AbstractMassiveUploadDecorator;
use Binaccle\Models\Equipments\EquipmentType;
use Binaccle\Repositories\Equipments\Types\TypeRepositoryInterface;

class MassiveUploadDecorator extends AbstractMassiveUploadDecorator
{
    public function __construct(
        private TypeRepositoryInterface $typeRepository
    ) {
    }

    public function decorate(array $columns): array
    {
        $equipmentTypeObject = $this->typeRepository->firstOrNew([EquipmentType::NAME => $columns[0]]);
        $columns[0] = $equipmentTypeObject->id() ? $equipmentTypeObject->id() : $columns[0];

        return [
            CreateEquipmentRequest::EQUIPMENT_TYPE_ID => $columns[0],
            CreateEquipmentRequest::QUANTITY => $columns[1],
            CreateEquipmentRequest::COMPANY_ID => $this->additionalData[CreateEquipmentRequest::COMPANY_ID],
        ];
    }
}
