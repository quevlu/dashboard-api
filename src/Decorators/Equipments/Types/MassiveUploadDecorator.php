<?php

namespace Binaccle\Decorators\Equipments\Types;

use App\Http\Requests\Equipments\Types\CreateTypeRequest;
use Binaccle\Decorators\MassiveUploads\AbstractMassiveUploadDecorator;

class MassiveUploadDecorator extends AbstractMassiveUploadDecorator
{
    public function decorate(array $columns): array
    {
        return [
            CreateTypeRequest::NAME => $columns[0],
            CreateTypeRequest::BRAND => $columns[1],
            CreateTypeRequest::MODEL => $columns[2],
            CreateTypeRequest::PREFIX => $columns[3],
            CreateTypeRequest::CHARACTERISTICS => $columns[4],
            CreateTypeRequest::QUANTITY => $columns[5],
            CreateTypeRequest::COMPANY_ID => $this->additionalData[CreateTypeRequest::COMPANY_ID],
        ];
    }
}
