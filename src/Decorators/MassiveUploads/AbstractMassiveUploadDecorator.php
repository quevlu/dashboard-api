<?php

namespace Binaccle\Decorators\MassiveUploads;

abstract class AbstractMassiveUploadDecorator
{
    protected array $additionalData = [];

    public function setAdditionalData(array $additionalData): void
    {
        $this->additionalData = $additionalData;
    }
}
