<?php

namespace Binaccle\Dtos\Users;

use Binaccle\Models\Users\User;
use Binaccle\Models\Users\UserNotifiable;
use Binaccle\Payloads\Users\CreateUserPayload;
use Binaccle\Payloads\Users\Notifiables\UserNotifiablePayload;
use Illuminate\Database\Eloquent\Collection;

class UserDto implements CreateUserPayload, UserNotifiablePayload
{
    private User $currentUser;

    public function __construct(
        private string $name,
        private string $email,
        private ?string $sectorId,
        private ?string $identityDocument,
        private ?Collection $roles,
        private string $companyId
    ) {
    }

    public function companyId(): string
    {
        return $this->companyId;
    }

    public function currentUser(): User
    {
        return $this->currentUser;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function identityDocument(): ?string
    {
        return $this->identityDocument;
    }

    public function modelToNotify(): ?UserNotifiable
    {
        return null;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function password(): ?string
    {
        return null;
    }

    public function roles(): ?Collection
    {
        return $this->roles;
    }

    public function sectorId(): ?string
    {
        return $this->sectorId;
    }

    public function setCurrentUser(User $currentUser): void
    {
        $this->currentUser = $currentUser;
    }

    public function typeToNotify(): string
    {
        return UserNotifiable::INVITATION_TYPE;
    }
}
