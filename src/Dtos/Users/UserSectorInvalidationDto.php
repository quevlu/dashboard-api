<?php

namespace Binaccle\Dtos\Users;

use Binaccle\Models\Users\User;

class UserSectorInvalidationDto
{
    public function __construct(
        private string $companyId
    ) {
    }

    public function companyId(): string
    {
        return $this->companyId;
    }

    public function propertyHasChanged(string $key): bool
    {
        return $key == User::SECTOR_ID;
    }
}
