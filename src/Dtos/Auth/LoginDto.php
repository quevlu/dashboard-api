<?php

namespace Binaccle\Dtos\Auth;

class LoginDto
{
    public function __construct(
        private string $token
    ) {
    }

    public function token(): string
    {
        return $this->token;
    }
}
