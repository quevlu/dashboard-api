<?php

namespace Binaccle\Dtos\Shared;

class PaginationDto
{
    public function __construct(
        private int $total
    ) {
    }

    public function total(): int
    {
        return $this->total;
    }
}
