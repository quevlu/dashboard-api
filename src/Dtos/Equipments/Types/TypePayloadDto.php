<?php

namespace Binaccle\Dtos\Equipments\Types;

use Binaccle\Payloads\Equipments\Types\CreateTypePayload;

class TypePayloadDto implements CreateTypePayload
{
    public function __construct(
        private string $name,
        private ?string $brand,
        private ?string $model,
        private string $prefix,
        private ?string $characteristics,
        private ?int $quantity,
        private string $companyId
    ) {
    }

    public function brand(): ?string
    {
        return $this->brand;
    }

    public function characteristics(): ?string
    {
        return $this->characteristics;
    }

    public function companyId(): string
    {
        return $this->companyId;
    }

    public function model(): ?string
    {
        return $this->model;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function prefix(): string
    {
        return $this->prefix;
    }

    public function quantity(): ?int
    {
        return $this->quantity;
    }
}
