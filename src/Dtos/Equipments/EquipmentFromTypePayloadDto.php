<?php

namespace Binaccle\Dtos\Equipments;

use Binaccle\Payloads\Equipments\CreateEquipmentPayload;

class EquipmentFromTypePayloadDto implements CreateEquipmentPayload
{
    public function __construct(
        private string $equipmentTypeId,
        private int $quantity,
        private string $companyId
    ) {
    }

    public function companyId(): string
    {
        return $this->companyId;
    }

    public function equipmentTypeId(): string
    {
        return $this->equipmentTypeId;
    }

    public function quantity(): int
    {
        return $this->quantity;
    }
}
