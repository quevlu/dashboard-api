<?php

namespace Binaccle\Dtos\Equipments;

use Binaccle\Payloads\Equipments\UpsertEquipmentPayload;

class EquipmentPayloadDto implements UpsertEquipmentPayload
{
    public function __construct(
        private string $identifier,
        private string $equipmentTypeId,
        private string $companyId
    ) {
    }

    public function companyId(): string
    {
        return $this->companyId;
    }

    public function equipmentTypeId(): string
    {
        return $this->equipmentTypeId;
    }

    public function identifier(): string
    {
        return $this->identifier;
    }
}
