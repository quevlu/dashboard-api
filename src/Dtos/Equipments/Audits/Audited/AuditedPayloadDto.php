<?php

namespace Binaccle\Dtos\Equipments\Audits\Audited;

use Binaccle\Payloads\Equipments\Audits\Audited\CreateAuditedPayload;

class AuditedPayloadDto implements CreateAuditedPayload
{
    public function __construct(
        private string $equipmentAuditId,
        private string $equipmentId,
        private ?string $equipmentAssignmentId,
        private bool $audited
    ) {
    }

    public function audited(): bool
    {
        return $this->audited;
    }

    public function equipmentAssignmentId(): ?string
    {
        return $this->equipmentAssignmentId;
    }

    public function equipmentAuditId(): string
    {
        return $this->equipmentAuditId;
    }

    public function equipmentId(): string
    {
        return $this->equipmentId;
    }
}
