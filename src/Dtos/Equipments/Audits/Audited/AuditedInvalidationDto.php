<?php

namespace Binaccle\Dtos\Equipments\Audits\Audited;

class AuditedInvalidationDto
{
    public function __construct(
        private string $companyId,
        private string $equipmentAuditId
    ) {
    }

    public function companyId(): string
    {
        return $this->companyId;
    }

    public function equipmentAuditId(): string
    {
        return $this->equipmentAuditId;
    }

    public function equipmentAuditRel()
    {
        return new class($this->companyId) {
            private string $companyId;

            public function __construct(string $companyId)
            {
                $this->companyId = $companyId;
            }

            public function companyId(): string
            {
                return $this->companyId;
            }
        };
    }
}
