<?php

namespace Binaccle\Listeners\Equipments\Audits\Audited;

use App\Events\Cache\InvalidateCacheEvent;
use App\Observers\Observer;
use Binaccle\Dtos\Equipments\Audits\Audited\AuditedInvalidationDto;
use Binaccle\Enums\Equipments\Audits\AuditStateEnum;
use Binaccle\Events\Equipments\Audits\Audited\CreateAuditedEvent;
use Binaccle\Models\Equipments\EquipmentAssignment;
use Binaccle\Repositories\Equipments\Assignments\AssignmentRepositoryInterface;
use Binaccle\Repositories\Equipments\Audits\AuditRepositoryInterface;
use Binaccle\Services\Equipments\Audits\Audited\MassiveAuditedService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Throwable;

class CreateAuditedListener implements ShouldQueue
{
    use InteractsWithQueue;

    public function __construct(
        private MassiveAuditedService $massiveEquipmentAuditAuditedService,
        private AuditRepositoryInterface $equipmentAuditRepository,
        private AssignmentRepositoryInterface $equipmentAssignmentRepository
    ) {
    }

    public function handle(CreateAuditedEvent $event): void
    {
        $equipmentAudit = $event->equipmentAudit();
        $equipmentAuditId = $equipmentAudit->id();
        $equipmentsAssignedBuilder = $this->equipmentAssignmentRepository->activeEquipmentsBy(EquipmentAssignment::SECTOR_ID, $equipmentAudit->sectorId());

        try {
            $equipmentsAssignedBuilder->chunk(200, function ($equipmentsAssigned) use ($equipmentAudit) {
                $this->massiveEquipmentAuditAuditedService->add($equipmentAudit, $equipmentsAssigned, false);
            });

            $this->equipmentAuditRepository->transition($equipmentAuditId, AuditStateEnum::CREATED);

            $fakeEquipmentAuditAudited = new AuditedInvalidationDto($equipmentAudit->companyId(), $equipmentAudit->id());

            event(new InvalidateCacheEvent($fakeEquipmentAuditAudited, Observer::CREATED));
        } catch (Throwable $th) {
            $this->equipmentAuditRepository->transition($equipmentAuditId, AuditStateEnum::FAILED);

            throw $th;
        }
    }

    public function viaConnection()
    {
        return config('queue.app.connection');
    }

    public function viaQueue()
    {
        return config('queue.app.queue');
    }
}
