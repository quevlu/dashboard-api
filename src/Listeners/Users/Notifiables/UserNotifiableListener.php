<?php

namespace Binaccle\Listeners\Users\Notifiables;

use App\Mail\Users\Notifiables\UserNotifiableMail;
use Binaccle\Events\Users\Notifiables\UserNotifiableEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class UserNotifiableListener implements ShouldQueue
{
    use InteractsWithQueue;

    public function handle(UserNotifiableEvent $event): void
    {
        Mail::to($event->email())
            ->send(new UserNotifiableMail($event->type(), $event->token()));
    }

    public function viaConnection()
    {
        return config('queue.mail.connection');
    }

    public function viaQueue()
    {
        return config('queue.mail.queue');
    }
}
