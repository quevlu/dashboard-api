<?php

namespace Binaccle\Listeners\Companies;

use Binaccle\Events\Companies\DeleteCompanyEvent;
use Binaccle\Services\Utils\Storage\PathResolver;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Infrastructure\Storage\S3Storage;

class DeleteCompanyListener implements ShouldQueue
{
    use InteractsWithQueue;

    public function __construct(
        private PathResolver $pathResolverService,
        private S3Storage $s3Storage
    ) {
    }

    public function handle(DeleteCompanyEvent $event): void
    {
        $company = $event->company();

        $companyFolder = $this->pathResolverService->folder(PathResolver::COMPANIES_PATH, [
            'company_id' => $company->id(),
        ]);

        $this->s3Storage->delete($companyFolder);

        $company->forceDelete();
    }

    public function viaConnection()
    {
        return config('queue.app.connection');
    }

    public function viaQueue()
    {
        return config('queue.app.queue');
    }
}
