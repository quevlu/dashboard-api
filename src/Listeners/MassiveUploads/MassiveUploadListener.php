<?php

namespace Binaccle\Listeners\MassiveUploads;

use Binaccle\Events\MassiveUploads\MassiveUploadEvent;
use Binaccle\Services\MassiveUploads\ProcessMassiveUploadService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\App;

class MassiveUploadListener // implements ShouldQueue
{
    // use InteractsWithQueue;

    public function __construct(
        private ProcessMassiveUploadService $service
    ) {
    }

    public function handle(MassiveUploadEvent $event): void
    {
        App::setLocale($event->locale());

        $this->service->setMassiveUpload($event->massiveUpload())
            ->setCsvFile($event->file())
            ->process();
    }

    public function viaConnection()
    {
        return config('queue.app.connection');
    }

    public function viaQueue()
    {
        return config('queue.app.queue');
    }
}
