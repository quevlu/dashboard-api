<?php

namespace Binaccle\Payloads\Auth\ResetPassword;

use Binaccle\Models\Users\User;
use Binaccle\Models\Users\UserNotifiable;

interface ResetPasswordPayload
{
    public function currentUser(): ?User;

    public function password(): string;

    public function userNotifiable(): ?UserNotifiable;
}
