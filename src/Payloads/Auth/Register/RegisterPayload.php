<?php

namespace Binaccle\Payloads\Auth\Register;

use Binaccle\Models\Users\User;
use Illuminate\Database\Eloquent\Collection;

interface RegisterPayload
{
    public function companyId(): string;

    public function companyName(): string;

    public function currentUser(): User;

    public function email(): string;

    public function identityDocument(): ?string;

    public function name(): string;

    public function password(): string;

    public function planId(): string;

    public function roles(): ?Collection;

    public function sectorId(): ?string;

    public function setCompanyId(string $companyId): void;

    public function setCurrentUser(User $user): void;
}
