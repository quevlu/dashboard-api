<?php

namespace Binaccle\Payloads\Auth;

use Binaccle\Models\Users\User;

interface LoginPayload
{
    public function currentUser(): User;
}
