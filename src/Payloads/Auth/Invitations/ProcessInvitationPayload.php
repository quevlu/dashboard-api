<?php

namespace Binaccle\Payloads\Auth\Invitations;

use Binaccle\Models\Users\User;
use Binaccle\Models\Users\UserNotifiable;

interface ProcessInvitationPayload
{
    public function currentUser(): ?User;

    public function password(): string;

    public function userNotifiable(): ?UserNotifiable;
}
