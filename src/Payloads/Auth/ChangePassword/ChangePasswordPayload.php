<?php

namespace Binaccle\Payloads\Auth\ChangePassword;

use Binaccle\Models\Users\User;

interface ChangePasswordPayload
{
    public function currentUser(): User;

    public function newPassword(): string;
}
