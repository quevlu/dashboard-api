<?php

namespace Binaccle\Payloads\Users;

use Binaccle\Models\Users\User;

interface CreateUserPayload extends UpsertUserPayload
{
    public function currentUser(): User;

    public function setCurrentUser(User $currentUser): void;

    public function typeToNotify(): string;
}
