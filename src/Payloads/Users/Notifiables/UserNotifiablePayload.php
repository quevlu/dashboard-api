<?php

namespace Binaccle\Payloads\Users\Notifiables;

use Binaccle\Models\Users\User;
use Binaccle\Models\Users\UserNotifiable;

interface UserNotifiablePayload
{
    public function currentUser(): ?User;

    public function modelToNotify(): ?UserNotifiable;
}
