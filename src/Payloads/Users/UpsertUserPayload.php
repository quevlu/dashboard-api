<?php

namespace Binaccle\Payloads\Users;

use Illuminate\Database\Eloquent\Collection;

interface UpsertUserPayload
{
    public function companyId(): string;

    public function email(): string;

    public function identityDocument(): ?string;

    public function name(): string;

    public function password(): ?string;

    public function roles(): ?Collection;

    public function sectorId(): ?string;
}
