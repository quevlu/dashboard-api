<?php

namespace Binaccle\Payloads\MassiveUploads;

use Illuminate\Http\UploadedFile;

interface MassiveUploadPayload
{
    public function companyId(): string;

    public function massiveUploadStateId(): string;

    public function massiveUploadTypeId(): string;

    public function relativePath(): string;

    public function setRelativePath(string $relativePath): void;

    public function setUrl(string $url): void;

    public function uploadedFile(): UploadedFile;

    public function url(): string;
}
