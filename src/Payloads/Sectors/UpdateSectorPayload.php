<?php

namespace Binaccle\Payloads\Sectors;

use Binaccle\Models\Sectors\Sector;

interface UpdateSectorPayload extends SectorPayload
{
    public function sector(): Sector;
}
