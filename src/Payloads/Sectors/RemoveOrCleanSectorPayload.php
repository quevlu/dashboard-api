<?php

namespace Binaccle\Payloads\Sectors;

interface RemoveOrCleanSectorPayload
{
    public function companyId(): string;

    public function sectorId(): string;

    public function userIdOrAction(): string;
}
