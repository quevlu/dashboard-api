<?php

namespace Binaccle\Payloads\Sectors;

interface SectorPayload
{
    public function name(): string;
}
