<?php

namespace Binaccle\Payloads\Companies;

use Binaccle\Models\Companies\Company;

interface DeleteCompanyPayload
{
    public function company(): ?Company;
}
