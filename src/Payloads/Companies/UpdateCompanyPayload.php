<?php

namespace Binaccle\Payloads\Companies;

use Binaccle\Models\Companies\Company;
use Binaccle\Models\Files\File;
use Illuminate\Http\UploadedFile;

interface UpdateCompanyPayload
{
    public function company(): ?Company;

    public function logo(): ?UploadedFile;

    public function logoCreated(): ?File;

    public function name(): string;

    public function relativePath(): string;

    public function setLogoCreated(?File $logoCreated = null): void;

    public function setRelativePath(string $relativePath): void;

    public function setUrl(string $url): void;

    public function url(): string;
}
