<?php

namespace Binaccle\Payloads\Equipments;

interface UpsertEquipmentPayload
{
    public function companyId(): string;

    public function equipmentTypeId(): string;
}
