<?php

namespace Binaccle\Payloads\Equipments\Audits;

use Binaccle\Models\Equipments\EquipmentAudit;

interface DeleteAuditPayload
{
    public function equipmentAudit(): EquipmentAudit;
}
