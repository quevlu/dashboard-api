<?php

namespace Binaccle\Payloads\Equipments\Audits;

interface CreateAuditPayload extends UpsertAuditPayload
{
}
