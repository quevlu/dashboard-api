<?php

namespace Binaccle\Payloads\Equipments\Audits;

use Binaccle\Models\Equipments\EquipmentAudit;

interface FinishAuditPayload
{
    public function equipmentAudit(): EquipmentAudit;
}
