<?php

namespace Binaccle\Payloads\Equipments\Audits;

use Binaccle\Models\Equipments\EquipmentAudit;

interface UpdateAuditPayload extends UpsertAuditPayload
{
    public function equipmentAudit(): EquipmentAudit;
}
