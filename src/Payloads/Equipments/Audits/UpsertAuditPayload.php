<?php

namespace Binaccle\Payloads\Equipments\Audits;

interface UpsertAuditPayload
{
    public function equipmentAuditStateId(): string;

    public function sectorId(): ?string;

    public function title(): string;
}
