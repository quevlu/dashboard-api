<?php

namespace Binaccle\Payloads\Equipments\Audits\Audited;

use Illuminate\Database\Eloquent\Collection;

interface AddAuditedPayload extends AuditedPayload
{
    public function equipments(): Collection;
}
