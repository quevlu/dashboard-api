<?php

namespace Binaccle\Payloads\Equipments\Audits\Audited;

use Binaccle\Models\Equipments\EquipmentAudit;

interface AuditedPayload
{
    public function equipmentAudit(): EquipmentAudit;

    public function equipmentIds(): array;
}
