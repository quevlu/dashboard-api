<?php

namespace Binaccle\Payloads\Equipments\Audits\Audited;

interface CreateAuditedPayload
{
    public function audited(): bool;

    public function equipmentAssignmentId(): ?string;

    public function equipmentAuditId(): string;

    public function equipmentId(): string;
}
