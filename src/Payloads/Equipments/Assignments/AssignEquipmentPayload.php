<?php

namespace Binaccle\Payloads\Equipments\Assignments;

interface AssignEquipmentPayload
{
    public function equipmentId(): string;

    public function observation(): ?string;

    public function sectorId(): ?string;

    public function userId(): ?string;
}
