<?php

namespace Binaccle\Payloads\Equipments\Assignments;

interface UnassignEquipmentPayload
{
    public function equipmentId(): string;

    public function observation(): ?string;
}
