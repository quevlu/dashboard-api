<?php

namespace Binaccle\Payloads\Equipments;

use Binaccle\Models\Equipments\Equipment;

interface DeleteEquipmentPayload
{
    public function equipment(): Equipment;

    public function observation(): ?string;
}
