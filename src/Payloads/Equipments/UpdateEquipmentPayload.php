<?php

namespace Binaccle\Payloads\Equipments;

use Binaccle\Models\Equipments\Equipment;

interface UpdateEquipmentPayload extends UpsertEquipmentPayload
{
    public function equipment(): Equipment;
}
