<?php

namespace Binaccle\Payloads\Equipments;

use Binaccle\Models\Equipments\Equipment;
use Binaccle\Models\Files\File;

interface PrintEquipmentPayload
{
    public function equipment(): Equipment;

    public function logo(): ?File;

    public function type(): string;
}
