<?php

namespace Binaccle\Payloads\Equipments;

use Binaccle\Models\Files\File;
use Illuminate\Database\Eloquent\Collection;

interface MassivePrintEquipmentPayload
{
    public function equipments(): Collection;

    public function logo(): ?File;

    public function type(): string;
}
