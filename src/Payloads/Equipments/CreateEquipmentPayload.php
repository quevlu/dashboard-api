<?php

namespace Binaccle\Payloads\Equipments;

interface CreateEquipmentPayload extends UpsertEquipmentPayload
{
    public function quantity(): int;
}
