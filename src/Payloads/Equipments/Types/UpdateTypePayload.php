<?php

namespace Binaccle\Payloads\Equipments\Types;

interface UpdateTypePayload extends UpsertTypePayload
{
}
