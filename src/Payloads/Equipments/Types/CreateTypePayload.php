<?php

namespace Binaccle\Payloads\Equipments\Types;

interface CreateTypePayload extends UpsertTypePayload
{
    public function quantity(): ?int;
}
