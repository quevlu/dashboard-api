<?php

namespace Binaccle\Payloads\Equipments\Types;

use Binaccle\Models\Equipments\EquipmentType;

interface UpleteTypePayload
{
    public function equipmentType(): EquipmentType;
}
