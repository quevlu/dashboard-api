<?php

namespace Binaccle\Payloads\Equipments\Types;

interface UpsertTypePayload
{
    public function brand(): ?string;

    public function characteristics(): ?string;

    public function companyId(): string;

    public function model(): ?string;

    public function name(): string;

    public function prefix(): string;
}
