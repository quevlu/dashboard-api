<?php

namespace Binaccle\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Scope;

class CompanyScope implements Scope
{
    private const COMPANY_ID = 'company_id';

    public function apply(Builder $builder, object $model)
    {
        if ($model->hasAttribute(self::COMPANY_ID)) {
            $auth = auth();
            $companyId = $auth && $auth->hasUser() ? $auth->user()->companyId() : $model->companyId();

            if ($companyId) {
                $builder->where(self::COMPANY_ID, $companyId);
            }
        }
    }
}
