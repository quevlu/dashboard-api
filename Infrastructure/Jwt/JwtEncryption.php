<?php

namespace Infrastructure\Jwt;

use Illuminate\Encryption\Encrypter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class JwtEncryption
{
    private const KEY_LENGTH = 32;

    public function __construct(
        private Request $request
    ) {
    }

    public function decrypt(string $value)
    {
        return Crypt::decryptString($value);
    }

    public function decryptJwt($jwt): string
    {
        return (new Encrypter($this->key(), config('app.cipher')))->decrypt($jwt);
    }

    public function encrypt($value): string
    {
        return Crypt::encryptString($value);
    }

    public function encryptJwt($jwt): string
    {
        return (new Encrypter($this->key(), config('app.cipher')))->encrypt($jwt);
    }

    private function key(): string
    {
        $ipAndUserAgentHashed = hash('sha512', $this->request->ip() . $this->request->header('User-Agent'));

        return substr($ipAndUserAgentHashed, 0, self::KEY_LENGTH);
    }
}
