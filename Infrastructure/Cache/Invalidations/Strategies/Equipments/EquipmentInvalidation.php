<?php

namespace Infrastructure\Cache\Invalidations\Strategies\Equipments;

use App\Enums\CacheEnum;
use Infrastructure\Cache\Invalidations\Strategies\AbstractInvalidation;
use Infrastructure\Cache\Invalidations\Strategies\Interfaces\CompanyPrefixInterface;

class EquipmentInvalidation extends AbstractInvalidation implements CompanyPrefixInterface
{
    public function buildOnCreated(object $equipment): void
    {
        $this->invalidationBuilder->add(CacheEnum::LIST_EQUIPMENTS);
    }

    public function buildOnDeleted(object $equipment): void
    {
        $this->invalidationBuilder->add(
            CacheEnum::LIST_EQUIPMENTS,
            CacheEnum::LIST_EQUIPMENT_ASSIGNMENTS,
            CacheEnum::LIST_EQUIPMENT_AUDIT_AUDITED
        );
    }

    public function buildOnUpdated(object $equipment): void
    {
        $this->invalidationBuilder->add(CacheEnum::BASE_EQUIPMENTS);
    }
}
