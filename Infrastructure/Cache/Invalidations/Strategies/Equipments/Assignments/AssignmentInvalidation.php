<?php

namespace Infrastructure\Cache\Invalidations\Strategies\Equipments\Assignments;

use App\Enums\CacheEnum;
use Infrastructure\Cache\Invalidations\Strategies\AbstractInvalidation;

class AssignmentInvalidation extends AbstractInvalidation
{
    public function buildOnCreated(object $equipmentAssignment): void
    {
        $this->addTags($equipmentAssignment);
    }

    public function buildOnDeleted(object $equipmentAssignment): void
    {
    }

    public function buildOnUpdated(object $equipmentAssignment): void
    {
        $this->addTags($equipmentAssignment);
    }

    private function addTags(object $equipmentAssignment): void
    {
        $companyId = $equipmentAssignment->equipmentRel()->companyId();

        $this->invalidationBuilder->addCompanyPrefix($companyId);

        $this->invalidationBuilder->add(
            CacheEnum::LIST_EQUIPMENTS,
            CacheEnum::LIST_EQUIPMENT_ASSIGNMENTS,
            CacheEnum::LIST_EQUIPMENT_ASSIGNMENT_HISTORY,
            CacheEnum::LIST_EQUIPMENT_AUDIT_AUDITED
        );
    }
}
