<?php

namespace Infrastructure\Cache\Invalidations\Strategies\Equipments\Audits\Audited;

use App\Enums\CacheEnum;
use Infrastructure\Cache\Invalidations\Strategies\AbstractInvalidation;

class AuditedInvalidation extends AbstractInvalidation
{
    public function buildOnCreated(object $equipmentAuditAudited): void
    {
        $this->addTags($equipmentAuditAudited);
    }

    public function buildOnDeleted(object $equipmentAuditAudited): void
    {
        $this->addTags($equipmentAuditAudited);
    }

    public function buildOnUpdated(object $equipmentAuditAudited): void
    {
        $this->addTags($equipmentAuditAudited);
    }

    private function addTags(object $equipmentAuditAudited): void
    {
        $companyId = $equipmentAuditAudited->equipmentAuditRel()->companyId();

        $this->invalidationBuilder->addCompanyPrefix($companyId);

        $this->invalidationBuilder->add(
            [
                CacheEnum::LIST_EQUIPMENT_AUDIT_AUDITED,
                $equipmentAuditAudited->equipmentAuditId(),
            ]
        );
    }
}
