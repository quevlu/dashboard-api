<?php

namespace Infrastructure\Cache\Invalidations\Strategies\Equipments\Audits;

use App\Enums\CacheEnum;
use Infrastructure\Cache\Invalidations\Strategies\AbstractInvalidation;
use Infrastructure\Cache\Invalidations\Strategies\Interfaces\CompanyPrefixInterface;

class AuditInvalidation extends AbstractInvalidation implements CompanyPrefixInterface
{
    public function buildOnCreated(object $equipmentAudit): void
    {
        $this->invalidationBuilder->add(CacheEnum::LIST_EQUIPMENT_AUDITS);
    }

    public function buildOnDeleted(object $equipmentAudit): void
    {
        $this->invalidationBuilder->add(
            CacheEnum::LIST_EQUIPMENT_AUDITS,
            [
                CacheEnum::LIST_EQUIPMENT_AUDIT_AUDITED,
                $equipmentAudit->id(),
            ]
        );
    }

    public function buildOnUpdated(object $equipmentAudit): void
    {
        $this->invalidationBuilder->add(CacheEnum::LIST_EQUIPMENT_AUDITS);
    }
}
