<?php

namespace Infrastructure\Cache\Invalidations\Strategies\Equipments\Types;

use App\Enums\CacheEnum;
use Infrastructure\Cache\Invalidations\Strategies\AbstractInvalidation;
use Infrastructure\Cache\Invalidations\Strategies\Interfaces\CompanyPrefixInterface;

class TypeInvalidation extends AbstractInvalidation implements CompanyPrefixInterface
{
    public function buildOnCreated(object $equipmentType): void
    {
        $this->invalidationBuilder->add(
            CacheEnum::LIST_EQUIPMENTS,
            CacheEnum::LIST_EQUIPMENT_TYPES
        );
    }

    public function buildOnDeleted(object $equipmentType): void
    {
        $this->invalidationBuilder->add(CacheEnum::LIST_EQUIPMENT_TYPES);
    }

    public function buildOnUpdated(object $equipmentType): void
    {
        $this->invalidationBuilder->add(CacheEnum::BASE_EQUIPMENTS);
    }
}
