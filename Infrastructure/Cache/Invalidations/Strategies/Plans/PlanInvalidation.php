<?php

namespace Infrastructure\Cache\Invalidations\Strategies\Plans;

use App\Enums\CacheEnum;
use Infrastructure\Cache\Invalidations\Strategies\AbstractInvalidation;

class PlanInvalidation extends AbstractInvalidation
{
    public function buildOnCreated(object $plan): void
    {
        $this->addTag();
    }

    public function buildOnDeleted(object $plan): void
    {
        $this->addTag();
    }

    public function buildOnUpdated(object $plan): void
    {
        $this->addTag();
    }

    private function addTag(): void
    {
        $this->invalidationBuilder->add(CacheEnum::LIST_PLANS);
    }
}
