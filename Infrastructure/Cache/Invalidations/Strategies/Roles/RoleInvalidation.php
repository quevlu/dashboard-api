<?php

namespace Infrastructure\Cache\Invalidations\Strategies\Roles;

use App\Enums\CacheEnum;
use Infrastructure\Cache\Invalidations\Strategies\AbstractInvalidation;

class RoleInvalidation extends AbstractInvalidation
{
    public function buildOnCreated(object $role): void
    {
        $this->addTag();
    }

    public function buildOnDeleted(object $role): void
    {
        $this->addTag();
    }

    public function buildOnUpdated(object $role): void
    {
        $this->addTag();
    }

    private function addTag(): void
    {
        $this->invalidationBuilder->add(CacheEnum::LIST_ROLES);
    }
}
