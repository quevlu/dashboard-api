<?php

namespace Infrastructure\Cache\Invalidations\Strategies;

use Infrastructure\Cache\Invalidations\InvalidationBuilder;
use Infrastructure\Cache\Invalidations\Strategies\Interfaces\CompanyPrefixInterface;

abstract class AbstractInvalidation
{
    protected InvalidationBuilder $invalidationBuilder;

    public function __construct(InvalidationBuilder $invalidationBuilder)
    {
        $this->invalidationBuilder = $invalidationBuilder;
    }

    public function build(object $model, string $method): array
    {
        $methodName = "buildOn{$method}";

        if ($this instanceof CompanyPrefixInterface) {
            $this->invalidationBuilder->addCompanyPrefix($model->companyId());
        }

        $this->$methodName($model);

        return $this->invalidationBuilder->tags();
    }

    abstract public function buildOnCreated(object $model): void;

    abstract public function buildOnDeleted(object $model): void;

    abstract public function buildOnUpdated(object $model): void;
}
