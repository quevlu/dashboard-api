<?php

namespace Infrastructure\Cache\Invalidations\Strategies\Users;

use App\Enums\CacheEnum;
use Infrastructure\Cache\Invalidations\Strategies\AbstractInvalidation;
use Infrastructure\Cache\Invalidations\Strategies\Interfaces\CompanyPrefixInterface;
use Binaccle\Models\Users\User;

class UserInvalidation extends AbstractInvalidation implements CompanyPrefixInterface
{
    public function buildOnCreated(object $user): void
    {
        $this->invalidationBuilder->add(
            CacheEnum::LIST_USERS,
            $this->listUserSectorTag($user)
        );
    }

    public function buildOnDeleted(object $user): void
    {
        $this->invalidationBuilder->add(CacheEnum::LIST_USERS);

        if ($dynamicTag = $this->listUserSectorTag($user)) {
            $this->invalidationBuilder->add($dynamicTag);
        }
    }

    public function buildOnUpdated(object $user): void
    {
        $this->invalidationBuilder->add(
            CacheEnum::LIST_USERS,
            CacheEnum::LIST_EQUIPMENT_AUDIT_AUDITED
        );

        $haveToClearUsersBySector = false;

        if ($user->propertyHasChanged(User::NAME)) {
            $this->invalidationBuilder->add(
                CacheEnum::LIST_EQUIPMENTS,
                CacheEnum::LIST_EQUIPMENT_ASSIGNMENTS,
                CacheEnum::LIST_EQUIPMENT_ASSIGNMENT_HISTORY,
            );

            $haveToClearUsersBySector = true;
        }

        if ($haveToClearUsersBySector || $user->propertyHasChanged(User::SECTOR_ID)) {
            $this->invalidationBuilder->add(CacheEnum::LIST_SECTOR_USERS);
        }
    }

    private function listUserSectorTag(User $user)
    {
        if ($user->sectorId()) {
            return [
                CacheEnum::LIST_SECTOR_USERS,
                $user->sectorId(),
            ];
        }
    }
}
