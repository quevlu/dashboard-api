<?php

namespace Infrastructure\Cache\Invalidations\Strategies\MassiveUploads;

use App\Enums\CacheEnum;
use Infrastructure\Cache\Invalidations\Strategies\AbstractInvalidation;
use Infrastructure\Cache\Invalidations\Strategies\Interfaces\CompanyPrefixInterface;
use Binaccle\Enums\MassiveUploads\MassiveUploadTypeEnum;

class MassiveUploadInvalidation extends AbstractInvalidation implements CompanyPrefixInterface
{
    public function buildOnCreated(object $massiveUpload): void
    {
        $this->addTags($massiveUpload);
    }

    public function buildOnDeleted(object $massiveUpload): void
    {
    }

    public function buildOnUpdated(object $massiveUpload): void
    {
        $this->addTags($massiveUpload);
    }

    private function addTags(object $massiveUpload): void
    {
        $massiveUploadTypeName = $massiveUpload->typeRel()->name();

        $dictionaryTags = [
            MassiveUploadTypeEnum::USERS => [
                CacheEnum::LIST_USERS,
                CacheEnum::LIST_SECTOR_USERS,
            ],
            MassiveUploadTypeEnum::EQUIPMENTS => [
                CacheEnum::LIST_EQUIPMENTS,
            ],
            MassiveUploadTypeEnum::EQUIPMENT_TYPES => [
                CacheEnum::LIST_EQUIPMENTS,
                CacheEnum::LIST_EQUIPMENT_TYPES,
            ],
        ][$massiveUploadTypeName];

        $this->invalidationBuilder->add(
            [
                CacheEnum::BASE_MASSIVE_UPLOADS,
                $massiveUploadTypeName,
            ],
            ...$dictionaryTags
        );
    }
}
