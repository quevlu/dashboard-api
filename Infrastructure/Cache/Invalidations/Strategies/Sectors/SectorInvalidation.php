<?php

namespace Infrastructure\Cache\Invalidations\Strategies\Sectors;

use App\Enums\CacheEnum;
use Infrastructure\Cache\Invalidations\Strategies\AbstractInvalidation;
use Infrastructure\Cache\Invalidations\Strategies\Interfaces\CompanyPrefixInterface;

class SectorInvalidation extends AbstractInvalidation implements CompanyPrefixInterface
{
    public function buildOnCreated(object $sector): void
    {
        $this->invalidationBuilder->add(CacheEnum::LIST_SECTORS);
    }

    public function buildOnDeleted(object $sector): void
    {
        $this->invalidationBuilder->add(
            CacheEnum::LIST_SECTORS,
            CacheEnum::LIST_USERS,
            [
                CacheEnum::LIST_SECTOR_USERS,
                $sector->id(),
            ]
        );
    }

    public function buildOnUpdated(object $sector): void
    {
        $this->invalidationBuilder->add(
            CacheEnum::LIST_SECTORS,
            CacheEnum::LIST_USERS,
            CacheEnum::LIST_EQUIPMENTS,
            CacheEnum::LIST_EQUIPMENT_ASSIGNMENTS,
            CacheEnum::LIST_EQUIPMENT_ASSIGNMENT_HISTORY,
            CacheEnum::LIST_EQUIPMENT_AUDITS,
            CacheEnum::LIST_EQUIPMENT_AUDIT_AUDITED
        );
    }
}
