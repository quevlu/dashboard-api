<?php

namespace Infrastructure\Cache\Invalidations;

use App\Enums\CacheEnum;
use App\Helpers\ConcatHelper;
use Exception;
use Illuminate\Support\Arr;

class InvalidationBuilder
{
    private ?string $prefix = null;

    private bool $prefixHasBeenAdded = false;

    private ?array $tags = null;

    public function add(...$tags): void
    {
        foreach ($tags as $tag) {
            if ($tag) {
                $tag = is_array($tag[0]) ? Arr::flatten($tag) : $tag;

                $this->addTag($tag);
            }
        }
    }

    public function addCompanyPrefix(string $companyId): void
    {
        $this->addPrefix(CacheEnum::COMPANY_PREFIX, $companyId);
    }

    public function addSystemPrefix(): void
    {
        $this->addPrefix(CacheEnum::SYSTEM_PREFIX);
    }

    public function tags(): array
    {
        return $this->tags;
    }

    private function addPrefix(...$args): void
    {
        if (! $this->prefixHasBeenAdded) {
            $this->prefix = ConcatHelper::cacheTag(...$args);

            $this->prefixHasBeenAdded = true;
        } else {
            throw new Exception('The prefix has been added');
        }
    }

    private function addTag($tag): void
    {
        $tag = is_array($tag) ? $tag : [$tag];
        $tags = [$this->prefix, ...$tag];

        $this->tags[] = ConcatHelper::cacheTag(...$tags);
    }
}
