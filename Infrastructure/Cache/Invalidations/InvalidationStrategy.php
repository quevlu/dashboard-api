<?php

namespace Infrastructure\Cache\Invalidations;

use Binaccle\Dtos;
use Binaccle\Models;

class InvalidationStrategy
{
    public function build(object $model, string $context): array
    {
        return (app($this->strategies($model::class)))->build($model, $context);
    }

    private function strategies($strategy): string
    {
        return [
            Models\Users\User::class => Strategies\Users\UserInvalidation::class,
            Models\Sectors\Sector::class => Strategies\Sectors\SectorInvalidation::class,
            Models\Plans\Plan::class => Strategies\Plans\PlanInvalidation::class,
            Models\Roles\Role::class => Strategies\Roles\RoleInvalidation::class,
            Models\MassiveUploads\MassiveUpload::class => Strategies\MassiveUploads\MassiveUploadInvalidation::class,
            Models\Equipments\Equipment::class => Strategies\Equipments\EquipmentInvalidation::class,
            Models\Equipments\EquipmentType::class => Strategies\Equipments\Types\TypeInvalidation::class,
            Models\Equipments\EquipmentAssignment::class => Strategies\Equipments\Assignments\AssignmentInvalidation::class,
            Models\Equipments\EquipmentAudit::class => Strategies\Equipments\Audits\AuditInvalidation::class,
            Models\Equipments\EquipmentAuditAudited::class => Strategies\Equipments\Audits\Audited\AuditedInvalidation::class,

            Dtos\Users\UserSectorInvalidationDto::class => Strategies\Users\UserInvalidation::class,
            Dtos\Equipments\Audits\Audited\AuditedInvalidationDto::class => Strategies\Equipments\Audits\Audited\AuditedInvalidation::class,
        ][$strategy];
    }
}
