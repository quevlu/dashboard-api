<?php

namespace Infrastructure\Cache\Invalidations;

use Illuminate\Support\Facades\Cache;

class InvalidationService
{
    public function __construct(
        private InvalidationStrategy $invalidationStrategy
    ) {
    }

    public function invalidate(object $model, string $context): void
    {
        $tags = $this->invalidationStrategy->build($model, $context);

        Cache::tags($tags)->flush();
    }
}
