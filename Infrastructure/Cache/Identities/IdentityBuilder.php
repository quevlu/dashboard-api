<?php

namespace Infrastructure\Cache\Identities;

use App\Enums\CacheEnum;
use App\Helpers\ConcatHelper;
use Exception;
use Infrastructure\Repositories\Criterias\BaseCriteria;

class IdentityBuilder
{
    private bool $prefixHasBeenAdded = false;

    private ?array $tags = null;

    public function __construct(
        private BaseCriteria $criteria
    ) {
    }

    public function add($element): void
    {
        if (is_array($element)) {
            $this->destructAdd($element);
        } else {
            $this->addTag($element);
        }
    }

    public function addCompanyPrefix(): void
    {
        $this->addPrefix(CacheEnum::COMPANY_PREFIX, $this->criteria->companyId());
    }

    public function addHashOfQueryParams(): void
    {
        if ($key = $this->criteria->hashOfQueryParams()) {
            $this->add($key);
        }
    }

    public function addIdentifier(string $identifier): void
    {
        $this->add($this->criteria->identifier($identifier));
    }

    public function addSystemPrefix(): void
    {
        $this->addPrefix(CacheEnum::SYSTEM_PREFIX);
    }

    public function tags(): array
    {
        return $this->tags;
    }

    private function addPrefix(...$args): void
    {
        if (! $this->prefixHasBeenAdded) {
            $prefixTag = ConcatHelper::cacheTag(...$args);

            $this->tags = [$prefixTag];

            $this->prefixHasBeenAdded = true;
        } else {
            throw new Exception('The prefix has been added');
        }
    }

    private function addTag($tag): void
    {
        $lastTag = end($this->tags);
        $tags = [$lastTag, $tag];

        $this->tags[] = ConcatHelper::cacheTag(...$tags);
    }

    private function destructAdd(array $elements)
    {
        foreach ($elements as $element) {
            if (is_array($element)) {
                throw new Exception('Pending logic');
            }
            $this->addTag($element);
        }
    }
}
