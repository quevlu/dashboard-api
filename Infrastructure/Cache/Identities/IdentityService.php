<?php

namespace Infrastructure\Cache\Identities;

use App\Enums\CacheEnum;
use Closure;
use Illuminate\Support\Facades\Cache;

class IdentityService
{
    public function __construct(
        private IdentityStrategy $identityStrategy
    ) {
    }

    public function __invoke(Closure $closure): mixed
    {
        $options = $this->identityStrategy->build();

        if ($options['cacheable'] && config('cache.enabled')) {
            $cacheTags = Cache::tags($options['tags']);

            if (! $cacheTags->has($options['key'])) {
                $closureResult = $closure();

                if ($options['lifetime'] != CacheEnum::FOREVER_TIME) {
                    $cacheTags->put($options['key'], $closureResult, $options['lifetime']);
                } else {
                    $cacheTags->forever($options['key'], $closureResult);
                }

                return $closureResult;
            }

            return $cacheTags->get($options['key']);
        }

        return $closure();
    }
}
