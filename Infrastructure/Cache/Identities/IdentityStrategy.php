<?php

namespace Infrastructure\Cache\Identities;

use App\Enums\CacheEnum;
use App\Enums\Routes\RouteNameEnum;
use App\Enums\Routes\RouteParamEnum;
use Illuminate\Http\Request;
use Infrastructure\Repositories\Criterias\BaseCriteria;

class IdentityStrategy
{
    private const HAS_COMPANY = 'has_company';

    private const HAS_PARAMS = 'has_params';

    private const PARAM_ROUTE = 'param_route';

    private const TAG = 'tag';

    public function __construct(
        private IdentityBuilder $identityBuilder,
        private Request $request,
        private BaseCriteria $criteria
    ) {
    }

    public function build(): array
    {
        $strategy = $this->request->route()->getName();
        $identityArray = $this->strategies($strategy);

        if ($identityArray[self::HAS_COMPANY]) {
            $this->identityBuilder->addCompanyPrefix();
        } else {
            $this->identityBuilder->addSystemPrefix();
        }

        $this->identityBuilder->add($identityArray[self::TAG]);

        if (@$identityArray[self::PARAM_ROUTE]) {
            $this->identityBuilder->addIdentifier($identityArray[self::PARAM_ROUTE]);
        }

        if ($identityArray[self::HAS_PARAMS]) {
            $this->identityBuilder->addHashOfQueryParams();
        }

        $tags = $this->identityBuilder->tags();
        $key = end($tags);

        return [
            'tags' => $tags,
            'lifetime' => $identityArray[self::HAS_COMPANY] ? CacheEnum::DEFAULT_TIME : CacheEnum::FOREVER_TIME,
            'cacheable' => ! $this->criteria->export(),
            'key' => md5($key),
        ];
    }

    private function strategies(string $strategy): array
    {
        return [
            RouteNameEnum::LIST_ROLES => [
                self::HAS_COMPANY => false,
                self::TAG => CacheEnum::LIST_ROLES,
                self::HAS_PARAMS => false,
            ],
            RouteNameEnum::LIST_SECTORS => [
                self::HAS_COMPANY => true,
                self::TAG => CacheEnum::LIST_SECTORS,
                self::HAS_PARAMS => true,
            ],
            RouteNameEnum::LIST_SECTOR_USERS => [
                self::HAS_COMPANY => true,
                self::PARAM_ROUTE => RouteParamEnum::SECTOR_ID,
                self::TAG => CacheEnum::LIST_SECTOR_USERS,
                self::HAS_PARAMS => true,
            ],
            RouteNameEnum::LIST_USERS => [
                self::HAS_COMPANY => true,
                self::TAG => CacheEnum::LIST_USERS,
                self::HAS_PARAMS => true,
            ],
            RouteNameEnum::LIST_MASSIVE_USER_UPLOADS => [
                self::HAS_COMPANY => true,
                self::TAG => CacheEnum::LIST_MASSIVE_USER_UPLOADS,
                self::HAS_PARAMS => true,
            ],
            RouteNameEnum::LIST_EQUIPMENTS => [
                self::HAS_COMPANY => true,
                self::TAG => CacheEnum::LIST_EQUIPMENTS,
                self::HAS_PARAMS => true,
            ],
            RouteNameEnum::LIST_MASSIVE_EQUIPMENT_UPLOADS => [
                self::HAS_COMPANY => true,
                self::TAG => CacheEnum::LIST_EQUIPMENTS,
                self::HAS_PARAMS => true,
            ],
            RouteNameEnum::LIST_EQUIPMENT_TYPES => [
                self::HAS_COMPANY => true,
                self::TAG => CacheEnum::LIST_EQUIPMENT_TYPES,
                self::HAS_PARAMS => true,
            ],
            RouteNameEnum::LIST_MASSIVE_EQUIPMENT_TYPE_UPLOADS => [
                self::HAS_COMPANY => true,
                self::TAG => CacheEnum::LIST_MASSIVE_EQUIPMENT_TYPE_UPLOADS,
                self::HAS_PARAMS => true,
            ],
            RouteNameEnum::LIST_EQUIPMENT_ASSIGNMENTS => [
                self::HAS_COMPANY => true,
                self::TAG => CacheEnum::LIST_EQUIPMENT_ASSIGNMENTS,
                self::HAS_PARAMS => true,
            ],
            RouteNameEnum::LIST_EQUIPMENT_ASSIGNMENT_HISTORY => [
                self::HAS_COMPANY => true,
                self::TAG => CacheEnum::LIST_EQUIPMENT_ASSIGNMENT_HISTORY,
                self::HAS_PARAMS => true,
            ],
            RouteNameEnum::LIST_EQUIPMENT_AUDITS => [
                self::HAS_COMPANY => true,
                self::TAG => CacheEnum::LIST_EQUIPMENT_AUDITS,
                self::HAS_PARAMS => true,
            ],
            RouteNameEnum::LIST_EQUIPMENT_AUDIT_AUDITED => [
                self::HAS_COMPANY => true,
                self::PARAM_ROUTE => RouteParamEnum::EQUIPMENT_AUDIT_ID,
                self::TAG => CacheEnum::LIST_EQUIPMENT_AUDIT_AUDITED,
                self::HAS_PARAMS => true,
            ],
        ][$strategy];
    }
}
