<?php

namespace Infrastructure\Storage;

use Illuminate\Support\Facades\Storage;

class S3Storage
{
    private const PRIVATE_VISIBILITY = 'private';

    private object $s3;

    public function __construct()
    {
        $this->s3 = Storage::disk(config('filesystems.default'));
    }

    public function delete(string $path): bool
    {
        return $this->s3->delete($path);
    }

    public function download(string $path)
    {
        return $this->s3->download($path);
    }

    public function get(string $path)
    {
        return $this->s3->get($path);
    }

    public function put(string $path, $content, $options = null): string
    {
        return $this->s3->put($path, $content, $options);
    }

    public function putPrivate(string $path, $content): string
    {
        return $this->s3->put($path, $content, self::PRIVATE_VISIBILITY);
    }

    public function stream(string $relativePath)
    {
        $s3Client = $this->s3->getDriver()->getAdapter()->getClient();
        $s3Client->registerStreamWrapper();

        $filesystemDriver = config('filesystems.default');

        $context = stream_context_create([
            's3' => [
                'seekable' => true,
            ],
        ]);

        $s3Path = 's3://' . config("filesystems.disks.{$filesystemDriver}.bucket") . '/' . $relativePath;

        return fopen($s3Path, 'r', false, $context);
    }

    public function url(string $path): ?string
    {
        return $this->s3->url($path);
    }
}
