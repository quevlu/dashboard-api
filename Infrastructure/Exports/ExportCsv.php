<?php

namespace Infrastructure\Exports;

use App\Http\Exportables\AbstractExportable;

class ExportCsv
{
    public function __invoke(object $builder, AbstractExportable $exportable)
    {
        return response()->streamDownload(
            function () use ($builder, $exportable) {
                $file = fopen('php://output', 'w+');
                fputcsv($file, $exportable->headers());

                $builder->lazy()->each(function ($record) use ($file, $exportable) {
                    fputcsv($file, $exportable->export($record));
                });

                fclose($file);
            },
            $exportable->nameFile(),
            [
                'Content-type' => 'text/csv',
                'Pragma' => 'no-cache',
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'Expires' => '0',
                'Content-Disposition' => 'attachment; filename=' . $exportable->nameFile(),
            ]
        );
    }
}
