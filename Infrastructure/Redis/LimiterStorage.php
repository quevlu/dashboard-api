<?php

namespace Infrastructure\Redis;

use Illuminate\Contracts\Cache\Repository;
use Illuminate\Support\Facades\Cache;

class LimiterStorage
{
    private Repository $cache;

    public function __construct()
    {
        $this->cache = Cache::store(config('cache.limiter'));
    }

    public function getCache()
    {
        return $this->cache;
    }
}
