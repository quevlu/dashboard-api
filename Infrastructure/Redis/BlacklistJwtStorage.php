<?php

namespace Infrastructure\Redis;

use Illuminate\Support\Facades\Cache;
use Tymon\JWTAuth\Providers\Storage\Illuminate;

class BlacklistJwtStorage extends Illuminate
{
    public function __construct()
    {
        $this->cache = Cache::store(config('jwt.connection'));
    }
}
