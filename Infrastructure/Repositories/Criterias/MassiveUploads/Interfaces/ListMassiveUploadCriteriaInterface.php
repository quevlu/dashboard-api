<?php

namespace Infrastructure\Repositories\Criterias\MassiveUploads\Interfaces;

interface ListMassiveUploadCriteriaInterface
{
    public function companyId(): string;

    public function orderBy(): string;

    public function type(): string;
}
