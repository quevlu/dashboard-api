<?php

namespace Infrastructure\Repositories\Criterias\MassiveUploads;

use Infrastructure\Repositories\Criterias\BaseCriteria;
use Binaccle\Models\MassiveUploads\MassiveUpload;

abstract class AbstractListMassiveUploadCriteria extends BaseCriteria
{
    public const ALLOWED_ORDER = [
        MassiveUpload::CREATED_AT,
    ];

    public const DEFAULT_ORDER = [
        MassiveUpload::CREATED_AT => self::ORDER_DESC,
    ];

    protected const ALLOWED_FILTERS = [];
}
