<?php

namespace Infrastructure\Repositories\Criterias\Sectors;

use Binaccle\Models\Sectors\Sector;
use Infrastructure\Repositories\Criterias\BaseCriteria;

class ListSectorCriteria extends BaseCriteria
{
    public const ALLOWED_ORDER = [
        Sector::CREATED_AT,
        Sector::NAME,
    ];

    public const DEFAULT_ORDER = [
        Sector::CREATED_AT => self::ORDER_DESC,
    ];

    protected const ALLOWED_FILTERS = [
        Sector::NAME,
    ];
}
