<?php

namespace Infrastructure\Repositories\Criterias\Sectors;

use Infrastructure\Repositories\Criterias\BaseCriteria;
use Binaccle\Models\Sectors\Sector;

class ListUserSectorCriteria extends BaseCriteria
{
    public const ALLOWED_ORDER = [
    ];

    public const DEFAULT_ORDER = [
        Sector::CREATED_AT => self::ORDER_DESC,
    ];

    protected const ALLOWED_FILTERS = [
    ];

    public function sectorId(): string
    {
        return $this->route('sectorId');
    }
}
