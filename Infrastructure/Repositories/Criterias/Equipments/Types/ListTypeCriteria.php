<?php

namespace Infrastructure\Repositories\Criterias\Equipments\Types;

use Infrastructure\Repositories\Criterias\BaseCriteria;
use Binaccle\Models\Equipments\EquipmentType;

class ListTypeCriteria extends BaseCriteria
{
    public const ALLOWED_ORDER = [];

    public const BRAND = EquipmentType::BRAND;

    public const DEFAULT_ORDER = [
        EquipmentType::CREATED_AT => self::ORDER_DESC,
    ];

    public const MODEL = EquipmentType::MODEL;

    public const NAME = EquipmentType::NAME;

    public const PREFIX = EquipmentType::PREFIX;

    protected const ALLOWED_FILTERS = [
        self::NAME,
        self::BRAND,
        self::MODEL,
        self::PREFIX,
    ];
}
