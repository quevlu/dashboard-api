<?php

namespace Infrastructure\Repositories\Criterias\Equipments\Types;

use Infrastructure\Repositories\Criterias\MassiveUploads\AbstractListMassiveUploadCriteria;
use Infrastructure\Repositories\Criterias\MassiveUploads\Interfaces\ListMassiveUploadCriteriaInterface;
use Binaccle\Enums\MassiveUploads\MassiveUploadTypeEnum;

class ListMassiveUploadCriteria extends AbstractListMassiveUploadCriteria implements ListMassiveUploadCriteriaInterface
{
    public function type(): string
    {
        return MassiveUploadTypeEnum::EQUIPMENT_TYPES;
    }
}
