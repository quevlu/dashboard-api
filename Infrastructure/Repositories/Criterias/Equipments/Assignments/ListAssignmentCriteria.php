<?php

namespace Infrastructure\Repositories\Criterias\Equipments\Assignments;

use Infrastructure\Repositories\Criterias\BaseCriteria;
use Binaccle\Models\Equipments\EquipmentAssignment;

class ListAssignmentCriteria extends BaseCriteria
{
    public const ALLOWED_ORDER = [];

    public const DEFAULT_ORDER = [
        EquipmentAssignment::UPDATED_AT => self::ORDER_DESC,
    ];

    public const IDENTIFIER = 'identifier';

    public const SECTOR = 'sector';

    public const USER = 'user';

    protected const ALLOWED_FILTERS = [
        self::USER,
        self::SECTOR,
        self::IDENTIFIER,
    ];
}
