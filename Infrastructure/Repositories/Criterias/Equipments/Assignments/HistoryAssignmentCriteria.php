<?php

namespace Infrastructure\Repositories\Criterias\Equipments\Assignments;

use Infrastructure\Repositories\Criterias\BaseCriteria;
use Binaccle\Models\Equipments\EquipmentAssignment;

class HistoryAssignmentCriteria extends BaseCriteria
{
    public const ALLOWED_ORDER = [];

    public const DEFAULT_ORDER = [
        EquipmentAssignment::UPDATED_AT => self::ORDER_DESC,
        EquipmentAssignment::ACTIVE => self::ORDER_DESC,
    ];

    protected const ALLOWED_FILTERS = [];

    public function equipmentId(): string
    {
        return $this->route('equipmentId');
    }
}
