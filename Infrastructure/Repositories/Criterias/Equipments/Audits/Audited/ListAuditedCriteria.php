<?php

namespace Infrastructure\Repositories\Criterias\Equipments\Audits\Audited;

use Infrastructure\Repositories\Criterias\BaseCriteria;
use Binaccle\Models\Equipments\EquipmentAuditAudited;

class ListAuditedCriteria extends BaseCriteria
{
    public const ALLOWED_ORDER = [];

    public const DEFAULT_ORDER = [
        EquipmentAuditAudited::UPDATED_AT => self::ORDER_DESC,
    ];

    protected const ALLOWED_FILTERS = [
    ];

    public function equipmentAuditId(): string
    {
        return $this->route('equipmentAuditId');
    }
}
