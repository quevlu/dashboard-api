<?php

namespace Infrastructure\Repositories\Criterias\Equipments\Audits;

use Infrastructure\Repositories\Criterias\BaseCriteria;
use Binaccle\Models\Equipments\EquipmentAudit;

class ListAuditCriteria extends BaseCriteria
{
    public const ALLOWED_ORDER = [];

    public const DEFAULT_ORDER = [
        EquipmentAudit::UPDATED_AT => self::ORDER_DESC,
    ];

    public const SECTOR = 'sector';

    public const STATE = 'state';

    public const TITLE = 'title';

    protected const ALLOWED_FILTERS = [
        self::SECTOR,
        self::STATE,
        self::TITLE,
    ];
}
