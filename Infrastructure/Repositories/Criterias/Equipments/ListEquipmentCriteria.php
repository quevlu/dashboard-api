<?php

namespace Infrastructure\Repositories\Criterias\Equipments;

use Infrastructure\Repositories\Criterias\BaseCriteria;
use Binaccle\Models\Equipments\Equipment;

class ListEquipmentCriteria extends BaseCriteria
{
    public const ALLOWED_ORDER = [
        Equipment::CREATED_AT,
        Equipment::IDENTIFIER,
    ];

    public const DEFAULT_ORDER = [
        Equipment::IDENTIFIER => self::ORDER_DESC,
    ];

    public const IDENTIFIER = Equipment::IDENTIFIER;

    protected const ALLOWED_FILTERS = [
        self::IDENTIFIER,
    ];
}
