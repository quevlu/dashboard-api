<?php

namespace Infrastructure\Repositories\Criterias;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class BaseCriteria extends FormRequest
{
    public const CSV = 'csv';

    public const OFFSETS = [
        15,
        30,
        50,
    ];

    public const ORDER_ASC = 'asc';

    public const ORDER_BY = 'order_by';

    public const ORDER_DESC = 'desc';

    public const ORDER_VALUES = [
        'asc',
        'desc',
    ];

    public const PAGE = 'page';

    public const TOTAL = 'total';

    private const DEFAULT_OFFSET = 15;

    private const OFFSET = 'offset';

    public function companyId(): string
    {
        return auth()->user()->companyId();
    }

    public function export(): bool
    {
        return $this->has(self::CSV);
    }

    public function hashOfQueryParams()
    {
        if ($params = $this->all()) {
            return sha1(json_encode($params));
        }
    }

    public function identifier(string $identifier): string
    {
        return $this->route($identifier);
    }

    public function orderBy(): string
    {
        $ordersFromQuery = $this->query(self::ORDER_BY) ?? static::DEFAULT_ORDER;
        $orders = [];

        foreach ($ordersFromQuery as $column => $order) {
            $orders[] = "{$column} {$order}";
        }

        return implode(',', $orders);
    }

    public function page(): int
    {
        return (int) $this->query(self::PAGE) ?? 1;
    }

    public function pagination(): int
    {
        $offset = $this->query(self::OFFSET, self::DEFAULT_OFFSET);

        return in_array($offset, self::OFFSETS) ? $offset : self::DEFAULT_OFFSET;
    }

    public function rules(): array
    {
        return [];
    }

    public function total(): bool
    {
        return $this->has(self::TOTAL);
    }

    public function validate(): void
    {
        $allowedParams = array_merge(
            [
                self::TOTAL,
                self::ORDER_BY,
                self::PAGE,
                self::OFFSET,
                self::CSV,
            ],
            static::ALLOWED_ORDER,
            static::ALLOWED_FILTERS
        );

        $presentQueryParams = array_keys($this->query());
        $hasANotAllowedParam = (bool) array_diff($presentQueryParams, $allowedParams);
        $hasANotAllowedOrder = false;

        if ($ordersFromQuery = $this->query(self::ORDER_BY)) {
            foreach ($ordersFromQuery as $column => $order) {
                if (! in_array($column, static::ALLOWED_ORDER) || ! in_array($order, self::ORDER_VALUES)) {
                    $hasANotAllowedOrder = true;
                    break;
                }
            }
        }

        if ($hasANotAllowedOrder || $hasANotAllowedParam) {
            throw new BadRequestException('There is a query param that it does not allowed');
        }
    }
}
