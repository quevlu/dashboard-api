<?php

namespace Infrastructure\Repositories\Criterias\Users;

use Infrastructure\Repositories\Criterias\MassiveUploads\AbstractListMassiveUploadCriteria;
use Infrastructure\Repositories\Criterias\MassiveUploads\Interfaces\ListMassiveUploadCriteriaInterface;
use Binaccle\Enums\MassiveUploads\MassiveUploadTypeEnum;

class ListMassiveUploadCriteria extends AbstractListMassiveUploadCriteria implements ListMassiveUploadCriteriaInterface
{
    public function type(): string
    {
        return MassiveUploadTypeEnum::USERS;
    }
}
