<?php

namespace Infrastructure\Repositories\Criterias\Users;

use Infrastructure\Repositories\Criterias\BaseCriteria;
use Binaccle\Models\Users\User;

class ListUserCriteria extends BaseCriteria
{
    public const ALLOWED_ORDER = [
        User::CREATED_AT,
    ];

    public const DEFAULT_ORDER = [
        User::CREATED_AT => self::ORDER_DESC,
    ];

    public const EMAIL = User::EMAIL;

    public const IDENTITY_DOCUMENT = User::IDENTITY_DOCUMENT;

    public const NAME = User::NAME;

    protected const ALLOWED_FILTERS = [
        self::NAME,
        self::EMAIL,
        self::IDENTITY_DOCUMENT,
    ];
}
