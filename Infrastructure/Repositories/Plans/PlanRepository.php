<?php

namespace Infrastructure\Repositories\Plans;

use Binaccle\Repositories\Plans\PlanRepositoryInterface;
use Infrastructure\Repositories\AbstractRepository;

class PlanRepository extends AbstractRepository implements PlanRepositoryInterface
{
}
