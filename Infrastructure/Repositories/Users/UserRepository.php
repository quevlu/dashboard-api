<?php

namespace Infrastructure\Repositories\Users;

use Infrastructure\Repositories\AbstractRepository;
use Infrastructure\Repositories\Criterias\Sectors\ListUserSectorCriteria;
use Infrastructure\Repositories\Criterias\Users\ListUserCriteria;
use Binaccle\Models\Users\User;
use Binaccle\Repositories\Users\UserRepositoryInterface;
use Illuminate\Support\Str;

class UserRepository extends AbstractRepository implements UserRepositoryInterface
{
    public function cleanOrRemoveSectors(string $sectorId, string $userIdOrAction): void
    {
        $object = $this->model::where(User::SECTOR_ID, $sectorId);

        if (Str::isUuid($userIdOrAction)) {
            $object->where(User::ID, $userIdOrAction);
        }

        $object->update([
            User::SECTOR_ID => null,
        ]);
    }

    public function hasPassword(string $id): bool
    {
        return $this->model::whereId($id)->where(User::PASSWORD, '!=', null)->exists();
    }

    public function list(ListUserCriteria $criteria)
    {
        $object = $this->model::orderByRaw($criteria->orderBy());

        if ($criteria->has($criteria::NAME)) {
            $object->where(User::NAME, 'like', '%' . $criteria->get($criteria::NAME) . '%');
        }

        if ($criteria->has($criteria::EMAIL)) {
            $object->where(User::EMAIL, 'like', '%' . $criteria->get($criteria::EMAIL) . '%');
        }

        if ($criteria->has($criteria::IDENTITY_DOCUMENT)) {
            $object->where(User::IDENTITY_DOCUMENT, 'like', '%' . $criteria->get($criteria::IDENTITY_DOCUMENT) . '%');
        }

        $object->with(User::SECTOR_RELATIONSHIP, User::INVITATION_RELATIONSHIP, User::ROLES_RELATIONSHIP);

        return $this->handleReturn($object, $criteria);
    }

    public function listBySector(ListUserSectorCriteria $criteria)
    {
        $object = $this->model::where(User::SECTOR_ID, $criteria->sectorId());

        return $this->handleReturn($object, $criteria);
    }
}
