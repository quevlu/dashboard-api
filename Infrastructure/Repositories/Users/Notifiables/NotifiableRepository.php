<?php

namespace Infrastructure\Repositories\Users\Notifiables;

use Infrastructure\Repositories\AbstractRepository;
use Binaccle\Models\Users\UserNotifiable;
use Binaccle\Repositories\Users\Notifiables\UserNotifiableRepositoryInterface;

class NotifiableRepository extends AbstractRepository implements UserNotifiableRepositoryInterface
{
    public function findByTypeAndToken(string $type, string $token): ?UserNotifiable
    {
        return $this->model::where(UserNotifiable::TYPE, $type)
            ->where(UserNotifiable::TOKEN, $token)
            ->first();
    }

    public function findByTypeAndUserId(string $type, ?string $userId): ?UserNotifiable
    {
        return $this->model::where(UserNotifiable::USER_ID, $userId)
            ->where(UserNotifiable::TYPE, $type)
            ->first();
    }

    public function userDoesntHaveToBeVerified(string $userId): bool
    {
        return $this->model::where(UserNotifiable::USER_ID, $userId)
            ->where(UserNotifiable::TYPE, UserNotifiable::VERIFICATION_TYPE)
            ->exists();
    }
}
