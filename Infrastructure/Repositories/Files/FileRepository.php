<?php

namespace Infrastructure\Repositories\Files;

use Infrastructure\Repositories\AbstractRepository;
use Binaccle\Repositories\Files\FileRepositoryInterface;

class FileRepository extends AbstractRepository implements FileRepositoryInterface
{
}
