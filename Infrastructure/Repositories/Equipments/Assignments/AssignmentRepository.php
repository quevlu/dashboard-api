<?php

namespace Infrastructure\Repositories\Equipments\Assignments;

use Infrastructure\Repositories\AbstractRepository;
use Infrastructure\Repositories\Criterias\Equipments\Assignments\HistoryAssignmentCriteria;
use Infrastructure\Repositories\Criterias\Equipments\Assignments\ListAssignmentCriteria;
use Binaccle\Models\Equipments\Equipment;
use Binaccle\Models\Equipments\EquipmentAssignment;
use Binaccle\Models\Equipments\EquipmentType;
use Binaccle\Models\Sectors\Sector;
use Binaccle\Models\Users\User;
use Binaccle\Repositories\Equipments\Assignments\AssignmentRepositoryInterface;
use Binaccle\Repositories\Equipments\EquipmentRepositoryInterface;
use Binaccle\Repositories\Equipments\Types\TypeRepositoryInterface;
use Binaccle\Repositories\Sectors\SectorRepositoryInterface;
use Binaccle\Repositories\Users\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class AssignmentRepository extends AbstractRepository implements AssignmentRepositoryInterface
{
    public function activeEquipmentsBy(string $actor, string $actorId): ?Builder
    {
        return $this->model::where(EquipmentAssignment::ACTIVE, true)
            ->where($actor, $actorId);
    }

    public function equipmentIsAssigned(string $equipmentId): bool
    {
        return $this->model::where(EquipmentAssignment::ACTIVE, true)
            ->where(EquipmentAssignment::EQUIPMENT_ID, $equipmentId)
            ->exists();
    }

    public function hasAnEquipmentAssigned(string $actorId): bool
    {
        return $this->model::where(EquipmentAssignment::ACTIVE, true)
            ->where(function (Builder $query) use ($actorId) {
                $query->where(EquipmentAssignment::USER_ID, $actorId)
                    ->orWhere(EquipmentAssignment::SECTOR_ID, $actorId);
            })
            ->exists();
    }

    public function history(HistoryAssignmentCriteria $criteria)
    {
        $object = $this->model::where(EquipmentAssignment::EQUIPMENT_ID, $criteria->equipmentId())
            ->with(
                EquipmentAssignment::USER_RELATIONSHIP,
                EquipmentAssignment::SECTOR_RELATIONSHIP
            )
            ->orderByRaw($criteria->orderBy());

        return $this->handleReturn($object, $criteria);
    }

    public function isTheSameDestination(string $equipmentId, ?string $userId, ?string $sectorId): bool
    {
        return $this->model::where(EquipmentAssignment::EQUIPMENT_ID, $equipmentId)
            ->where(EquipmentAssignment::USER_ID, $userId)
            ->where(EquipmentAssignment::SECTOR_ID, $sectorId)
            ->where(EquipmentAssignment::ACTIVE, true)
            ->exists();
    }

    public function list(ListAssignmentCriteria $criteria)
    {
        $equipmentAssignmentTable = $this->table();
        $equipmentTable = app(EquipmentRepositoryInterface::class)->table();
        $equipmentTypeTable = app(TypeRepositoryInterface::class)->table();
        $userTable = app(UserRepositoryInterface::class)->table();
        $sectorTable = app(SectorRepositoryInterface::class)->table();

        $equipmentAssignmentAssignedTable = $equipmentAssignmentTable;

        $object = DB::table($equipmentTable)
        ->join($equipmentTypeTable, function (JoinClause $join) use ($equipmentTypeTable, $equipmentTable) {
            $join->on(
                "{$equipmentTypeTable}." . EquipmentType::ID,
                "{$equipmentTable}." . Equipment::EQUIPMENT_TYPE_ID
            );
        })
        ->join($equipmentAssignmentTable, function (JoinClause $join) use ($equipmentAssignmentTable, $equipmentTable, $criteria) {
            $join->on(
                "{$equipmentAssignmentTable}." . EquipmentAssignment::EQUIPMENT_ID,
                "{$equipmentTable}." . Equipment::ID
            );

            if ($criteria->has($criteria::SECTOR)) {
                $join->where(EquipmentAssignment::SECTOR_ID, $criteria->get($criteria::SECTOR));
            }

            if ($criteria->has($criteria::USER)) {
                $join->where(EquipmentAssignment::USER_ID, $criteria->get($criteria::USER));
            }
        });

        $equipmentAssignmentAssignedTable .= 'assigned';
        $object->leftJoin(
            "{$equipmentAssignmentTable} as {$equipmentAssignmentAssignedTable}",
            function (JoinClause $join) use ($equipmentAssignmentAssignedTable, $equipmentTable) {
                $join->on(
                    "{$equipmentAssignmentAssignedTable}." . EquipmentAssignment::EQUIPMENT_ID,
                    "{$equipmentTable}." . Equipment::ID
                )
                ->where("{$equipmentAssignmentAssignedTable}." . EquipmentAssignment::ACTIVE, true);
            }
        );

        $object->leftJoin($userTable, function (JoinClause $join) use ($userTable, $equipmentAssignmentAssignedTable) {
            $join->on(
                "{$userTable}." . User::ID,
                "{$equipmentAssignmentAssignedTable}." . EquipmentAssignment::USER_ID
            );
        })
        ->leftJoin($sectorTable, function (JoinClause $join) use ($sectorTable, $equipmentAssignmentAssignedTable) {
            $join->on(
                "{$sectorTable}." . Sector::ID,
                "{$equipmentAssignmentAssignedTable}." . EquipmentAssignment::SECTOR_ID
            );
        })
        ->orderByRaw("{$equipmentAssignmentTable}." . $criteria->orderBy());

        if ($criteria->has($criteria::IDENTIFIER)) {
            $object->whereRaw(
                "CONCAT({$equipmentTypeTable}." . EquipmentType::PREFIX . ",{$equipmentTable}." . Equipment::IDENTIFIER . ') LIKE ?',
                [
                    '%' . $criteria->get($criteria::IDENTIFIER) . '%',
                ]
            );
        }

        $object->where("{$equipmentTable}." . Equipment::COMPANY_ID, $criteria->companyId())
            ->select(
                "{$equipmentTable}.*",
                "{$equipmentTypeTable}." . EquipmentType::NAME,
                "{$equipmentTypeTable}." . EquipmentType::PREFIX,
                DB::raw("IF({$userTable}." . User::NAME . " is not null,{$userTable}." . User::NAME . ",{$sectorTable}." . Sector::NAME . ') as assigned'),
                DB::raw('(' . $this->totalAssignments($equipmentTable)->toSql() . ') as ' . Equipment::ASSIGNMENTS_COUNT)
            )
            ->groupBy("{$equipmentTable}." . Equipment::ID);

        return $this->handleReturn($object, $criteria);
    }

    public function unassign($mixedValue): void
    {
        $equipmentId = $mixedValue;
        $observation = null;

        if (is_object($mixedValue)) {
            $observation = $mixedValue->observation();
            $equipmentId = $mixedValue->equipmentId();
        }

        $equipmentAssignment = $this->model::where(EquipmentAssignment::EQUIPMENT_ID, $equipmentId)
            ->where(EquipmentAssignment::ACTIVE, true)
            ->first();

        if ($equipmentAssignment) {
            $equipmentAssignment->unassign($observation);
        }
    }

    public function view(string $equipmentId): ?Equipment
    {
        $equipmentModel = app(EquipmentRepositoryInterface::class);

        return $equipmentModel->model()::where(Equipment::ID, $equipmentId)
            ->withTrashed()
            ->with(
                Equipment::EQUIPMENT_TYPE_RELATIONSHIP,
                Equipment::EQUIPMENT_ASSIGNED_RELATIONSHIP,
                Equipment::EQUIPMENT_ASSIGNED_RELATIONSHIP . '.' . EquipmentAssignment::USER_RELATIONSHIP,
                Equipment::EQUIPMENT_ASSIGNED_RELATIONSHIP . '.' . EquipmentAssignment::SECTOR_RELATIONSHIP
            )
            ->firstOrFail();
    }

    private function totalAssignments($equipmentTable): object
    {
        return DB::table($this->table())->selectRaw(DB::raw('count(id)'))
            ->whereRaw(EquipmentAssignment::EQUIPMENT_ID . " = {$equipmentTable}." . Equipment::ID);
    }
}
