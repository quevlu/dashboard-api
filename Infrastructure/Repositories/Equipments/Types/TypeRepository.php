<?php

namespace Infrastructure\Repositories\Equipments\Types;

use Infrastructure\Repositories\AbstractRepository;
use Infrastructure\Repositories\Criterias\Equipments\Types\ListTypeCriteria;
use Binaccle\Models\Equipments\EquipmentType;
use Binaccle\Repositories\Equipments\Types\TypeRepositoryInterface;

class TypeRepository extends AbstractRepository implements TypeRepositoryInterface
{
    public function list(ListTypeCriteria $criteria)
    {
        $object = $this->model::orderByRaw($criteria->orderBy());

        if ($criteria->has($criteria::NAME)) {
            $object->where(EquipmentType::NAME, 'like', '%' . $criteria->get($criteria::NAME) . '%');
        }

        if ($criteria->has($criteria::BRAND)) {
            $object->where(EquipmentType::BRAND, 'like', '%' . $criteria->get($criteria::BRAND) . '%');
        }

        if ($criteria->has($criteria::MODEL)) {
            $object->where(EquipmentType::MODEL, 'like', '%' . $criteria->get($criteria::MODEL) . '%');
        }

        if ($criteria->has($criteria::PREFIX)) {
            $object->where(EquipmentType::PREFIX, 'like', '%' . $criteria->get($criteria::PREFIX) . '%');
        }

        return $this->handleReturn($object, $criteria);
    }
}
