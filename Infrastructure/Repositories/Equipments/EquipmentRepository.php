<?php

namespace Infrastructure\Repositories\Equipments;

use Infrastructure\Repositories\AbstractRepository;
use Infrastructure\Repositories\Criterias\Equipments\ListEquipmentCriteria;
use Binaccle\Models\Equipments\Equipment;
use Binaccle\Models\Equipments\EquipmentAssignment;
use Binaccle\Models\Equipments\EquipmentType;
use Binaccle\Models\Sectors\Sector;
use Binaccle\Models\Users\User;
use Binaccle\Repositories\Equipments\Assignments\AssignmentRepositoryInterface;
use Binaccle\Repositories\Equipments\EquipmentRepositoryInterface;
use Binaccle\Repositories\Equipments\Types\TypeRepositoryInterface;
use Binaccle\Repositories\Sectors\SectorRepositoryInterface;
use Binaccle\Repositories\Users\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class EquipmentRepository extends AbstractRepository implements EquipmentRepositoryInterface
{
    public function list(ListEquipmentCriteria $criteria)
    {
        $equipmentTable = $this->table();
        $equipmentTypeTable = app(TypeRepositoryInterface::class)->table();
        $equipmentAssignmentTable = app(AssignmentRepositoryInterface::class)->table();
        $userTable = app(UserRepositoryInterface::class)->table();
        $sectorTable = app(SectorRepositoryInterface::class)->table();

        $object = DB::table($equipmentTable)
        ->join($equipmentTypeTable, function (JoinClause $join) use ($equipmentTypeTable, $equipmentTable) {
            $join->on(
                "{$equipmentTypeTable}." . EquipmentType::ID,
                "{$equipmentTable}." . Equipment::EQUIPMENT_TYPE_ID
            );
        })
        ->leftJoin($equipmentAssignmentTable, function (JoinClause $join) use ($equipmentAssignmentTable, $equipmentTable) {
            $join->on(
                "{$equipmentAssignmentTable}." . EquipmentAssignment::EQUIPMENT_ID,
                "{$equipmentTable}." . Equipment::ID
            )
                ->where(EquipmentAssignment::ACTIVE, true);
        })
        ->leftJoin($userTable, function (JoinClause $join) use ($userTable, $equipmentAssignmentTable) {
            $join->on(
                "{$userTable}." . User::ID,
                "{$equipmentAssignmentTable}." . EquipmentAssignment::USER_ID
            );
        })
        ->leftJoin($sectorTable, function (JoinClause $join) use ($sectorTable, $equipmentAssignmentTable) {
            $join->on(
                "{$sectorTable}." . Sector::ID,
                "{$equipmentAssignmentTable}." . EquipmentAssignment::SECTOR_ID
            );
        })
        ->orderByRaw("{$equipmentTable}." . $criteria->orderBy());

        if ($criteria->has($criteria::IDENTIFIER)) {
            $object->whereRaw(
                "CONCAT({$equipmentTypeTable}." . EquipmentType::PREFIX . ",{$equipmentTable}." . Equipment::IDENTIFIER . ') LIKE ?',
                [
                    '%' . $criteria->get($criteria::IDENTIFIER) . '%',
                ]
            );
        }

        $object->where("{$equipmentTable}." . Equipment::COMPANY_ID, $criteria->companyId())
            ->where("{$equipmentTable}.deleted_at", null)
            ->select(
                "{$equipmentTable}.*",
                "{$equipmentTypeTable}." . EquipmentType::NAME,
                "{$equipmentTypeTable}." . EquipmentType::BRAND,
                "{$equipmentTypeTable}." . EquipmentType::MODEL,
                "{$equipmentTypeTable}." . EquipmentType::PREFIX,
                DB::raw("SUBSTRING({$equipmentTypeTable}." . EquipmentType::CHARACTERISTICS . ',1, 100) as ' . EquipmentType::CHARACTERISTICS),
                DB::raw("IF({$userTable}." . User::NAME . " is not null,{$userTable}." . User::NAME . ",{$sectorTable}." . Sector::NAME . ') as assigned')
            );

        return $this->handleReturn($object, $criteria);
    }

    public function nextIdentifier(): int
    {
        $result = @$this->model::orderBy(Equipment::IDENTIFIER, 'desc')
            ->select(Equipment::IDENTIFIER)
            ->limit(1)
            ->first()
            ->{Equipment::IDENTIFIER};

        return $result ? $result + 1 : 1;
    }

    public function whereInWithAssignments(array $equipmentIds): Collection
    {
        return $this->model::whereIn(Equipment::ID, $equipmentIds)
            ->with(Equipment::EQUIPMENT_ASSIGNED_RELATIONSHIP)
            ->get();
    }

    public function whereInWithTypes(array $equipmentIds): Collection
    {
        return $this->model::whereIn(Equipment::ID, $equipmentIds)
            ->with(Equipment::EQUIPMENT_TYPE_RELATIONSHIP)
            ->get();
    }
}
