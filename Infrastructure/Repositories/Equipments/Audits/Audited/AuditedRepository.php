<?php

namespace Infrastructure\Repositories\Equipments\Audits\Audited;

use Infrastructure\Repositories\AbstractRepository;
use Infrastructure\Repositories\Criterias\Equipments\Audits\Audited\ListAuditedCriteria;
use Infrastructure\Repositories\Equipments\Audits\AuditRepository;
use Binaccle\Models\Equipments\Equipment;
use Binaccle\Models\Equipments\EquipmentAssignment;
use Binaccle\Models\Equipments\EquipmentAudit;
use Binaccle\Models\Equipments\EquipmentAuditAudited;
use Binaccle\Repositories\Equipments\Audits\Audited\AuditedRepositoryInterface;
use Illuminate\Database\Query\Builder;

class AuditedRepository extends AbstractRepository implements AuditedRepositoryInterface
{
    public function anyHaveBeenAudited(string $equipmentAuditId, array $equipmentIds): bool
    {
        return $this->model::where(EquipmentAuditAudited::EQUIPMENT_AUDIT_ID, $equipmentAuditId)
            ->whereIn(EquipmentAuditAudited::EQUIPMENT_ID, $equipmentIds)
            ->exists();
    }

    public function deleteByEquipmentAuditId(string $equipmentAuditId): void
    {
        $this->model::where(EquipmentAuditAudited::EQUIPMENT_AUDIT_ID, $equipmentAuditId)->delete();
    }

    public function list(ListAuditedCriteria $criteria)
    {
        $equipmentAuditTable = app(AuditRepository::class)->table();

        $object = $this->model::orderByRaw($criteria->orderBy())
            ->where(EquipmentAuditAudited::EQUIPMENT_AUDIT_ID, function (Builder $query) use ($equipmentAuditTable, $criteria) {
                $query->select(EquipmentAudit::ID)
                    ->from($equipmentAuditTable)
                    ->where(EquipmentAudit::ID, $criteria->equipmentAuditId())
                    ->where(EquipmentAudit::COMPANY_ID, $criteria->companyId());
            })
            ->with(
                EquipmentAuditAudited::EQUIPMENT_RELATIONSHIP,
                EquipmentAuditAudited::EQUIPMENT_RELATIONSHIP . '.' . Equipment::EQUIPMENT_TYPE_RELATIONSHIP,
                EquipmentAuditAudited::EQUIPMENT_ASSIGNMENT_RELATIONSHIP,
                EquipmentAuditAudited::EQUIPMENT_ASSIGNMENT_RELATIONSHIP . '.' . EquipmentAssignment::SECTOR_RELATIONSHIP,
                EquipmentAuditAudited::EQUIPMENT_ASSIGNMENT_RELATIONSHIP . '.' . EquipmentAssignment::USER_RELATIONSHIP
            );

        return $this->handleReturn($object, $criteria);
    }
}
