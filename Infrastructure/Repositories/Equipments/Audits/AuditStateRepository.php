<?php

namespace Infrastructure\Repositories\Equipments\Audits;

use Infrastructure\Repositories\AbstractRepository;
use Binaccle\Repositories\Equipments\Audits\AuditStateRepositoryInterface;

class AuditStateRepository extends AbstractRepository implements AuditStateRepositoryInterface
{
}
