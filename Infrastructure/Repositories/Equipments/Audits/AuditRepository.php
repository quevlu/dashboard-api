<?php

namespace Infrastructure\Repositories\Equipments\Audits;

use Infrastructure\Repositories\AbstractRepository;
use Infrastructure\Repositories\Criterias\Equipments\Audits\ListAuditCriteria;
use Binaccle\Enums\Equipments\Audits\AuditStateEnum;
use Binaccle\Exceptions\Equipments\Audits\AuditException;
use Binaccle\Models\Equipments\EquipmentAudit;
use Binaccle\Models\Equipments\EquipmentAuditState;
use Binaccle\Repositories\Equipments\Audits\AuditRepositoryInterface;
use Binaccle\Repositories\Equipments\Audits\AuditStateRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Throwable;

class AuditRepository extends AbstractRepository implements AuditRepositoryInterface
{
    public function auditNotFailedOrFinished(string $equipmentAuditId): EquipmentAudit
    {
        try {
            return $this->model::where(EquipmentAudit::ID, $equipmentAuditId)
            ->with(EquipmentAudit::EQUIPMENT_AUDIT_STATE_RELATIONSHIP)
            ->whereHas(EquipmentAudit::EQUIPMENT_AUDIT_STATE_RELATIONSHIP, function (Builder $query) {
                $query->whereNotIn(EquipmentAuditState::NAME, [
                    AuditStateEnum::FAILED,
                    AuditStateEnum::FINISHED,
                ]);
            })
            ->firstOrFail();
        } catch (Throwable $th) {
            throw AuditException::isNotInCreatedCreatingOrUpdating();
        }
    }

    public function list(ListAuditCriteria $criteria)
    {
        $object = $this->model::orderByRaw($criteria->orderBy())
            ->with(
                EquipmentAudit::EQUIPMENT_AUDIT_STATE_RELATIONSHIP,
                EquipmentAudit::EQUIPMENT_AUDIT_SECTOR_RELATIONSHIP
            );

        if ($criteria->has($criteria::SECTOR)) {
            $object->where(EquipmentAudit::SECTOR_ID, $criteria->get($criteria::SECTOR));
        }

        if ($criteria->has($criteria::TITLE)) {
            $object->where(EquipmentAudit::TITLE, 'like', '%' . $criteria->get($criteria::TITLE) . '%');
        }

        if ($criteria->has($criteria::STATE)) {
            $object->whereRelation(EquipmentAudit::EQUIPMENT_AUDIT_STATE_RELATIONSHIP, EquipmentAuditState::ID, $criteria->get($criteria::STATE));
        }

        return $this->handleReturn($object, $criteria);
    }

    public function transition(string $equipmentAuditId, string $state): void
    {
        $equipmentAuditStateId = app(AuditStateRepositoryInterface::class)->firstOrFailBy(EquipmentAuditState::NAME, $state)->id();

        $this->model::where(EquipmentAudit::ID, $equipmentAuditId)->update([
            EquipmentAudit::EQUIPMENT_AUDIT_STATE_ID => $equipmentAuditStateId,
        ]);
    }

    public function view(string $equipmentAuditId): EquipmentAudit
    {
        return $this->model::where(EquipmentAudit::ID, $equipmentAuditId)
            ->with(
                EquipmentAudit::EQUIPMENT_AUDIT_STATE_RELATIONSHIP,
                EquipmentAudit::EQUIPMENT_AUDIT_SECTOR_RELATIONSHIP
            )
            ->firstOrFail();
    }
}
