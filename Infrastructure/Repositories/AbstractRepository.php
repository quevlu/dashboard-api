<?php

namespace Infrastructure\Repositories;

use Infrastructure\Repositories\Criterias\BaseCriteria;
use Binaccle\Dtos\Shared\PaginationDto;
use Binaccle\Repositories\AbstractRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

abstract class AbstractRepository implements AbstractRepositoryInterface
{
    protected ?string $model = null;

    protected ?object $modelObject = null;

    protected ?string $table = null;

    public function __construct()
    {
        $this->model = $this->model();
    }

    public function all(): Collection
    {
        return $this->model::all();
    }

    public function baseWhereIn(array $values, ?string $keyToSearch = null): Builder
    {
        $keyToSearch = $keyToSearch ? $keyToSearch : $this->primaryKey();

        return $this->model::whereIn($keyToSearch, $values);
    }

    public function create(object $payload): object
    {
        $model = $this->model::instance($payload);
        $model->save();

        return $model;
    }

    public function exists(string $column, $value): bool
    {
        return $this->model::where($column, $value)->exists();
    }

    public function find(string $id): ?object
    {
        return $this->model::find($id);
    }

    public function findBy(): ?array
    {
        return [];
    }

    public function findLikeBy(string $column, $value): Collection
    {
        return $this->model::where($column, 'like', "%{$value}%")->get();
    }

    public function findOrFail(string $id): object
    {
        return $this->model::findOrFail($id);
    }

    public function firstBy(string $column, $value): ?object
    {
        return $this->searchBy($column, $value)->first();
    }

    public function firstOrCreate(array $conditions, array $attributes = []): object
    {
        return $this->model::firstOrCreate($conditions, $attributes);
    }

    public function firstOrFailBy(string $column, $value): ?object
    {
        return $this->searchBy($column, $value)->firstOrFail();
    }

    public function firstOrNew(array $conditions, array $attributes = []): object
    {
        return $this->model::firstOrNew($conditions, $attributes);
    }

    public function handleReturn(object $eloquentObject, BaseCriteria $criteria)
    {
        if ($criteria->total()) {
            $total = $eloquentObject->count();

            return new PaginationDto($total);
        }

        if ($criteria->export()) {
            return $eloquentObject;
        }

        return $eloquentObject->simplePaginate($criteria->pagination());
    }

    public function insert(array $models): bool
    {
        $arrayData = array_map(function ($model) {
            return $model->toArray();
        }, $models);

        return $this->model::insert($arrayData);
    }

    public function instance(...$vars): object
    {
        return $this->model::instance(...$vars);
    }

    public function model(): string
    {
        return get_called_class()::MODEL;
    }

    public function primaryKey(): string
    {
        if (! $this->modelObject) {
            $this->modelObject = (new $this->model);
        }

        return $this->modelObject->getKeyName();
    }

    public function random(int $limit = 1)
    {
        $object = $this->model::inRandomOrder();
        $limit = abs($limit);

        if ($limit == 1) {
            return $object->first();
        }

        return $object->limit($limit)->get();
    }

    public function table(): string
    {
        if (! $this->modelObject) {
            $this->modelObject = (new $this->model);
        }

        return $this->modelObject->getTable();
    }

    public function truncate(): void
    {
        $this->model::truncate();
    }

    public function update(object $model, array $attributes): void
    {
        $model->fill($attributes);
        $model->save();
    }

    public function whereIn(array $values, ?string $keyToSearch = null): Collection
    {
        return $this->baseWhereIn($values, $keyToSearch)->get();
    }

    public function whereInCount(array $values, ?string $keyToSearch = null): ?int
    {
        return $this->baseWhereIn($values, $keyToSearch)->count();
    }

    private function searchBy(string $column, $value): object
    {
        return $this->model::where($column, $value);
    }
}
