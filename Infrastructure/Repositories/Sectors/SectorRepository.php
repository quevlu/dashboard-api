<?php

namespace Infrastructure\Repositories\Sectors;

use Binaccle\Models\Sectors\Sector;
use Binaccle\Models\Users\User;
use Binaccle\Repositories\Sectors\SectorRepositoryInterface;
use Binaccle\Repositories\Users\UserRepositoryInterface;
use Infrastructure\Repositories\AbstractRepository;
use Infrastructure\Repositories\Criterias\Sectors\ListSectorCriteria;

class SectorRepository extends AbstractRepository implements SectorRepositoryInterface
{
    public function hasUsers(string $id): bool
    {
        $userRepository = app(UserRepositoryInterface::class);

        return $userRepository->model::where(User::SECTOR_ID, $id)->exists();
    }

    public function list(ListSectorCriteria $criteria)
    {
        $object = $this->model::orderByRaw($criteria->orderBy());

        if ($criteria->has(Sector::NAME)) {
            $object->where(Sector::NAME, 'like', '%' . $criteria->get(Sector::NAME) . '%');
        }

        return $this->handleReturn($object, $criteria);
    }
}
