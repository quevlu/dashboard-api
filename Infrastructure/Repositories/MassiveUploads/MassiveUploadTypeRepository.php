<?php

namespace Infrastructure\Repositories\MassiveUploads;

use Infrastructure\Repositories\AbstractRepository;
use Binaccle\Repositories\MassiveUploads\MassiveUploadTypeRepositoryInterface;

class MassiveUploadTypeRepository extends AbstractRepository implements MassiveUploadTypeRepositoryInterface
{
}
