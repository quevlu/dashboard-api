<?php

namespace Infrastructure\Repositories\MassiveUploads;

use Infrastructure\Repositories\AbstractRepository;
use Infrastructure\Repositories\Criterias\MassiveUploads\Interfaces\ListMassiveUploadCriteriaInterface;
use Binaccle\Enums\MassiveUploads\MassiveUploadStateEnum;
use Binaccle\Enums\MassiveUploads\MassiveUploadTypeEnum;
use Binaccle\Models\MassiveUploads\MassiveUpload;
use Binaccle\Models\MassiveUploads\MassiveUploadState;
use Binaccle\Models\MassiveUploads\MassiveUploadType;
use Binaccle\Repositories\MassiveUploads\MassiveUploadRepositoryInterface;
use Binaccle\Repositories\MassiveUploads\MassiveUploadStateRepositoryInterface;
use Binaccle\Repositories\MassiveUploads\MassiveUploadTypeRepositoryInterface;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class MassiveUploadRepository extends AbstractRepository implements MassiveUploadRepositoryInterface
{
    public function isThereAnyPendingMassiveEquipmentUpload(string $companyId): bool
    {
        $massiveUploadTable = $this->table();
        $massiveUploadTypeTable = app(MassiveUploadTypeRepositoryInterface::class)->table();
        $massiveUploadStateTable = app(MassiveUploadStateRepositoryInterface::class)->table();

        return DB::table($massiveUploadTable)
            ->join($massiveUploadStateTable, function (JoinClause $join) use ($massiveUploadTable, $massiveUploadStateTable) {
                $join->on(
                    $massiveUploadTable . '.' . MassiveUpload::MASSIVE_UPLOAD_TYPE_ID,
                    $massiveUploadStateTable . '.' . MassiveUploadState::ID
                )
                ->where($massiveUploadStateTable . '.' . MassiveUploadState::NAME, MassiveUploadStateEnum::PENDING);
            })
            ->join($massiveUploadTypeTable, function (JoinClause $join) use ($massiveUploadTable, $massiveUploadTypeTable) {
                $join->on(
                    $massiveUploadTable . '.' . MassiveUpload::MASSIVE_UPLOAD_TYPE_ID,
                    $massiveUploadTypeTable . '.' . MassiveUploadType::ID
                )
                ->whereIn(
                    $massiveUploadTypeTable . '.' . MassiveUploadType::NAME,
                    [
                        MassiveUploadTypeEnum::EQUIPMENTS,
                        MassiveUploadTypeEnum::EQUIPMENT_TYPES,
                    ]
                );
            })
            ->where(MassiveUpload::COMPANY_ID, $companyId)
            ->exists();
    }

    public function list(ListMassiveUploadCriteriaInterface $criteria)
    {
        $massiveUploadTable = $this->table();
        $massiveUploadTypeTable = app(MassiveUploadTypeRepositoryInterface::class)->table();
        $massiveUploadStateTable = app(MassiveUploadStateRepositoryInterface::class)->table();

        $object = DB::table($massiveUploadTable)
            ->orderByRaw($criteria->orderBy())
            ->join(
                $massiveUploadStateTable,
                $massiveUploadStateTable . '.' . MassiveUploadState::ID,
                $massiveUploadTable . '.' . MassiveUpload::MASSIVE_UPLOAD_STATE_ID
            )
            ->join($massiveUploadTypeTable, function (JoinClause $join) use ($massiveUploadTable, $massiveUploadTypeTable, $criteria) {
                $join->on(
                    $massiveUploadTable . '.' . MassiveUpload::MASSIVE_UPLOAD_TYPE_ID,
                    $massiveUploadTypeTable . '.' . MassiveUploadType::ID
                )
                ->where($massiveUploadTypeTable . '.' . MassiveUploadType::NAME, $criteria->type());
            })
            ->where(MassiveUpload::COMPANY_ID, $criteria->companyId())
            ->select(
                $massiveUploadTable . '.' . MassiveUpload::ID,
                $massiveUploadTable . '.' . MassiveUpload::PROCESSED,
                $massiveUploadTable . '.' . MassiveUpload::ERROR,
                $massiveUploadTable . '.' . MassiveUpload::CREATED_AT,
                $massiveUploadStateTable . '.' . MassiveUploadState::NAME . ' as ' . MassiveUpload::MASSIVE_UPLOAD_STATE_RELATIONSHIP,
            );

        return $this->handleReturn($object, $criteria);
    }

    public function view(string $massiveUploadId, string $type): MassiveUpload
    {
        return $this->model::where(MassiveUpload::ID, $massiveUploadId)
            ->with(
                MassiveUpload::MASSIVE_UPLOAD_STATE_RELATIONSHIP,
                MassiveUpload::MASSIVE_UPLOAD_TYPE_RELATIONSHIP
            )
            ->whereRelation(MassiveUpload::MASSIVE_UPLOAD_TYPE_RELATIONSHIP, MassiveUploadType::NAME, $type)
            ->firstOrFail();
    }
}
