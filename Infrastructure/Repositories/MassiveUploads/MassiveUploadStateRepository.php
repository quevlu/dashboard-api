<?php

namespace Infrastructure\Repositories\MassiveUploads;

use Infrastructure\Repositories\AbstractRepository;
use Binaccle\Repositories\MassiveUploads\MassiveUploadStateRepositoryInterface;

class MassiveUploadStateRepository extends AbstractRepository implements MassiveUploadStateRepositoryInterface
{
}
