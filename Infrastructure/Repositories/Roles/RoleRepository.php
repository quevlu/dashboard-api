<?php

namespace Infrastructure\Repositories\Roles;

use Infrastructure\Repositories\AbstractRepository;
use Binaccle\Enums\Roles\RoleEnum;
use Binaccle\Models\Roles\Role;
use Binaccle\Repositories\Roles\RoleRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class RoleRepository extends AbstractRepository implements RoleRepositoryInterface
{
    public function allWithoutAdmin(): Collection
    {
        return $this->model::where(Role::NAME, '!=', RoleEnum::ADMIN)
            ->orderBy(Role::NAME, 'asc')
            ->get();
    }

    public function whereNameIn(array $roles): Collection
    {
        return $this->whereIn($roles, Role::NAME);
    }
}
