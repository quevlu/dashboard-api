<?php

namespace Infrastructure\Repositories\Companies;

use Infrastructure\Repositories\AbstractRepository;
use Binaccle\Repositories\Companies\CompanyRepositoryInterface;

class CompanyRepository extends AbstractRepository implements CompanyRepositoryInterface
{
}
