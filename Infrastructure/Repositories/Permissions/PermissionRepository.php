<?php

namespace Infrastructure\Repositories\Permissions;

use Infrastructure\Repositories\AbstractRepository;
use Binaccle\Repositories\Permissions\PermissionRepositoryInterface;

class PermissionRepository extends AbstractRepository implements PermissionRepositoryInterface
{
}
