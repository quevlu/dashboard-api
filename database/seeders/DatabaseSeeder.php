<?php

namespace Database\Seeders;

use App\Enums\AppEnum;
use Binaccle\Enums\Plans\PlanEnum;
use Binaccle\Models\Companies\Company;
use Binaccle\Models\Plans\Plan;
use Binaccle\Models\Sectors\Sector;
use Binaccle\Models\Users\User;
use Binaccle\Repositories\Plans\PlanRepositoryInterface;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        $planRepository = app(PlanRepositoryInterface::class);
        $enterprisePlanId = $planRepository->firstOrFailBy(Plan::NAME, PlanEnum::ENTERPRISE)->id();

        Company::factory([
            Company::NAME => AppEnum::COMPANY_TEST,
            Company::PLAN_ID => $enterprisePlanId,
        ])
        ->has(Sector::factory()->count(50))
        ->has(User::factory([
            User::EMAIL => AppEnum::EMAIL_TEST,
        ]))
        ->has(User::factory())
        ->create();
    }
}
