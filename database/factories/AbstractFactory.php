<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use ReflectionClass;

abstract class AbstractFactory extends Factory
{
    public static function resolveFactoryName(string $modelName): string
    {
        $resolver = static::$factoryNameResolver ?: function (string $modelName) {
            $reflection = new ReflectionClass($modelName);
            $shortModelName = $reflection->getShortName();

            return static::$namespace . $shortModelName . 'Factory';
        };

        return $resolver($modelName);
    }
}
