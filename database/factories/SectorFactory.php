<?php

namespace Database\Factories;

use Binaccle\Models\Sectors\Sector;
use Binaccle\Models\Users\User;

class SectorFactory extends AbstractFactory
{
    protected $model = Sector::class;

    public function configure(): self
    {
        return $this->afterCreating(function (Sector $sector) {
            if ($this->faker->boolean()) {
                User::factory([
                    User::SECTOR_ID => $sector->id(),
                    User::COMPANY_ID => $sector->companyId(),
                ])->count(3)
                ->create();
            }
        });
    }

    public function definition(): array
    {
        return [
            Sector::ID => randomUuid(),
            Sector::NAME => $this->faker->unique()->name(),
        ];
    }
}
