<?php

namespace Database\Factories;

use App\Enums\AppEnum;
use Binaccle\Enums\Roles\RoleEnum;
use Binaccle\Models\Users\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class UserFactory extends Factory
{
    protected $model = User::class;

    public function configure(): self
    {
        return $this->afterCreating(function (User $user) {
            $roles = RoleEnum::allWithoutAdmin();
            $role = Arr::random($roles);

            if ($user->email() == AppEnum::EMAIL_TEST) {
                $role = RoleEnum::ADMIN;
            }

            $user->assignRole($role);
        });
    }

    public function definition(): array
    {
        return [
            User::ID => randomUuid(),
            User::NAME => $this->faker->name(),
            User::EMAIL => $this->faker->unique()->safeEmail(),
            User::PASSWORD => password(AppEnum::PASSWORD_TEST),
            User::IDENTITY_DOCUMENT => $this->faker->randomNumber(),
        ];
    }
}
