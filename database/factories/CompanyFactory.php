<?php

namespace Database\Factories;

use Binaccle\Models\Companies\Company;
use Binaccle\Repositories\Plans\PlanRepositoryInterface;

class CompanyFactory extends AbstractFactory
{
    protected $model = Company::class;

    public function definition(): array
    {
        $planRepository = app(PlanRepositoryInterface::class);

        return [
            Company::ID => randomUuid(),
            Company::NAME => $this->faker->name(),
            Company::PLAN_ID => $planRepository->random()->id(),
        ];
    }
}
