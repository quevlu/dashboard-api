<?php

namespace Database\Factories;

use Binaccle\Models\Users\UserNotifiable;
use Illuminate\Support\Str;

class UserNotifiableFactory extends AbstractFactory
{
    protected $model = UserNotifiable::class;

    public function definition(): array
    {
        return [
            UserNotifiable::ID => randomUuid(),
            UserNotifiable::TOKEN => Str::random(UserNotifiable::TOKEN_LENGTH),
        ];
    }
}
