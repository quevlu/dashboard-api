<?php

use Binaccle\Models\MassiveUploads\MassiveUploadType;
use Binaccle\Repositories\MassiveUploads\MassiveUploadTypeRepositoryInterface;
use Binaccle\Traits\Migrations\MagicMigrationTrait;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMassiveUploadTypeTable extends Migration
{
    use MagicMigrationTrait;

    private const REPOSITORY = MassiveUploadTypeRepositoryInterface::class;

    public function down(): void
    {
        Schema::dropIfExists($this->table);
    }

    public function up(): void
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->uuid(MassiveUploadType::ID)->primary();
            $table->string(MassiveUploadType::NAME)->unique();
        });
    }
}
