<?php

use Binaccle\Models\Equipments\Equipment;
use Binaccle\Models\Equipments\EquipmentAssignment;
use Binaccle\Models\Sectors\Sector;
use Binaccle\Models\Users\User;
use Binaccle\Repositories\Equipments\Assignments\AssignmentRepositoryInterface;
use Binaccle\Repositories\Equipments\EquipmentRepositoryInterface;
use Binaccle\Repositories\Sectors\SectorRepositoryInterface;
use Binaccle\Repositories\Users\UserRepositoryInterface;
use Binaccle\Traits\Migrations\MagicMigrationTrait;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEquipmentAssignments extends Migration
{
    use MagicMigrationTrait;

    private const REPOSITORY = AssignmentRepositoryInterface::class;

    private EquipmentRepositoryInterface $equipmentRepository;

    private SectorRepositoryInterface $sectorRepository;

    private UserRepositoryInterface $userRepository;

    public function down()
    {
        Schema::dropIfExists($this->table);
    }

    public function up()
    {
        $this->sectorRepository = app(SectorRepositoryInterface::class);
        $this->userRepository = app(UserRepositoryInterface::class);
        $this->equipmentRepository = app(EquipmentRepositoryInterface::class);

        Schema::create($this->table, function (Blueprint $table) {
            $table->uuid(EquipmentAssignment::ID)->primary();
            $table->foreignUuid(EquipmentAssignment::SECTOR_ID)->nullable()->references(Sector::ID)->on($this->sectorRepository->table());
            $table->foreignUuid(EquipmentAssignment::USER_ID)->nullable()->references(User::ID)->on($this->userRepository->table());
            $table->foreignUuid(EquipmentAssignment::EQUIPMENT_ID)->nullable()->references(Equipment::ID)->on($this->equipmentRepository->table());
            $table->boolean(EquipmentAssignment::ACTIVE)->default(true)->index();
            $table->tinyText(EquipmentAssignment::OBSERVATION)->nullable()->index();
            $this->nativeTimestamps($table);
        });
    }
}
