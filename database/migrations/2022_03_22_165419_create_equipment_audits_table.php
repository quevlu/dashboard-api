<?php

use Binaccle\Models\Companies\Company;
use Binaccle\Models\Equipments\EquipmentAudit;
use Binaccle\Models\Equipments\EquipmentAuditState;
use Binaccle\Models\Sectors\Sector;
use Binaccle\Repositories\Companies\CompanyRepositoryInterface;
use Binaccle\Repositories\Equipments\Audits\AuditRepositoryInterface;
use Binaccle\Repositories\Equipments\Audits\AuditStateRepositoryInterface;
use Binaccle\Repositories\Sectors\SectorRepositoryInterface;
use Binaccle\Traits\Migrations\MagicMigrationTrait;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEquipmentAuditsTable extends Migration
{
    use MagicMigrationTrait;

    private const REPOSITORY = AuditRepositoryInterface::class;

    private CompanyRepositoryInterface $companyRepository;

    private AuditStateRepositoryInterface $equipmentAuditStateRepository;

    private SectorRepositoryInterface $sectorRepository;

    public function down()
    {
        Schema::dropIfExists($this->table);
    }

    public function up()
    {
        $this->companyRepository = app(CompanyRepositoryInterface::class);
        $this->sectorRepository = app(SectorRepositoryInterface::class);
        $this->equipmentAuditStateRepository = app(AuditStateRepositoryInterface::class);

        Schema::create($this->table, function (Blueprint $table) {
            $table->uuid(EquipmentAudit::ID)->primary();
            $table->string(EquipmentAudit::TITLE)->index();

            $table->foreignUuid(EquipmentAudit::SECTOR_ID)
                ->nullable()
                ->references(Sector::ID)
                ->on($this->sectorRepository->table())
                ->onDelete('cascade');

            $table->foreignUuid(EquipmentAudit::COMPANY_ID)
                ->references(Company::ID)
                ->on($this->companyRepository->table())
                ->onDelete('cascade');

            $table->foreignUuid(EquipmentAudit::EQUIPMENT_AUDIT_STATE_ID)
                ->references(EquipmentAuditState::ID)
                ->on($this->equipmentAuditStateRepository->table())
                ->onDelete('cascade');

            $this->nativeTimestamps($table);
            $this->nativeDeletedAt($table);
        });
    }
}
