<?php

use Binaccle\Models\Companies\Company;
use Binaccle\Models\Equipments\EquipmentType;
use Binaccle\Repositories\Companies\CompanyRepositoryInterface;
use Binaccle\Repositories\Equipments\Types\TypeRepositoryInterface;
use Binaccle\Traits\Migrations\MagicMigrationTrait;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEquipmentTypesTable extends Migration
{
    use MagicMigrationTrait;

    private const REPOSITORY = TypeRepositoryInterface::class;

    private CompanyRepositoryInterface $companyRepository;

    public function down()
    {
        Schema::dropIfExists($this->table);
    }

    public function up()
    {
        $this->companyRepository = app(CompanyRepositoryInterface::class);

        Schema::create($this->table, function (Blueprint $table) {
            $table->uuid(EquipmentType::ID)->primary();
            $table->string(EquipmentType::NAME);
            $table->string(EquipmentType::BRAND)->nullable()->index();
            $table->string(EquipmentType::MODEL)->nullable()->index();
            $table->string(EquipmentType::PREFIX)->index();
            $table->text(EquipmentType::CHARACTERISTICS)->nullable()->index();
            $table->foreignUuid(EquipmentType::COMPANY_ID)->references(Company::ID)->on($this->companyRepository->table())->onDelete('cascade');
            $table->unique([EquipmentType::NAME, EquipmentType::COMPANY_ID, EquipmentType::DELETED_AT]);
            $this->nativeTimestamps($table);
            $this->nativeDeletedAt($table);
        });
    }
}
