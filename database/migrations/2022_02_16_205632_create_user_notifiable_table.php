<?php

use Binaccle\Models\Users\UserNotifiable;
use Binaccle\Repositories\Users\Notifiables\UserNotifiableRepositoryInterface;
use Binaccle\Repositories\Users\UserRepositoryInterface;
use Binaccle\Traits\Migrations\MagicMigrationTrait;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserNotifiableTable extends Migration
{
    use MagicMigrationTrait;

    private const REPOSITORY = UserNotifiableRepositoryInterface::class;

    public function down(): void
    {
        Schema::dropIfExists($this->table);
    }

    public function up(): void
    {
        $userRepository = app(UserRepositoryInterface::class);

        Schema::create($this->table, function (Blueprint $table) use ($userRepository) {
            $table->uuid(UserNotifiable::ID)->primary();
            $table->foreignUuid(UserNotifiable::USER_ID)->references(UserNotifiable::ID)->on($userRepository->table())->onDelete('cascade');
            $table->string(UserNotifiable::TOKEN)->index();
            $table->integer(UserNotifiable::LEFT)->default(UserNotifiable::LEFT_LIMIT)->index();
            $table->enum(UserNotifiable::TYPE, [
                UserNotifiable::VERIFICATION_TYPE,
                UserNotifiable::RESET_TYPE,
                UserNotifiable::INVITATION_TYPE,
            ])->index();

            $table->unique([UserNotifiable::USER_ID, UserNotifiable::TYPE]);
            $this->nativeTimestamps($table);
        });
    }
}
