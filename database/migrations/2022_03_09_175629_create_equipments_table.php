<?php

use Binaccle\Models\Companies\Company;
use Binaccle\Models\Equipments\Equipment;
use Binaccle\Models\Equipments\EquipmentType;
use Binaccle\Repositories\Companies\CompanyRepositoryInterface;
use Binaccle\Repositories\Equipments\EquipmentRepositoryInterface;
use Binaccle\Repositories\Equipments\Types\TypeRepositoryInterface;
use Binaccle\Traits\Migrations\MagicMigrationTrait;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEquipmentsTable extends Migration
{
    use MagicMigrationTrait;

    private const REPOSITORY = EquipmentRepositoryInterface::class;

    private CompanyRepositoryInterface $companyRepository;

    private TypeRepositoryInterface $equipmentTypeRepository;

    public function down()
    {
        Schema::dropIfExists($this->table);
    }

    public function up()
    {
        $this->companyRepository = app(CompanyRepositoryInterface::class);
        $this->equipmentTypeRepository = app(TypeRepositoryInterface::class);

        Schema::create($this->table, function (Blueprint $table) {
            $table->uuid(Equipment::ID)->primary();
            $table->integer(Equipment::IDENTIFIER)->index();
            $table->foreignUuid(Equipment::EQUIPMENT_TYPE_ID)->references(EquipmentType::ID)->on($this->equipmentTypeRepository->table())->onDelete('cascade');
            $table->foreignUuid(Equipment::COMPANY_ID)->references(Company::ID)->on($this->companyRepository->table())->onDelete('cascade');
            $table->tinyText(Equipment::OBSERVATION)->nullable()->index();
            $this->nativeTimestamps($table);
            $this->nativeDeletedAt($table);
            $table->unique([Equipment::COMPANY_ID, Equipment::IDENTIFIER]);
        });
    }
}
