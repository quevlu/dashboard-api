<?php

use Binaccle\Models\Equipments\Equipment;
use Binaccle\Models\Equipments\EquipmentAssignment;
use Binaccle\Models\Equipments\EquipmentAudit;
use Binaccle\Models\Equipments\EquipmentAuditAudited;
use Binaccle\Repositories\Equipments\Assignments\AssignmentRepositoryInterface;
use Binaccle\Repositories\Equipments\Audits\Audited\AuditedRepositoryInterface;
use Binaccle\Repositories\Equipments\Audits\AuditRepositoryInterface;
use Binaccle\Repositories\Equipments\EquipmentRepositoryInterface;
use Binaccle\Traits\Migrations\MagicMigrationTrait;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEquipmentAuditAuditedTable extends Migration
{
    use MagicMigrationTrait;

    private const REPOSITORY = AuditedRepositoryInterface::class;

    private AssignmentRepositoryInterface $equipmentAssignmentRepository;

    private AuditRepositoryInterface $equipmentAuditRepository;

    private EquipmentRepositoryInterface $equipmentRepository;

    public function down()
    {
        Schema::dropIfExists($this->table);
    }

    public function up()
    {
        $this->equipmentRepository = app(EquipmentRepositoryInterface::class);
        $this->equipmentAuditRepository = app(AuditRepositoryInterface::class);
        $this->equipmentAssignmentRepository = app(AssignmentRepositoryInterface::class);

        Schema::create($this->table, function (Blueprint $table) {
            $table->uuid(EquipmentAuditAudited::ID)->primary();
            $table->boolean(EquipmentAuditAudited::AUDITED)->nullable()->index();

            $table->foreignUuid(EquipmentAuditAudited::EQUIPMENT_AUDIT_ID)
                ->nullable()
                ->references(EquipmentAudit::ID)
                ->on($this->equipmentAuditRepository->table())
                ->onDelete('cascade');

            $table->foreignUuid(EquipmentAuditAudited::EQUIPMENT_ID)
                ->references(Equipment::ID)
                ->on($this->equipmentRepository->table())
                ->onDelete('cascade');

            $table->foreignUuid(EquipmentAuditAudited::EQUIPMENT_ASSIGNMENT_ID)
                ->nullable()
                ->references(EquipmentAssignment::ID)
                ->on($this->equipmentAssignmentRepository->table())
                ->onDelete('cascade');

            $table->unique([EquipmentAuditAudited::EQUIPMENT_ID, EquipmentAuditAudited::EQUIPMENT_AUDIT_ID]);

            $this->nativeTimestamps($table);
        });
    }
}
