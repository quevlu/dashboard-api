<?php

use Binaccle\Models\Companies\Company;
use Binaccle\Models\Sectors\Sector;
use Binaccle\Models\Users\User;
use Binaccle\Repositories\Companies\CompanyRepositoryInterface;
use Binaccle\Repositories\Sectors\SectorRepositoryInterface;
use Binaccle\Repositories\Users\UserRepositoryInterface;
use Binaccle\Traits\Migrations\MagicMigrationTrait;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    use MagicMigrationTrait;

    private const REPOSITORY = UserRepositoryInterface::class;

    private CompanyRepositoryInterface $companyRepository;

    private SectorRepositoryInterface $sectorRepository;

    public function down(): void
    {
        Schema::dropIfExists($this->table);
    }

    public function up(): void
    {
        $this->companyRepository = app(CompanyRepositoryInterface::class);
        $this->sectorRepository = app(SectorRepositoryInterface::class);

        Schema::create($this->table, function (Blueprint $table) {
            $table->uuid(User::ID)->primary();
            $table->string(User::NAME)->index();
            $table->string(User::EMAIL)->unique();
            $table->string(User::PASSWORD)->nullable();
            $table->foreignUuid(User::COMPANY_ID)->references(Company::ID)->on($this->companyRepository->table())->onDelete('cascade');
            $table->foreignUuid(User::SECTOR_ID)->nullable()->references(Sector::ID)->on($this->sectorRepository->table());
            $table->string(User::IDENTITY_DOCUMENT)->nullable()->index();
            $table->string(User::TIMEZONE)->nullable()->index();
            $this->nativeTimestamps($table);
            $this->nativeDeletedAt($table);
        });
    }
}
