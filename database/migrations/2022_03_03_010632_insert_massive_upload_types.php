<?php

use Binaccle\Enums\MassiveUploads\MassiveUploadTypeEnum;
use Binaccle\Repositories\MassiveUploads\MassiveUploadTypeRepositoryInterface;
use Illuminate\Database\Migrations\Migration;

class InsertMassiveUploadTypes extends Migration
{
    private MassiveUploadTypeRepositoryInterface $massiveUploadTypeRepository;

    public function __construct()
    {
        $this->massiveUploadTypeRepository = app(MassiveUploadTypeRepositoryInterface::class);
    }

    public function down(): void
    {
        $this->massiveUploadTypeRepository->truncate();
    }

    public function up(): void
    {
        $massiveUploadTypes = MassiveUploadTypeEnum::types();
        $massiveUploadTypesToInsert = [];

        foreach ($massiveUploadTypes as $massiveUploadType) {
            $massiveUploadTypesToInsert[] = $this->massiveUploadTypeRepository->instance($massiveUploadType);
        }

        $this->massiveUploadTypeRepository->insert($massiveUploadTypesToInsert);
    }
}
