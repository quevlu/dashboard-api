<?php

use Binaccle\Models\Files\File;
use Binaccle\Repositories\Files\FileRepositoryInterface;
use Binaccle\Traits\Migrations\MagicMigrationTrait;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    use MagicMigrationTrait;

    private const REPOSITORY = FileRepositoryInterface::class;

    public function down(): void
    {
        Schema::dropIfExists($this->table);
    }

    public function up(): void
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->uuid(File::ID)->primary();
            $table->text(File::URL);
            $table->text(File::RELATIVE_PATH);
            $this->nativeTimestamps($table);
        });
    }
}
