<?php

use Binaccle\Enums\Plans\PlanEnum;
use Binaccle\Repositories\Plans\PlanRepositoryInterface;
use Illuminate\Database\Migrations\Migration;

class InsertPlans extends Migration
{
    private PlanRepositoryInterface $planRepository;

    public function __construct()
    {
        $this->planRepository = app(PlanRepositoryInterface::class);
    }

    public function down(): void
    {
        $this->planRepository->truncate();
    }

    public function up(): void
    {
        $plans = PlanEnum::plans();
        $plansToInsert = [];

        foreach ($plans as $plan) {
            $plansToInsert[] = $this->planRepository->instance($plan['name']);
        }

        $this->planRepository->insert($plansToInsert);
    }
}
