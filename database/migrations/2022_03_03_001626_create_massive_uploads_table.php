<?php

use Binaccle\Models\Companies\Company;
use Binaccle\Models\MassiveUploads\MassiveUpload;
use Binaccle\Models\MassiveUploads\MassiveUploadState;
use Binaccle\Models\MassiveUploads\MassiveUploadType;
use Binaccle\Repositories\Companies\CompanyRepositoryInterface;
use Binaccle\Repositories\MassiveUploads\MassiveUploadRepositoryInterface;
use Binaccle\Repositories\MassiveUploads\MassiveUploadStateRepositoryInterface;
use Binaccle\Repositories\MassiveUploads\MassiveUploadTypeRepositoryInterface;
use Binaccle\Traits\Migrations\MagicMigrationTrait;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMassiveUploadsTable extends Migration
{
    use MagicMigrationTrait;

    private const ON_DELETE = 'cascade';

    private const REPOSITORY = MassiveUploadRepositoryInterface::class;

    public function down(): void
    {
        Schema::dropIfExists($this->table);
    }

    public function up(): void
    {
        $companyRepository = app(CompanyRepositoryInterface::class);
        $massiveUploadTypeRepository = app(MassiveUploadTypeRepositoryInterface::class);
        $massiveUploadStateRepository = app(MassiveUploadStateRepositoryInterface::class);

        Schema::create($this->table, function (Blueprint $table) use ($companyRepository, $massiveUploadTypeRepository, $massiveUploadStateRepository) {
            $table->uuid(MassiveUpload::ID)->primary();
            $table->foreignUuid(MassiveUpload::COMPANY_ID)->references(Company::ID)->on($companyRepository->table())->onDelete(self::ON_DELETE);
            $table->foreignUuid(MassiveUpload::MASSIVE_UPLOAD_TYPE_ID)->references(MassiveUploadType::ID)->on($massiveUploadTypeRepository->table());
            $table->foreignUuid(MassiveUpload::MASSIVE_UPLOAD_STATE_ID)->references(MassiveUploadState::ID)->on($massiveUploadStateRepository->table());

            $table->integer(MassiveUpload::PROCESSED)->index()->nullable();
            $table->integer(MassiveUpload::ERROR)->index()->nullable();
            $table->mediumText(MassiveUpload::RESULT)->nullable();
            $this->nativeTimestamps($table);
        });
    }
}
