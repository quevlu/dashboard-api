<?php

use Binaccle\Models\Companies\Company;
use Binaccle\Models\Files\File;
use Binaccle\Models\Plans\Plan;
use Binaccle\Repositories\Companies\CompanyRepositoryInterface;
use Binaccle\Repositories\Files\FileRepositoryInterface;
use Binaccle\Repositories\Plans\PlanRepositoryInterface;
use Binaccle\Traits\Migrations\MagicMigrationTrait;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    use MagicMigrationTrait;

    private const REPOSITORY = CompanyRepositoryInterface::class;

    private FileRepositoryInterface $fileRepository;

    private PlanRepositoryInterface $planRepository;

    public function down(): void
    {
        Schema::dropIfExists($this->table);
    }

    public function up(): void
    {
        $this->fileRepository = app(FileRepositoryInterface::class);
        $this->planRepository = app(PlanRepositoryInterface::class);

        Schema::create($this->table, function (Blueprint $table) {
            $table->uuid(Company::ID)->primary();
            $table->string(Company::NAME)->index();
            $table->foreignUuid(Company::LOGO_ID)->nullable()->references(File::ID)->on($this->fileRepository->table());
            $table->foreignUuid(Company::PLAN_ID)->references(Plan::ID)->on($this->planRepository->table());
            $this->nativeTimestamps($table);
            $this->nativeDeletedAt($table);
        });
    }
}
