<?php

use Binaccle\Models\Plans\Plan;
use Binaccle\Repositories\Plans\PlanRepositoryInterface;
use Binaccle\Traits\Migrations\MagicMigrationTrait;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    use MagicMigrationTrait;

    private const REPOSITORY = PlanRepositoryInterface::class;

    public function down(): void
    {
        Schema::dropIfExists($this->table);
    }

    public function up(): void
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->uuid(Plan::ID)->primary();
            $table->string(Plan::NAME)->unique();
            $this->nativeTimestamps($table);
        });
    }
}
