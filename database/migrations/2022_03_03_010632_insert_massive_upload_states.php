<?php

use Binaccle\Enums\MassiveUploads\MassiveUploadStateEnum;
use Binaccle\Repositories\MassiveUploads\MassiveUploadStateRepositoryInterface;
use Illuminate\Database\Migrations\Migration;

class InsertMassiveUploadStates extends Migration
{
    private MassiveUploadStateRepositoryInterface $massiveUploadStateRepository;

    public function __construct()
    {
        $this->massiveUploadStateRepository = app(MassiveUploadStateRepositoryInterface::class);
    }

    public function down(): void
    {
        $this->massiveUploadStateRepository->truncate();
    }

    public function up(): void
    {
        $massiveUploadStates = MassiveUploadStateEnum::allValues();
        $massiveUploadStatesToInsert = [];

        foreach ($massiveUploadStates as $massiveUploadState) {
            $massiveUploadStatesToInsert[] = $this->massiveUploadStateRepository->instance($massiveUploadState);
        }

        $this->massiveUploadStateRepository->insert($massiveUploadStatesToInsert);
    }
}
