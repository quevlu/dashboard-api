<?php

use Binaccle\Models\MassiveUploads\MassiveUploadState;
use Binaccle\Repositories\MassiveUploads\MassiveUploadStateRepositoryInterface;
use Binaccle\Traits\Migrations\MagicMigrationTrait;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMassiveUploadStateTable extends Migration
{
    use MagicMigrationTrait;

    private const REPOSITORY = MassiveUploadStateRepositoryInterface::class;

    public function down(): void
    {
        Schema::dropIfExists($this->table);
    }

    public function up(): void
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->uuid(MassiveUploadState::ID)->primary();
            $table->string(MassiveUploadState::NAME)->unique();
        });
    }
}
