<?php

use Binaccle\Enums\Equipments\Audits\AuditStateEnum;
use Binaccle\Repositories\Equipments\Audits\AuditStateRepositoryInterface;
use Illuminate\Database\Migrations\Migration;

class InsertEquipmentAuditStates extends Migration
{
    private AuditStateRepositoryInterface $equipmentAuditStateRepository;

    public function __construct()
    {
        $this->equipmentAuditStateRepository = app(AuditStateRepositoryInterface::class);
    }

    public function down(): void
    {
        $this->equipmentAuditStateRepository->truncate();
    }

    public function up(): void
    {
        $equipmentAuditStates = AuditStateEnum::states();
        $equipmentAuditStatesToInsert = [];

        foreach ($equipmentAuditStates as $equipmentAuditState) {
            $equipmentAuditStatesToInsert[] = $this->equipmentAuditStateRepository->instance($equipmentAuditState);
        }

        $this->equipmentAuditStateRepository->insert($equipmentAuditStatesToInsert);
    }
}
