<?php

use Binaccle\Models\Equipments\EquipmentAuditState;
use Binaccle\Repositories\Equipments\Audits\AuditStateRepositoryInterface;
use Binaccle\Traits\Migrations\MagicMigrationTrait;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEquipmentAuditStatesTable extends Migration
{
    use MagicMigrationTrait;

    private const REPOSITORY = AuditStateRepositoryInterface::class;

    public function down(): void
    {
        Schema::dropIfExists($this->table);
    }

    public function up(): void
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->uuid(EquipmentAuditState::ID)->primary();
            $table->string(EquipmentAuditState::NAME)->unique();
        });
    }
}
