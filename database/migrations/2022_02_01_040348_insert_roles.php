<?php

use Binaccle\Enums\Roles\RoleEnum;
use Binaccle\Repositories\Roles\RoleRepositoryInterface;
use Illuminate\Database\Migrations\Migration;

class InsertRoles extends Migration
{
    private RoleRepositoryInterface $roleRepository;

    public function __construct()
    {
        $this->roleRepository = app(RoleRepositoryInterface::class);
    }

    public function down()
    {
        $this->roleRepository->truncate();
    }

    public function up()
    {
        $roles = RoleEnum::roles();
        $rolesToInsert = [];

        foreach ($roles as $role) {
            $rolesToInsert[] = $this->roleRepository->instance($role, RoleEnum::GUARD);
        }

        $this->roleRepository->insert($rolesToInsert);
    }
}
