<?php

use Binaccle\Models\Companies\Company;
use Binaccle\Models\Sectors\Sector;
use Binaccle\Repositories\Companies\CompanyRepositoryInterface;
use Binaccle\Repositories\Sectors\SectorRepositoryInterface;
use Binaccle\Traits\Migrations\MagicMigrationTrait;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectorsTable extends Migration
{
    use MagicMigrationTrait;

    private const REPOSITORY = SectorRepositoryInterface::class;

    private CompanyRepositoryInterface $companyRepository;

    public function down(): void
    {
        Schema::dropIfExists($this->table);
    }

    public function up(): void
    {
        $this->companyRepository = app(CompanyRepositoryInterface::class);

        Schema::create($this->table, function (Blueprint $table) {
            $table->uuid(Sector::ID)->primary();
            $table->string(Sector::NAME)->index();
            $table->foreignUuid(Sector::COMPANY_ID)->references(Company::ID)->on($this->companyRepository->table())->onDelete('cascade');
            $this->nativeTimestamps($table);
            $this->nativeDeletedAt($table);

            $table->unique([Sector::NAME, Sector::COMPANY_ID, Sector::DELETED_AT]);
        });
    }
}
