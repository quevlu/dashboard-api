<?php

namespace App\Events\Cache;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class InvalidateCacheEvent
{
    use Dispatchable;
    use SerializesModels;

    public function __construct(
        private object $model,
        private string $action
    ) {
    }

    public function action(): string
    {
        return $this->action;
    }

    public function model(): object
    {
        return $this->model;
    }
}
