<?php

use App\Enums\AppEnum;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Ramsey\Uuid\Rfc4122\UuidV4;

if (! function_exists('randomUuid')) {
    function randomUuid()
    {
        return UuidV4::uuid4();
    }
}

if (! function_exists('captchaEnabled')) {
    function captchaEnabled(): bool
    {
        return config('app.captcha');
    }
}

if (! function_exists('isProd')) {
    function isProd(): bool
    {
        $runningEnvironment = AppEnum::fromString(config('app.env'));

        return $runningEnvironment->is(AppEnum::PRODUCTION_ENVIRONMENT);
    }
}

if (! function_exists('password')) {
    function password($passwordToHash, $passwordHashed = null)
    {
        if ($passwordToHash && $passwordHashed) {
            return Hash::check($passwordToHash, $passwordHashed);
        }

        return Hash::make($passwordToHash);
    }
}

if (! function_exists('middleware')) {
    function middleware(string $middleware, ...$args): string
    {
        return $middleware . ':' . implode(',', $args);
    }
}

if (! function_exists('appResolve')) {
    function appResolve($class, string $method = '__invoke')
    {
        $app = app();
        if (! method_exists($class, $method)) {
            throw new Exception("The method '{$method}' must exist in this class");
        }

        $r = new ReflectionMethod($class, $method);
        $args = [];
        $params = $r->getParameters();

        foreach ($params as $param) {
            $args[] = $app->make((string) $param->getType());
        }

        $objectToCall = is_object($class) ? $class : app($class);

        return call_user_func([$objectToCall, $method], ...$args);
    }
}

if (! function_exists('customTrans')) {
    function customTrans(string $path, array $elements): array
    {
        return array_map(function ($item) use ($path) {
            return @trans($path)[$item] ?? Str::replace('_', ' ', $item);
        }, $elements);
    }
}

if (! function_exists('callIf')) {
    function callIf(?object $object, string $method)
    {
        return $object ? $object->{$method}() : null;
    }
}

if (! function_exists('param')) {
    function param(string $param)
    {
        return "{{$param}}";
    }
}
