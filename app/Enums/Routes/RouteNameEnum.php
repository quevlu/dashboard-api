<?php

namespace App\Enums\Routes;

use Binaccle\Enums\AbstractEnum;

class RouteNameEnum extends AbstractEnum
{
    public const LIST_EQUIPMENT_ASSIGNMENT_HISTORY = 'equipments.assignments.history';

    public const LIST_EQUIPMENT_ASSIGNMENTS = 'equipments.assignments';

    public const LIST_EQUIPMENT_AUDIT_AUDITED = 'equipments.audits.audited';

    public const LIST_EQUIPMENT_AUDITS = 'equipments.audits';

    public const LIST_EQUIPMENT_TYPES = 'equipment_types.list';

    public const LIST_EQUIPMENTS = 'equipments.list';

    public const LIST_MASSIVE_EQUIPMENT_TYPE_UPLOADS = 'equipment_types.massive_uploads';

    public const LIST_MASSIVE_EQUIPMENT_UPLOADS = 'equipments.massive_uploads';

    public const LIST_MASSIVE_USER_UPLOADS = 'users.massive_uploads';

    public const LIST_ROLES = 'roles.list';

    public const LIST_SECTOR_USERS = 'sectors.users.list';

    public const LIST_SECTORS = 'sectors.list';

    public const LIST_USERS = 'users.list';

    public const REFRESH_AUTH = 'auth.refresh';
}
