<?php

namespace App\Enums\Routes;

use Binaccle\Enums\AbstractEnum;

class RouteParamEnum extends AbstractEnum
{
    public const EQUIPMENT_AUDIT_ID = 'equipmentAuditId';

    public const EQUIPMENT_ID = 'equipmentId';

    public const EQUIPMENT_TYPE_ID = 'equipmentTypeId';

    public const MASSIVE_UPLOAD_ID = 'massiveUploadId';

    public const SECTOR_ID = 'sectorId';

    public const TOKEN = 'token';

    public const TYPE = 'type';

    public const USER_ID = 'userId';
}
