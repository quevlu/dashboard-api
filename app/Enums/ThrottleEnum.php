<?php

namespace App\Enums;

use Binaccle\Enums\AbstractEnum;

class ThrottleEnum extends AbstractEnum
{
    public const LOGIN_RETRIES = 10;

    public const LOGIN_TIME_TO_WAIT = 5;

    public const NOTIFIABLE_RETRIES = 5;

    public const NOTIFIABLE_TIME_TO_WAIT = 30;
}
