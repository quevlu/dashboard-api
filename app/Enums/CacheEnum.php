<?php

namespace App\Enums;

use Binaccle\Enums\AbstractEnum;
use Binaccle\Enums\MassiveUploads\MassiveUploadTypeEnum;

class CacheEnum extends AbstractEnum
{
    public const BASE_EQUIPMENTS = 'equipments';

    public const BASE_MASSIVE_UPLOADS = 'massive_uploads';

    public const COMPANY_PREFIX = 'companies';

    public const DEFAULT_TIME = 2629800;

    public const FOREVER_TIME = -1;

    public const LIST_EQUIPMENT_ASSIGNMENT_HISTORY = [
        self::BASE_EQUIPMENTS,
        'assignments_history',
    ];

    public const LIST_EQUIPMENT_ASSIGNMENTS = [
        self::BASE_EQUIPMENTS,
        'assignments',
    ];

    public const LIST_EQUIPMENT_AUDIT_AUDITED = [
        self::BASE_EQUIPMENTS,
        'audits_audited',
    ];

    public const LIST_EQUIPMENT_AUDITS = [
        self::BASE_EQUIPMENTS,
        'audits',
    ];

    public const LIST_EQUIPMENT_TYPES = [
        self::BASE_EQUIPMENTS,
        'types',
    ];

    public const LIST_EQUIPMENTS = [
        self::BASE_EQUIPMENTS,
        'list',
    ];

    public const LIST_MASSIVE_EQUIPMENT_TYPE_UPLOADS = [
        self::BASE_MASSIVE_UPLOADS,
        MassiveUploadTypeEnum::EQUIPMENT_TYPES,
    ];

    public const LIST_MASSIVE_EQUIPMENT_UPLOADS = [
        self::BASE_MASSIVE_UPLOADS,
        MassiveUploadTypeEnum::EQUIPMENTS,
    ];

    public const LIST_MASSIVE_USER_UPLOADS = [
        self::BASE_MASSIVE_UPLOADS,
        MassiveUploadTypeEnum::USERS,
    ];

    public const LIST_PLANS = 'plans';

    public const LIST_ROLES = 'roles';

    public const LIST_SECTOR_USERS = [
        self::BASE_SECTORS,
        'users',
    ];

    public const LIST_SECTORS = [
        self::BASE_SECTORS,
        'list',
    ];

    public const LIST_USERS = [
        self::BASE_USERS,
        'list',
    ];

    public const SYSTEM_PREFIX = 'system';

    private const BASE_SECTORS = 'sectors';

    private const BASE_USERS = 'users';
}
