<?php

namespace App\Enums;

use Binaccle\Enums\AbstractEnum;

class AppEnum extends AbstractEnum
{
    public const COMPANY_TEST = 'binaccle';

    public const EMAIL_TEST = 'app@binaccle.com';

    public const LOCAL_ENVIRONMENT = 'local';

    public const PASSWORD_TEST = 'AppBinaccle01';

    public const PRODUCTION_ENVIRONMENT = 'production';

    public const TESTING_ENVIRONMENT = 'testing';
}
