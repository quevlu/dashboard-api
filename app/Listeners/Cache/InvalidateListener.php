<?php

namespace App\Listeners\Cache;

use App\Events\Cache\InvalidateCacheEvent;
use App\Observers\Observer;

class InvalidateListener
{
    public function __construct(
        private Observer $observer
    ) {
    }

    public function handle(InvalidateCacheEvent $event): void
    {
        $model = $event->model();
        $action = $event->action();

        $this->observer->{$action}($model);
    }
}
