<?php

namespace App\Listeners\Throttle;

use App\Helpers\ConcatHelper;
use Illuminate\Cache\RateLimiter;
use Illuminate\Http\Request;
use Infrastructure\Redis\LimiterStorage;

class ResetThrottleListener
{
    private RateLimiter $rateLimiter;

    public function __construct(
        private Request $request,
        LimiterStorage $limiterStorage
    ) {
        $this->rateLimiter = new RateLimiter($limiterStorage->getCache());
    }

    public function handle(): void
    {
        $ip = $this->request->ip();
        $routeName = $this->request->route()->getName();

        $key = ConcatHelper::cacheTag($routeName, $ip);

        $this->rateLimiter->clear($key);
    }
}
