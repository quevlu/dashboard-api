<?php

namespace App\Http\Services;

class SanitizeService
{
    public static function removeBreakLines(?string $subject = ''): string
    {
        return str_replace(["\r", "\n"], '', $subject);
    }
}
