<?php

namespace App\Http\Exportables\MassiveUploads;

use App\Http\Exportables\AbstractExportable;
use Binaccle\Models\MassiveUploads\MassiveUpload;
use Illuminate\Support\Carbon;

class ListMassiveUploadExportable extends AbstractExportable
{
    public function export($massiveUpload): array
    {
        $createdAt = Carbon::createFromTimestamp($massiveUpload->{MassiveUpload::CREATED_AT});

        return [
            MassiveUpload::PROCESSED => $massiveUpload->{MassiveUpload::PROCESSED},
            MassiveUpload::ERROR => $massiveUpload->{MassiveUpload::ERROR},
            MassiveUpload::MASSIVE_UPLOAD_STATE_RELATIONSHIP => $massiveUpload->{MassiveUpload::MASSIVE_UPLOAD_STATE_RELATIONSHIP},
            MassiveUpload::CREATED_AT => $this->displayTime($createdAt),
        ];
    }

    protected function columns(): array
    {
        return [
            MassiveUpload::PROCESSED,
            MassiveUpload::ERROR,
            MassiveUpload::MASSIVE_UPLOAD_STATE_RELATIONSHIP,
            MassiveUpload::CREATED_AT,
        ];
    }
}
