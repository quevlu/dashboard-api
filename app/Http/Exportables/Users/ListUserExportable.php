<?php

namespace App\Http\Exportables\Users;

use App\Http\Exportables\AbstractExportable;
use Binaccle\Enums\Users\UserVerifiedEnum;
use Binaccle\Models\Roles\Role;
use Binaccle\Models\Sectors\Sector;
use Binaccle\Models\Users\User;
use Binaccle\Models\Users\UserNotifiable;

class ListUserExportable extends AbstractExportable
{
    public function export($user): array
    {
        $additionalData = [];
        $translationUserVerified = trans('enums.' . UserVerifiedEnum::class);
        $verified = null;

        if ($user->password) {
            $verified = $translationUserVerified[UserVerifiedEnum::VERIFIED];
        } else {
            $userInvitation = $user->invitationRel();

            if (! $userInvitation && ! $user->password()) {
                $verified = $translationUserVerified[UserVerifiedEnum::DONT_APPLY];
            } else {
                $verified = $translationUserVerified[UserVerifiedEnum::PENDING];
            }
        }

        $roles = $user->rolesRel()->implode(Role::NAME, ', ');

        $data = [
            User::NAME => $user->name(),
            User::EMAIL => $user->email(),
            User::SECTOR_RELATIONSHIP => callIf($user->sectorRel(), Sector::NAME),
            User::ROLES_RELATIONSHIP => $roles,
            User::IDENTITY_DOCUMENT => $user->identityDocument(),
            UserNotifiable::VERIFIED => $verified,
            User::CREATED_AT => $this->displayTime($user->createdAt()),
        ];

        return array_merge($data, $additionalData);
    }

    protected function columns(): array
    {
        return [
            User::NAME,
            User::EMAIL,
            User::SECTOR_RELATIONSHIP,
            User::ROLES_RELATIONSHIP,
            User::IDENTITY_DOCUMENT,
            User::CREATED_AT,
            UserNotifiable::VERIFIED,
        ];
    }
}
