<?php

namespace App\Http\Exportables\Equipments\Types;

use App\Http\Exportables\AbstractExportable;
use Binaccle\Models\Equipments\EquipmentType;

class ListTypeExportable extends AbstractExportable
{
    public function export($equipmentType): array
    {
        return [
            EquipmentType::NAME => $equipmentType->name(),
            EquipmentType::BRAND => $equipmentType->brand(),
            EquipmentType::MODEL => $equipmentType->model(),
            EquipmentType::PREFIX => $equipmentType->prefix(),
            EquipmentType::CHARACTERISTICS => $equipmentType->characteristics(),
            EquipmentType::CREATED_AT => $this->displayTime($equipmentType->createdAt()),
        ];
    }

    protected function columns(): array
    {
        return [
            EquipmentType::NAME,
            EquipmentType::BRAND,
            EquipmentType::MODEL,
            EquipmentType::PREFIX,
            EquipmentType::CHARACTERISTICS,
            EquipmentType::CREATED_AT,
        ];
    }
}
