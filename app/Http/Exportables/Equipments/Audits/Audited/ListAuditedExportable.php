<?php

namespace App\Http\Exportables\Equipments\Audits\Audited;

use App\Http\Exportables\AbstractExportable;
use Binaccle\Models\Equipments\Equipment;
use Binaccle\Models\Equipments\EquipmentAssignment;
use Binaccle\Models\Equipments\EquipmentAuditAudited;

class ListAuditedExportable extends AbstractExportable
{
    public function export($equipmentAuditAudited): array
    {
        $attributes = trans('validation.attributes');

        return [
            Equipment::IDENTIFIER => $equipmentAuditAudited->equipmentRel()->fullIdentifier(),
            EquipmentAssignment::EQUIPMENT_ASSIGNED_RELATIONSHIP => $equipmentAuditAudited->equipmentAssignmentRel()->assignedTo(),
            EquipmentAuditAudited::AUDITED => $equipmentAuditAudited->audited() ? $attributes['yes'] : $attributes['no'],
            EquipmentAuditAudited::UPDATED_AT => $equipmentAuditAudited->updatedAt(),
        ];
    }

    protected function columns(): array
    {
        return [
            Equipment::IDENTIFIER,
            EquipmentAssignment::EQUIPMENT_ASSIGNED_RELATIONSHIP,
            EquipmentAuditAudited::AUDITED,
            EquipmentAuditAudited::UPDATED_AT,
        ];
    }
}
