<?php

namespace App\Http\Exportables\Equipments\Audits;

use App\Http\Exportables\AbstractExportable;
use Binaccle\Models\Equipments\EquipmentAudit;
use Binaccle\Models\Sectors\Sector;

class ListAuditExportable extends AbstractExportable
{
    public function export($equipmentAudit): array
    {
        return [
            EquipmentAudit::TITLE => $equipmentAudit->title(),
            EquipmentAudit::EQUIPMENT_AUDIT_SECTOR_RELATIONSHIP => callIf($equipmentAudit->sectorRel(), Sector::NAME),
            EquipmentAudit::EQUIPMENT_AUDIT_STATE_RELATIONSHIP => $equipmentAudit->stateRel()->name(),
            EquipmentAudit::CREATED_AT => $this->displayTime($equipmentAudit->createdAt()),
            EquipmentAudit::UPDATED_AT => $this->displayTime($equipmentAudit->updatedAt()),
        ];
    }

    protected function columns(): array
    {
        return [
            EquipmentAudit::TITLE,
            EquipmentAudit::EQUIPMENT_AUDIT_SECTOR_RELATIONSHIP,
            EquipmentAudit::EQUIPMENT_AUDIT_STATE_RELATIONSHIP,
            EquipmentAudit::CREATED_AT,
            EquipmentAudit::UPDATED_AT,
        ];
    }
}
