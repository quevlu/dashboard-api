<?php

namespace App\Http\Exportables\Equipments;

use App\Http\Exportables\AbstractExportable;
use Binaccle\Models\Equipments\Equipment;
use Binaccle\Models\Equipments\EquipmentType;
use Illuminate\Support\Carbon;

class ListEquipmentExportable extends AbstractExportable
{
    public function export($equipment): array
    {
        $createdAt = Carbon::createFromTimestamp($equipment->{Equipment::CREATED_AT});

        return [
            Equipment::IDENTIFIER => $equipment->{EquipmentType::PREFIX} . $equipment->{Equipment::IDENTIFIER},
            EquipmentType::NAME => $equipment->{EquipmentType::NAME},
            EquipmentType::BRAND => $equipment->{EquipmentType::BRAND},
            EquipmentType::MODEL => $equipment->{EquipmentType::MODEL},
            EquipmentType::CHARACTERISTICS => $equipment->{EquipmentType::CHARACTERISTICS},
            Equipment::EQUIPMENT_ASSIGNED_RELATIONSHIP => $equipment->{Equipment::EQUIPMENT_ASSIGNED_RELATIONSHIP},
            Equipment::CREATED_AT => $this->displayTime($createdAt),
        ];
    }

    protected function columns(): array
    {
        return [
            Equipment::IDENTIFIER,
            EquipmentType::NAME,
            EquipmentType::BRAND,
            EquipmentType::MODEL,
            EquipmentType::CHARACTERISTICS,
            Equipment::EQUIPMENT_ASSIGNED_RELATIONSHIP,
            Equipment::CREATED_AT,
        ];
    }
}
