<?php

namespace App\Http\Exportables\Equipments\Assignments;

use App\Http\Exportables\AbstractExportable;
use Binaccle\Models\Equipments\Equipment;
use Binaccle\Models\Equipments\EquipmentType;
use Illuminate\Support\Carbon;

class ListAssignmentExportable extends AbstractExportable
{
    public function export($equipment): array
    {
        $createdAt = Carbon::createFromTimestamp($equipment->{Equipment::CREATED_AT});

        return [
            Equipment::FULL_IDENTIFIER => $equipment->{EquipmentType::PREFIX} . $equipment->{Equipment::IDENTIFIER},
            Equipment::OBSERVATION => $equipment->{Equipment::OBSERVATION},
            EquipmentType::NAME => $equipment->{EquipmentType::NAME},
            Equipment::EQUIPMENT_ASSIGNED_RELATIONSHIP => $equipment->{Equipment::EQUIPMENT_ASSIGNED_RELATIONSHIP},
            Equipment::ASSIGNMENTS_COUNT => $equipment->{Equipment::ASSIGNMENTS_COUNT},
            Equipment::TRASHED => (bool) $equipment->deleted_at,
            Equipment::CREATED_AT => $this->displayTime($createdAt),
        ];
    }

    protected function columns(): array
    {
        return [
            Equipment::FULL_IDENTIFIER,
            Equipment::OBSERVATION,
            EquipmentType::NAME,
            Equipment::EQUIPMENT_ASSIGNED_RELATIONSHIP,
            Equipment::ASSIGNMENTS_COUNT,
            Equipment::TRASHED,
            Equipment::CREATED_AT,
        ];
    }
}
