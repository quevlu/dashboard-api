<?php

namespace App\Http\Exportables\Equipments\Assignments;

use App\Http\Exportables\AbstractExportable;
use Binaccle\Models\Equipments\EquipmentAssignment;

class HistoryAssignmentExportable extends AbstractExportable
{
    public function export($equipmentAssignment): array
    {
        $active = $equipmentAssignment->active();
        $attributes = trans('validation.attributes');

        return [
            EquipmentAssignment::ACTIVE => $active ? $attributes['yes'] : $attributes['no'],
            EquipmentAssignment::OBSERVATION => $equipmentAssignment->observation(),
            EquipmentAssignment::CREATED_AT => $this->displayTime($equipmentAssignment->createdAt()),
            EquipmentAssignment::UPDATED_AT => $active ? null : $this->displayTime($equipmentAssignment->updatedAt()),
            EquipmentAssignment::ASSIGNED_TIME => $active ? null : $equipmentAssignment->updatedAt()->longAbsoluteDiffForHumans($equipmentAssignment->createdAt()),
            EquipmentAssignment::USER_RELATIONSHIP =>  callIf($equipmentAssignment->userRel(), 'name'),
            EquipmentAssignment::SECTOR_RELATIONSHIP => callIf($equipmentAssignment->sectorRel(), 'name'),
        ];
    }

    protected function columns(): array
    {
        return [
            EquipmentAssignment::ACTIVE,
            EquipmentAssignment::OBSERVATION,
            EquipmentAssignment::CREATED_AT,
            EquipmentAssignment::UPDATED_AT,
            EquipmentAssignment::ASSIGNED_TIME,
            EquipmentAssignment::USER_RELATIONSHIP,
            EquipmentAssignment::SECTOR_RELATIONSHIP,
        ];
    }
}
