<?php

namespace App\Http\Exportables\Sectors;

use App\Http\Exportables\AbstractExportable;
use Binaccle\Models\Sectors\Sector;

class ListSectorExportable extends AbstractExportable
{
    public function export($sector): array
    {
        return [
            Sector::NAME => $sector->name(),
            Sector::CREATED_AT => $this->displayTime($sector->createdAt()),
        ];
    }

    protected function columns(): array
    {
        return [
            Sector::NAME,
            Sector::CREATED_AT,
        ];
    }
}
