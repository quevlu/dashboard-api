<?php

namespace App\Http\Exportables\Sectors;

use App\Http\Exportables\AbstractExportable;
use Binaccle\Models\Sectors\Sector;

class ListUserSectorExportable extends AbstractExportable
{
    public function export($user): array
    {
        return [
            Sector::NAME => $user->name(),
        ];
    }

    protected function columns(): array
    {
        return [
            Sector::NAME,
        ];
    }
}
