<?php

namespace App\Http\Exportables;

use Illuminate\Support\Carbon;
use JamesMills\LaravelTimezone\Facades\Timezone;

abstract class AbstractExportable
{
    abstract public function export($item): array;

    public function headers(): array
    {
        return customTrans('validation.attributes', $this->columns());
    }

    public function nameFile(): string
    {
        return trans('csvs.file_names.exportables.' . get_called_class(), [
            'timestamp' => now()->getTimestamp(),
        ]);
    }

    abstract protected function columns(): array;

    protected function displayTime(Carbon $timestamp)
    {
        return Timezone::convertToLocal($timestamp);
    }
}
