<?php

namespace App\Http\Requests\Sectors;

use App\Helpers\ConcatHelper;
use App\Http\Requests\BaseRequest;
use Binaccle\Models\Sectors\Sector;
use Binaccle\Payloads\Sectors\CreateSectorPayload;
use Binaccle\Repositories\Sectors\SectorRepositoryInterface;

abstract class AbstractUpsertSectorRequest extends BaseRequest implements CreateSectorPayload
{
    public const NAME = 'name';

    public const SECTOR_ID = 'sectorId';

    protected SectorRepositoryInterface $sectorRepository;

    public function __construct(SectorRepositoryInterface $sectorRepository)
    {
        $this->sectorRepository = $sectorRepository;
    }

    public function name(): string
    {
        return $this->input(self::NAME);
    }

    public function rules(): array
    {
        return [
            self::NAME => [
                'bail',
                'required',
                'string',
                'max:30',
                'unique:' . ConcatHelper::rules(
                    $this->sectorRepository->table(),
                    Sector::NAME,
                    $this->sectorId(),
                    Sector::ID,
                    Sector::COMPANY_ID,
                    $this->companyId()
                ),
            ],
        ];
    }

    protected function sectorId(): string
    {
        return $this->route(self::SECTOR_ID, 'NULL');
    }
}
