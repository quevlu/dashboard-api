<?php

namespace App\Http\Requests\Sectors;

use App\Http\Requests\BaseRequest;
use Binaccle\Payloads\Sectors\RemoveOrCleanSectorPayload;

class RemoveOrCleanSectorRequest extends BaseRequest implements RemoveOrCleanSectorPayload
{
    public const SECTOR_ID = 'sectorId';

    public const USER_ID_OR_ACTION = 'userIdOrAction';

    public function sectorId(): string
    {
        return $this->route(self::SECTOR_ID);
    }

    public function userIdOrAction(): string
    {
        return $this->route(self::USER_ID_OR_ACTION);
    }
}
