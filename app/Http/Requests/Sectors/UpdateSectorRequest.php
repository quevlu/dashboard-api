<?php

namespace App\Http\Requests\Sectors;

use Binaccle\Models\Sectors\Sector;
use Binaccle\Payloads\Sectors\UpdateSectorPayload;

class UpdateSectorRequest extends AbstractUpsertSectorRequest implements UpdateSectorPayload
{
    private ?Sector $sector = null;

    public function sector(): Sector
    {
        if (! $this->sector) {
            $this->sector = $this->sectorRepository->findOrFail($this->sectorId());
        }

        return $this->sector;
    }
}
