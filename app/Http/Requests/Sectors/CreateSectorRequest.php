<?php

namespace App\Http\Requests\Sectors;

use Binaccle\Payloads\Sectors\CreateSectorPayload;

class CreateSectorRequest extends AbstractUpsertSectorRequest implements CreateSectorPayload
{
}
