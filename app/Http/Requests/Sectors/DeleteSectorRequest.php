<?php

namespace App\Http\Requests\Sectors;

use App\Http\Requests\BaseRequest;
use Binaccle\Models\Sectors\Sector;
use Binaccle\Repositories\Sectors\SectorRepositoryInterface;
use Binaccle\Rules\Equipments\Assignments\HasAnEquipmentAssignedRule;
use Binaccle\Rules\Sectors\HasUsersRule;

class DeleteSectorRequest extends BaseRequest
{
    public const SECTOR_ID = 'sectorId';

    private ?Sector $sector = null;

    public function __construct(
        private SectorRepositoryInterface $sectorRepository
    ) {
        $this->sectorRepository = $sectorRepository;
    }

    public function all($keys = null): array
    {
        $data = parent::all();
        $data[self::SECTOR_ID] = $this->sectorId();

        return $data;
    }

    public function rules(): array
    {
        return [
            self::SECTOR_ID => [
                'bail',
                'required',
                'uuid',
                new HasUsersRule(),
                new HasAnEquipmentAssignedRule(),
            ],
        ];
    }

    public function sector(): Sector
    {
        if (! $this->sector) {
            $this->sector = $this->sectorRepository->findOrFail($this->sectorId());
        }

        return $this->sector;
    }

    public function sectorId(): string
    {
        return $this->route(self::SECTOR_ID);
    }
}
