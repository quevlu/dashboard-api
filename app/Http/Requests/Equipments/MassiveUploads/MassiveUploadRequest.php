<?php

namespace App\Http\Requests\Equipments\MassiveUploads;

use App\Http\Requests\Shared\AbstractMassiveUploadRequest;
use Binaccle\Enums\MassiveUploads\MassiveUploadTypeEnum;

class MassiveUploadRequest extends AbstractMassiveUploadRequest
{
    public const TYPE = MassiveUploadTypeEnum::EQUIPMENTS;
}
