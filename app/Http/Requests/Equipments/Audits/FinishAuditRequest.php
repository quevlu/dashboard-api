<?php

namespace App\Http\Requests\Equipments\Audits;

use App\Helpers\ConcatHelper;
use App\Http\Requests\BaseRequest;
use Binaccle\Models\Equipments\EquipmentAudit;
use Binaccle\Models\Equipments\EquipmentAuditAudited;
use Binaccle\Payloads\Equipments\Audits\FinishAuditPayload;
use Binaccle\Repositories\Equipments\Audits\Audited\AuditedRepositoryInterface;
use Binaccle\Repositories\Equipments\Audits\AuditRepositoryInterface;

class FinishAuditRequest extends BaseRequest implements FinishAuditPayload
{
    private const EQUIPMENT_AUDIT_ID = 'equipmentAuditId';

    private const EQUIPMENT_AUDIT_ID_REQUEST = 'equipment_audit_id';

    private ?EquipmentAudit $equipmentAudit = null;

    public function __construct(
        private AuditRepositoryInterface $equipmentAuditRepository,
        private AuditedRepositoryInterface $equipmentAuditAuditedRepository
    ) {
        $this->equipmentAuditRepository = $equipmentAuditRepository;
        $this->equipmentAuditAuditedRepository = $equipmentAuditAuditedRepository;
    }

    public function all($keys = null): array
    {
        $all = parent::all();
        $all[self::EQUIPMENT_AUDIT_ID_REQUEST] = $this->equipmentAuditId();

        return $all;
    }

    public function equipmentAudit(): EquipmentAudit
    {
        if (! $this->equipmentAudit) {
            $this->equipmentAudit = $this->equipmentAuditRepository->auditNotFailedOrFinished($this->equipmentAuditId());
        }

        return $this->equipmentAudit;
    }

    public function messages(): array
    {
        return [
            self::EQUIPMENT_AUDIT_ID_REQUEST . '.exists' => trans('validation.equipment_audit_without_equipments'),
        ];
    }

    public function rules(): array
    {
        return [
            self::EQUIPMENT_AUDIT_ID_REQUEST => [
                'bail',
                'required',
                'uuid',
                'exists:' . ConcatHelper::rules(
                    $this->equipmentAuditAuditedRepository->table(),
                    EquipmentAuditAudited::EQUIPMENT_AUDIT_ID,
                    EquipmentAuditAudited::AUDITED,
                    true
                ),
            ],
        ];
    }

    private function equipmentAuditId(): string
    {
        return $this->route(self::EQUIPMENT_AUDIT_ID);
    }
}
