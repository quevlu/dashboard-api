<?php

namespace App\Http\Requests\Equipments\Audits;

use Binaccle\Enums\Equipments\Audits\AuditStateEnum;
use Binaccle\Models\Equipments\EquipmentAudit;
use Binaccle\Payloads\Equipments\Audits\UpdateAuditPayload;

class UpdateAuditRequest extends AbstractUpsertAuditRequest implements UpdateAuditPayload
{
    public const EQUIPMENT_AUDIT_ID = 'equipmentAuditId';

    private ?EquipmentAudit $equipmentAudit = null;

    public function equipmentAudit(): EquipmentAudit
    {
        if (! $this->equipmentAudit) {
            $this->equipmentAudit = $this->equipmentAuditRepository->auditNotFailedOrFinished($this->equipmentAuditId());
        }

        return $this->equipmentAudit;
    }

    public function messages(): array
    {
        return [
            self::SECTOR_ID . '.prohibited' => trans('validation.equipment_audit.sector_prohibited'),
        ];
    }

    public function rules(): array
    {
        $baseRules = parent::rules();

        $equipmentAuditStateName = $this->equipmentAudit()->stateRel()->name();

        if ($equipmentAuditStateName == AuditStateEnum::CREATING) {
            $baseRules[self::SECTOR_ID] = 'prohibited';
        }

        return $baseRules;
    }

    private function equipmentAuditId(): string
    {
        return $this->route(self::EQUIPMENT_AUDIT_ID);
    }
}
