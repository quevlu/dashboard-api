<?php

namespace App\Http\Requests\Equipments\Audits\Audited;

use Binaccle\Payloads\Equipments\Audits\Audited\UpeleteAuditedPayload;

class UpleteAuditedRequest extends AbstractAuditedRequest implements UpeleteAuditedPayload
{
    public function rules(): array
    {
        $equipmentIdsRules = parent::rules()[self::EQUIPMENT_IDS];

        array_pop($equipmentIdsRules);

        return [
            self::EQUIPMENT_IDS => $equipmentIdsRules,
        ];
    }
}
