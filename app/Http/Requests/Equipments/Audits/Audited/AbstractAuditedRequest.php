<?php

namespace App\Http\Requests\Equipments\Audits\Audited;

use App\Http\Requests\BaseRequest;
use Binaccle\Models\Equipments\EquipmentAudit;
use Binaccle\Payloads\Equipments\Audits\Audited\AuditedPayload;
use Binaccle\Repositories\Equipments\Audits\AuditRepositoryInterface;
use Binaccle\Repositories\Equipments\EquipmentRepositoryInterface;
use Binaccle\Rules\Equipments\Audits\HaveBeenAudited;
use Binaccle\Rules\Shared\WhereInCountRule;
use Infrastructure\Repositories\Criterias\BaseCriteria;

abstract class AbstractAuditedRequest extends BaseRequest implements AuditedPayload
{
    protected const EQUIPMENT_IDS = 'equipment_ids';

    private const EQUIPMENT_AUDIT_ID = 'equipmentAuditId';

    protected AuditRepositoryInterface $equipmentAuditRepository;

    protected EquipmentRepositoryInterface $equipmentRepository;

    private ?EquipmentAudit $equipmentAudit = null;

    public function __construct(
        EquipmentRepositoryInterface $equipmentRepository,
        AuditRepositoryInterface $equipmentAuditRepository
    ) {
        $this->equipmentRepository = $equipmentRepository;
        $this->equipmentAuditRepository = $equipmentAuditRepository;
    }

    public function equipmentAudit(): EquipmentAudit
    {
        if (! $this->equipmentAudit) {
            $this->equipmentAudit = $this->equipmentAuditRepository->auditNotFailedOrFinished($this->equipmentAuditId());
        }

        return $this->equipmentAudit;
    }

    public function equipmentIds(): array
    {
        return $this->input(self::EQUIPMENT_IDS) ?? [];
    }

    public function rules(): array
    {
        return [
            self::EQUIPMENT_IDS => [
                'bail',
                'required',
                'array',
                'min:1',
                'distinct',
                'max:' . $this->maxQuantity(),
                new WhereInCountRule($this->equipmentRepository),
                new HaveBeenAudited($this->equipmentAudit()->id()),
            ],
        ];
    }

    private function equipmentAuditId(): string
    {
        return $this->route(self::EQUIPMENT_AUDIT_ID);
    }

    private function maxQuantity(): int
    {
        $offsets = BaseCriteria::OFFSETS;

        return end($offsets);
    }
}
