<?php

namespace App\Http\Requests\Equipments\Audits\Audited;

use Binaccle\Payloads\Equipments\Audits\Audited\AddAuditedPayload;
use Illuminate\Database\Eloquent\Collection;

class AddAuditedRequest extends AbstractAuditedRequest implements AddAuditedPayload
{
    private ?Collection $equipments = null;

    public function equipments(): Collection
    {
        if (! $this->equipments) {
            $this->equipments = $this->equipmentRepository->whereInWithAssignments($this->equipmentIds());
        }

        return $this->equipments;
    }
}
