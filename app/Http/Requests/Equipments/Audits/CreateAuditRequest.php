<?php

namespace App\Http\Requests\Equipments\Audits;

use Binaccle\Payloads\Equipments\Audits\CreateAuditPayload;

class CreateAuditRequest extends AbstractUpsertAuditRequest implements CreateAuditPayload
{
}
