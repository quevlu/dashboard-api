<?php

namespace App\Http\Requests\Equipments\Audits;

use App\Http\Requests\BaseRequest;
use Binaccle\Models\Equipments\EquipmentAudit;
use Binaccle\Payloads\Equipments\Audits\DeleteAuditPayload;
use Binaccle\Repositories\Equipments\Audits\AuditRepositoryInterface;

class DeleteAuditRequest extends BaseRequest implements DeleteAuditPayload
{
    private const EQUIPMENT_AUDIT_ID = 'equipmentAuditId';

    private ?EquipmentAudit $equipmentAudit = null;

    public function __construct(
        private AuditRepositoryInterface $equipmentAuditRepository
    ) {
        $this->equipmentAuditRepository = $equipmentAuditRepository;
    }

    public function equipmentAudit(): EquipmentAudit
    {
        if (! $this->equipmentAudit) {
            $this->equipmentAudit = $this->equipmentAuditRepository->auditNotFailedOrFinished($this->equipmentAuditId());
        }

        return $this->equipmentAudit;
    }

    private function equipmentAuditId(): string
    {
        return $this->route(self::EQUIPMENT_AUDIT_ID);
    }
}
