<?php

namespace App\Http\Requests\Equipments\Audits;

use App\Helpers\ConcatHelper;
use App\Http\Requests\BaseRequest;
use Binaccle\Enums\Equipments\Audits\AuditStateEnum;
use Binaccle\Models\Equipments\EquipmentAuditState;
use Binaccle\Models\Sectors\Sector;
use Binaccle\Payloads\Equipments\Audits\UpsertAuditPayload;
use Binaccle\Repositories\Equipments\Audits\Audited\AuditedRepositoryInterface;
use Binaccle\Repositories\Equipments\Audits\AuditRepositoryInterface;
use Binaccle\Repositories\Equipments\Audits\AuditStateRepositoryInterface;
use Binaccle\Repositories\Sectors\SectorRepositoryInterface;

abstract class AbstractUpsertAuditRequest extends BaseRequest implements UpsertAuditPayload
{
    protected const SECTOR_ID = 'sector_id';

    private const TITLE = 'title';

    protected AuditedRepositoryInterface $equipmentAuditAuditedRepository;

    protected AuditRepositoryInterface $equipmentAuditRepository;

    private ?string $equipmentAuditStateId = null;

    private ?string $sectorId = null;

    public function __construct(
        private SectorRepositoryInterface $sectorRepository,
        AuditRepositoryInterface $equipmentAuditRepository,
        private AuditStateRepositoryInterface $equipmentAuditStateRepository
    ) {
        $this->sectorRepository = $sectorRepository;
        $this->equipmentAuditRepository = $equipmentAuditRepository;
        $this->equipmentAuditStateRepository = $equipmentAuditStateRepository;
    }

    public function equipmentAuditStateId(): string
    {
        if (! $this->equipmentAuditStateId) {
            $state = $this->sectorId() ? AuditStateEnum::CREATING : AuditStateEnum::CREATED;

            $this->equipmentAuditStateId = $this->equipmentAuditStateRepository->firstOrFailBy(
                EquipmentAuditState::NAME,
                $state
            )
            ->id();
        }

        return $this->equipmentAuditStateId;
    }

    public function rules(): array
    {
        return [
            self::TITLE => 'bail|required|string|min:2|max:60',
            self::SECTOR_ID => 'bail|nullable|exists:' . ConcatHelper::rules(
                $this->sectorRepository->table(),
                Sector::ID,
                Sector::COMPANY_ID,
                $this->companyId()
            ),
        ];
    }

    public function sectorId(): ?string
    {
        if (! $this->sectorId && $this->has(self::SECTOR_ID)) {
            $this->sectorId = $this->sectorRepository->findOrFail($this->input(self::SECTOR_ID))->id();
        }

        return $this->sectorId;
    }

    public function title(): string
    {
        return $this->input(self::TITLE);
    }
}
