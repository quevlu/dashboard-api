<?php

namespace App\Http\Requests\Equipments;

use App\Http\Requests\BaseRequest;
use Binaccle\Enums\Equipments\PrintEquipmentEnum;
use Binaccle\Models\Equipments\Equipment;
use Binaccle\Models\Files\File;
use Binaccle\Payloads\Equipments\PrintEquipmentPayload;
use Binaccle\Repositories\Equipments\EquipmentRepositoryInterface;
use Illuminate\Validation\Rule;

class PrintEquipmentRequest extends BaseRequest implements PrintEquipmentPayload
{
    private const EQUIPMENT_ID = 'equipmentId';

    private const TYPE = 'type';

    private ?Equipment $equipment = null;

    public function __construct(
        private EquipmentRepositoryInterface $equipmentRepository
    ) {
        $this->equipmentRepository = $equipmentRepository;
    }

    public function all($keys = null): array
    {
        $data = parent::all();
        $data[self::TYPE] = $this->type();

        return $data;
    }

    public function equipment(): Equipment
    {
        if (! $this->equipment) {
            $this->equipment = $this->equipmentRepository->findOrFail($this->equipmentId());
        }

        return $this->equipment;
    }

    public function logo(): ?File
    {
        return $this->company()->logoRel();
    }

    public function rules(): array
    {
        return [
            self::TYPE => [
                'bail',
                'required',
                Rule::in(PrintEquipmentEnum::allValues()),
            ],
        ];
    }

    public function type(): string
    {
        return $this->route(self::TYPE);
    }

    private function equipmentId(): string
    {
        return $this->route(self::EQUIPMENT_ID);
    }
}
