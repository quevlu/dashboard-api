<?php

namespace App\Http\Requests\Equipments\Types;

use App\Enums\Routes\RouteParamEnum;
use App\Helpers\ConcatHelper;
use App\Http\Requests\BaseRequest;
use Binaccle\Models\Equipments\EquipmentType;
use Binaccle\Payloads\Equipments\Types\DeleteTypePayload;
use Binaccle\Repositories\Equipments\Types\TypeRepositoryInterface;
use Binaccle\Rules\Equipments\Types\TypeHasEquipmentRule;

class DeleteTypeRequest extends BaseRequest implements DeleteTypePayload
{
    private ?EquipmentType $equipmentType = null;

    public function __construct(
        private TypeRepositoryInterface $equipmentTypeRepository
    ) {
        $this->equipmentTypeRepository = $equipmentTypeRepository;
    }

    public function all($keys = null): array
    {
        $all = parent::all();
        $all[RouteParamEnum::EQUIPMENT_TYPE_ID] = $this->equipmentTypeId();

        return $all;
    }

    public function equipmentType(): EquipmentType
    {
        if (! $this->equipmentType) {
            $this->equipmentType = $this->equipmentTypeRepository->findOrFail($this->equipmentTypeId());
        }

        return $this->equipmentType;
    }

    public function rules(): array
    {
        return [
            RouteParamEnum::EQUIPMENT_TYPE_ID => [
                'bail',
                'required',
                'uuid',
                'exists:' . ConcatHelper::rules(
                    $this->equipmentTypeRepository->table(),
                    EquipmentType::ID,
                    EquipmentType::COMPANY_ID,
                    $this->companyId()
                ),
                new TypeHasEquipmentRule(),
            ],
        ];
    }

    private function equipmentTypeId(): string
    {
        return request()->route()->parameter(RouteParamEnum::EQUIPMENT_TYPE_ID);
    }
}
