<?php

namespace App\Http\Requests\Equipments\Types;

use App\Http\Requests\BaseRequest;
use Binaccle\Payloads\Equipments\Types\UpsertTypePayload;
use Binaccle\Repositories\Equipments\Types\TypeRepositoryInterface;

abstract class AbstractUpsertTypeRequest implements UpsertTypePayload
{
    public const BRAND = 'brand';

    public const CHARACTERISTICS = 'characteristics';

    public const COMPANY_ID = 'company_id';

    public const EQUIPMENT_TYPE_ID = 'equipmentTypeId';

    public const MODEL = 'model';

    public const NAME = 'name';

    public const PREFIX = 'prefix';

    protected TypeRepositoryInterface $equipmentTypeRepository;

    protected BaseRequest $request;

    public function __construct(
        BaseRequest $request,
        TypeRepositoryInterface $equipmentTypeRepository
    ) {
        $this->request = $request;
        $this->equipmentTypeRepository = $equipmentTypeRepository;
    }

    public function brand(): ?string
    {
        return $this->request->input(self::BRAND);
    }

    public function characteristics(): ?string
    {
        return $this->request->input(self::CHARACTERISTICS);
    }

    public function companyId(): string
    {
        return $this->request->companyId();
    }

    public function model(): ?string
    {
        return $this->request->input(self::MODEL);
    }

    public function name(): string
    {
        return $this->request->input(self::NAME);
    }

    public function prefix(): string
    {
        return $this->request->input(self::PREFIX);
    }

    public function rules(): array
    {
        return [
            self::NAME => 'bail|required|string|min:1|max:60',
            self::BRAND => 'bail|nullable|string|min:1|max:60',
            self::MODEL => 'bail|nullable|string|min:1|max:60',
            self::PREFIX => 'bail|required|string|alpha_dash|max:6',
            self::CHARACTERISTICS => 'bail|nullable|string|max:60000',
        ];
    }

    public function validate(): void
    {
        $this->request->validate($this->rules());
    }
}
