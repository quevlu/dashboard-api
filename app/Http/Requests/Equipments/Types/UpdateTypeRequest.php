<?php

namespace App\Http\Requests\Equipments\Types;

use Binaccle\Models\Equipments\EquipmentType;
use Binaccle\Payloads\Equipments\Types\UpdateTypePayload;
use Binaccle\Payloads\Equipments\Types\UpleteTypePayload;

class UpdateTypeRequest extends AbstractUpsertTypeRequest implements UpdateTypePayload, UpleteTypePayload
{
    private ?EquipmentType $equipmentType = null;

    public function equipmentType(): EquipmentType
    {
        if (! $this->equipmentType) {
            $this->equipmentType = $this->equipmentTypeRepository->findOrFail($this->equipmentTypeId());
        }

        return $this->equipmentType;
    }

    private function equipmentTypeId(): ?string
    {
        return $this->request->route(self::EQUIPMENT_TYPE_ID);
    }
}
