<?php

namespace App\Http\Requests\Equipments\Types\MassiveUploads;

use App\Http\Requests\Shared\AbstractMassiveUploadRequest;
use Binaccle\Enums\MassiveUploads\MassiveUploadTypeEnum;

class MassiveUploadRequest extends AbstractMassiveUploadRequest
{
    public const TYPE = MassiveUploadTypeEnum::EQUIPMENT_TYPES;
}
