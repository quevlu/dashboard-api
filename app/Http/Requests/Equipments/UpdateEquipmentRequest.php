<?php

namespace App\Http\Requests\Equipments;

use App\Enums\Routes\RouteParamEnum;
use Binaccle\Models\Equipments\Equipment;
use Binaccle\Payloads\Equipments\UpdateEquipmentPayload;

class UpdateEquipmentRequest extends AbstractUpsertEquipmentRequest implements UpdateEquipmentPayload
{
    private ?Equipment $equipment = null;

    public function equipment(): Equipment
    {
        if (! $this->equipment) {
            $this->equipment = $this->equipmentRepository->findOrFail($this->equipmentId());
        }

        return $this->equipment;
    }

    public function equipmentId(): string
    {
        return request()->route()->parameter(RouteParamEnum::EQUIPMENT_ID);
    }
}
