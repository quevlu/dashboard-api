<?php

namespace App\Http\Requests\Equipments;

use Binaccle\Payloads\Equipments\CreateEquipmentPayload;

class CreateEquipmentRequest extends AbstractUpsertEquipmentRequest implements CreateEquipmentPayload
{
    public const QUANTITY = 'quantity';

    private ?string $companyId = null;

    public function companyId(): string
    {
        return $this->companyId ?? $this->request->companyId();
    }

    public function quantity(): int
    {
        return $this->request->input(self::QUANTITY);
    }

    public function rules(): array
    {
        $rules = [
            self::QUANTITY => 'bail|required|numeric|gt:0',
        ];

        return array_merge(parent::rules(), $rules);
    }

    public function setCompanyId($companyId): void
    {
        $this->companyId = $companyId;
    }
}
