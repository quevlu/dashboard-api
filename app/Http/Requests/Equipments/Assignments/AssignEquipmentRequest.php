<?php

namespace App\Http\Requests\Equipments\Assignments;

use App\Helpers\ConcatHelper;
use App\Http\Requests\BaseRequest;
use App\Http\Services\SanitizeService;
use Binaccle\Models\Equipments\Equipment;
use Binaccle\Models\Sectors\Sector;
use Binaccle\Models\Users\User;
use Binaccle\Payloads\Equipments\Assignments\AssignEquipmentPayload;
use Binaccle\Repositories\Equipments\EquipmentRepositoryInterface;
use Binaccle\Repositories\Sectors\SectorRepositoryInterface;
use Binaccle\Repositories\Users\UserRepositoryInterface;
use Binaccle\Rules\Equipments\Assignments\IsTheSameDestinationRule;

class AssignEquipmentRequest extends BaseRequest implements AssignEquipmentPayload
{
    public const SECTOR_ID = 'sector_id';

    public const USER_ID = 'user_id';

    private const EQUIPMENT_ID = 'equipment_id';

    private const EQUIPMENT_ROUTE_ID = 'equipmentId';

    private const OBSERVATION = 'observation';

    public function __construct(
        private EquipmentRepositoryInterface $equipmentRepository,
        private UserRepositoryInterface $userRepository,
        private SectorRepositoryInterface $sectorRepository
    ) {
        $this->equipmentRepository = $equipmentRepository;
        $this->userRepository = $userRepository;
        $this->sectorRepository = $sectorRepository;
    }

    public function all($keys = null): array
    {
        $all = parent::all();
        $all[self::EQUIPMENT_ID] = $this->equipmentId();

        return $all;
    }

    public function equipmentId(): string
    {
        return $this->route(self::EQUIPMENT_ROUTE_ID);
    }

    public function observation(): ?string
    {
        return SanitizeService::removeBreakLines($this->input(self::OBSERVATION));
    }

    public function rules(): array
    {
        return [
            self::SECTOR_ID => [
                'bail',
                'nullable',
                'uuid',
                'prohibits:' . self::USER_ID,
                'exists:' . ConcatHelper::rules($this->sectorRepository->table(), Sector::ID, Sector::COMPANY_ID, $this->companyId()),
            ],
            self::USER_ID => [
                'bail',
                'nullable',
                'uuid',
                'prohibits:' . self::SECTOR_ID,
                'exists:' . ConcatHelper::rules($this->userRepository->table(), User::ID, User::COMPANY_ID, $this->companyId()),
            ],
            self::EQUIPMENT_ID => [
                'bail',
                'required',
                'uuid',
                'exists:' . ConcatHelper::rules($this->equipmentRepository->table(), Equipment::ID, Equipment::COMPANY_ID, $this->companyId()),
                new IsTheSameDestinationRule(),
            ],
            self::OBSERVATION => 'bail|nullable|string|max:250',
        ];
    }

    public function sectorId(): ?string
    {
        return $this->input(self::SECTOR_ID);
    }

    public function userId(): ?string
    {
        return $this->input(self::USER_ID);
    }
}
