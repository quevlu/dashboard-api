<?php

namespace App\Http\Requests\Equipments\Assignments;

use App\Helpers\ConcatHelper;
use App\Http\Requests\BaseRequest;
use App\Http\Services\SanitizeService;
use Binaccle\Models\Equipments\Equipment;
use Binaccle\Models\Equipments\EquipmentAssignment;
use Binaccle\Payloads\Equipments\Assignments\UnassignEquipmentPayload;
use Binaccle\Repositories\Equipments\Assignments\AssignmentRepositoryInterface;
use Binaccle\Repositories\Equipments\EquipmentRepositoryInterface;

class UnassignEquipmentRequest extends BaseRequest implements UnassignEquipmentPayload
{
    private const EQUIPMENT_ID = 'equipment_id';

    private const EQUIPMENT_ROUTE_ID = 'equipmentId';

    private const OBSERVATION = 'observation';

    public function __construct(
        private EquipmentRepositoryInterface $equipmentRepository,
        private AssignmentRepositoryInterface $equipmentAssignmentRepository
    ) {
        $this->equipmentRepository = $equipmentRepository;
        $this->equipmentAssignmentRepository = $equipmentAssignmentRepository;
    }

    public function all($keys = null): array
    {
        $all = parent::all();
        $all[self::EQUIPMENT_ID] = $this->equipmentId();

        return $all;
    }

    public function equipmentId(): string
    {
        return $this->route(self::EQUIPMENT_ROUTE_ID);
    }

    public function observation(): ?string
    {
        return SanitizeService::removeBreakLines($this->input(self::OBSERVATION));
    }

    public function rules(): array
    {
        return [
            self::EQUIPMENT_ID => [
                'bail',
                'required',
                'uuid',
                'exists:' . ConcatHelper::rules(
                    $this->equipmentRepository->table(),
                    Equipment::ID,
                    Equipment::COMPANY_ID,
                    $this->companyId()
                ),
                'exists:' . ConcatHelper::rules(
                    $this->equipmentAssignmentRepository->table(),
                    EquipmentAssignment::EQUIPMENT_ID,
                    EquipmentAssignment::ACTIVE,
                    true
                ),
            ],
            self::OBSERVATION => 'bail|nullable|string|max:250',
        ];
    }
}
