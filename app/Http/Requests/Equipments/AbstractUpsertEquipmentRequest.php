<?php

namespace App\Http\Requests\Equipments;

use App\Helpers\ConcatHelper;
use App\Http\Requests\BaseRequest;
use Binaccle\Models\Equipments\EquipmentType;
use Binaccle\Payloads\Equipments\UpsertEquipmentPayload;
use Binaccle\Repositories\Equipments\EquipmentRepositoryInterface;
use Binaccle\Repositories\Equipments\Types\TypeRepositoryInterface;

abstract class AbstractUpsertEquipmentRequest implements UpsertEquipmentPayload
{
    public const COMPANY_ID = 'company_id';

    public const EQUIPMENT_TYPE_ID = 'equipment_type_id';

    protected EquipmentRepositoryInterface $equipmentRepository;

    protected TypeRepositoryInterface $equipmentTypeRepository;

    protected BaseRequest $request;

    public function __construct(
        BaseRequest $request,
        TypeRepositoryInterface $equipmentTypeRepository,
        EquipmentRepositoryInterface $equipmentRepository
    ) {
        $this->request = $request;
        $this->equipmentTypeRepository = $equipmentTypeRepository;
        $this->equipmentRepository = $equipmentRepository;
    }

    public function companyId(): string
    {
        return $this->request->companyId();
    }

    public function equipmentTypeId(): string
    {
        return $this->request->input(self::EQUIPMENT_TYPE_ID);
    }

    public function rules(): array
    {
        return [
            self::EQUIPMENT_TYPE_ID => [
                'bail',
                'required',
                'uuid',
                'exists:' . ConcatHelper::rules(
                    $this->equipmentTypeRepository->table(),
                    EquipmentType::ID,
                    EquipmentType::COMPANY_ID,
                    $this->companyId()
                ),
            ],
        ];
    }

    public function validate(): void
    {
        $this->request->validate($this->rules());
    }
}
