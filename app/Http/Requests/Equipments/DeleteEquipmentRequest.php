<?php

namespace App\Http\Requests\Equipments;

use App\Enums\Routes\RouteParamEnum;
use App\Helpers\ConcatHelper;
use App\Http\Requests\BaseRequest;
use App\Http\Services\SanitizeService;
use Binaccle\Models\Equipments\Equipment;
use Binaccle\Payloads\Equipments\DeleteEquipmentPayload;
use Binaccle\Repositories\Equipments\EquipmentRepositoryInterface;
use Binaccle\Rules\Equipments\Assignments\IsAssignedRule;

class DeleteEquipmentRequest extends BaseRequest implements DeleteEquipmentPayload
{
    public const OBSERVATION = 'observation';

    private ?Equipment $equipment = null;

    public function __construct(
        private EquipmentRepositoryInterface $equipmentRepository
    ) {
        $this->equipmentRepository = $equipmentRepository;
    }

    public function all($keys = null): array
    {
        $all = parent::all();
        $all[RouteParamEnum::EQUIPMENT_ID] = $this->equipmentId();

        return $all;
    }

    public function equipment(): Equipment
    {
        if (! $this->equipment) {
            $this->equipment = $this->equipmentRepository->findOrFail($this->equipmentId());
        }

        return $this->equipment;
    }

    public function equipmentId(): string
    {
        return request()->route()->parameter(RouteParamEnum::EQUIPMENT_ID);
    }

    public function observation(): ?string
    {
        return SanitizeService::removeBreakLines($this->input(self::OBSERVATION));
    }

    public function rules(): array
    {
        return [
            RouteParamEnum::EQUIPMENT_ID => [
                'bail',
                'required',
                'uuid',
                'exists:' . ConcatHelper::rules(
                    $this->equipmentRepository->table(),
                    Equipment::ID,
                    Equipment::COMPANY_ID,
                    $this->companyId()
                ),
                new IsAssignedRule(),
            ],
            self::OBSERVATION => 'bail|nullable|string|max:250',
        ];
    }
}
