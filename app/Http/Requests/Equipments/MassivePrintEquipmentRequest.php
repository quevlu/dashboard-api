<?php

namespace App\Http\Requests\Equipments;

use App\Http\Requests\BaseRequest;
use Binaccle\Enums\Equipments\PrintEquipmentEnum;
use Binaccle\Models\Files\File;
use Binaccle\Payloads\Equipments\MassivePrintEquipmentPayload;
use Binaccle\Repositories\Equipments\EquipmentRepositoryInterface;
use Binaccle\Rules\Shared\WhereInCountRule;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Validation\Rule;
use Infrastructure\Repositories\Criterias\BaseCriteria;

class MassivePrintEquipmentRequest extends BaseRequest implements MassivePrintEquipmentPayload
{
    private const EQUIPMENT_IDS = 'equipment_ids';

    private const TYPE = 'type';

    private ?Collection $equipments = null;

    public function __construct(
        private EquipmentRepositoryInterface $equipmentRepository
    ) {
        $this->equipmentRepository = $equipmentRepository;
    }

    public function all($keys = null): array
    {
        $data = parent::all();
        $data[self::TYPE] = $this->type();

        return $data;
    }

    public function equipments(): Collection
    {
        if (! $this->equipments) {
            $this->equipments = $this->equipmentRepository->whereInWithTypes($this->equipmentIds());
        }

        return $this->equipments;
    }

    public function logo(): ?File
    {
        return $this->company()->logoRel();
    }

    public function rules(): array
    {
        return [
            self::TYPE => [
                'bail',
                'required',
                Rule::in(PrintEquipmentEnum::allValues()),
            ],
            self::EQUIPMENT_IDS => [
                'bail',
                'required',
                'array',
                'min:1',
                'distinct',
                'max:' . $this->maxQuantity(),
                new WhereInCountRule($this->equipmentRepository),
            ],
        ];
    }

    public function type(): string
    {
        return $this->route(self::TYPE);
    }

    private function equipmentIds(): array
    {
        return $this->input(self::EQUIPMENT_IDS);
    }

    private function maxQuantity(): int
    {
        $offsets = BaseCriteria::OFFSETS;

        return end($offsets);
    }
}
