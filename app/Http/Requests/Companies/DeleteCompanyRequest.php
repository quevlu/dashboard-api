<?php

namespace App\Http\Requests\Companies;

use App\Http\Requests\BaseRequest;
use Binaccle\Payloads\Companies\DeleteCompanyPayload;

class DeleteCompanyRequest extends BaseRequest implements DeleteCompanyPayload
{
    public function rules(): array
    {
        $commonPasswordRules = $this->passwordRules();

        return [
            self::PASSWORD => $commonPasswordRules . '|current_password:api',
        ];
    }
}
