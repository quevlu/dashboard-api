<?php

namespace App\Http\Requests\Companies;

use App\Http\Requests\BaseRequest;
use Binaccle\Models\Files\File;
use Binaccle\Payloads\Companies\UpdateCompanyPayload;
use Illuminate\Http\UploadedFile;

class UpdateCompanyRequest extends BaseRequest implements UpdateCompanyPayload
{
    private const LOGO = 'logo';

    private const NAME = 'name';

    private ?File $logoCreated = null;

    private ?string $relativePath = null;

    private ?string $url = null;

    public function logo(): ?UploadedFile
    {
        return $this->has(self::LOGO) ? $this->file(self::LOGO) : null;
    }

    public function logoCreated(): ?File
    {
        return $this->logoCreated;
    }

    public function name(): string
    {
        return $this->input(self::NAME);
    }

    public function relativePath(): string
    {
        return $this->relativePath;
    }

    public function rules(): array
    {
        return [
            self::NAME => $this->companyNameRules(),
            self::LOGO => 'bail|nullable|image|file|max:150', // |dimensions:max_width=300,max_height=300',
        ];
    }

    public function setLogoCreated(?File $logoCreated = null): void
    {
        $this->logoCreated = $logoCreated;
    }

    public function setRelativePath(string $relativePath): void
    {
        $this->relativePath = $relativePath;
    }

    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    public function url(): string
    {
        return $this->url;
    }
}
