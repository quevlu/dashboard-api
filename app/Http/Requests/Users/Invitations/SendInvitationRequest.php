<?php

namespace App\Http\Requests\Users\Invitations;

use App\Http\Requests\Shared\AbstractSendNotificationRequest;
use Binaccle\Models\Users\User;
use Binaccle\Models\Users\UserNotifiable;
use Binaccle\Payloads\Users\Notifiables\UserNotifiablePayload;
use Binaccle\Repositories\Users\Notifiables\UserNotifiableRepositoryInterface;
use Binaccle\Repositories\Users\UserRepositoryInterface;

class SendInvitationRequest extends AbstractSendNotificationRequest implements UserNotifiablePayload
{
    public const TYPE = UserNotifiable::INVITATION_TYPE;

    public const USER_ID = 'userId';

    private ?User $currentUser = null;

    private ?UserNotifiable $modelToNotify = null;

    public function __construct(UserRepositoryInterface $userRepository, UserNotifiableRepositoryInterface $userNotifiableRepository)
    {
        $this->userRepository = $userRepository;
        $this->userNotifiableRepository = $userNotifiableRepository;
    }

    public function all($keys = null): array
    {
        $data = parent::all();
        $data[self::USER_ID] = $this->userToId();

        return $data;
    }

    public function currentUser(): User
    {
        if (! $this->currentUser) {
            $this->currentUser = $this->userRepository->findOrFail($this->userToId());
        }

        return $this->currentUser;
    }

    public function modelToNotify(): ?UserNotifiable
    {
        if ($this->currentUser() && ! $this->modelToNotify) {
            $this->modelToNotify = $this->currentUser->invitationRel();
        }

        return $this->modelToNotify;
    }

    public function rules(): array
    {
        return [
            self::USER_ID => 'bail|required|uuid',
        ];
    }

    private function userToId(): string
    {
        return $this->route(self::USER_ID);
    }
}
