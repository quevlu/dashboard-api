<?php

namespace App\Http\Requests\Users;

use Binaccle\Models\Users\User;

class UpdateUserRequest extends AbstractUpsertUserRequest
{
    public const USER_ID = 'userId';

    private ?User $currentUser = null;

    public function currentUser(): User
    {
        if (! $this->currentUser) {
            $this->currentUser = $this->userRepository->findOrFail($this->userId());
        }

        return $this->currentUser;
    }

    public function rules(): array
    {
        $rules = parent::rules();

        if ($this->currentUser()->isAdmin()) {
            unset($rules[self::ROLE_IDS],$rules[self::ROLE_IDS . '.*']);
        }

        return $rules;
    }

    public function userId(): string
    {
        return $this->request->route(self::USER_ID);
    }
}
