<?php

namespace App\Http\Requests\Users\MassiveUploads;

use App\Http\Requests\Shared\AbstractMassiveUploadRequest;
use Binaccle\Enums\MassiveUploads\MassiveUploadTypeEnum;

class MassiveUploadRequest extends AbstractMassiveUploadRequest
{
    public const TYPE = MassiveUploadTypeEnum::USERS;
}
