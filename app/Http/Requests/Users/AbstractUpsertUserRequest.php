<?php

namespace App\Http\Requests\Users;

use App\Helpers\ConcatHelper;
use App\Http\Requests\BaseRequest;
use Binaccle\Models\Sectors\Sector;
use Binaccle\Models\Users\UserNotifiable;
use Binaccle\Payloads\Users\Notifiables\UserNotifiablePayload;
use Binaccle\Payloads\Users\UpsertUserPayload;
use Binaccle\Repositories\Roles\RoleRepositoryInterface;
use Binaccle\Repositories\Sectors\SectorRepositoryInterface;
use Binaccle\Repositories\Users\UserRepositoryInterface;
use Binaccle\Rules\Users\OnlyUserRoleRule;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Validation\Rule;

abstract class AbstractUpsertUserRequest implements UpsertUserPayload, UserNotifiablePayload
{
    public const COMPANY_ID = 'company_id';

    public const EMAIL = 'email';

    public const IDENTITY_DOCUMENT = 'identity_document';

    public const NAME = 'name';

    public const ROLE_IDS = 'role_ids';

    public const ROLES = 'roles';

    public const SECTOR_ID = 'sector_id';

    protected BaseRequest $request;

    protected RoleRepositoryInterface $roleRepository;

    protected SectorRepositoryInterface $sectorRepository;

    protected UserRepositoryInterface $userRepository;

    private ?Collection $roles = null;

    public function __construct(
        BaseRequest $request,
        UserRepositoryInterface $userRepository,
        SectorRepositoryInterface $sectorRepository,
        RoleRepositoryInterface $roleRepository
    ) {
        $this->request = $request;
        $this->userRepository = $userRepository;
        $this->sectorRepository = $sectorRepository;
        $this->roleRepository = $roleRepository;
    }

    public function companyId(): string
    {
        return $this->request->companyId();
    }

    public function email(): string
    {
        return $this->request->input(self::EMAIL);
    }

    public function identityDocument(): ?string
    {
        return $this->request->input(self::IDENTITY_DOCUMENT);
    }

    public function modelToNotify(): ?UserNotifiable
    {
        return null;
    }

    public function name(): string
    {
        return $this->request->input(self::NAME);
    }

    public function password(): ?string
    {
        return null;
    }

    public function roles(): ?Collection
    {
        if (! $this->roles) {
            $this->roles = $this->roleRepository->whereIn($this->roleIds());
        }

        return $this->roles;
    }

    public function rules(): array
    {
        return [
            self::NAME => 'bail|required|string|alpha|min:2|max:40',
            self::SECTOR_ID => [
                'bail',
                'nullable',
                'uuid',
                'exists:' . ConcatHelper::rules(
                    $this->sectorRepository->table(),
                    Sector::ID,
                    Sector::COMPANY_ID,
                    $this->companyId()
                ),
            ],
            self::IDENTITY_DOCUMENT => 'bail|nullable|string|min:2|max:30|alpha_num',
            self::ROLE_IDS => [
                'bail',
                'required',
                'array',
                new OnlyUserRoleRule(),
            ],
            self::ROLE_IDS . '.*' => [
                'bail',
                'required',
                'uuid',
                'distinct',
                Rule::in($this->roleRepository->allWithoutAdmin()->pluck(Sector::ID)->toArray()),
            ],
        ];
    }

    public function sectorId(): ?string
    {
        return $this->request->input(self::SECTOR_ID);
    }

    public function typeToNotify(): string
    {
        return UserNotifiable::INVITATION_TYPE;
    }

    public function validate(): void
    {
        $this->request->validate($this->rules());
    }

    private function roleIds(): array
    {
        return $this->request->input(self::ROLE_IDS);
    }
}
