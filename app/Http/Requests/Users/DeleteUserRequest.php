<?php

namespace App\Http\Requests\Users;

use App\Helpers\ConcatHelper;
use App\Http\Requests\BaseRequest;
use Binaccle\Models\Users\User;
use Binaccle\Repositories\Users\UserRepositoryInterface;
use Binaccle\Rules\Equipments\Assignments\HasAnEquipmentAssignedRule;

class DeleteUserRequest extends BaseRequest
{
    public const USER_ID = 'userId';

    private ?User $userTo = null;

    public function __construct(
        private UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    public function all($keys = null): array
    {
        $all = parent::all();
        $all[self::USER_ID] = $this->userToId();

        return $all;
    }

    public function rules(): array
    {
        return [
            self::USER_ID => [
                'bail',
                'required',
                'uuid',
                'exists:' . ConcatHelper::rules(
                    $this->userRepository->table(),
                    User::ID,
                    User::COMPANY_ID,
                    $this->companyId()
                ),
                new HasAnEquipmentAssignedRule(),
            ],
        ];
    }

    public function userTo(): User
    {
        if (! $this->userTo) {
            $this->userTo = $this->userRepository->findOrFail($this->userToId());
        }

        return $this->userTo;
    }

    public function userToId(): string
    {
        return $this->route(self::USER_ID);
    }
}
