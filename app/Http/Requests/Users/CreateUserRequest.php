<?php

namespace App\Http\Requests\Users;

use App\Helpers\ConcatHelper;
use Binaccle\Models\Users\User;
use Binaccle\Payloads\Users\CreateUserPayload;

class CreateUserRequest extends AbstractUpsertUserRequest implements CreateUserPayload
{
    private ?string $companyId = null;

    private User $currentUser;

    public function companyId(): string
    {
        return $this->companyId ?? $this->request->companyId();
    }

    public function currentUser(): User
    {
        return $this->currentUser;
    }

    public function rules(): array
    {
        $rules = [
            self::EMAIL => [
                'bail',
                'required',
                'email',
                'regex:/^((?!\+{1,}).)*$/',
                'unique:' . ConcatHelper::rules(
                    $this->userRepository->table(),
                    User::EMAIL,
                    'NULL',
                    User::ID,
                    User::COMPANY_ID,
                    $this->companyId()
                ),
            ],
        ];

        return array_merge(parent::rules(), $rules);
    }

    public function setCompanyId($companyId): void
    {
        $this->companyId = $companyId;
    }

    public function setCurrentUser(User $currentUser): void
    {
        $this->currentUser = $currentUser;
    }
}
