<?php

namespace App\Http\Requests\Shared;

use App\Http\Requests\BaseRequest;
use Binaccle\Payloads\Users\Notifiables\UserNotifiablePayload;
use Binaccle\Repositories\Users\Notifiables\UserNotifiableRepositoryInterface;
use Binaccle\Repositories\Users\UserRepositoryInterface;

abstract class AbstractSendNotificationRequest extends BaseRequest implements UserNotifiablePayload
{
    public const EMAIL = 'email';

    protected UserNotifiableRepositoryInterface $userNotifiableRepository;

    protected UserRepositoryInterface $userRepository;

    public function rules(): array
    {
        return [
            self::EMAIL => 'bail|required|email',
        ];
    }

    public function typeToNotify(): string
    {
        return static::TYPE;
    }
}
