<?php

namespace App\Http\Requests\Shared;

use App\Http\Requests\BaseRequest;
use Binaccle\Enums\MassiveUploads\MassiveUploadHeaderEnum;
use Binaccle\Enums\MassiveUploads\MassiveUploadStateEnum;
use Binaccle\Models\MassiveUploads\MassiveUploadState;
use Binaccle\Models\MassiveUploads\MassiveUploadType;
use Binaccle\Payloads\MassiveUploads\MassiveUploadPayload;
use Binaccle\Repositories\MassiveUploads\MassiveUploadStateRepositoryInterface;
use Binaccle\Repositories\MassiveUploads\MassiveUploadTypeRepositoryInterface;
use Binaccle\Rules\MassiveUploads\CsvHeaderRule;
use Binaccle\Rules\MassiveUploads\EmptyCsvRule;
use Illuminate\Http\UploadedFile;

abstract class AbstractMassiveUploadRequest extends BaseRequest implements MassiveUploadPayload
{
    public const FILE = 'file';

    private ?string $massiveUploadStateId = null;

    private ?string $massiveUploadTypeId = null;

    private string $relativePath;

    private string $url;

    public function __construct(
        private MassiveUploadStateRepositoryInterface $massiveUploadStateRepository,
        private MassiveUploadTypeRepositoryInterface $massiveUploadTypeRepository
    ) {
        $this->massiveUploadStateRepository = $massiveUploadStateRepository;
        $this->massiveUploadTypeRepository = $massiveUploadTypeRepository;
    }

    public function massiveUploadStateId(): string
    {
        if (! $this->massiveUploadStateId) {
            $this->massiveUploadStateId = $this->massiveUploadStateRepository->firstOrFailBy(
                MassiveUploadState::NAME,
                MassiveUploadStateEnum::PENDING
            )
            ->id();
        }

        return $this->massiveUploadStateId;
    }

    public function massiveUploadTypeId(): string
    {
        if (! $this->massiveUploadTypeId) {
            $this->massiveUploadTypeId = $this->massiveUploadTypeRepository->firstOrFailBy(MassiveUploadType::NAME, static::TYPE)->id();
        }

        return $this->massiveUploadTypeId;
    }

    public function relativePath(): string
    {
        return $this->relativePath;
    }

    public function rules(): array
    {
        return [
            self::FILE => [
                'bail',
                'required',
                'file',
                // 'mimetypes:text/csv,text/plain',
                'max:500',
                new CsvHeaderRule($this->headers()),
                new EmptyCsvRule(),
            ],
        ];
    }

    public function setRelativePath(string $relativePath): void
    {
        $this->relativePath = $relativePath;
    }

    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    public function uploadedFile(): UploadedFile
    {
        return $this->file(self::FILE);
    }

    public function url(): string
    {
        return $this->url;
    }

    private function headers(): array
    {
        return customTrans('validation.attributes', MassiveUploadHeaderEnum::HEADERS[static::TYPE]);
    }
}
