<?php

namespace App\Http\Requests\Auth\Register;

use App\Helpers\ConcatHelper;
use App\Http\Requests\BaseRequest;
use Binaccle\Enums\Roles\RoleEnum;
use Binaccle\Models\Users\User;
use Binaccle\Models\Users\UserNotifiable;
use Binaccle\Payloads\Auth\Register\RegisterPayload;
use Binaccle\Payloads\Users\Notifiables\UserNotifiablePayload;
use Binaccle\Repositories\Plans\PlanRepositoryInterface;
use Binaccle\Repositories\Roles\RoleRepositoryInterface;
use Binaccle\Repositories\Users\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Validation\Rule;

class RegisterUserRequest extends BaseRequest implements RegisterPayload, UserNotifiablePayload
{
    public const COMPANY_NAME = 'company_name';

    public const EMAIL = 'email';

    public const NAME = 'name';

    public const PLAN_ID = 'plan_id';

    private ?string $companyId = null;

    private User $currentUser;

    private ?Collection $roles = null;

    public function __construct(
        private PlanRepositoryInterface $planRepository,
        private UserRepositoryInterface $userRepository,
        private RoleRepositoryInterface $roleRepository
    ) {
        $this->planRepository = $planRepository;
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
    }

    public function companyId(): string
    {
        return $this->companyId;
    }

    public function companyName(): string
    {
        return $this->input(self::COMPANY_NAME);
    }

    public function currentUser(): User
    {
        return $this->currentUser;
    }

    public function email(): string
    {
        return $this->input(self::EMAIL);
    }

    public function identityDocument(): ?string
    {
        return null;
    }

    public function modelToNotify(): ?UserNotifiable
    {
        return null;
    }

    public function name(): string
    {
        return $this->input(self::NAME);
    }

    public function password(): string
    {
        return $this->input(self::PASSWORD);
    }

    public function planId(): string
    {
        return $this->input(self::PLAN_ID);
    }

    public function roles(): ?Collection
    {
        if (! $this->roles) {
            $this->roles = $this->roleRepository->whereNameIn([RoleEnum::ADMIN]);
        }

        return $this->roles;
    }

    public function rules(): array
    {
        $rules = [
            self::NAME => 'bail|required|string|min:2|max:40',
            self::PASSWORD => $this->passwordRules(),
            self::EMAIL => [
                'bail',
                'required',
                'email',
                'regex:/^((?!\+{1,}).)*$/',
                'unique:' . ConcatHelper::rules(
                    $this->userRepository->table(),
                    User::EMAIL
                ),
            ],
            self::COMPANY_NAME => $this->companyNameRules(),
            self::PLAN_ID => [
                'bail',
                'required',
                'uuid',
                Rule::in(
                    []
                ),
            ],
        ];

        return array_merge($rules, $this->captchaRules());
    }

    public function sectorId(): ?string
    {
        return null;
    }

    public function setCompanyId(string $companyId): void
    {
        $this->companyId = $companyId;
    }

    public function setCurrentUser(User $user): void
    {
        $this->currentUser = $user;
    }

    public function typeToNotify(): string
    {
        return UserNotifiable::VERIFICATION_TYPE;
    }
}
