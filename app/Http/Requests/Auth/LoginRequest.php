<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\BaseRequest;
use Binaccle\Models\Users\User;
use Binaccle\Payloads\Auth\LoginPayload;
use Binaccle\Repositories\Users\UserRepositoryInterface;
use Binaccle\Rules\Users\IsVerifiedRule;
use Binaccle\Rules\Users\ValidCredentialsRule;

class LoginRequest extends BaseRequest implements LoginPayload
{
    public const EMAIL = 'email';

    private ?User $currentUser = null;

    public function __construct(
        private UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    public function currentUser(): User
    {
        if (! $this->currentUser) {
            $this->currentUser = $this->userRepository->firstBy(User::EMAIL, $this->email());
        }

        return $this->currentUser;
    }

    public function rules(): array
    {
        $rules = [
            self::EMAIL => [
                'bail',
                'required',
                'email',
                new ValidCredentialsRule(),
                new IsVerifiedRule(),
            ],
            self::PASSWORD => $this->passwordRules(),
        ];

        return array_merge($rules, $this->captchaRules());
    }

    private function email(): string
    {
        return $this->input(self::EMAIL);
    }
}
