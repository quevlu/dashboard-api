<?php

namespace App\Http\Requests\Auth\ChangePassword;

use App\Http\Requests\BaseRequest;
use Binaccle\Models\Users\User;
use Binaccle\Payloads\Auth\ChangePassword\ChangePasswordPayload;

class ChangePasswordRequest extends BaseRequest implements ChangePasswordPayload
{
    private const NEW_PASSWORD = 'new_password';

    private ?User $currentUser = null;

    public function currentUser(): User
    {
        if (! $this->currentUser) {
            /** @var User $user */
            $user = auth()->user();
            $this->currentUser = $user;
        }

        return $this->currentUser;
    }

    public function newPassword(): string
    {
        return $this->input(self::NEW_PASSWORD);
    }

    public function rules(): array
    {
        $commonPasswordRules = $this->passwordRules();

        return [
            self::PASSWORD => $commonPasswordRules . '|current_password:api',
            self::NEW_PASSWORD => $commonPasswordRules . '|different:' . self::PASSWORD,
        ];
    }
}
