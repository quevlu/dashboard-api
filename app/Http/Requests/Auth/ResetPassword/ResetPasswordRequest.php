<?php

namespace App\Http\Requests\Auth\ResetPassword;

use App\Http\Requests\BaseRequest;
use Binaccle\Models\Users\User;
use Binaccle\Models\Users\UserNotifiable;
use Binaccle\Payloads\Auth\ResetPassword\ResetPasswordPayload;
use Binaccle\Repositories\Users\Notifiables\UserNotifiableRepositoryInterface;

class ResetPasswordRequest extends BaseRequest implements ResetPasswordPayload
{
    private const TOKEN = 'token';

    private ?User $currentUser = null;

    private ?UserNotifiable $userNotifiable = null;

    public function __construct(
        private UserNotifiableRepositoryInterface $userNotifiableRepository
    ) {
        $this->userNotifiableRepository = $userNotifiableRepository;
    }

    public function all($keys = null): array
    {
        $all = parent::all();
        $all[self::TOKEN] = $this->token();

        return $all;
    }

    public function currentUser(): ?User
    {
        if ($this->userNotifiable() && ! $this->currentUser) {
            $this->currentUser = $this->userNotifiable->userRel();
        }

        return $this->currentUser;
    }

    public function password(): string
    {
        return $this->input(self::PASSWORD);
    }

    public function rules(): array
    {
        $rules = [
            self::TOKEN => 'bail|required|string|size:' . UserNotifiable::TOKEN_LENGTH,
            self::PASSWORD => $this->passwordRules(),
        ];

        return array_merge($rules, $this->captchaRules());
    }

    public function userNotifiable(): ?UserNotifiable
    {
        if (! $this->userNotifiable) {
            $this->userNotifiable = $this->userNotifiableRepository->findByTypeAndToken(UserNotifiable::RESET_TYPE, $this->token());
        }

        return $this->userNotifiable;
    }

    private function token(): string
    {
        return $this->route(self::TOKEN);
    }
}
