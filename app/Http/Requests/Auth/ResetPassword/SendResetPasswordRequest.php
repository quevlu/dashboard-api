<?php

namespace App\Http\Requests\Auth\ResetPassword;

use App\Http\Requests\Shared\AbstractSendNotificationRequest;
use Binaccle\Models\Users\User;
use Binaccle\Models\Users\UserNotifiable;
use Binaccle\Repositories\Users\UserRepositoryInterface;

class SendResetPasswordRequest extends AbstractSendNotificationRequest
{
    protected const TYPE = UserNotifiable::RESET_TYPE;

    private ?User $currentUser = null;

    private ?UserNotifiable $userNotifiable = null;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function currentUser(): ?User
    {
        if (! $this->currentUser) {
            $this->currentUser = $this->userRepository->firstBy(User::EMAIL, $this->input(self::EMAIL));
        }

        return $this->currentUser;
    }

    public function modelToNotify(): ?UserNotifiable
    {
        if ($this->currentUser() && ! $this->userNotifiable) {
            $this->userNotifiable = $this->currentUser->resetRel();
        }

        return $this->userNotifiable;
    }

    public function rules(): array
    {
        return array_merge(parent::rules(), $this->captchaRules());
    }
}
