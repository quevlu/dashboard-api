<?php

namespace App\Http\Requests\Auth\Verifications;

use App\Http\Requests\BaseRequest;
use Binaccle\Models\Users\UserNotifiable;
use Binaccle\Repositories\Users\Notifiables\UserNotifiableRepositoryInterface;

class VerifyTokenRequest extends BaseRequest
{
    private const TOKEN = 'token';

    private ?UserNotifiable $userNotifiable = null;

    public function __construct(
        private UserNotifiableRepositoryInterface $userNotifiableRepository
    ) {
        $this->userNotifiableRepository = $userNotifiableRepository;
    }

    public function all($keys = null): array
    {
        $all = parent::all();
        $all[self::TOKEN] = $this->token();

        return $all;
    }

    public function rules(): array
    {
        $rules = [
            self::TOKEN => 'bail|required|string|size:' . UserNotifiable::TOKEN_LENGTH,
        ];

        return array_merge($rules, $this->captchaRules());
    }

    public function userNotifiable(): ?UserNotifiable
    {
        if (! $this->userNotifiable) {
            $this->userNotifiable = $this->userNotifiableRepository->findByTypeAndToken(UserNotifiable::VERIFICATION_TYPE, $this->token());
        }

        return $this->userNotifiable;
    }

    private function token(): string
    {
        return $this->route(self::TOKEN);
    }
}
