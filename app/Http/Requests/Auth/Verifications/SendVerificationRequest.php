<?php

namespace App\Http\Requests\Auth\Verifications;

use App\Http\Requests\Shared\AbstractSendNotificationRequest;
use Binaccle\Models\Users\User;
use Binaccle\Models\Users\UserNotifiable;
use Binaccle\Repositories\Users\Notifiables\UserNotifiableRepositoryInterface;
use Binaccle\Repositories\Users\UserRepositoryInterface;

class SendVerificationRequest extends AbstractSendNotificationRequest
{
    protected const TYPE = UserNotifiable::VERIFICATION_TYPE;

    private ?User $currentUser = null;

    private ?UserNotifiable $userVerification = null;

    public function __construct(UserRepositoryInterface $userRepository, UserNotifiableRepositoryInterface $userNotifiableRepository)
    {
        $this->userRepository = $userRepository;
        $this->userNotifiableRepository = $userNotifiableRepository;
    }

    public function currentUser(): ?User
    {
        if (! $this->currentUser) {
            $this->currentUser = $this->userRepository->firstBy(User::EMAIL, $this->input(self::EMAIL));
        }

        return $this->currentUser;
    }

    public function modelToNotify(): ?UserNotifiable
    {
        if ($this->currentUser() && ! $this->userVerification) {
            $this->userVerification = $this->currentUser->verificationRel();
        }

        return $this->userVerification;
    }

    public function rules(): array
    {
        return array_merge(parent::rules(), $this->captchaRules());
    }
}
