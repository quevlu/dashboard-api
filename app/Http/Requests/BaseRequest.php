<?php

namespace App\Http\Requests;

use Binaccle\Models\Companies\Company;
use Illuminate\Foundation\Http\FormRequest;

class BaseRequest extends FormRequest
{
    public const PASSWORD = 'password';

    private const CAPTCHA = 'captcha';

    protected ?Company $company = null;

    public function authorize(): bool
    {
        return true;
    }

    public function company(): ?Company
    {
        if (! $this->company && auth()->hasUser()) {
            $this->company = auth()->user()->companyRel();
        }

        return $this->company;
    }

    public function companyId(): string
    {
        return auth()->hasUser() ? $this->company()->id() : null;
    }

    public function rules(): array
    {
        return [];
    }

    protected function captchaRules(): ?array
    {
        return [
            self::CAPTCHA => captchaEnabled() ? 'bail|required|captcha' : '',
        ];
    }

    protected function companyNameRules(): string
    {
        return 'bail|required|min:1|max:40';
    }

    protected function passwordRules(): string
    {
        return 'bail|required|case_diff|numbers|letters|min:8|max:30';
    }
}
