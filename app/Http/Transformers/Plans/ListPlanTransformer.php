<?php

namespace App\Http\Transformers\Plans;

use App\Http\Transformers\AbstractTransformer;

class ListPlanTransformer extends AbstractTransformer
{
    public function nativeTransform($plan): array
    {
        return $plan;
    }
}
