<?php

namespace App\Http\Transformers\Sectors;

use App\Http\Transformers\AbstractTransformer;
use Binaccle\Models\Sectors\Sector;

class AbstractSectorTransformer extends AbstractTransformer
{
    public function nativeTransform($sector): array
    {
        return [
            Sector::ID => $sector->id(),
            Sector::NAME => $sector->name(),
            Sector::CREATED_AT => $this->displayTime($sector->createdAt()),
        ];
    }
}
