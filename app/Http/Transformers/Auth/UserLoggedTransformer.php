<?php

namespace App\Http\Transformers\Auth;

use App\Http\Transformers\AbstractTransformer;
use Binaccle\Enums\Permissions\PermissionEnum;
use Binaccle\Models\Users\User;

class UserLoggedTransformer extends AbstractTransformer
{
    public function nativeTransform($user): array
    {
        $companyPlan = $user->companyRel()->planRel();

        $permissions = PermissionEnum::rolePermissionsByPlan($companyPlan->name(), $user->getRoleNames());

        return [
            User::NAME => $user->name(),
            User::EMAIL => $user->email(),
            User::IDENTITY_DOCUMENT => $user->identityDocument(),
            User::TIMEZONE => $user->timezone(),
            User::CREATED_AT => $this->displayTime($user->createdAt()),
            config('permission.table_names.permissions') => $permissions,
        ];
    }
}
