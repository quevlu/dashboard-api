<?php

namespace App\Http\Transformers\Auth;

use Flugg\Responder\Transformers\Transformer;

class LoginTransformer extends Transformer
{
    public function transform(object $object): array
    {
        return [
            'token' => $object->token(),
        ];
    }
}
