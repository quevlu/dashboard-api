<?php

namespace App\Http\Transformers\MassiveUploads;

use App\Http\Transformers\AbstractTransformer;
use Binaccle\Models\MassiveUploads\MassiveUpload;
use Binaccle\Models\MassiveUploads\MassiveUploadState;
use Illuminate\Support\Carbon;

class AbstractMassiveUploadTransformer extends AbstractTransformer
{
    public function nativeTransform($massiveUpload): array
    {
        $createdAt = Carbon::createFromTimestamp($massiveUpload->{MassiveUpload::CREATED_AT});

        return [
            MassiveUpload::ID => $massiveUpload->{MassiveUpload::ID},
            MassiveUpload::PROCESSED => $massiveUpload->{MassiveUpload::PROCESSED},
            MassiveUpload::ERROR => $massiveUpload->{MassiveUpload::ERROR},
            MassiveUpload::CREATED_AT => $this->displayTime($createdAt),
            MassiveUpload::MASSIVE_UPLOAD_STATE_RELATIONSHIP => [
                MassiveUploadState::NAME => $massiveUpload->{MassiveUpload::MASSIVE_UPLOAD_STATE_RELATIONSHIP},
            ],
        ];
    }
}
