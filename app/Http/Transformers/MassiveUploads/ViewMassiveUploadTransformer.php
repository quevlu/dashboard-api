<?php

namespace App\Http\Transformers\MassiveUploads;

use Binaccle\Models\MassiveUploads\MassiveUpload;
use Binaccle\Models\MassiveUploads\MassiveUploadState;
use Binaccle\Models\MassiveUploads\MassiveUploadType;

class ViewMassiveUploadTransformer extends AbstractMassiveUploadTransformer
{
    public function nativeTransform($massiveUpload): array
    {
        $parentResult = parent::nativeTransform($massiveUpload);

        $parentResult[MassiveUpload::RESULT] = $this->renderResult($massiveUpload->result());
        $parentResult[MassiveUpload::CREATED_AT] = $this->displayTime($massiveUpload->createdAt());
        $parentResult[MassiveUpload::MASSIVE_UPLOAD_STATE_RELATIONSHIP] = [
            MassiveUploadState::NAME => $massiveUpload->stateRel()->name(),
        ];
        $parentResult[MassiveUpload::MASSIVE_UPLOAD_TYPE_RELATIONSHIP] = [
            MassiveUploadType::NAME => $massiveUpload->typeRel()->name(),
        ];

        return $parentResult;
    }

    private function renderResult(string $result)
    {
        $lines = json_decode($result, true);

        $allAttributes = trans('validation.attributes');

        foreach ($lines as $lineError => $errors) {
            if (! is_string($errors)) {
                foreach ($errors as $attribute => $error) {
                    $keyError = array_keys($error)[0];
                    $keyErrorMsg = strtolower($keyError);

                    $ruleParams = $error[$keyError];
                    $ruleParamsReplacers = [];

                    if ($ruleParams) {
                        $ruleParamsReplacers[$keyErrorMsg] = $ruleParams[0];
                    }

                    $styledAttribute = @$allAttributes[$attribute] ?? str_replace('_', ' ', $attribute);

                    $suffixKeyMessage = is_array(trans("validation.{$keyErrorMsg}")) ? '.string' : null;

                    $replacers = array_merge(
                        [
                            'attribute' => $styledAttribute,
                        ],
                        $ruleParamsReplacers
                    );

                    $errorMessage = trans("validation.{$keyErrorMsg}{$suffixKeyMessage}", $replacers);

                    $lines[$lineError][$attribute] = $errorMessage;

                    // dump($error);
                }
            } else {
                $lines[$lineError] = trans($errors);
            }
        }

        return $lines;
    }
}
