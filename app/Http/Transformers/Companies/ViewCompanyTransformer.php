<?php

namespace App\Http\Transformers\Companies;

use Binaccle\Models\Companies\Company;
use Flugg\Responder\Transformers\Transformer;

class ViewCompanyTransformer extends Transformer
{
    public function transform(Company $company): array
    {
        return [
            Company::NAME => $company->name(),
            Company::LOGO_RELATIONSHIP => callIf($company->logoRel(), 'url'),
        ];
    }
}
