<?php

namespace App\Http\Transformers;

use Flugg\Responder\Transformers\Transformer;
use Illuminate\Support\Carbon;
use Infrastructure\Repositories\Criterias\BaseCriteria;
use JamesMills\LaravelTimezone\Facades\Timezone;

abstract class AbstractTransformer extends Transformer
{
    protected $load = [];

    protected $relations = [];

    public function __construct(
        private BaseCriteria $criteria
    ) {
    }

    abstract public function nativeTransform($object);

    public function transform($object): array
    {
        if ($this->criteria->total()) {
            return $this->transformPagination($object);
        }

        return $this->nativeTransform($object);
    }

    public function transformPagination(object $object): array
    {
        return [
            'total' => $object->total(),
        ];
    }

    protected function displayTime(Carbon $timestamp)
    {
        return Timezone::convertToLocal($timestamp);
    }
}
