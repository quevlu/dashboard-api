<?php

namespace App\Http\Transformers\Roles;

use App\Http\Transformers\AbstractTransformer;
use Binaccle\Models\Roles\Role;

class ListRoleTransformer extends AbstractTransformer
{
    public function nativeTransform($role): array
    {
        return [
            Role::ID => $role->id(),
            Role::NAME => $role->name(),
        ];
    }
}
