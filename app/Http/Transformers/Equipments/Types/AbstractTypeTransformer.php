<?php

namespace App\Http\Transformers\Equipments\Types;

use App\Http\Transformers\AbstractTransformer;
use Binaccle\Models\Equipments\EquipmentType;

class AbstractTypeTransformer extends AbstractTransformer
{
    public function nativeTransform($equipmentType): array
    {
        return [
            EquipmentType::ID => $equipmentType->id(),
            EquipmentType::NAME => $equipmentType->name(),
            EquipmentType::BRAND => $equipmentType->brand(),
            EquipmentType::MODEL => $equipmentType->model(),
            EquipmentType::PREFIX => $equipmentType->prefix(),
            EquipmentType::CHARACTERISTICS => $equipmentType->characteristics(),
            EquipmentType::CREATED_AT => $this->displayTime($equipmentType->createdAt()),
        ];
    }
}
