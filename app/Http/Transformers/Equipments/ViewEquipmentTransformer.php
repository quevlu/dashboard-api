<?php

namespace App\Http\Transformers\Equipments;

use App\Http\Transformers\AbstractTransformer;
use App\Http\Transformers\Equipments\Types\ViewTypeTransformer;
use Binaccle\Models\Equipments\Equipment;
use Binaccle\Models\Equipments\EquipmentType;

class ViewEquipmentTransformer extends AbstractTransformer
{
    public function nativeTransform($equipment): array
    {
        $equipmentType = $equipment->typeRel();

        $equipmentTypeTransformer = (app(ViewTypeTransformer::class))->transform($equipmentType);
        unset($equipmentTypeTransformer[EquipmentType::ID],$equipmentTypeTransformer[EquipmentType::PREFIX]);

        $equipmentArray = [
            Equipment::ID => $equipment->id(),
            Equipment::FULL_IDENTIFIER => $equipment->fullIdentifier(),
            Equipment::EQUIPMENT_ASSIGNED_RELATIONSHIP => $equipment->assignedTo(),
        ];

        return array_merge($equipmentArray, $equipmentTypeTransformer);
    }
}
