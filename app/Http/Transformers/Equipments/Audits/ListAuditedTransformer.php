<?php

namespace App\Http\Transformers\Equipments\Audits;

use App\Http\Transformers\AbstractTransformer;
use Binaccle\Models\Equipments\Equipment;
use Binaccle\Models\Equipments\EquipmentAssignment;
use Binaccle\Models\Equipments\EquipmentAuditAudited;

class ListAuditedTransformer extends AbstractTransformer
{
    public function nativeTransform($equipmentAuditAudited): array
    {
        $equipment = $equipmentAuditAudited->equipmentRel();
        $equipmentAssignment = $equipmentAuditAudited->equipmentAssignmentRel();

        return [
            EquipmentAuditAudited::AUDITED => $equipmentAuditAudited->audited(),
            EquipmentAuditAudited::UPDATED_AT => $equipmentAuditAudited->updatedAt(),
            EquipmentAssignment::EQUIPMENT_ASSIGNED_RELATIONSHIP => $equipmentAssignment ? $equipmentAssignment->assignedTo() : null,
            Equipment::FULL_IDENTIFIER => $equipment->fullIdentifier(),
            Equipment::TRASHED => $equipment->trashed(),
        ];
    }
}
