<?php

namespace App\Http\Transformers\Equipments\Audits;

use App\Http\Transformers\AbstractTransformer;
use Binaccle\Models\Equipments\EquipmentAudit;
use Binaccle\Models\Equipments\EquipmentAuditState;
use Binaccle\Models\Sectors\Sector;

class ViewAuditTransformer extends AbstractTransformer
{
    public function nativeTransform($equipmentAudit): array
    {
        return [
            EquipmentAudit::ID => $equipmentAudit->id(),
            EquipmentAudit::TITLE => $equipmentAudit->title(),
            EquipmentAudit::EQUIPMENT_AUDIT_SECTOR_RELATIONSHIP => [
                Sector::NAME => callIf($equipmentAudit->sectorRel(), 'name'),
            ],
            EquipmentAudit::EQUIPMENT_AUDIT_STATE_RELATIONSHIP => [
                EquipmentAuditState::NAME => $equipmentAudit->stateRel()->name(),
            ],
            EquipmentAudit::CREATED_AT => $this->displayTime($equipmentAudit->createdAt()),
            EquipmentAudit::UPDATED_AT => $this->displayTime($equipmentAudit->updatedAt()),
        ];
    }
}
