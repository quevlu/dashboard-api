<?php

namespace App\Http\Transformers\Equipments;

use App\Http\Transformers\AbstractTransformer;
use Binaccle\Models\Equipments\Equipment;
use Binaccle\Models\Equipments\EquipmentType;
use Illuminate\Support\Carbon;

class ListEquipmentTransformer extends AbstractTransformer
{
    public function nativeTransform($equipment): array
    {
        $createdAt = Carbon::createFromTimestamp($equipment->{Equipment::CREATED_AT});

        return [
            Equipment::ID => $equipment->{Equipment::ID},
            Equipment::IDENTIFIER => $equipment->{EquipmentType::PREFIX} . $equipment->{Equipment::IDENTIFIER},
            EquipmentType::NAME => $equipment->{EquipmentType::NAME},
            EquipmentType::BRAND => $equipment->{EquipmentType::BRAND},
            EquipmentType::MODEL => $equipment->{EquipmentType::MODEL},
            EquipmentType::CHARACTERISTICS => $equipment->{EquipmentType::CHARACTERISTICS},
            Equipment::EQUIPMENT_ASSIGNED_RELATIONSHIP => $equipment->{Equipment::EQUIPMENT_ASSIGNED_RELATIONSHIP},
            Equipment::CREATED_AT => $this->displayTime($createdAt),
        ];
    }
}
