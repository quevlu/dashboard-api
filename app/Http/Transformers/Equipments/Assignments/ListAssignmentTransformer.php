<?php

namespace App\Http\Transformers\Equipments\Assignments;

use App\Http\Transformers\AbstractTransformer;
use Binaccle\Models\Equipments\Equipment;
use Binaccle\Models\Equipments\EquipmentType;
use Illuminate\Support\Carbon;

class ListAssignmentTransformer extends AbstractTransformer
{
    public function nativeTransform($equipment): array
    {
        $createdAt = Carbon::createFromTimestamp($equipment->{Equipment::CREATED_AT});

        return [
            Equipment::ID => $equipment->{Equipment::ID},
            Equipment::FULL_IDENTIFIER => $equipment->{EquipmentType::PREFIX} . $equipment->{Equipment::IDENTIFIER},
            Equipment::OBSERVATION => $equipment->{Equipment::OBSERVATION},
            EquipmentType::NAME => $equipment->{EquipmentType::NAME},
            Equipment::EQUIPMENT_ASSIGNED_RELATIONSHIP => $equipment->{Equipment::EQUIPMENT_ASSIGNED_RELATIONSHIP},
            Equipment::ASSIGNMENTS_COUNT => $equipment->{Equipment::ASSIGNMENTS_COUNT},
            Equipment::TRASHED => (bool) $equipment->deleted_at,
            Equipment::CREATED_AT => $this->displayTime($createdAt),
        ];
    }
}
