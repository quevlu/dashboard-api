<?php

namespace App\Http\Transformers\Equipments\Assignments;

use App\Http\Transformers\AbstractTransformer;
use Binaccle\Models\Equipments\Equipment;
use Binaccle\Models\Equipments\EquipmentType;

class ViewAssignmentTransformer extends AbstractTransformer
{
    public function nativeTransform($equipment): array
    {
        return [
            Equipment::ID => $equipment->id(),
            Equipment::FULL_IDENTIFIER => $equipment->fullIdentifier(),
            Equipment::OBSERVATION => $equipment->observation(),
            EquipmentType::NAME => $equipment->type->name(),
            Equipment::EQUIPMENT_ASSIGNED_RELATIONSHIP => $equipment->assignedTo(),
            Equipment::TRASHED => $equipment->trashed(),
            Equipment::CREATED_AT => $this->displayTime($equipment->createdAt()),
        ];
    }
}
