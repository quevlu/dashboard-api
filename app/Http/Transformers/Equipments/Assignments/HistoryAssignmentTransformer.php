<?php

namespace App\Http\Transformers\Equipments\Assignments;

use App\Http\Transformers\AbstractTransformer;
use Binaccle\Models\Equipments\EquipmentAssignment;
use Binaccle\Models\Sectors\Sector;
use Binaccle\Models\Users\User;

class HistoryAssignmentTransformer extends AbstractTransformer
{
    public function nativeTransform($equipmentAssignment): array
    {
        $active = $equipmentAssignment->active();

        return [
            EquipmentAssignment::ACTIVE => $active,
            EquipmentAssignment::OBSERVATION => $equipmentAssignment->observation(),
            EquipmentAssignment::CREATED_AT => $this->displayTime($equipmentAssignment->createdAt()),
            EquipmentAssignment::UPDATED_AT => $active ? null : $this->displayTime($equipmentAssignment->updatedAt()),
            EquipmentAssignment::ASSIGNED_TIME => $active ? null : $equipmentAssignment->updatedAt()->diffInSeconds($equipmentAssignment->createdAt()),
            EquipmentAssignment::USER_RELATIONSHIP => [
                User::NAME => callIf($equipmentAssignment->userRel(), 'name'),
            ],
            EquipmentAssignment::SECTOR_RELATIONSHIP => [
                Sector::NAME => callIf($equipmentAssignment->sectorRel(), 'name'),
            ],
        ];
    }
}
