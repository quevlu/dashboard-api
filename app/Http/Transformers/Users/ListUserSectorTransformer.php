<?php

namespace App\Http\Transformers\Users;

use App\Http\Transformers\AbstractTransformer;
use Binaccle\Models\Users\User;

class ListUserSectorTransformer extends AbstractTransformer
{
    public function nativeTransform($user): array
    {
        $data = [
            User::ID => $user->id(),
            User::NAME => $user->name(),
        ];

        return $data;
    }
}
