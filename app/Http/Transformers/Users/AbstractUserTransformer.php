<?php

namespace App\Http\Transformers\Users;

use App\Http\Transformers\AbstractTransformer;
use Binaccle\Enums\Users\UserVerifiedEnum;
use Binaccle\Models\Roles\Role;
use Binaccle\Models\Sectors\Sector;
use Binaccle\Models\Users\User;
use Binaccle\Models\Users\UserNotifiable;

class AbstractUserTransformer extends AbstractTransformer
{
    public function nativeTransform($user): array
    {
        $additionalData = [];
        $translationUserVerified = trans('enums.' . UserVerifiedEnum::class);

        if ($user->password) {
            $additionalData = [
                UserNotifiable::VERIFIED => $translationUserVerified[UserVerifiedEnum::VERIFIED],
            ];
        } else {
            $userInvitation = $user->invitationRel();

            if (! $userInvitation && ! $user->password()) {
                $additionalData[UserNotifiable::VERIFIED] = $translationUserVerified[UserVerifiedEnum::DONT_APPLY];
            } else {
                $additionalData = [
                    User::INVITATION_RELATIONSHIP => [
                        UserNotifiable::LEFT => $userInvitation->left(),
                    ],
                    UserNotifiable::VERIFIED => $translationUserVerified[UserVerifiedEnum::PENDING],
                ];
            }
        }

        $roles = $user->rolesRel()->map(function ($item, $key) {
            return [
                Role::ID => $item->id(),
                Role::NAME => $item->name(),
            ];
        });

        $data = [
            User::ID => $user->id(),
            User::NAME => $user->name(),
            User::EMAIL => $user->email(),
            User::SECTOR_RELATIONSHIP => [
                Sector::NAME => callIf($user->sectorRel(), 'name'),
            ],
            User::ROLES_RELATIONSHIP => $roles,
            User::IDENTITY_DOCUMENT => $user->identityDocument(),
            User::CREATED_AT => $this->displayTime($user->createdAt()),
        ];

        return array_merge($data, $additionalData);
    }
}
