<?php

namespace App\Http\Middlewares;

use App\Helpers\ConcatHelper;
use Closure;
use Illuminate\Cache\RateLimiter;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Http\Request;
use Infrastructure\Redis\LimiterStorage;

class ThrottleMiddleware
{
    private RateLimiter $rateLimiter;

    public function __construct(LimiterStorage $limiterStorage)
    {
        $this->rateLimiter = new RateLimiter($limiterStorage->getCache());
    }

    public function handle(Request $request, Closure $next, int $requestsPerTime = 3, int $minutes = 60)
    {
        if (config('app.throttle')) {
            $minutesToSeconds = (int) $minutes * 60;
            $key = ConcatHelper::cacheTag($request->route()->getName(), $request->ip());

            if ($this->rateLimiter->tooManyAttempts($key, $requestsPerTime)) {
                throw new ThrottleRequestsException($minutesToSeconds);
            }

            $response = $next($request);

            $this->rateLimiter->hit($key, $minutesToSeconds);
        } else {
            $response = $next($request);
        }

        return $response;
    }
}
