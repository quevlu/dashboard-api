<?php

namespace App\Http\Middlewares\Auth;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;
use Throwable;
use Tymon\JWTAuth\JWTAuth;

class AuthenticateMiddleware
{
    public function __construct(
        private JWTAuth $auth
    ) {
    }

    public function handle(Request $request, Closure $next)
    {
        try {
            if (
                ! $this->auth->parser()->setRequest($request)->hasToken() ||
                ! $this->auth->parseToken()->authenticate() ||
                ! $this->auth->user()->password()
            ) {
                throw new UnauthorizedException();
            }
        } catch (Throwable $th) {
            throw new UnauthorizedException();
        }

        return $next($request);
    }
}
