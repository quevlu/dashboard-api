<?php

namespace App\Http\Middlewares\Auth;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;

class NotAuthenticatedMiddleware
{
    private const AUTHORIZATION_HEADER = 'Authorization';

    public function handle(Request $request, Closure $next)
    {
        if ($request->header(self::AUTHORIZATION_HEADER)) {
            throw new UnauthorizedException();
        }

        return $next($request);
    }
}
