<?php

namespace App\Http\Middlewares\Auth;

use Closure;
use Illuminate\Http\Request;
use Infrastructure\Jwt\JwtEncryption;
use Throwable;

class DecryptJwtMiddleware
{
    protected const AUTHORIZATION_HEADER = 'Authorization';

    public function __construct(
        private JwtEncryption $encryptionService
    ) {
    }

    public function handle(Request $request, Closure $next)
    {
        $this->decryptToken($request);

        return $next($request);
    }

    protected function decryptToken(Request $request): void
    {
        $separator = ' ';
        $requestToArray = explode($separator, $request->header(self::AUTHORIZATION_HEADER));

        try {
            $requestToArray[1] = $this->encryptionService->decryptJwt($requestToArray[1]);
        } catch (Throwable $th) {
            $requestToArray[1] = '';
        }

        $requestToString = implode($separator, $requestToArray);

        $request->headers->set(self::AUTHORIZATION_HEADER, $requestToString);
    }
}
