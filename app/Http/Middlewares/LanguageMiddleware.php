<?php

namespace App\Http\Middlewares;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LanguageMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $language = $request->header('Accept-Language');
        $languageToSet = in_array($language, config('app.locales')) ? $language : config('app.fallback_locale');

        App::setLocale($languageToSet);

        return $next($request);
    }
}
