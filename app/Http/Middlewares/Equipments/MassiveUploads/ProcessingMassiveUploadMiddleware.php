<?php

namespace App\Http\Middlewares\Equipments\MassiveUploads;

use Binaccle\Repositories\MassiveUploads\MassiveUploadRepositoryInterface;
use Closure;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Http\Request;

class ProcessingMassiveUploadMiddleware
{
    public function __construct(
        private MassiveUploadRepositoryInterface $massiveUploadRepository
    ) {
    }

    public function handle(Request $request, Closure $next)
    {
        $companyId = auth()->user()->companyId();

        $isThereAnyPendingMassiveEquipmentUpload = $this->massiveUploadRepository->isThereAnyPendingMassiveEquipmentUpload($companyId);

        if ($isThereAnyPendingMassiveEquipmentUpload) {
            throw new ThrottleRequestsException('There\'s a pending massive upload. You have to wait to process it');
        }

        return $next($request);
    }
}
