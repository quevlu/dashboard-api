<?php

namespace App\Http\Middlewares\Users;

use Closure;
use Illuminate\Http\Request;
use Spatie\Permission\Exceptions\UnauthorizedException;

class PreventDestroyYourselfMiddleware
{
    private const USER_ID = 'userId';

    public function handle(Request $request, Closure $next)
    {
        $userId = $request->route(self::USER_ID);

        if (auth()->user()->id() == $userId) {
            throw new UnauthorizedException(1, 'You can\'t delete yourself');
        }

        return $next($request);
    }
}
