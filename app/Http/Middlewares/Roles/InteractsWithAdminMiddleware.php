<?php

namespace App\Http\Middlewares\Roles;

use Binaccle\Repositories\Users\UserRepositoryInterface;
use Closure;
use Illuminate\Http\Request;
use Spatie\Permission\Exceptions\UnauthorizedException;

class InteractsWithAdminMiddleware
{
    private const USER_ID = 'userId';

    public function __construct(
        private UserRepositoryInterface $userRepository
    ) {
    }

    public function handle(Request $request, Closure $next)
    {
        $userId = $request->route(self::USER_ID);
        $user = $this->userRepository->findOrFail($userId);

        if (auth()->user()->id() != $userId && $user->isAdmin()) {
            throw new UnauthorizedException(1, 'You are not allowed to view this user');
        }

        return $next($request);
    }
}
