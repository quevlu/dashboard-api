<?php

namespace App\Http\Middlewares\Roles;

use Binaccle\Enums\Permissions\PermissionEnum;
use Closure;
use Illuminate\Auth\Access\AuthorizationException;

class HasPermissionMiddleware
{
    public function handle($request, Closure $next, string $permission)
    {
        $user = auth()->user();
        $companyPlan = $user->companyRel()->planRel();

        if (! PermissionEnum::hasPermission($companyPlan->name(), $user->getRoleNames(), $permission)) {
            throw new AuthorizationException();
        }

        return $next($request);
    }
}
