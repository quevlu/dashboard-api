<?php

namespace App\Http\Middlewares\Roles;

use Binaccle\Enums\Plans\PlanEnum;
use Binaccle\Exceptions\Shared\PaymentRequiredException;
use Closure;
use Illuminate\Http\Request;

class HasAFreePlanMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $plan = auth()->user()->companyRel()->planRel();

        if ($plan->name() == PlanEnum::FREE) {
            throw new PaymentRequiredException();
        }

        return $next($request);
    }
}
