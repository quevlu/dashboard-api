<?php

namespace App\Http\Controllers\Roles;

use App\Http\Controllers\Controller;
use App\Http\Transformers\Roles\ListRoleTransformer;
use Binaccle\Services\Roles\ListRoleService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Infrastructure\Cache\Identities\IdentityService;

class ListRoleController extends Controller
{
    public function __invoke(ListRoleService $service, IdentityService $cacheService): JsonResponse
    {
        $roles = $cacheService(fn () => $service->list());

        return responder()
            ->success($roles, ListRoleTransformer::class)
            ->respond(Response::HTTP_OK);
    }
}
