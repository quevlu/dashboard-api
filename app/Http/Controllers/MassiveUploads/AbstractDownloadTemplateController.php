<?php

namespace App\Http\Controllers\MassiveUploads;

use App\Http\Controllers\Controller;
use Binaccle\Services\MassiveUploads\DownloadCsvTemplateService;

abstract class AbstractDownloadTemplateController extends Controller
{
    public function __invoke(DownloadCsvTemplateService $service)
    {
        return $service->download(static::TYPE);
    }
}
