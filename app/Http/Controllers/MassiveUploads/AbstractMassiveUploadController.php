<?php

namespace App\Http\Controllers\MassiveUploads;

use App\Http\Controllers\Controller;
use Binaccle\Services\MassiveUploads\MassiveUploadService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

abstract class AbstractMassiveUploadController extends Controller
{
    public function __invoke(MassiveUploadService $service): JsonResponse
    {
        $request = app(static::REQUEST);

        $service->upload($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_CREATED);
    }
}
