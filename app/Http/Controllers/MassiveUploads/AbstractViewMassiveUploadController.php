<?php

namespace App\Http\Controllers\MassiveUploads;

use App\Http\Controllers\Controller;
use App\Http\Transformers\MassiveUploads\ViewMassiveUploadTransformer;
use Binaccle\Services\MassiveUploads\ViewMassiveUploadService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

abstract class AbstractViewMassiveUploadController extends Controller
{
    public function __invoke(ViewMassiveUploadService $service, string $massiveUploadId): JsonResponse
    {
        $massiveUpload = $service->view($massiveUploadId, static::TYPE);

        return responder()
            ->success($massiveUpload, ViewMassiveUploadTransformer::class)
            ->respond(Response::HTTP_OK);
    }
}
