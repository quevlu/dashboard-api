<?php

namespace App\Http\Controllers\MassiveUploads;

use App\Http\Controllers\Controller;
use App\Http\Exportables\MassiveUploads\ListMassiveUploadExportable;
use App\Http\Transformers\MassiveUploads\ListMassiveUploadTransformer;
use Binaccle\Services\MassiveUploads\ListMassiveUploadService;
use Illuminate\Http\Response;
use Infrastructure\Cache\Identities\IdentityService;
use Infrastructure\Exports\ExportCsv;

abstract class AbstractListMassiveUploadController extends Controller
{
    public function __invoke(
        ListMassiveUploadService $service,
        ExportCsv $exportCsv,
        ListMassiveUploadExportable $exportable,
        IdentityService $cacheService
    ) {
        $criteria = app(static::CRITERIA);

        $criteria->validate();

        $massiveUploads = $cacheService(fn () => $service->list($criteria));

        if ($criteria->export()) {
            return $exportCsv($massiveUploads, $exportable);
        }

        return responder()
            ->success($massiveUploads, ListMassiveUploadTransformer::class)
            ->respond(Response::HTTP_OK);
    }
}
