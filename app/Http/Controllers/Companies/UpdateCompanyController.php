<?php

namespace App\Http\Controllers\Companies;

use App\Http\Controllers\Controller;
use App\Http\Requests\Companies\UpdateCompanyRequest;
use Binaccle\Services\Companies\UpdateCompanyService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class UpdateCompanyController extends Controller
{
    public function __invoke(UpdateCompanyRequest $request, UpdateCompanyService $service): JsonResponse
    {
        $service->update($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
