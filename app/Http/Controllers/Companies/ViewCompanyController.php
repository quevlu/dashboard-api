<?php

namespace App\Http\Controllers\Companies;

use App\Http\Controllers\Controller;
use App\Http\Transformers\Companies\ViewCompanyTransformer;
use Binaccle\Services\Companies\ViewCompanyService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ViewCompanyController extends Controller
{
    public function __invoke(ViewCompanyService $service): JsonResponse
    {
        $company = $service->view();

        return responder()
            ->success($company, ViewCompanyTransformer::class)
            ->respond(Response::HTTP_OK);
    }
}
