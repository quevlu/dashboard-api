<?php

namespace App\Http\Controllers\Companies;

use App\Http\Controllers\Controller;
use App\Http\Requests\Companies\DeleteCompanyRequest;
use Binaccle\Services\Companies\DeleteCompanyService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class DeleteCompanyController extends Controller
{
    public function __invoke(DeleteCompanyRequest $request, DeleteCompanyService $service): JsonResponse
    {
        $service->delete($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
