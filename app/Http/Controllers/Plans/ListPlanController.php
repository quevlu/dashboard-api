<?php

namespace App\Http\Controllers\Plans;

use App\Http\Controllers\Controller;
use App\Http\Transformers\Plans\ListPlanTransformer;
use Binaccle\Enums\Plans\PlanEnum;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ListPlanController extends Controller
{
    public function __invoke(): JsonResponse
    {
        $plans = PlanEnum::plansWithoutFree();

        return responder()
            ->success($plans, ListPlanTransformer::class)
            ->respond(Response::HTTP_OK);
    }
}
