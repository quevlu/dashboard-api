<?php

namespace App\Http\Controllers\Auth\ResetPassword;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ResetPassword\SendResetPasswordRequest;
use Binaccle\Services\Auth\ResetPassword\SendResetPasswordService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class SendResetPasswordController extends Controller
{
    public function __invoke(SendResetPasswordRequest $request, SendResetPasswordService $service): JsonResponse
    {
        $service->send($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
