<?php

namespace App\Http\Controllers\Auth\ResetPassword;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ResetPassword\ResetPasswordRequest;
use Binaccle\Services\Auth\ResetPassword\ResetPasswordService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ResetPasswordController extends Controller
{
    public function __invoke(ResetPasswordRequest $request, ResetPasswordService $service): JsonResponse
    {
        $service->reset($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
