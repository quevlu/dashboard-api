<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Binaccle\Services\Auth\LogoutService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class LogoutController extends Controller
{
    public function __invoke(LogoutService $service): JsonResponse
    {
        $service->logout();

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
