<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Transformers\Auth\UserLoggedTransformer;
use Binaccle\Services\Auth\UserLoggedService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class UserLoggedController extends Controller
{
    public function __invoke(UserLoggedService $userLoggedService): JsonResponse
    {
        $userLogged = $userLoggedService->logged(auth()->user());

        return responder()
            ->success($userLogged, UserLoggedTransformer::class)
            ->respond(Response::HTTP_OK);
    }
}
