<?php

namespace App\Http\Controllers\Auth\Verifications;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\Verifications\SendVerificationRequest;
use Binaccle\Services\Auth\Verifications\SendVerificationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class SendVerificationController extends Controller
{
    public function __invoke(SendVerificationRequest $request, SendVerificationService $service): JsonResponse
    {
        $service->send($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
