<?php

namespace App\Http\Controllers\Auth\Verifications;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\Verifications\VerifyTokenRequest;
use Binaccle\Services\Auth\Verifications\VerifyTokenService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class VerifyTokenController extends Controller
{
    public function __invoke(VerifyTokenRequest $request, VerifyTokenService $service): JsonResponse
    {
        $service->verify($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
