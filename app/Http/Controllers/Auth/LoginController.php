<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Transformers\Auth\LoginTransformer;
use Binaccle\Dtos\Auth\LoginDto;
use Binaccle\Services\Auth\LoginService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class LoginController extends Controller
{
    public function __invoke(LoginRequest $request, LoginService $service): JsonResponse
    {
        $token = $service->login($request);

        return responder()
            ->success(new LoginDto($token), LoginTransformer::class)
            ->respond(Response::HTTP_OK);
    }
}
