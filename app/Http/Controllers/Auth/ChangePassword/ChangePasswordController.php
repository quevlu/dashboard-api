<?php

namespace App\Http\Controllers\Auth\ChangePassword;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ChangePassword\ChangePasswordRequest;
use Binaccle\Services\Auth\ChangePassword\ChangePasswordService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ChangePasswordController extends Controller
{
    public function __invoke(ChangePasswordRequest $request, ChangePasswordService $service): JsonResponse
    {
        $service->change($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
