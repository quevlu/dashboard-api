<?php

namespace App\Http\Controllers\Auth\Register;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\Register\RegisterUserRequest;
use Binaccle\Services\Auth\Register\RegisterService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class RegisterController extends Controller
{
    public function __invoke(RegisterUserRequest $request, RegisterService $service): JsonResponse
    {
        $service->create($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_CREATED);
    }
}
