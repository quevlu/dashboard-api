<?php

namespace App\Http\Controllers\Auth\Invitations;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\Invitations\ProcessInvitationRequest;
use Binaccle\Services\Auth\Invitations\ProcessInvitationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ProcessInvitationController extends Controller
{
    public function __invoke(ProcessInvitationRequest $request, ProcessInvitationService $service): JsonResponse
    {
        $service->process($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
