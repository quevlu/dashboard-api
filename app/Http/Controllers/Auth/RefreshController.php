<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Transformers\Auth\LoginTransformer;
use Binaccle\Dtos\Auth\LoginDto;
use Binaccle\Services\Auth\RefreshService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class RefreshController extends Controller
{
    public function __invoke(RefreshService $service): JsonResponse
    {
        $token = $service->refresh();

        return responder()
            ->success(new LoginDto($token), LoginTransformer::class)
            ->respond(Response::HTTP_CREATED);
    }
}
