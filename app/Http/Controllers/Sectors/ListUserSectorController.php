<?php

namespace App\Http\Controllers\Sectors;

use App\Http\Controllers\Controller;
use App\Http\Exportables\Sectors\ListUserSectorExportable;
use App\Http\Transformers\Users\ListUserSectorTransformer;
use Binaccle\Services\Sectors\ListUserSectorService;
use Illuminate\Http\Response;
use Infrastructure\Cache\Identities\IdentityService;
use Infrastructure\Exports\ExportCsv;
use Infrastructure\Repositories\Criterias\Sectors\ListUserSectorCriteria;

class ListUserSectorController extends Controller
{
    public function __invoke(
        ListUserSectorService $service,
        ListUserSectorCriteria $criteria,
        ExportCsv $exportCsv,
        ListUserSectorExportable $exportable,
        IdentityService $cacheService
    ) {
        $criteria->validate();

        $users = $cacheService(fn () => $service->list($criteria));

        if ($criteria->export()) {
            return $exportCsv($users, $exportable);
        }

        return responder()
            ->success($users, ListUserSectorTransformer::class)
            ->respond(Response::HTTP_OK);
    }
}
