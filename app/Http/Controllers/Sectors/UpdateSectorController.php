<?php

namespace App\Http\Controllers\Sectors;

use App\Http\Controllers\Controller;
use App\Http\Requests\Sectors\UpdateSectorRequest;
use Binaccle\Services\Sectors\UpdateSectorService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class UpdateSectorController extends Controller
{
    public function __invoke(UpdateSectorRequest $request, UpdateSectorService $service): JsonResponse
    {
        $service->update($request->sector(), $request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
