<?php

namespace App\Http\Controllers\Sectors;

use App\Http\Controllers\Controller;
use App\Http\Requests\Sectors\CreateSectorRequest;
use Binaccle\Services\Sectors\CreateSectorService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class CreateSectorController extends Controller
{
    public function __invoke(CreateSectorRequest $request, CreateSectorService $service): JsonResponse
    {
        $service->create($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_CREATED);
    }
}
