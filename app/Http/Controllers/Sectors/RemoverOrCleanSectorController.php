<?php

namespace App\Http\Controllers\Sectors;

use App\Http\Controllers\Controller;
use App\Http\Requests\Sectors\RemoveOrCleanSectorRequest;
use Binaccle\Services\Sectors\RemoveOrCleanSectorService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class RemoverOrCleanSectorController extends Controller
{
    public function __invoke(RemoveOrCleanSectorRequest $request, RemoveOrCleanSectorService $service): JsonResponse
    {
        $service->cleanOrRemove($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
