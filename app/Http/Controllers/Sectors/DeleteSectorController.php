<?php

namespace App\Http\Controllers\Sectors;

use App\Http\Controllers\Controller;
use App\Http\Requests\Sectors\DeleteSectorRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class DeleteSectorController extends Controller
{
    public function __invoke(DeleteSectorRequest $request): JsonResponse
    {
        $request->sector()->delete();

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
