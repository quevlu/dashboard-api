<?php

namespace App\Http\Controllers\Sectors;

use App\Http\Controllers\Controller;
use App\Http\Exportables\Sectors\ListSectorExportable;
use App\Http\Transformers\Sectors\ListSectorTransformer;
use Binaccle\Services\Sectors\ListSectorService;
use Illuminate\Http\Response;
use Infrastructure\Cache\Identities\IdentityService;
use Infrastructure\Exports\ExportCsv;
use Infrastructure\Repositories\Criterias\Sectors\ListSectorCriteria;

class ListSectorController extends Controller
{
    public function __invoke(
        ListSectorService $service,
        ListSectorCriteria $criteria,
        ExportCsv $exportCsv,
        ListSectorExportable $exportable,
        IdentityService $cacheService
    ) {
        $criteria->validate();

        $sectors = $cacheService(fn () => $service->list($criteria));

        if ($criteria->export()) {
            return $exportCsv($sectors, $exportable);
        }

        return responder()
            ->success($sectors, ListSectorTransformer::class)
            ->respond(Response::HTTP_OK);
    }
}
