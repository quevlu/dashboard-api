<?php

namespace App\Http\Controllers\Sectors;

use App\Http\Controllers\Controller;
use App\Http\Transformers\Sectors\ViewSectorTransformer;
use Binaccle\Services\Sectors\ViewSectorService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ViewSectorController extends Controller
{
    public function __invoke(ViewSectorService $service, string $sectorId): JsonResponse
    {
        $sector = $service->view($sectorId);

        return responder()
            ->success($sector, ViewSectorTransformer::class)
            ->respond(Response::HTTP_OK);
    }
}
