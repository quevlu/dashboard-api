<?php

namespace App\Http\Controllers\Equipments;

use App\Http\Controllers\Controller;
use App\Http\Requests\Equipments\CreateEquipmentRequest;
use Binaccle\Services\Equipments\CreateEquipmentService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class CreateEquipmentController extends Controller
{
    public function __invoke(CreateEquipmentRequest $request, CreateEquipmentService $service): JsonResponse
    {
        $request->validate();

        $service->create($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_CREATED);
    }
}
