<?php

namespace App\Http\Controllers\Equipments\Types;

use App\Http\Controllers\Controller;
use App\Http\Transformers\Equipments\Types\ViewTypeTransformer;
use Binaccle\Services\Equipments\Types\ViewTypeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ViewTypeController extends Controller
{
    public function __invoke(ViewTypeService $service, string $equipmentTypeId): JsonResponse
    {
        $equipmentType = $service->view($equipmentTypeId);

        return responder()
            ->success($equipmentType, ViewTypeTransformer::class)
            ->respond(Response::HTTP_OK);
    }
}
