<?php

namespace App\Http\Controllers\Equipments\Types\MassiveUploads;

use App\Http\Controllers\MassiveUploads\AbstractDownloadTemplateController;
use Binaccle\Enums\MassiveUploads\MassiveUploadTypeEnum;

class DownloadCsvTemplateController extends AbstractDownloadTemplateController
{
    protected const TYPE = MassiveUploadTypeEnum::EQUIPMENT_TYPES;
}
