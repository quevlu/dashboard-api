<?php

namespace App\Http\Controllers\Equipments\Types\MassiveUploads;

use App\Http\Controllers\MassiveUploads\AbstractMassiveUploadController;
use App\Http\Requests\Equipments\Types\MassiveUploads\MassiveUploadRequest;

class MassiveUploadController extends AbstractMassiveUploadController
{
    protected const REQUEST = MassiveUploadRequest::class;
}
