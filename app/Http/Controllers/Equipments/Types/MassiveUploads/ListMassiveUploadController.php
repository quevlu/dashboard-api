<?php

namespace App\Http\Controllers\Equipments\Types\MassiveUploads;

use App\Http\Controllers\MassiveUploads\AbstractListMassiveUploadController;
use Infrastructure\Repositories\Criterias\Equipments\Types\ListMassiveUploadCriteria;

class ListMassiveUploadController extends AbstractListMassiveUploadController
{
    protected const CRITERIA = ListMassiveUploadCriteria::class;
}
