<?php

namespace App\Http\Controllers\Equipments\Types;

use App\Http\Controllers\Controller;
use App\Http\Requests\Equipments\Types\CreateTypeRequest;
use Binaccle\Services\Equipments\Types\CreateTypeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class CreateTypeController extends Controller
{
    public function __invoke(CreateTypeRequest $request, CreateTypeService $service): JsonResponse
    {
        $request->validate();

        $service->create($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_CREATED);
    }
}
