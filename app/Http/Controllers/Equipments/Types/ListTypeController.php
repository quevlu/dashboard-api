<?php

namespace App\Http\Controllers\Equipments\Types;

use App\Http\Controllers\Controller;
use App\Http\Exportables\Equipments\Types\ListTypeExportable;
use App\Http\Transformers\Equipments\Types\ListTypeTransformer;
use Binaccle\Services\Equipments\Types\ListTypeService;
use Illuminate\Http\Response;
use Infrastructure\Cache\Identities\IdentityService;
use Infrastructure\Exports\ExportCsv;
use Infrastructure\Repositories\Criterias\Equipments\Types\ListTypeCriteria;

class ListTypeController extends Controller
{
    public function __invoke(
        ListTypeService $service,
        ListTypeCriteria $criteria,
        ExportCsv $exportCsv,
        ListTypeExportable $exportable,
        IdentityService $cacheService
    ) {
        $criteria->validate();

        $types = $cacheService(fn () => $service->list($criteria));

        if ($criteria->export()) {
            return $exportCsv($types, $exportable);
        }

        return responder()
            ->success($types, ListTypeTransformer::class)
            ->respond(Response::HTTP_OK);
    }
}
