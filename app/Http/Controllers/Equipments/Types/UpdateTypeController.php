<?php

namespace App\Http\Controllers\Equipments\Types;

use App\Http\Controllers\Controller;
use App\Http\Requests\Equipments\Types\UpdateTypeRequest;
use Binaccle\Services\Equipments\Types\UpdateTypeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class UpdateTypeController extends Controller
{
    public function __invoke(UpdateTypeRequest $request, UpdateTypeService $service): JsonResponse
    {
        $request->validate();

        $service->update($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
