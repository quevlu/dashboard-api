<?php

namespace App\Http\Controllers\Equipments\Types;

use App\Http\Controllers\Controller;
use App\Http\Requests\Equipments\Types\DeleteTypeRequest;
use Binaccle\Services\Equipments\Types\DeleteTypeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class DeleteTypeController extends Controller
{
    public function __invoke(DeleteTypeRequest $request, DeleteTypeService $service): JsonResponse
    {
        $service->delete($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
