<?php

namespace App\Http\Controllers\Equipments;

use App\Http\Controllers\Controller;
use App\Http\Requests\Equipments\PrintEquipmentRequest;
use Binaccle\Services\Equipments\PrintEquipmentService;

class PrintEquipmentController extends Controller
{
    public function __invoke(PrintEquipmentRequest $request, PrintEquipmentService $service)
    {
        return response($service->print($request))->header('Content-Type', 'image/png');
    }
}
