<?php

namespace App\Http\Controllers\Equipments;

use App\Http\Controllers\Controller;
use App\Http\Requests\Equipments\UpdateEquipmentRequest;
use Binaccle\Services\Equipments\UpdateEquipmentService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class UpdateEquipmentController extends Controller
{
    public function __invoke(UpdateEquipmentRequest $request, UpdateEquipmentService $service): JsonResponse
    {
        $request->validate();

        $service->update($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
