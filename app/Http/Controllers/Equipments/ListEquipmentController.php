<?php

namespace App\Http\Controllers\Equipments;

use App\Http\Controllers\Controller;
use App\Http\Exportables\Equipments\ListEquipmentExportable;
use App\Http\Transformers\Equipments\ListEquipmentTransformer;
use Binaccle\Services\Equipments\ListEquipmentService;
use Illuminate\Http\Response;
use Infrastructure\Cache\Identities\IdentityService;
use Infrastructure\Exports\ExportCsv;
use Infrastructure\Repositories\Criterias\Equipments\ListEquipmentCriteria;

class ListEquipmentController extends Controller
{
    public function __invoke(
        ListEquipmentService $service,
        ListEquipmentCriteria $criteria,
        ExportCsv $exportCsv,
        ListEquipmentExportable $exportable,
        IdentityService $cacheService
    ) {
        $criteria->validate();

        $equipments = $cacheService(fn () => $service->list($criteria));

        if ($criteria->export()) {
            return $exportCsv($equipments, $exportable);
        }

        return responder()
            ->success($equipments, ListEquipmentTransformer::class)
            ->respond(Response::HTTP_OK);
    }
}
