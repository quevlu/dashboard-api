<?php

namespace App\Http\Controllers\Equipments;

use App\Http\Controllers\Controller;
use App\Http\Requests\Equipments\MassivePrintEquipmentRequest;
use Binaccle\Services\Equipments\MassivePrintEquipmentService;

class MassivePrintEquipmentController extends Controller
{
    public function __invoke(MassivePrintEquipmentRequest $request, MassivePrintEquipmentService $service)
    {
        return response()->download($service->print($request));
    }
}
