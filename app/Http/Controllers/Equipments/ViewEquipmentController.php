<?php

namespace App\Http\Controllers\Equipments;

use App\Http\Controllers\Controller;
use App\Http\Transformers\Equipments\ViewEquipmentTransformer;
use Binaccle\Services\Equipments\ViewEquipmentService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ViewEquipmentController extends Controller
{
    public function __invoke(ViewEquipmentService $service, string $equipmentId): JsonResponse
    {
        $equipment = $service->view($equipmentId);

        return responder()
            ->success($equipment, ViewEquipmentTransformer::class)
            ->respond(Response::HTTP_OK);
    }
}
