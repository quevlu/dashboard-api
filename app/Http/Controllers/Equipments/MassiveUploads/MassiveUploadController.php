<?php

namespace App\Http\Controllers\Equipments\MassiveUploads;

use App\Http\Controllers\MassiveUploads\AbstractMassiveUploadController;
use App\Http\Requests\Equipments\MassiveUploads\MassiveUploadRequest;

class MassiveUploadController extends AbstractMassiveUploadController
{
    protected const REQUEST = MassiveUploadRequest::class;
}
