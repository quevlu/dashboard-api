<?php

namespace App\Http\Controllers\Equipments\MassiveUploads;

use App\Http\Controllers\MassiveUploads\AbstractListMassiveUploadController;
use Infrastructure\Repositories\Criterias\Equipments\ListMassiveUploadCriteria;

class ListMassiveUploadController extends AbstractListMassiveUploadController
{
    protected const CRITERIA = ListMassiveUploadCriteria::class;
}
