<?php

namespace App\Http\Controllers\Equipments\MassiveUploads;

use App\Http\Controllers\MassiveUploads\AbstractDownloadTemplateController;
use Binaccle\Enums\MassiveUploads\MassiveUploadTypeEnum;

class DownloadUploadCsvTemplateController extends AbstractDownloadTemplateController
{
    protected const TYPE = MassiveUploadTypeEnum::EQUIPMENTS;
}
