<?php

namespace App\Http\Controllers\Equipments;

use App\Http\Controllers\Controller;
use App\Http\Requests\Equipments\DeleteEquipmentRequest;
use Binaccle\Services\Equipments\DeleteEquipmentService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class DeleteEquipmentController extends Controller
{
    public function __invoke(DeleteEquipmentRequest $request, DeleteEquipmentService $service): JsonResponse
    {
        $service->delete($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
