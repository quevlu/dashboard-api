<?php

namespace App\Http\Controllers\Equipments\Audits;

use App\Http\Controllers\Controller;
use App\Http\Requests\Equipments\Audits\FinishAuditRequest;
use Binaccle\Services\Equipments\Audits\FinishAuditService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class FinishAuditController extends Controller
{
    public function __invoke(FinishAuditRequest $request, FinishAuditService $service): JsonResponse
    {
        $service->finish($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
