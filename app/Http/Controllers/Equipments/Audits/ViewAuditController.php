<?php

namespace App\Http\Controllers\Equipments\Audits;

use App\Http\Controllers\Controller;
use App\Http\Transformers\Equipments\Audits\ViewAuditTransformer;
use Binaccle\Services\Equipments\Audits\ViewAuditService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ViewAuditController extends Controller
{
    public function __invoke(ViewAuditService $service, string $equipmentAuditId): JsonResponse
    {
        $equipmentAudit = $service->view($equipmentAuditId);

        return responder()
            ->success($equipmentAudit, ViewAuditTransformer::class)
            ->respond(Response::HTTP_OK);
    }
}
