<?php

namespace App\Http\Controllers\Equipments\Audits;

use App\Http\Controllers\Controller;
use App\Http\Exportables\Equipments\Audits\ListAuditExportable;
use App\Http\Transformers\Equipments\Audits\ListAuditTransformer;
use Binaccle\Services\Equipments\Audits\ListAuditService;
use Illuminate\Http\Response;
use Infrastructure\Cache\Identities\IdentityService;
use Infrastructure\Exports\ExportCsv;
use Infrastructure\Repositories\Criterias\Equipments\Audits\ListAuditCriteria;

class ListAuditController extends Controller
{
    public function __invoke(
        ListAuditService $service,
        ListAuditCriteria $criteria,
        ExportCsv $exportCsv,
        ListAuditExportable $exportable,
        IdentityService $cacheService
    ) {
        $criteria->validate();

        $audits = $cacheService(fn () => $service->list($criteria));

        if ($criteria->export()) {
            return $exportCsv($audits, $exportable);
        }

        return responder()
            ->success($audits, ListAuditTransformer::class)
            ->respond(Response::HTTP_OK);
    }
}
