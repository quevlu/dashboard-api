<?php

namespace App\Http\Controllers\Equipments\Audits\Audited;

use App\Http\Controllers\Controller;
use App\Http\Exportables\Equipments\Audits\Audited\ListAuditedExportable;
use App\Http\Transformers\Equipments\Audits\ListAuditedTransformer;
use Binaccle\Services\Equipments\Audits\Audited\ListAuditedService;
use Illuminate\Http\Response;
use Infrastructure\Cache\Identities\IdentityService;
use Infrastructure\Exports\ExportCsv;
use Infrastructure\Repositories\Criterias\Equipments\Audits\Audited\ListAuditedCriteria;

class ListAuditedController extends Controller
{
    public function __invoke(
        ListAuditedService $service,
        ListAuditedCriteria $criteria,
        ExportCsv $exportCsv,
        ListAuditedExportable $exportable,
        IdentityService $cacheService
    ) {
        $criteria->validate();

        $audited = $cacheService(fn () => $service->list($criteria));

        if ($criteria->export()) {
            return $exportCsv($audited, $exportable);
        }

        return responder()
            ->success($audited, ListAuditedTransformer::class)
            ->respond(Response::HTTP_OK);
    }
}
