<?php

namespace App\Http\Controllers\Equipments\Audits\Audited;

use App\Http\Controllers\Controller;
use App\Http\Requests\Equipments\Audits\Audited\UpleteAuditedRequest;
use Binaccle\Services\Equipments\Audits\Audited\UpdateAuditedService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class UpdateAuditedController extends Controller
{
    public function __invoke(UpleteAuditedRequest $request, UpdateAuditedService $service): JsonResponse
    {
        $service->update($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
