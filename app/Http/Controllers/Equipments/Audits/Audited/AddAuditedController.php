<?php

namespace App\Http\Controllers\Equipments\Audits\Audited;

use App\Http\Controllers\Controller;
use App\Http\Requests\Equipments\Audits\Audited\AddAuditedRequest;
use Binaccle\Services\Equipments\Audits\Audited\MassiveAuditedService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class AddAuditedController extends Controller
{
    public function __invoke(AddAuditedRequest $request, MassiveAuditedService $service): JsonResponse
    {
        $service->add($request->equipmentAudit(), $request->equipments(), true, true);

        return responder()
            ->success()
            ->respond(Response::HTTP_CREATED);
    }
}
