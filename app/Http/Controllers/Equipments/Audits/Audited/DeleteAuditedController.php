<?php

namespace App\Http\Controllers\Equipments\Audits\Audited;

use App\Http\Controllers\Controller;
use App\Http\Requests\Equipments\Audits\Audited\UpleteAuditedRequest;
use Binaccle\Services\Equipments\Audits\Audited\DeleteAuditedService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class DeleteAuditedController extends Controller
{
    public function __invoke(UpleteAuditedRequest $request, DeleteAuditedService $service): JsonResponse
    {
        $service->delete($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
