<?php

namespace App\Http\Controllers\Equipments\Audits;

use App\Http\Controllers\Controller;
use App\Http\Requests\Equipments\Audits\DeleteAuditRequest;
use Binaccle\Services\Equipments\Audits\DeleteAuditService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class DeleteAuditController extends Controller
{
    public function __invoke(DeleteAuditRequest $request, DeleteAuditService $service): JsonResponse
    {
        $service->delete($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
