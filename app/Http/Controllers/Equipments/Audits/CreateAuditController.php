<?php

namespace App\Http\Controllers\Equipments\Audits;

use App\Http\Controllers\Controller;
use App\Http\Requests\Equipments\Audits\CreateAuditRequest;
use Binaccle\Services\Equipments\Audits\CreateAuditService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class CreateAuditController extends Controller
{
    public function __invoke(CreateAuditRequest $request, CreateAuditService $service): JsonResponse
    {
        $service->create($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_CREATED);
    }
}
