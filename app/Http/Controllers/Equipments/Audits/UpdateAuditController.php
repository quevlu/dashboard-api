<?php

namespace App\Http\Controllers\Equipments\Audits;

use App\Http\Controllers\Controller;
use App\Http\Requests\Equipments\Audits\UpdateAuditRequest;
use Binaccle\Services\Equipments\Audits\UpdateAuditService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class UpdateAuditController extends Controller
{
    public function __invoke(UpdateAuditRequest $request, UpdateAuditService $service): JsonResponse
    {
        $service->update($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
