<?php

namespace App\Http\Controllers\Equipments\Assignments;

use App\Http\Controllers\Controller;
use App\Http\Exportables\Equipments\Assignments\HistoryAssignmentExportable;
use App\Http\Transformers\Equipments\Assignments\HistoryAssignmentTransformer;
use Binaccle\Services\Equipments\Assignments\HistoryAssignmentService;
use Illuminate\Http\Response;
use Infrastructure\Cache\Identities\IdentityService;
use Infrastructure\Exports\ExportCsv;
use Infrastructure\Repositories\Criterias\Equipments\Assignments\HistoryAssignmentCriteria;

class HistoryAssignmentController extends Controller
{
    public function __invoke(
        HistoryAssignmentService $service,
        HistoryAssignmentCriteria $criteria,
        ExportCsv $exportCsv,
        HistoryAssignmentExportable $exportable,
        IdentityService $cacheService
    ) {
        $criteria->validate();

        $assignmentsHistory = $cacheService(fn () => $service->history($criteria));

        if ($criteria->export()) {
            return $exportCsv($assignmentsHistory, $exportable);
        }

        return responder()
            ->success($assignmentsHistory, HistoryAssignmentTransformer::class)
            ->respond(Response::HTTP_OK);
    }
}
