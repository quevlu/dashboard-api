<?php

namespace App\Http\Controllers\Equipments\Assignments;

use App\Http\Controllers\Controller;
use App\Http\Exportables\Equipments\Assignments\ListAssignmentExportable;
use App\Http\Transformers\Equipments\Assignments\ListAssignmentTransformer;
use Binaccle\Services\Equipments\Assignments\ListAssignmentService;
use Illuminate\Http\Response;
use Infrastructure\Cache\Identities\IdentityService;
use Infrastructure\Exports\ExportCsv;
use Infrastructure\Repositories\Criterias\Equipments\Assignments\ListAssignmentCriteria;

class ListAssignmentController extends Controller
{
    public function __invoke(
        ListAssignmentService $service,
        ListAssignmentCriteria $criteria,
        ExportCsv $exportCsv,
        ListAssignmentExportable $exportable,
        IdentityService $cacheService
    ) {
        $criteria->validate();

        $assignments = $cacheService(fn () => $service->list($criteria));

        if ($criteria->export()) {
            return $exportCsv($assignments, $exportable);
        }

        return responder()
            ->success($assignments, ListAssignmentTransformer::class)
            ->respond(Response::HTTP_OK);
    }
}
