<?php

namespace App\Http\Controllers\Equipments\Assignments;

use App\Http\Controllers\Controller;
use App\Http\Transformers\Equipments\Assignments\ViewAssignmentTransformer;
use Binaccle\Services\Equipments\Assignments\ViewAssignmentService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ViewAssignmentController extends Controller
{
    public function __invoke(ViewAssignmentService $service, string $equipmentId): JsonResponse
    {
        $equipmentTypes = $service->view($equipmentId);

        return responder()
            ->success($equipmentTypes, ViewAssignmentTransformer::class)
            ->respond(Response::HTTP_OK);
    }
}
