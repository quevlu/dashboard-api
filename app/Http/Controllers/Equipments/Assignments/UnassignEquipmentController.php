<?php

namespace App\Http\Controllers\Equipments\Assignments;

use App\Http\Controllers\Controller;
use App\Http\Requests\Equipments\Assignments\UnassignEquipmentRequest;
use Binaccle\Services\Equipments\Assignments\UnassignEquipmentService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class UnassignEquipmentController extends Controller
{
    public function __invoke(UnassignEquipmentRequest $request, UnassignEquipmentService $service): JsonResponse
    {
        $service->unassign($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
