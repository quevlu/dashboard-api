<?php

namespace App\Http\Controllers\Equipments\Assignments;

use App\Http\Controllers\Controller;
use App\Http\Requests\Equipments\Assignments\AssignEquipmentRequest;
use Binaccle\Services\Equipments\Assignments\AssignEquipmentService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class AssignEquipmentController extends Controller
{
    public function __invoke(AssignEquipmentRequest $request, AssignEquipmentService $service): JsonResponse
    {
        $service->assign($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_CREATED);
    }
}
