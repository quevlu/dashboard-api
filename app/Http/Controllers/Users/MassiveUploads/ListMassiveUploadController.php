<?php

namespace App\Http\Controllers\Users\MassiveUploads;

use App\Http\Controllers\MassiveUploads\AbstractListMassiveUploadController;
use Infrastructure\Repositories\Criterias\Users\ListMassiveUploadCriteria;

class ListMassiveUploadController extends AbstractListMassiveUploadController
{
    protected const CRITERIA = ListMassiveUploadCriteria::class;
}
