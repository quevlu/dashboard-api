<?php

namespace App\Http\Controllers\Users\MassiveUploads;

use App\Http\Controllers\MassiveUploads\AbstractViewMassiveUploadController;
use Binaccle\Enums\MassiveUploads\MassiveUploadTypeEnum;

class ViewMassiveUploadController extends AbstractViewMassiveUploadController
{
    protected const TYPE = MassiveUploadTypeEnum::USERS;
}
