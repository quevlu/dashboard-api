<?php

namespace App\Http\Controllers\Users\MassiveUploads;

use App\Http\Controllers\MassiveUploads\AbstractDownloadTemplateController;
use Binaccle\Enums\MassiveUploads\MassiveUploadTypeEnum;

class DownloadCsvTemplateController extends AbstractDownloadTemplateController
{
    protected const TYPE = MassiveUploadTypeEnum::USERS;
}
