<?php

namespace App\Http\Controllers\Users\MassiveUploads;

use App\Http\Controllers\MassiveUploads\AbstractMassiveUploadController;
use App\Http\Requests\Users\MassiveUploads\MassiveUploadRequest;

class MassiveUploadController extends AbstractMassiveUploadController
{
    protected const REQUEST = MassiveUploadRequest::class;
}
