<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Transformers\Users\ViewUserTransformer;
use Binaccle\Services\Users\ViewUserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ViewUserController extends Controller
{
    public function __invoke(ViewUserService $service, string $userId): JsonResponse
    {
        $sector = $service->view($userId);

        return responder()
            ->success($sector, ViewUserTransformer::class)
            ->respond(Response::HTTP_OK);
    }
}
