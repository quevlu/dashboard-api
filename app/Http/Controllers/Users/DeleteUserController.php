<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\DeleteUserRequest;
use Binaccle\Services\Users\DeleteUserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class DeleteUserController extends Controller
{
    public function __invoke(DeleteUserRequest $request, DeleteUserService $service): JsonResponse
    {
        $service->delete($request->userTo());

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
