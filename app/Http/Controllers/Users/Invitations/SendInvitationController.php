<?php

namespace App\Http\Controllers\Users\Invitations;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\Invitations\SendInvitationRequest;
use Binaccle\Services\Users\Invitations\SendInvitationUserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class SendInvitationController extends Controller
{
    public function __invoke(SendInvitationRequest $request, SendInvitationUserService $service): JsonResponse
    {
        $service->send($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
