<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\CreateUserRequest;
use Binaccle\Services\Users\CreateUserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class CreateUserController extends Controller
{
    public function __invoke(CreateUserRequest $request, CreateUserService $service): JsonResponse
    {
        $request->validate();

        $service->create($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_CREATED);
    }
}
