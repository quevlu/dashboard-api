<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Exportables\Users\ListUserExportable;
use App\Http\Transformers\Users\ListUserTransformer;
use Binaccle\Services\Users\ListUserService;
use Illuminate\Http\Response;
use Infrastructure\Cache\Identities\IdentityService;
use Infrastructure\Exports\ExportCsv;
use Infrastructure\Repositories\Criterias\Users\ListUserCriteria;

class ListUserController extends Controller
{
    public function __invoke(
        ListUserService $service,
        ListUserCriteria $criteria,
        ExportCsv $exportCsv,
        ListUserExportable $exportable,
        IdentityService $cacheService
    ) {
        $criteria->validate();

        $users = $cacheService(fn () => $service->list($criteria));

        if ($criteria->export()) {
            return $exportCsv($users, $exportable);
        }

        return responder()
                ->success($users, ListUserTransformer::class)
                ->respond(Response::HTTP_OK);
    }
}
