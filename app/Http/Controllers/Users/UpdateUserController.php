<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\UpdateUserRequest;
use Binaccle\Services\Users\UpdateUserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class UpdateUserController extends Controller
{
    public function __invoke(UpdateUserRequest $request, UpdateUserService $service): JsonResponse
    {
        $request->validate();

        $service->update($request);

        return responder()
            ->success()
            ->respond(Response::HTTP_NO_CONTENT);
    }
}
