<?php

namespace App\Observers;

use Infrastructure\Cache\Invalidations\InvalidationService;

class Observer
{
    public const CREATED = 'Created';

    public const DELETED = 'Deleted';

    public const UPDATED = 'Updated';

    public function __construct(
        private InvalidationService $invalidationService
    ) {
    }

    public function created(object $model)
    {
        $this->callInvalidate($model, self::CREATED);
    }

    public function deleted(object $model)
    {
        $this->callInvalidate($model, self::DELETED);
    }

    public function updated(object $model)
    {
        $this->callInvalidate($model, self::UPDATED);
    }

    private function callInvalidate(object $model, string $context): void
    {
        $this->invalidationService->invalidate($model, $context);
    }
}
