<?php

namespace App\Providers;

use Binaccle\Repositories;
use Illuminate\Support\ServiceProvider;
use Infrastructure\Repositories as InfraRepositories;
use Laravel\Cashier\Cashier;

class AppServiceProvider extends ServiceProvider
{
    private array $bindingClasses = [
        Repositories\Roles\RoleRepositoryInterface::class => InfraRepositories\Roles\RoleRepository::class,
        Repositories\Users\UserRepositoryInterface::class => InfraRepositories\Users\UserRepository::class,
        Repositories\Files\FileRepositoryInterface::class => InfraRepositories\Files\FileRepository::class,
        Repositories\Companies\CompanyRepositoryInterface::class => InfraRepositories\Companies\CompanyRepository::class,
        Repositories\Permissions\PermissionRepositoryInterface::class => InfraRepositories\Permissions\PermissionRepository::class,
        Repositories\Plans\PlanRepositoryInterface::class => InfraRepositories\Plans\PlanRepository::class,
        Repositories\Sectors\SectorRepositoryInterface::class => InfraRepositories\Sectors\SectorRepository::class,
        Repositories\Users\Notifiables\UserNotifiableRepositoryInterface::class => InfraRepositories\Users\Notifiables\NotifiableRepository::class,
        Repositories\MassiveUploads\MassiveUploadTypeRepositoryInterface::class => InfraRepositories\MassiveUploads\MassiveUploadTypeRepository::class,
        Repositories\MassiveUploads\MassiveUploadRepositoryInterface::class => InfraRepositories\MassiveUploads\MassiveUploadRepository::class,
        Repositories\MassiveUploads\MassiveUploadStateRepositoryInterface::class => InfraRepositories\MassiveUploads\MassiveUploadStateRepository::class,
        Repositories\Equipments\Types\TypeRepositoryInterface::class => InfraRepositories\Equipments\Types\TypeRepository::class,
        Repositories\Equipments\EquipmentRepositoryInterface::class => InfraRepositories\Equipments\EquipmentRepository::class,
        Repositories\Equipments\Assignments\AssignmentRepositoryInterface::class => InfraRepositories\Equipments\Assignments\AssignmentRepository::class,
        Repositories\Equipments\Audits\AuditRepositoryInterface::class => InfraRepositories\Equipments\Audits\AuditRepository::class,
        Repositories\Equipments\Audits\AuditStateRepositoryInterface::class => InfraRepositories\Equipments\Audits\AuditStateRepository::class,
        Repositories\Equipments\Audits\Audited\AuditedRepositoryInterface::class => InfraRepositories\Equipments\Audits\Audited\AuditedRepository::class,
    ];

    public function boot(): void
    {
        Cashier::calculateTaxes();
    }

    public function register(): void
    {
        foreach ($this->bindingClasses as $interface => $class) {
            $this->app->bind($interface, $class);
        }
    }
}
