<?php

namespace App\Providers;

use App\Events;
use App\Listeners;
use App\Observers\Observer;
use Binaccle\Events as DomainEvents;
use Binaccle\Listeners as DomainListeners;
use Binaccle\Models;
use Binaccle\Models\Equipments;
use Binaccle\Observers\Observer as DomainObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        DomainEvents\Users\Notifiables\UserNotifiableEvent::class => [
            DomainListeners\Users\Notifiables\UserNotifiableListener::class,
        ],
        DomainEvents\MassiveUploads\MassiveUploadEvent::class => [
            DomainListeners\MassiveUploads\MassiveUploadListener::class,
        ],
        DomainEvents\Equipments\Audits\Audited\CreateAuditedEvent::class => [
            DomainListeners\Equipments\Audits\Audited\CreateAuditedListener::class,
        ],
        DomainEvents\Companies\DeleteCompanyEvent::class => [
            DomainListeners\Companies\DeleteCompanyListener::class,
        ],
        Events\Cache\InvalidateCacheEvent::class => [
            Listeners\Cache\InvalidateListener::class,
        ],
        DomainEvents\Shared\SuccessEvent::class => [
            Listeners\Throttle\ResetThrottleListener::class,
        ],
        \Illuminate\Auth\Events\Login::class => [
            Listeners\Throttle\ResetThrottleListener::class,
        ],
    ];

    public function boot(): void
    {
        Models\Roles\Role::observe([
            DomainObserver::class,
            Observer::class,
        ]);

        Models\Plans\Plan::observe([
            DomainObserver::class,
            Observer::class,
        ]);

        Models\Sectors\Sector::observe([
            DomainObserver::class,
            Observer::class,
        ]);

        Models\Users\User::observe([
            DomainObserver::class,
            Observer::class,
        ]);

        Models\MassiveUploads\MassiveUpload::observe([
            DomainObserver::class,
            Observer::class,
        ]);

        Equipments\Equipment::observe([
            DomainObserver::class,
            Observer::class,
        ]);

        Equipments\EquipmentType::observe([
            DomainObserver::class,
            Observer::class,
        ]);

        Equipments\EquipmentAssignment::observe([
            DomainObserver::class,
            Observer::class,
        ]);

        Equipments\EquipmentAudit::observe([
            DomainObserver::class,
            Observer::class,
        ]);

        Equipments\EquipmentAuditAudited::observe([
            DomainObserver::class,
            Observer::class,
        ]);
    }
}
