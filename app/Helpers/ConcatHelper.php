<?php

namespace App\Helpers;

class ConcatHelper
{
    public static function cacheTag(...$args): string
    {
        return self::implode(':', ...$args);
    }

    public static function middleware(string $middleware, ...$args): string
    {
        return $middleware . ':' . self::rules(...$args);
    }

    public static function roles(...$roles): string
    {
        return self::implode('|', ...$roles);
    }

    public static function rules(...$args): string
    {
        return self::implode(',', ...$args);
    }

    private static function implode(string $separator, ...$args): string
    {
        return implode($separator, $args);
    }
}
