<?php

namespace App\Exceptions\Strategies\Domain;

use App\Exceptions\Strategies\AbstractRendereable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Throwable;

final class DomainRendereable extends AbstractRendereable
{
    public function render(Throwable $exception): JsonResponse
    {
        $code = $exception->getCode() ?? Response::HTTP_INTERNAL_SERVER_ERROR;

        return $this->renderError($exception->getMessage(), $code);
    }
}
