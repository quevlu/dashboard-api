<?php

namespace App\Exceptions\Strategies\MethodNotAllowed;

use App\Exceptions\Strategies\AbstractRendereable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Throwable;

final class MethodNotAllowedRendereable extends AbstractRendereable
{
    public function render(Throwable $exception): JsonResponse
    {
        return $this->renderError(trans('errors.exceptions.methodNotAllowed'), Response::HTTP_METHOD_NOT_ALLOWED);
    }
}
