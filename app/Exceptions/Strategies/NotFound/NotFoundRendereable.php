<?php

namespace App\Exceptions\Strategies\NotFound;

use App\Exceptions\Strategies\AbstractRendereable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Throwable;

final class NotFoundRendereable extends AbstractRendereable
{
    public function render(Throwable $exception): JsonResponse
    {
        return $this->renderError(trans('errors.exceptions.notFound'), Response::HTTP_NOT_FOUND);
    }
}
