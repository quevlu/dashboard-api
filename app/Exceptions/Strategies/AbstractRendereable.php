<?php

namespace App\Exceptions\Strategies;

use Illuminate\Http\JsonResponse;
use Throwable;

abstract class AbstractRendereable
{
    abstract public function render(Throwable $exception): JsonResponse;

    protected function renderError(string $message, int $statusCode): JsonResponse
    {
        return responder()->error($message)->respond($statusCode);
    }
}
