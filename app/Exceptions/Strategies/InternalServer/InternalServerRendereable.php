<?php

namespace App\Exceptions\Strategies\InternalServer;

use App\Exceptions\Strategies\AbstractRendereable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Throwable;

final class InternalServerRendereable extends AbstractRendereable
{
    public function render(Throwable $exception): JsonResponse
    {
        $msg = config('app.debug') ? $exception : trans('errors.exceptions.internalServer');

        return $this->renderError($msg, Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
