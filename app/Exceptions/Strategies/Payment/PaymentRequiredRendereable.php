<?php

namespace App\Exceptions\Strategies\Payment;

use App\Exceptions\Strategies\AbstractRendereable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Throwable;

final class PaymentRequiredRendereable extends AbstractRendereable
{
    public function render(Throwable $exception): JsonResponse
    {
        return $this->renderError(trans('errors.exceptions.paymentRequired'), Response::HTTP_PAYMENT_REQUIRED);
    }
}
