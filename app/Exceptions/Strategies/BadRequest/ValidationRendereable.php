<?php

namespace App\Exceptions\Strategies\BadRequest;

use App\Exceptions\Strategies\AbstractRendereable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Throwable;

class ValidationRendereable extends AbstractRendereable
{
    public function render(Throwable $exception): JsonResponse
    {
        $errors = $exception->validator->errors();
        $rules = $exception->validator->failed();

        $errorCode = Response::HTTP_BAD_REQUEST;
        $response = [
            'status' => $errorCode,
            'success' => false,
            'errors' => [],
        ];

        foreach ($rules as $fieldName => $rules) {
            $response['errors'][$fieldName] = $errors->first($fieldName);
        }

        return response()->json($response, $errorCode);
    }
}
