<?php

namespace App\Exceptions\Strategies\BadRequest;

use App\Exceptions\Strategies\AbstractRendereable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Throwable;

final class BadRequestRendereable extends AbstractRendereable
{
    public function render(Throwable $exception): JsonResponse
    {
        return $this->renderError($exception->getMessage(), Response::HTTP_BAD_REQUEST);
    }
}
