<?php

namespace App\Exceptions\Strategies\Forbidden;

use App\Exceptions\Strategies\AbstractRendereable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Throwable;

final class AuthorizationRendereable extends AbstractRendereable
{
    public function render(Throwable $exception): JsonResponse
    {
        return $this->renderError(trans('errors.exceptions.unauthorized'), Response::HTTP_FORBIDDEN);
    }
}
