<?php

namespace App\Exceptions\Strategies\TooMany;

use App\Exceptions\Strategies\AbstractRendereable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Throwable;

class ThrottleRendereable extends AbstractRendereable
{
    public function render(Throwable $exception): JsonResponse
    {
        return $this->renderError(trans('errors.exceptions.tooMany'), Response::HTTP_TOO_MANY_REQUESTS);
    }
}
