<?php

namespace App\Exceptions;

use Binaccle\Exceptions\AbstractDomainException;
use Binaccle\Exceptions\Shared\PaymentRequiredException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    private const DEFAULT = 'default';

    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    protected $dontReport = [
        BadRequestException::class,
        ValidationException::class,
        AbstractDomainException::class,
        AuthorizationException::class,
        MethodNotAllowedHttpException::class,
        ModelNotFoundException::class,
        NotFoundHttpException::class,
        ThrottleRequestsException::class,
        UnprocessableEntityHttpException::class,
        UnauthorizedException::class,
        PaymentRequiredException::class,
    ];

    private array $rendereables = [
        BadRequestException::class => Strategies\BadRequest\BadRequestRendereable::class,
        ValidationException::class => Strategies\BadRequest\ValidationRendereable::class,

        AbstractDomainException::class => Strategies\Domain\DomainRendereable::class,

        AuthorizationException::class => Strategies\Forbidden\AuthorizationRendereable::class,
        AccessDeniedHttpException::class => Strategies\Forbidden\AuthorizationRendereable::class,

        MethodNotAllowedHttpException::class => Strategies\MethodNotAllowed\MethodNotAllowedRendereable::class,

        ModelNotFoundException::class => Strategies\NotFound\ModelNotFoundRendereable::class,
        NotFoundHttpException::class => Strategies\NotFound\NotFoundRendereable::class,

        ThrottleRequestsException::class => Strategies\TooMany\ThrottleRendereable::class,

        UnauthorizedException::class => Strategies\Unauthorized\UnauthorizedRendereable::class,

        PaymentRequiredException::class => Strategies\Payment\PaymentRequiredRendereable::class,

        self::DEFAULT => Strategies\InternalServer\InternalServerRendereable::class,
    ];

    public function register(): void
    {
        $this->reportable(function (Throwable $exception) {
            if (app()->bound('sentry')) {
                app('sentry')->captureException($exception);
            }
        });

        $this->renderable(fn (Throwable $exception) => $this->customRender($exception));
    }

    private function customRender(Throwable $exception)
    {
        $exceptionClass = $exception::class;

        if (@$this->rendereables[$exceptionClass]) {
            $rendereableClass = $this->rendereables[$exceptionClass];
        } else {
            $parentExceptionClass = get_parent_class($exception);

            $rendereableClass = (@$this->rendereables[$parentExceptionClass] ?? $this->rendereables[self::DEFAULT]);
        }

        return (new $rendereableClass)->render($exception);
    }
}
