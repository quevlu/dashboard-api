<?php

namespace App\Mail\Users\Notifiables;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;

class UserNotifiableMail extends Mailable implements ShouldQueue
{
    use Queueable;

    public function __construct(
        private string $type,
        private string $token
    ) {
    }

    public function build()
    {
        $from = config('mail.from');
        $subject = trans("mails.{$this->type}.subject");

        return $this
            ->from($from['address'], $from['name'])
            ->subject($subject)
            ->view('emails.users.notifiables.notifiable', [
                'token' => $this->token,
            ]);
    }
}
