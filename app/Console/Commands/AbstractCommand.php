<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

abstract class AbstractCommand extends Command
{
    public function signature(): string
    {
        return $this->signature;
    }
}
