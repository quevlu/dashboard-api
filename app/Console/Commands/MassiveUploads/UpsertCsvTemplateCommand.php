<?php

namespace App\Console\Commands\MassiveUploads;

use App\Console\Commands\AbstractCommand;
use Binaccle\Enums\MassiveUploads\MassiveUploadHeaderEnum;
use Binaccle\Enums\MassiveUploads\MassiveUploadTypeEnum;
use Binaccle\Services\Utils\Storage\PathResolver;
use Illuminate\Support\Facades\App;
use Infrastructure\Storage\S3Storage;

class UpsertCsvTemplateCommand extends AbstractCommand
{
    protected $description = 'Create and upload csv templates for each language available';

    protected $signature = 'massive-uploads:upsert';

    public function handle(PathResolver $pathResolverService, S3Storage $s3Storage)
    {
        $languages = config('app.locales');
        $massiveUploadTypes = MassiveUploadTypeEnum::types();

        foreach ($languages as $language) {
            App::setLocale($language);

            $fileNames = trans('csvs.file_names.massive_uploads');

            foreach ($massiveUploadTypes as $massiveUploadType) {
                $path = $pathResolverService->folder($pathResolverService::MASSIVE_UPLOADS_PATH, [
                    'lang' => $language,
                    'fileName' => $fileNames[$massiveUploadType],
                ]);

                $csvFile = ($this->createCsv($massiveUploadType));
                $s3Storage->putPrivate($path, $csvFile);
            }
        }
    }

    private function createCsv(string $type)
    {
        $fd = fopen('php://temp', 'w');
        if ($fd === false) {
            exit('Failed to open temporary file');
        }

        $headers = customTrans('validation.attributes', MassiveUploadHeaderEnum::HEADERS[$type]);

        fputcsv($fd, $headers);

        rewind($fd);
        $csv = stream_get_contents($fd);
        fclose($fd);

        return $csv;
    }
}
