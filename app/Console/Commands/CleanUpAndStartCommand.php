<?php

namespace App\Console\Commands;

use App\Console\Commands\MassiveUploads\UpsertCsvTemplateCommand;
use Binaccle\Models\Equipments\EquipmentAuditState;
use Binaccle\Models\MassiveUploads\MassiveUploadState;
use Binaccle\Models\MassiveUploads\MassiveUploadType;
use Binaccle\Models\Plans\Plan;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;

class CleanUpAndStartCommand extends Command
{
    protected $description = 'Clear all databases';

    protected $signature = 'cleanup-and-start';

    public function __construct(
        private UpsertCsvTemplateCommand $upsertCsvTemplateCommand
    ) {
        parent::__construct();
    }

    public function handle()
    {
        $stores = [
            config('cache.default'),
            config('queue.app.connection'),
            config('queue.mail.connection'),
            config('jwt.connection'),
            config('cache.limiter'),
            'default',
        ];

        $models = [
            Plan::class,
            EquipmentAuditState::class,
            MassiveUploadState::class,
            MassiveUploadType::class,
        ];

        array_walk($models, fn ($model) => $model::flushQueryCache());
        array_walk($stores, fn ($store) =>  Cache::store($store)->flush());

        exec('> ' . storage_path('logs/laravel.log'));

        Artisan::call('migrate:fresh --seed');

        Artisan::call($this->upsertCsvTemplateCommand->signature());
    }
}
