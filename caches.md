Plans
    Add
    Update
    Delete
        x plans


Roles
    Add
    Update
    Delete
        x roles


Sectors
    Add 
        x sectors:list
    Update
        x users:list
        x sectors:list
        x equipments:list
        x equipments:assignments
        x equipments:assignments_history
        x equipments:audits
        x equipments:audits_audited
    Delete
        x users:list
        x sectors:list
        x sectors:users:{sector}


Users
    Add
        x users:list
        x sectors:users:{sector}
    Update
        x users:list
        
    SOLO SI SE ACTUALIZA EL NOMBRE
        x sectors:users
        x equipments:list
        x equipments:assignments
        x equipments:assignments_history
        x equipments:audits_audited
    Delete
        x users:list
        x sectors:users:{sector}
    Massive uploads
        x users:list
        x massive_uploads:users
        x sectors:users*


Equipments
    Add
        x equipments:list
    Update
        x equipments*
    Delete
        x equipments:list
        x equipments:assignments
        x equipments:audits_audited
    Massive uploads
        x equipments:list
        x massive_uploads:equipments

Equipments types
    Add
        x equipments:list
        x equipments:types
    Update
        x equipments*
    Delete
        x equipments:types
    Massive uploads
        x equipments:list
        x equipments:types
        x massive_uploads:equipment_types

Equipments assignments
    Add
    Delete
    Update
        x equipments:list
        x equipments:assignments
        x equipments:assignments_history
        x equipments:audits_audited

Equipments audits
    Add
        x equipments:audits
    Update
        x equipments:audits
        x equipments:audits_audited:{equipmentAuditId}
    Delete
        x equipments:audits
        x equipments:audits_audited:{equipmentAuditId}

Equipments Audit Audited
    Add
    Update
    Delete
        x equipments:audits_audited:{equipmentAuditId}