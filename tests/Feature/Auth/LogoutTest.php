<?php

namespace Tests\Feature\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class LogoutTest extends TestCase
{
    use RefreshDatabase;

    public function __construct()
    {
        parent::__construct();
    }

    public function testValidLogout(): void
    {
        $this->makeTheCall($this->loginUser()['jwt'])->assertStatus(Response::HTTP_NO_CONTENT);
    }

    private function makeTheCall(?string $token = null): TestResponse
    {
        return $this->delete('api/auth', [], [
            'Authorization' => "Bearer {$token}",
        ]);
    }
}
