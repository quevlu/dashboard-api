<?php

namespace Tests\Feature\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class UserLoggedTest extends TestCase
{
    use RefreshDatabase;

    public function __construct()
    {
        parent::__construct();
    }

    public function testBlankJwt(): void
    {
        $this->makeTheCall()->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testInvalidJwt(): void
    {
        $this->makeTheCall('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c')
        ->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testValidUserLogged(): void
    {
        $this->makeTheCall($this->loginUser()['jwt'])->assertStatus(Response::HTTP_OK);
    }

    private function makeTheCall(?string $token = null): TestResponse
    {
        return $this->get('api/auth', [
            'Authorization' => "Bearer {$token}",
        ]);
    }
}
