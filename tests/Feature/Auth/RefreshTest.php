<?php

namespace Tests\Feature\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class RefreshTest extends TestCase
{
    use RefreshDatabase;

    public function __construct()
    {
        parent::__construct();
    }

    public function testRefreshToken(): void
    {
        $this->makeTheCall($this->loginUser()['jwt'])
        ->assertStatus(Response::HTTP_CREATED)
        ->assertJsonStructure([
            'data' => [
                'token',
            ],
        ]);
    }

    private function makeTheCall(?string $token = null): TestResponse
    {
        return $this->put('api/auth', [], [
            'Authorization' => "Bearer {$token}",
        ]);
    }
}
