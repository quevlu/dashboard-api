<?php

namespace Tests\Feature\Auth;

use App\Enums\AppEnum;
use App\Http\Requests\Auth\LoginRequest;
use Binaccle\Models\Companies\Company;
use Binaccle\Models\Users\User;
use Binaccle\Models\Users\UserNotifiable;
use Illuminate\Auth\Events\Login;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Event;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    private const DUMMY_PASSWORD = 'exampletest';

    public function testLoginWithInvalidData()
    {
        $this->makeTheCall('test@test.com', self::DUMMY_PASSWORD)
        ->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    public function testValidLogin()
    {
        $user = $this->createUser();

        Event::fake();

        $this->makeTheCall($user->email(), AppEnum::PASSWORD_TEST)
        ->assertStatus(Response::HTTP_OK);

        Event::assertDispatched(Login::class);
    }

    public function testWithCorrectEmailAndInvalidPassword()
    {
        $user = $this->createUser();

        Event::fake();

        $this->makeTheCall($user->email(), self::DUMMY_PASSWORD)
        ->assertStatus(Response::HTTP_BAD_REQUEST);

        Event::assertNotDispatched(Login::class);
    }

    public function testWithCorrectEmailAndWithoutPassword()
    {
        $user = $this->createUserWithBlankPassword();

        $this->makeTheCall($user->email(), self::DUMMY_PASSWORD)
        ->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    public function testWithValidDataButTheyHaveAnVerificationPending()
    {
        $user = $this->createUserWithVerification();

        $this->makeTheCall($user->email(), AppEnum::PASSWORD_TEST)
        ->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    private function createUser(): User
    {
        return Company::factory()
        ->has(User::factory())
        ->create()->users->first();
    }

    private function createUserWithBlankPassword(): User
    {
        return Company::factory()
        ->has(User::factory([
            User::PASSWORD => null,
        ]))
        ->create()->users->first();
    }

    private function createUserWithVerification(): User
    {
        return Company::factory()
        ->has(
            User::factory()
            ->has(UserNotifiable::factory([
                UserNotifiable::TYPE => UserNotifiable::VERIFICATION_TYPE,
            ]), User::VERIFICATION_RELATIONSHIP)
        )
        ->create()->users->first();
    }

    private function makeTheCall(string $email, string $password): TestResponse
    {
        return $this->post('api/auth', [
            LoginRequest::EMAIL => $email,
            LoginRequest::PASSWORD => $password,
        ]);
    }
}
