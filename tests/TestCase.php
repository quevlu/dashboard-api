<?php

namespace Tests;

use App\Enums\AppEnum;
use App\Http\Requests\Auth\LoginRequest;
use Binaccle\Enums\Roles\RoleEnum;
use Binaccle\Models\Companies\Company;
use Binaccle\Models\Users\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\TestResponse;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use WithFaker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->setUpFaker();
    }

    protected function getContentOfResponse(TestResponse $response): array
    {
        return json_decode($response->getContent(), true)['data'];
    }

    protected function loginUser(): array
    {
        $user = Company::factory()
            ->has(User::factory())
            ->create()->users->first();

        $user->assignRole(RoleEnum::ADMIN);

        $response = $this->post('api/auth', [
            LoginRequest::EMAIL => $user->email(),
            LoginRequest::PASSWORD => AppEnum::PASSWORD_TEST,
        ]);

        $jwt = $this->getContentOfResponse($response)['token'];

        return [
            'user' => $user,
            'jwt' => $jwt,
        ];
    }
}
