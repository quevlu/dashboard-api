<?php

use App\Http\Exportables\Equipments\Assignments\HistoryAssignmentExportable;
use App\Http\Exportables\Equipments\Assignments\ListAssignmentExportable;
use App\Http\Exportables\Equipments\Audits\Audited\ListAuditedExportable;
use App\Http\Exportables\Equipments\Audits\ListAuditExportable;
use App\Http\Exportables\Equipments\ListEquipmentExportable;
use App\Http\Exportables\Equipments\Types\ListTypeExportable;
use App\Http\Exportables\MassiveUploads\ListMassiveUploadExportable;
use App\Http\Exportables\Sectors\ListSectorExportable;
use App\Http\Exportables\Sectors\ListUserSectorExportable;
use App\Http\Exportables\Users\ListUserExportable;
use Binaccle\Enums\MassiveUploads\MassiveUploadTypeEnum;

return [
    'line' => 'line',

    'file_names' => [
        'massive_uploads' => [
            MassiveUploadTypeEnum::USERS => 'massive-upload-users.csv',
            MassiveUploadTypeEnum::EQUIPMENTS => 'massive-upload-equipment.csv',
            MassiveUploadTypeEnum::EQUIPMENT_TYPES => 'massive-upload-equipment-types.csv',
        ],
        'exportables' => [
            ListUserExportable::class => 'export-list-users-:timestamp.csv',
            ListSectorExportable::class => 'export-list-sectors-:timestamp.csv',
            ListUserSectorExportable::class => 'export-list-users-by-sector-:timestamp.csv',
            ListEquipmentExportable::class => 'export-list-equipments-:timestamp.csv',
            ListTypeExportable::class => 'export-list-equipment-types-:timestamp.csv',
            ListAssignmentExportable::class => 'export-list-equipment-assignments-:timestamp.csv',
            HistoryAssignmentExportable::class => 'export-list-history-equipment-assignments-:timestamp.csv',
            ListMassiveUploadExportable::class => 'export-list-massive-upload-:timestamp.csv',
            ListAuditExportable::class => 'export-list-equipment-audit-:timestamp.csv',
            ListAuditedExportable::class => 'export-list-equipment-audit-audited-:timestamp.csv',
        ],
    ],
];
