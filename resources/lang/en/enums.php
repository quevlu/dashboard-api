<?php

use Binaccle\Enums\Users\UserVerifiedEnum;

return [
    UserVerifiedEnum::class => [
        UserVerifiedEnum::DONT_APPLY => 'don\'t apply',
        UserVerifiedEnum::PENDING => 'pending',
        UserVerifiedEnum::VERIFIED => 'verified',
    ],
];
