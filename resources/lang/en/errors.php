<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Error Message Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the Laravel Responder package.
    | When it generates error responses, it will search the messages array
    | below for any key matching the given error code for the response.
    |
    */

    'exceptions' => [
        'internalServer' => 'Internal server error',
        'forbidden' => 'Forbidden',
        'methodNotAllowed' => 'Method not allowed',
        'notFound' => 'The resource does not exist',
        'paymentRequired' => 'You should upgrade your plan to access to this functionality',
        'tooMany' => 'Take a rest and try again in a couple of minutes',
        'unauthorized' => 'Unauthorized',
    ],

    'equipments' => [
        'audits' => [
            'is_not_in_created_creating_or_updating' => 'The audit is not in created, creating or updating states',
        ],
    ],

    'csvs' => [
        'process_error' => 'We have a problem processing the record',
    ],
];
