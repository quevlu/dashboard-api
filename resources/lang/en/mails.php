<?php

use Binaccle\Models\Users\UserNotifiable;

return [
    UserNotifiable::VERIFICATION_TYPE => [
        'subject' => 'Account Verification',
    ],

    UserNotifiable::RESET_TYPE => [
        'subject' => 'Reset Password',
    ],

    UserNotifiable::INVITATION_TYPE => [
        'subject' => 'User invitation',
    ],
];
