<?php

use Binaccle\Enums\Equipments\PrintEquipmentEnum;

return [
    'title' => 'List of ',
    'types' => [
        PrintEquipmentEnum::BARCODE => 'barcodes',
        PrintEquipmentEnum::QR => 'Qrs',
    ],
];
