<?php

use Binaccle\Enums\MassiveUploads\MassiveUploadTypeEnum;

return [
    'line' => 'linea',
    'process_error' => 'Tuvimos un problema procesando el registro',

    'file_names' => [
        'massive_uploads' => [
            MassiveUploadTypeEnum::USERS => 'carga-masiva-usuarios.csv',
            MassiveUploadTypeEnum::EQUIPMENTS => 'carga-masiva-equipos.csv',
            MassiveUploadTypeEnum::EQUIPMENT_TYPES => 'carga-masiva-tipos-de-equipos.csv',
        ],
    ],
];
